-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local Blizzard = addon.API.Blizzard;
local gsub, setmetatable, strtrim = gsub, setmetatable, strtrim;
local Cache = setmetatable({}, { __mode = "kv" });

addonTable.classPassive = function(ID, Type)
	local spellID = ID;
	local classType = Type or "Passive";

	local self = Cache[ID] or {};

	self.ClassID = function()
		return spellID;
	end

	self.ClassType = function()
		return classType;
	end

	self.Enabled = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "IsAvailable") == nil then
			if self.SpellType() == "pet" then
				spellInfo.IsAvailable = self.IsKnown();
			else
				spellInfo.IsAvailable = Blizzard.IsPlayerSpell(currentID) or self.IsKnown();
			end

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "IsAvailable", false);
	end

	-- Check to see if the spell is known or not, will return false if the spell is replaced by another spell.
	self.IsKnown = function()
		local addonCache = addonTable.Cache;

		local currentID = self.SpellID();
		local spellReplaced = addonCache.Persistant.ReplacedSpells[currentID];

		if spellReplaced ~= nil then
			return spellReplaced;
		end

		return addonCache.Persistant.SpellLearned[self.SpellType()][currentID] or false;
	end

	self.Name = function()
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);
		local returnValue = Tables.GetValue(spellInfo, "Name", "");

		returnValue = gsub(returnValue, "[0-9.]", "");
		returnValue = strtrim(returnValue);

		return returnValue;
	end

	self.SpellID = function()
		local Core = addon.Core.General;

		return Core.GetSpellReplacementID(spellID);
	end

	-- Get the spell Type.
	self.SpellType = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "SpellType") == nil then
			if Blizzard.GetSpellBookItemInfo(self.Name()) == "PETACTION" then
				spellInfo.SpellType = "Pet";
			else
				spellInfo.SpellType = "Player";
			end

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "SpellType", "Player");
	end

	Cache[ID] = self;

	return self;
end