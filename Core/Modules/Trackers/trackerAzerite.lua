-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local Blizzard = addon.API.Blizzard;
local GetAllTierInfo, GetPowerInfo, HasActiveAzeriteItem, IsAzeriteEmpoweredItem, IsPowerSelected = C_AzeriteEmpoweredItem.GetAllTierInfo, C_AzeriteEmpoweredItem.GetPowerInfo, C_AzeriteItem.HasActiveAzeriteItem, C_AzeriteEmpoweredItem.IsAzeriteEmpoweredItem, C_AzeriteEmpoweredItem.IsPowerSelected;
local ipairs = ipairs;

-- GUIDs used as keys for this table are units that have either attacked or been attacked by the pet.
local AzeritePowers = {};

local function updatePowers()
	local coreTables = addon.Core.Tables;

	coreTables.Wipe(AzeritePowers);

	if HasActiveAzeriteItem() then
		local MAX_INV_SLOTS = 19;

		for inventorySlot = 1, MAX_INV_SLOTS do
			if Blizzard.GetInventoryItemID("player", inventorySlot) then
				local itemLocation = ItemLocation:CreateFromEquipmentSlot(inventorySlot);

				if IsAzeriteEmpoweredItem(itemLocation) then
					local azeriteTiers = GetAllTierInfo(itemLocation);

					for _, tierInfo in ipairs(azeriteTiers) do
						for _, azeritePower in ipairs(tierInfo.azeritePowerIDs) do
							if IsPowerSelected(itemLocation, azeritePower) then
								local powerInfo = GetPowerInfo(azeritePower);
								local spellID = coreTables.GetValue(powerInfo, "spellID");

								if spellID then
									local spellName = Blizzard.GetSpellInfo(spellID);

									AzeritePowers[spellName] = (AzeritePowers[spellName] or 0) + 1;
								end
							end
						end
					end
				end
			end
		end
	end
end

addonTable.AzeritePowers = {
	Enabled = function(spellID)
		if spellID then
			local spellName = Blizzard.GetSpellInfo(spellID);

			return AzeritePowers[spellName] ~= nil;
		end

		return false;
	end,

	Rank = function(spellID)
		if spellID then
			local spellName = Blizzard.GetSpellInfo(spellID);

			return AzeritePowers[spellName] or 0;
		end

		return 0;
	end,
};

local moduleEvents = {
	playerLogin = function()
		updatePowers();
	end,

	changeSpecFinish = function()
		updatePowers();
	end,
};

local tableEvents = {
	AZERITE_ITEM_POWER_LEVEL_CHANGED = updatePowers,
	AZERITE_EMPOWERED_ITEM_SELECTION_UPDATED = updatePowers,
	PLAYER_EQUIPMENT_CHANGED = updatePowers,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);