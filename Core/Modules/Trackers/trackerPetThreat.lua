-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- GUIDs used as keys for this table are units that have either attacked or been attacked by the pet.
local threatTable = {};

-- List of CLEU event suffixes that can correspond to the pet/guardian damaging or trying to
-- damage (tag) an enemy, or vice versa via auto attacks.
local CLEU_AUTO_ATTACK = {
	RANGED_DAMAGE = true,
	RANGED_MISSED = true,
	SWING_DAMAGE = true,
	SWING_MISSED = true
};

-- List of CLEU event prefixes that can correspond to the pet/guardian damaging or trying to
-- damage (tag) an enemy, or vice versa via abilities.
local CLEU_TAG_PREFIXES = {
	"SWING_",
	"RANGE_",
	"SPELL_",
	"SPELL_PERIODIC_",
};

-- List of CLEU event suffixes that can correspond to the pet/guardian damaging or trying to
-- damage (tag) an enemy, or vice versa via abilities.
local CLEU_TAG_SUFFIXES = {
	"_CAST_START",
	"_DAMAGE",
	"_MISSED",
	"_DRAIN",
	"_LEECH",
	"_INTERRUPT",
	"_DISPEL",
	"_DISPEL_FAILED",
	"_STOLEN",
	"_AURA_APPLIED",
	"_AURA_APPLIED_DOSE",
	"_AURA_REFRESH",
};

-- Table of CLEU events for when a unit is removed from combat.
local CLEU_UNIT_REMOVED = {
	UNIT_DESTROYED = true,
	UNIT_DIED = true,
	UNIT_DISSIPATES = true,
};

addonTable.PetThreat = {
	IsAttacking = function(unitID)
		local returnValue;

		if unitID then
			local unitGUID = addon.API.Blizzard.UnitGUID(unitID);

			returnValue = threatTable[unitGUID];
		end

		return returnValue or false;
	end,
};

local function isPet(unitFlags)
	local COMBATLOG_OBJECT_AFFILIATION_MINE, bit = COMBATLOG_OBJECT_AFFILIATION_MINE, bit;

	return bit.band(unitFlags, COMBATLOG_OBJECT_AFFILIATION_MINE) ~= 0;
end

local function isEnemy(unitFlags)
	local COMBATLOG_OBJECT_REACTION_FRIENDLY, bit = COMBATLOG_OBJECT_REACTION_FRIENDLY, bit;

	return bit.band(unitFlags, COMBATLOG_OBJECT_REACTION_FRIENDLY) == 0;
end

local function isTagEvent(combatEvent)
	local format, ipairs, strfind = format, ipairs, strfind;
	local returnValue = false;

	for _, eventPrefix in ipairs(CLEU_TAG_PREFIXES) do
		if strfind(combatEvent, format("^%s", eventPrefix)) then
			returnValue = true;
			break;
		end
	end

	if returnValue then
		returnValue = false;

		for _, eventSuffix in ipairs(CLEU_TAG_SUFFIXES) do
			if strfind(combatEvent, format("%s$", eventSuffix)) then
				returnValue = true;
				break;
			end
		end
	end

	return returnValue;
end

local function getGUID(sourceGUID, sourceFlags, destGUID, destFlags)
	return (isPet(sourceFlags) and isEnemy(destFlags) and destGUID)
		or (isPet(destFlags) and isEnemy(sourceFlags) and sourceGUID);
end

local function validEvent(combatEvent)
	return CLEU_AUTO_ATTACK[combatEvent]
		or isTagEvent(combatEvent);
end

local function combatLog(...)
	local _, _, combatEvent, _, sourceGUID, _, sourceFlags, _, destGUID, _, destFlags = ...;
	local unitGUID = getGUID(sourceGUID, sourceFlags, destGUID, destFlags);

	if CLEU_UNIT_REMOVED[combatEvent] and threatTable[destGUID] then
		-- Unit died, so lets clear it from the threat table.
		threatTable[destGUID] = nil;
	elseif unitGUID and validEvent(combatEvent) then
		-- Unit is in combat with a pet/guardian so lets add it to the threat table.
		threatTable[unitGUID] = true;
	end
end

local moduleEvents = {
	Disabled = function()
		local wipe = wipe;

		-- No longer using rotations so clear the table.
		wipe(threatTable);
	end,

	leaveCombat = function()
		local wipe = wipe;

		-- Player is no longer in combat so the pets/guardians aren't either.
		-- So clear the table.
		wipe(threatTable);
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = combatLog,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);