-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard = addon.API.Blizzard;
local strupper = strupper;

addonTable.classPet = function(ID)
	local unitID = ID;
	local self = addonTable.classUnit(ID);

	self.Family = function(checkFamily)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "Family") == nil then
			unitInfo.Family = Blizzard.UnitCreatureFamily("pet");

			Cache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		local petFamily = Tables.GetValue(unitInfo, "Family", "");

		return (not checkFamily and petFamily)
			or strupper(petFamily) == strupper(checkFamily);
	end

	self.IsActive = function()
		local addonCache = addonTable.Cache;
		local Player = addon.Units.Player;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsActive") == nil then
			unitInfo.IsActive = Blizzard.IsPetActive();

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsActive", false)
			or Player.IsMounted();
	end

	-- "pet.prev_gcd.x.foo"
	self.PrevGCD = function(index, Spell)
		local Tables = addon.Core.Tables;
		local previousSpell = addonTable.PreviousSpell.PetGCD or {};

		return Tables.GetValue(previousSpell, index, 0) == Spell.SpellID()
			or false;
	end

	-- "pet.prev_off_gcd.x.foo"
	self.PrevOffGCD = function(index, Spell)
		local Tables = addon.Core.Tables;
		local previousSpell = addonTable.PreviousSpell.PetOffGCD or {};

		return Tables.GetValue(previousSpell, index, 0) == Spell.SpellID()
			or false;
	end

	self.PrevSpell = function(index, Spell)
		local Tables = addon.Core.Tables;
		local previousSpell = addonTable.PreviousSpell.PetAll or {};

		return Tables.GetValue(previousSpell, index, 0) == Spell.SpellID()
			or false;
	end

	return self;
end