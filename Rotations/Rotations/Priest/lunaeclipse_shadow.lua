local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local coreGeneral = addon.Core.General;
local Objects = addon.Core.Objects;
local val = coreGeneral.ToNumber;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

-- Rotation Variables
local nameAPL = "lunaeclipse_priest_shadow";

-- AOE Rotation
local function AOE(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.aoe+=/dark_ascension,if=buff.voidform.down
		DarkAscension = function()
			return Player.Buff(Aura.Voidform).Down();
		end,

		-- actions.aoe+=/mind_sear,chain=1,interrupt_immediate=1,interrupt_if=ticks>=2&(cooldown.void_bolt.up|cooldown.mind_blast.up)
		MindSear = function()
			return Player.Casting.Tick() >= 2
			   and (Spell.VoidBolt.Cooldown.Up() or Spell.MindBlast.Cooldown.Up());
		end,

		-- actions.aoe+=/surrender_to_madness,if=buff.voidform.stack>=(15+buff.bloodlust.up)
		SurrenderToMadness = function()
			return Player.Buff(Aura.Voidform).Stack() >= 15 + val(Player.HasBloodlust());
		end,

		-- actions.aoe+=/void_bolt,if=talent.dark_void.enabled&dot.shadow_word_pain.remains>travel_time
		VoidBolt = function()
			return Talent.DarkVoid.Enabled()
			   and Target.Debuff(Aura.ShadowWordPain).Remains() > Spell.VoidBolt.TravelTime();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.aoe=void_eruption
		action.EvaluateAction(Spell.VoidEruption, true);
		action.EvaluateAction(Talent.DarkAscension, self.Requirements.DarkAscension);
		action.EvaluateAction(Spell.VoidBolt, self.Requirements.VoidBolt);
		action.EvaluateAction(Talent.SurrenderToMadness, self.Requirements.SurrenderToMadness);

		-- We can't do raid add events, so lets just suggest this on cooldown.
		-- actions.aoe+=/dark_void,if=raid_event.adds.in>10
		action.EvaluateAction(Talent.DarkVoid, true);

		-- actions.aoe+=/mindbender
		action.EvaluateAction(Talent.Mindbender, true);

		-- We can't do raid add events, so lets just suggest this on cooldown.
		-- actions.aoe+=/shadow_crash,if=raid_event.adds.in>5&raid_event.adds.duration<20
		action.EvaluateAction(Talent.ShadowCrash, true);

		action.EvaluateInterruptCondition(Spell.MindSear, true, self.Requirements.MindSear);
		-- actions.aoe+=/shadow_word_pain
		action.EvaluateAction(Spell.ShadowWordPain, true);
	end

	-- actions+=/run_action_list,name=aoe,if=spell_targets.mind_sear>(5+1*talent.misery.enabled)
	function self.Use(numEnemies)
		return numEnemies > 5 + 1 * val(Talent.Misery.Enabled());
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationAOE = AOE("AOE");

-- Cleave Rotation
local function Cleave(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cleave+=/dark_ascension,if=buff.voidform.down
		DarkAscension = function()
			return Player.Buff(Aura.Voidform).Down();
		end,

		-- actions.cleave+=/mind_flay,chain=1,interrupt_immediate=1,interrupt_if=ticks>=2&(cooldown.void_bolt.up|cooldown.mind_blast.up)
		MindFlay = function()
			return Player.Casting.Tick() >= 2
			   and (Spell.VoidBolt.Cooldown.Up() or Spell.MindBlast.Cooldown.Up());
		end,

		-- actions.cleave+=/mind_sear,target_if=spell_targets.mind_sear>2,chain=1,interrupt=1
		MindSear = function(numEnemies, Target)
			return numEnemies > 2;
		end,

		-- actions.cleave+=/shadow_word_death,target_if=target.time_to_die<3|buff.voidform.down
		ShadowWordDeath = function(numEnemies, Target)
			return Target.TimeToDie() < 3
			    or Player.Buff(Aura.Voidform).Down();
		end,

		-- actions.cleave+=/shadow_word_pain,target_if=refreshable&target.time_to_die>4,if=!talent.misery.enabled&!talent.dark_void.enabled
		ShadowWordPain = function(numEnemies, Target)
			return Target.Debuff(Aura.ShadowWordPain).Refreshable()
			   and Target.TimeToDie() > 4
			   and not Talent.Misery.Enabled()
			   and not Talent.DarkVoid.Enabled();
		end,

		-- actions.cleave+=/surrender_to_madness,if=buff.voidform.stack>=(15+buff.bloodlust.up)
		SurrenderToMadness = function()
			return Player.Buff(Aura.Voidform).Stack() >= 15 + val(Player.HasBloodlust());
		end,

		VampiricTouch = {
			-- actions.cleave+=/vampiric_touch,target_if=dot.shadow_word_pain.refreshable,if=(talent.misery.enabled&target.time_to_die>4)
			Misery = function(numEnemies, Target)
				return Target.Debuff(Aura.ShadowWordPain).Refreshable()
				   and Talent.Misery.Enabled()
				   and Target.TimeToDie() > 4;
			end,

			-- actions.cleave+=/vampiric_touch,target_if=refreshable,if=(target.time_to_die>6)
			Use = function(numEnemies, Target)
				return Target.Debuff(Aura.VampiricTouch).Refreshable()
				   and Target.TimeToDie() > 6;
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.cleave=void_eruption
		action.EvaluateAction(Spell.VoidEruption, true);
		action.EvaluateAction(Talent.DarkAscension, self.Requirements.DarkAscension);
		-- actions.cleave+=/void_bolt
		action.EvaluateAction(Spell.VoidBolt, true);
		action.EvaluateCycleAction(Talent.ShadowWordDeath, self.Requirements.ShadowWordDeath);
		action.EvaluateAction(Talent.SurrenderToMadness, self.Requirements.SurrenderToMadness);

		-- We can't do raid add events, so lets just suggest this on cooldown.
		-- actions.cleave+=/dark_void,if=raid_event.adds.in>10
		action.EvaluateAction(Talent.DarkVoid, true);

		-- actions.cleave+=/mindbender
		action.EvaluateAction(Talent.Mindbender, true);
		-- actions.cleave+=/mind_blast
		action.EvaluateAction(Spell.MindBlast, true);

		-- We can't do raid add events, so lets just suggest this on cooldown.
		-- actions.cleave+=/shadow_crash,if=(raid_event.adds.in>5&raid_event.adds.duration<2)|raid_event.adds.duration>2
		action.EvaluateAction(Talent.ShadowCrash, true);

		action.EvaluateCycleAction(Spell.ShadowWordPain, self.Requirements.ShadowWordPain);
		action.EvaluateCycleAction(Spell.VampiricTouch, self.Requirements.VampiricTouch.Use);
		action.EvaluateCycleAction(Spell.VampiricTouch, self.Requirements.VampiricTouch.Misery);
		-- actions.cleave+=/void_torrent
		action.EvaluateAction(Talent.VoidTorrent, true);
		action.EvaluateCycleAction(Spell.MindSear, self.Requirements.MindSear);
		-- actions.cleave+=/mind_flay,chain=1,interrupt_immediate=1,interrupt_if=ticks>=2&(cooldown.void_bolt.up|cooldown.mind_blast.up)
		action.EvaluateInterruptCondition(Spell.MindFlay, true, self.Requirements.MindFlay);
		-- actions.cleave+=/shadow_word_pain
		action.EvaluateAction(Spell.ShadowWordPain, true);
	end

	-- actions+=/run_action_list,name=cleave,if=active_enemies>1
	function self.Use(numEnemies)
		return numEnemies > 1;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCleave = Cleave("Cleave");

-- SingleTarget Rotation
local function SingleTarget(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.single+=/dark_ascension,if=buff.voidform.down
		DarkAscension = function()
			return Player.Buff(Aura.Voidform).Down();
		end,

		-- actions.single+=/mind_blast,if=variable.dots_up
		MindBlast = function()
			return Variables.dots_up;
		end,

		-- actions.single+=/mind_flay,chain=1,interrupt_immediate=1,interrupt_if=ticks>=2&(cooldown.void_bolt.up|cooldown.mind_blast.up)
		MindFlay = function()
			return Player.Casting.Tick() >= 2
			   and (Spell.VoidBolt.Cooldown.Up() or Spell.MindBlast.Cooldown.Up());
		end,

		ShadowWordDeath = {
			-- actions.single+=/shadow_word_death,if=target.time_to_die<3|cooldown.shadow_word_death.charges=2|(cooldown.shadow_word_death.charges=1&cooldown.shadow_word_death.remains<gcd.max)
			Charges = function()
				return Target.TimeToDie() < 3
				    or Talent.ShadowWordDeath.Charges() == 2
				    or (Talent.ShadowWordDeath.Charges() == 1 and Talent.ShadowWordDeath.Cooldown.Remains() < Player.GCD());
			end,

			-- actions.single+=/shadow_word_death,if=!buff.voidform.up|(cooldown.shadow_word_death.charges=2&buff.voidform.stack<15)
			Use = function()
				return not Player.Buff(Aura.Voidform).Up()
				    or (Talent.ShadowWordDeath.Charges() == 2 and Player.Buff(Aura.Voidform()).Stack() < 15);
			end,
		},

		-- actions.single+=/shadow_word_pain,if=refreshable&target.time_to_die>4&!talent.misery.enabled&!talent.dark_void.enabled
		ShadowWordPain = function()
			return Target.Debuff(Aura.ShadowWordPain).Refreshable()
			   and Target.TimeToDie() > 4
			   and not Talent.Misery.Enabled()
			   and not Talent.DarkVoid.Enabled();
		end,

		-- actions.single+=/surrender_to_madness,if=buff.voidform.stack>=(15+buff.bloodlust.up)&target.time_to_die>200|target.time_to_die<75
		SurrenderToMadness = function()
			return Player.Buff(Aura.Voidform).Stack() >= 15 + val(Player.HasBloodlust())
			   and (Target.TimeToDie() > 200 or Target.TimeToDie() < 75);
		end,

		-- actions.single+=/vampiric_touch,if=refreshable&target.time_to_die>6|(talent.misery.enabled&dot.shadow_word_pain.refreshable)
		VampiricTouch = function()
			return Target.Debuff(Aura.VampiricTouch).Refreshable()
			   and Target.TimeToDie() > 6
			    or (Talent.Misery.Enabled() and Target.Debuff(Aura.ShadowWordPain).Refreshable());
		end,

		-- actions.single+=/void_torrent,if=dot.shadow_word_pain.remains>4&dot.vampiric_touch.remains>4
		VoidTorrent = function()
			return Target.Debuff(Aura.ShadowWordPain).Remains() > 4
			   and Target.Debuff(Aura.VampiricTouch).Remains() > 4;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.single=void_eruption
		action.EvaluateAction(Spell.VoidEruption, true);
		action.EvaluateAction(Talent.DarkAscension, self.Requirements.DarkAscension);
		-- actions.single+=/void_bolt
		action.EvaluateAction(Spell.VoidBolt, true);
		action.EvaluateAction(Talent.ShadowWordDeath, self.Requirements.ShadowWordDeath.Charges);
		action.EvaluateAction(Talent.SurrenderToMadness, self.Requirements.SurrenderToMadness);

		-- We can't do raid add events, so lets just suggest this on cooldown.
		-- actions.single+=/dark_void,if=raid_event.adds.in>10
		action.EvaluateAction(Talent.DarkVoid, true);

		-- actions.single+=/mindbender
		action.EvaluateAction(Talent.Mindbender, true);
		action.EvaluateAction(Talent.ShadowWordDeath, self.Requirements.ShadowWordDeath.Use);

		-- We can't do raid add events, so lets just suggest this on cooldown.
		-- actions.single+=/shadow_crash,if=raid_event.adds.in>5&raid_event.adds.duration<20
		action.EvaluateAction(Talent.ShadowCrash, true);

		action.EvaluateAction(Spell.MindBlast, self.Requirements.MindBlast);
		action.EvaluateAction(Talent.VoidTorrent, self.Requirements.VoidTorrent);
		action.EvaluateAction(Spell.ShadowWordPain, self.Requirements.ShadowWordPain);
		action.EvaluateAction(Spell.VampiricTouch, self.Requirements.VampiricTouch);
		action.EvaluateInterruptCondition(Spell.MindFlay, true, self.Requirements.MindFlay);
		-- actions.single+=/shadow_word_pain
		action.EvaluateAction(Spell.ShadowWordPain, true);
	end

	-- actions+=/run_action_list,name=single,if=active_enemies=1
	function self.Use(numEnemies)
		return numEnemies == 1;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSingleTarget = SingleTarget("SingleTarget");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Djriff and SimCraft",
		GuideLink = "https://www.wowhead.com/shadow-priest-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		Dispersion = function()
			return Player.DamagePredicted(3) >= 30;
		end,

		Fade = function()
			return Player.DamagePredicted(3) >= 15
		end,

		PowerWordShield = function()
			return not Player.Buff(Aura.PowerWordShield).Up()
					and Player.DamagePredicted(5) >= 15;
		end,

		ShadowMend = function()
			return Player.Health.Percent() < 60;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions=potion,if=buff.bloodlust.react|target.time_to_die<=80|target.health.pct<35
		RisingDeath = function()
			return Player.HasBloodlust()
			    or Target.TimeToDie() <= 80
			    or Target.Health.Percent() < 35;
		end,

		-- actions.precombat+=/shadowform,if=!buff.shadowform.up
		Shadowform = function()
			return not Player.Buff(Aura.Shadowform).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			RisingDeath = Objects.newItem(152559),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Berserking);
		config.BuffOptions(options, Spell.Shadowform);
		config.AOEOptions(options, Talent.DarkVoid, Spell.MindSear);
		config.CooldownOptions(options, Talent.DarkAscension, Talent.Mindbender, Talent.ShadowCrash, Talent.ShadowWordDeath, Talent.ShadowWordVoid, Talent.SurrenderToMadness,
							   			Spell.VoidBolt, Spell.VoidEruption, Talent.VoidTorrent);
		config.DefensiveOptions(options, Spell.Dispersion, Spell.Fade, Spell.PowerWordShield, Spell.ShadowMend);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.Dispersion, self.Defensives.Dispersion);
		action.EvaluateDefensiveAction(Spell.PowerWordShield, self.Defensives.PowerWordShield);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Spell.ShadowMend, self.Defensives.ShadowMend);

		-- Suggest Fade if no other cooldowns are appropriate.
		action.EvaluateDefensiveAction(Spell.Fade, self.Defensives.Fade);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.RisingDeath, true);
		action.EvaluateAction(Spell.Shadowform, self.Requirements.Shadowform);
		-- actions.precombat+=/mind_blast
		action.EvaluateAction(Spell.MindBlast, true);
		-- actions.precombat+=/shadow_word_void
		action.EvaluateAction(Talent.ShadowWordVoid, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		action.EvaluateAction(Consumable.RisingDeath, self.Requirements.RisingDeath);

		-- actions+=/variable,name=dots_up,op=set,value=dot.shadow_word_pain.ticking&dot.vampiric_touch.ticking
		Variables.dots_up = Target.Debuff(Aura.ShadowWordPain).Ticking() and Target.Debuff(Aura.VampiricTouch).Ticking();

		-- actions+=/berserking
		action.EvaluateAction(Racial.Berserking, true);

		action.RunActionList(rotationAOE);
		action.RunActionList(rotationCleave);
		action.RunActionList(rotationSingleTarget);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Shadow Priest", addonTable.Enum.SpecID.PRIEST_SHADOW);
