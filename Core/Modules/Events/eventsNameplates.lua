-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local max = max;

-- Table of CLEU events for when a unit is removed from combat.
local CLEU_UNIT_REMOVED = {
	UNIT_DESTROYED = true,
	UNIT_DIED = true,
	UNIT_DISSIPATES = true,
};

local enabled = false;
local Events, Nameplates = {}, {};

Nameplates = {
	Disable = function()
		if enabled then
			-- Reset the settings back to their previous setting, now that we are out of combat.
			local Profile = addonTable.accountSettings;
			local settingsNameplates = Profile.NameplateSettings;

			addon.API.Blizzard.SetCVar("nameplateShowAll", 1);
			addon.API.Blizzard.SetCVar("nameplateMaxDistance", settingsNameplates.nameplateMaxDistance);

			if settingsNameplates.nameplateShowAlways == 0 then
				addon.API.Blizzard.SetCVar("nameplateShowFriends", settingsNameplates.nameplateShowFriends);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyGuardians", settingsNameplates.nameplateShowFriendlyGuardians);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyMinions", settingsNameplates.nameplateShowFriendlyMinions);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyNPCs", settingsNameplates.nameplateShowFriendlyNPCs);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyPets", settingsNameplates.nameplateShowFriendlyPets);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyTotems", settingsNameplates.nameplateShowFriendlyTotems);
			end

			-- Because the addon changes enemy nameplates regardless of Show Always we need to put them back to their saved setting.
			addon.API.Blizzard.SetCVar("nameplateShowEnemies", settingsNameplates.nameplateShowEnemies);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyGuardians", settingsNameplates.nameplateShowEnemyGuardians);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyMinions", settingsNameplates.nameplateShowEnemyMinions);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyMinus", settingsNameplates.nameplateShowEnemyMinus);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyPets", settingsNameplates.nameplateShowEnemyPets);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyTotems", settingsNameplates.nameplateShowEnemyTotems);

			addon.System.Callbacks.Callback("WeakAurasTexts", false);

			enabled = false;
		end
	end,

	Enable = function()
		if not enabled then
			local Profile = addonTable.accountSettings;
			local settingsNameplates = Profile.NameplateSettings;

			enabled = true;

			addon.API.Blizzard.SetCVar("nameplateShowAll", 1);
			addon.API.Blizzard.SetCVar("nameplateMaxDistance", max(addonTable.nameplateDistance, 40));

			if settingsNameplates.nameplateShowAlways == 0 then
				addon.API.Blizzard.SetCVar("nameplateShowFriends", 0);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyGuardians", 0);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyMinions", 0);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyNPCs", 0);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyPets", 0);
				addon.API.Blizzard.SetCVar("nameplateShowFriendlyTotems", 0);
			end

			-- If the addon needs to use nameplates, so enable the needed nameplates.
			addon.API.Blizzard.SetCVar("nameplateShowEnemies", 1);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyGuardians", 1);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyMinions", 1);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyMinus", 1);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyPets", 1);
			addon.API.Blizzard.SetCVar("nameplateShowEnemyTotems", 1);

			addon.System.Callbacks.Callback("WeakAurasTexts", true);
		end
	end,
};

Events = {
	CombatLog = function(...)
		local Target = addon.Units.Target;
		local Unit = addon.API.Unit;

		local _, _, combatEvent, _, _, _, _, _, destGUID = ...;

		if CLEU_UNIT_REMOVED[combatEvent] and destGUID == Target.GUID() and not Unit.InCombat() then
			Nameplates.Disable();
		end
	end,

	TargetChanged = function()
		local Target = addon.Units.Target;
		local Unit = addon.API.Unit;

		if Target.IsValidTarget() then
			Nameplates.Enable();
		elseif not Unit.InCombat() then
			Nameplates.Disable();
		end
	end,
};

local moduleEvents = {
	enterCombat = function()
		Nameplates.Enable();
	end,

	leaveCombat = function()
		local Target = addon.Units.Target;

		if not Target.IsValidTarget() then
			Nameplates.Disable();
		end
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = Events.CombatLog,
	INSTANCE_ENCOUNTER_ENGAGE_UNIT = Nameplates.Enable,
	PLAYER_TARGET_CHANGED = Events.TargetChanged,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents);