-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local buttonMapping, keybinds = {}, {};

local actionBar = {
	Item = {},
	Spell = {},
};
addon.Core.Tables.CaseInsensitive(actionBar);

local Actions, Buttons, Keybinds, Process, Scan, UIButtons = {}, {}, {}, {}, {}, {};

Actions = {
	Add = function(buttonID, actionType, actionID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and actionType and actionID and not Actions.Exists(buttonID, actionType, actionID) then
			local sort, tinsert = sort, tinsert;

			local Tables = addon.Core.Tables;
			local actionTable = Tables.GetValue(actionBar[actionType], actionID, {});

			tinsert(actionTable, buttonID);
			sort(actionTable);

			actionBar[actionType][actionID] = actionTable;
			buttonMapping[buttonID] = {
				actionType = actionType,
				actionID = actionID,
			};

			return true;
		end

		return false;
	end,

	Changed = function(buttonID, actionType, actionID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and actionType and actionID then
			local Tables = addon.Core.Tables;

			-- Get the previously stored information in that slotID.
			local previousType = Tables.GetValue(buttonMapping[buttonID], "actionType", "");
			local previousID = Tables.GetValue(buttonMapping[buttonID], "actionID", 0);

			return actionType ~= previousType or actionID ~= previousID, previousType, previousID;
		end

		return false;
	end,

	Exists = function(buttonID, actionType, actionID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and actionType and actionID then
			local Tables = addon.Core.Tables;
			local actionTable = Tables.GetValue(actionBar[actionType], actionID, {});

			if not Tables.IsEmpty(actionTable) then
				local ipairs = ipairs;

				for index, data in ipairs(actionTable) do
					if data == buttonID then
						return true, index;
					end
				end
			end
		end

		return false;
	end,

	Remove = function(buttonID, actionType, actionID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and actionType and actionID then
			local buttonFound, buttonIndex = Actions.Exists(buttonID, actionType, actionID);

			if buttonFound then
				local sort, tremove = sort, tremove;
				local Tables = addon.Core.Tables;
				local actionTable = Tables.GetValue(actionBar[actionType], actionID, {});

				tremove(actionTable, buttonIndex);
				sort(actionTable);

				actionBar[actionType][actionID] = actionTable;
				buttonMapping[buttonID] = nil;

				return true;
			end
		end

		return false;
	end,
};

Buttons = {
	Active = function(buttonID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and buttonID >= 1 and buttonID <= 12 then
			local currentPage = addon.API.Blizzard.GetActionBarPage();
			local bonusBarIndex = addon.API.Blizzard.GetBonusBarIndex();
			local newID = addon.Core.Maths.mod(buttonID - 1, 12) + 1;

			if addon.API.Blizzard.HasBonusActionBar() and bonusBarIndex ~= 0 then
				currentPage = bonusBarIndex;
			end

			return newID == ((currentPage - 1) * 12) + newID;
		end

		return true;
	end,

	DefaultName = function(buttonID)
		local format = format;

		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID then
			if buttonID >= 1 and buttonID <= 24 then
				return format("ACTIONBUTTON%i", addon.Core.Maths.mod(buttonID - 1, 12) + 1);
			elseif buttonID <= 36 then
				return format("MULTIACTIONBAR3BUTTON%i", buttonID - 24);
			elseif buttonID <= 48 then
				return format("MULTIACTIONBAR4BUTTON%i", buttonID - 36);
			elseif buttonID <= 60 then
				return format("MULTIACTIONBAR2BUTTON%i", buttonID - 48);
			elseif buttonID <= 72 then
				return format("MULTIACTIONBAR1BUTTON%i", buttonID - 60);
			elseif buttonID <= 120 then
				return format("ACTIONBUTTON%i", addon.Core.Maths.mod(buttonID - 1, 12) + 1);
			end
		end

		return nil;
	end,

	Name = function(buttonID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID then
			local format = format;
			local buttonName = Buttons.DefaultName(buttonID);

			if addon.API.Blizzard.IsAddOnLoaded("ElvUI") then
				return (buttonID >= 13 and buttonID <= 24 and format("ELVUIBAR6BUTTON%i", addon.Core.Maths.mod(buttonID - 1, 12) + 1)) or buttonName;
			elseif addon.API.Blizzard.IsAddOnLoaded("Bartender4") then
				return buttonID >= 1 and buttonID <= 120 and format("BT4Button%i", buttonID);
			else
				return buttonName;
			end
		end

		return nil;
	end,
};

Process = {
	Action = function(buttonID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID then
			-- Map the current action in the slotID.
			local actionType, actionID = addon.API.Blizzard.GetActionInfo(buttonID);

			if actionType == "spell" then
				Process.Spell(buttonID, actionType, actionID);
			elseif actionType == "item" then
				Process.Item(buttonID, actionType, actionID);
			elseif actionType == "macro" then
				Process.Macro(buttonID, actionType, actionID);
			else
				local actionChanged, previousType, previousID = Actions.Changed(buttonID, actionType, actionID);

				-- We have no action information so we are only removing the old existing data if its changed.
				if actionChanged then
					Actions.Remove(buttonID, previousType, previousID);
				end
			end
		end
	end,

	Item = function(buttonID, actionType, actionID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and actionType and actionID then
			local actionChanged, previousType, previousID = Actions.Changed(buttonID, actionType, actionID);

			if actionChanged then
				-- Clear previous bindings for this slotID
				Actions.Remove(buttonID, previousType, previousID);
				Actions.Add(buttonID, actionType, actionID);
			end
		end
	end,

	Keybind = function(keybind)
		if keybind and keybind ~= "" then
			local gsub, strlen, strupper = gsub, strlen, strupper;

			-- Shorten the keybinding names.
			if keybind and strlen(keybind) > 4 then
				-- Convert to Uppercase
				keybind = strupper(keybind);
				-- Strip whitespace.
				keybind = gsub(keybind, "%s+", "");
				-- Convert modifiers to a single character.
				keybind = gsub(keybind, "ALT%-", "A-");
				keybind = gsub(keybind, "CTRL%-", "C-");
				keybind = gsub(keybind, "SHIFT%-", "S-");
				-- Shorten number pad keybinding names.
				keybind = gsub(keybind, "NUMPAD", "N");
				keybind = gsub(keybind, "PLUS", "+");
				keybind = gsub(keybind, "MINUS", "-");
				keybind = gsub(keybind, "MULTIPLY", "*");
				keybind = gsub(keybind, "DIVIDE", "/");
				keybind = gsub(keybind, "SPACE", "SP");
				-- Shorten mouse keybinding names.
				keybind = gsub(keybind, "BUTTON", "MB");
			end

			return keybind or "";
		end

		return "";
	end,

	Macro = function(buttonID, actionType, macroID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and actionType and macroID and addon.API.Blizzard.GetActionText(buttonID) ~= nil then
			local actionID = addon.API.Blizzard.GetMacroSpell(macroID);

			if actionID then
				Process.Spell(buttonID, "spell", actionID);
			else
				local gsub, select, strmatch = gsub, select, strmatch;
				local hyperlink = select(2, addon.API.Blizzard.GetMacroItem(macroID));

				if hyperlink then
					local linkData = select(3, strmatch(hyperlink, "|?c?f?f?(%x*)|?H?([^:]*):?(%d+)|?h?%[?([^%[%] ]*)%]?|?h?|?r?"));
					local replacedString = gsub(linkData, ":.*", "");

					actionID = addon.Core.General.ToNumber(replacedString);

					if actionID then
						Process.Item(buttonID, "item", actionID);
					end
				end
			end
		end
	end,

	Spell = function(buttonID, actionType, actionID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID and actionType and actionID then
			local actionChanged, previousType, previousID = Actions.Changed(buttonID, actionType, actionID);

			if actionChanged then
				-- Clear previous bindings for this slotID
				Actions.Remove(buttonID, previousType, previousID);
				Actions.Add(buttonID, actionType, actionID);
			end
		end
	end,
};

Scan = {
	ActionBar = function()
		local wipe = wipe;

		wipe(buttonMapping);
		wipe(actionBar.item);
		wipe(actionBar.spell);

		for buttonID = 1, 120 do
			Process.Action(buttonID);
		end
	end,

	ActionButton = function(...)
		local _, buttonID = ...;

		if buttonID == 0 then
			Scan.ActionBar();
		else
			Process.Action(buttonID);
		end
	end,

	Bindings = function()
		local wipe = wipe;

		wipe(keybinds);

		for buttonID = 1, 120 do
			local buttonName = Buttons.Name(buttonID);
			local keybind = addon.API.Blizzard.GetBindingKey(buttonName);

			keybinds[buttonID] = Process.Keybind(keybind);
		end
	end
};

UIButtons = {
	Bartender = function(buttonID)
		local buttonName = Buttons.Name(buttonID);

		return buttonName ~= nil and _G[buttonName];
	end,

	Blizzard = function(buttonID)
		local buttonName = Buttons.Name(buttonID);

		return buttonName ~= nil and _G[buttonName];
	end,

	Button = function(buttonID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID then
			if addon.API.Blizzard.IsAddOnLoaded("ElvUI") then
				return UIButtons.ElvUI(buttonID);
			elseif addon.API.Blizzard.IsAddOnLoaded("Bartender4") then
				return UIButtons.Bartender(buttonID);
			else
				return UIButtons.Blizzard(buttonID);
			end
		end

		return nil;
	end,

	ElvUI = function(buttonID)
		local buttonName = Buttons.Name(buttonID);

		if buttonName then
			local LibStub, pairs = LibStub, pairs;
			local Tables = addon.Core.Tables;
			local buttonLibrary = LibStub:GetLibrary("LibActionButton-1.0-ElvUI", true);

			if buttonLibrary then
				local buttonTable = buttonLibrary:GetAllButtons();

				if not Tables.IsEmpty(buttonTable) then
					for frame in pairs(buttonLibrary:GetAllButtons()) do
						if buttonName == frame:GetBindingAction() then
							return frame;
						end
					end
				end
			end
		end

		return nil;
	end,
};

-- This is stored on the addon so its publically available to all other addons.
addon.Modules.ActionBar = {
	Active = function(buttonID)
		return Buttons.Active(buttonID);
	end,

	ButtonID = function(actionType, actionID)
		if actionType and actionID then
			local Tables = addon.Core.Tables;
			local actionTable = Tables.GetValue(actionBar[actionType], actionID, {});

			if not Tables.IsEmpty(actionTable) then
				local ipairs = ipairs;

				for _, data in ipairs(actionTable) do
					if Buttons.Active(data) then
						return data;
					end
				end
			end
		end

		return nil;
	end,

	GetButton = function(buttonID)
		buttonID = addon.Core.General.ToNumber(buttonID);

		if buttonID then
			return UIButtons.Button(buttonID);
		end

		return nil;
	end,

	Keybind = function(actionType, actionID)
		if actionType and actionID then
			local Tables = addon.Core.Tables;
			local buttonID = addon.Modules.ActionBar.ButtonID(actionType, actionID);

			return Tables.GetValue(keybinds, buttonID, "");
		end

		return "";
	end,
};

local moduleEvents = {
	playerLogin = function()
		Scan.ActionBar();
		Scan.Bindings();
	end,

	UpdateKeybinds = function()
		Scan.ActionBar();
		Scan.Bindings();
	end,
};

local tableEvents = {
	ACTIONBAR_SLOT_CHANGED = Scan.ActionBar,
	SPELLS_CHANGED = Scan.ActionBar,
	UPDATE_BINDINGS = Scan.Bindings,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);