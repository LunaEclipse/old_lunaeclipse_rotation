-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local MAGIC_DAMAGE = {
	[2] = true, -- Holy Damage
	[4] = true, -- Fire Damage
	[8] = true, -- Nature Damage
	[16] = true, -- Frost Damage
	[32] = true, -- Shadow Damage
	[64] = true, -- Arcane Damage
};

local PHYSICAL_DAMAGE = {
	[1] = true, -- Physical Damage
};

local function calculateAmountInX(unitData, seconds, minSamples)
	local Tables = addon.Core.Tables;

	if not Tables.IsEmpty(unitData) then
		local totalDamage = 0;
		local totalTime = 0;
		local numSamples = #unitData;

		minSamples = minSamples or 3;

		if numSamples > minSamples then
			for counter = 2, numSamples do
				totalDamage = totalDamage + Tables.GetValue(unitData[counter], "Damage", 0);
				totalTime = totalTime + (Tables.GetValue(unitData[counter], "TimeStamp", 0) - Tables.GetValue(unitData[counter - 1], "TimeStamp", 0));
			end

			return (totalDamage / totalTime) * seconds;
		end
	end

	return 0;
end

local function calculateMatrix(unitData, minSamples)
	local X, Y, XY, X2 = 0, 0, 0, 0;
	local numSamples = (unitData ~= nil and #unitData) or 0;

	minSamples = minSamples or 3;

	if numSamples >= minSamples then
		for counter = 1, numSamples do
			local healthPercent = unitData[counter].HealthPercent;
			local timeStamp = unitData[counter].TimeStamp;

			X = X + timeStamp;
			Y = Y + healthPercent;
			XY = XY + (timeStamp * healthPercent);
			X2 = X2 + (timeStamp ^ 2);
		end
	end

	return X, Y, XY, X2;
end

local function calculateTimeToX(Object, healthPercent, minSamples)
	local huge, max = math.huge, max;
	local Cache = addonTable.Cache;

	local returnValue = huge;
	local currentTime = addon.API.Blizzard.GetTime();
	local GUID = addon.API.Blizzard.UnitGUID(Object.ClassID());

	local unitData = Cache.Persistant.TimeToDie.Units[GUID];
	local numSamples = (unitData ~= nil and #unitData) or 0;

	minSamples = minSamples or 3;

	-- Simple linear regression
	-- Solve to find a and b, satisfying y = a + bx
	-- Matrix arithmetic has been expanded and solved to make the following operation as fast as possible
	if unitData then
		local X, Y, XY, X2 = calculateMatrix(unitData, minSamples)
		local divisor = ((X2 * numSamples) - (X * X));

		if divisor ~= 0 then
			-- Invariant to find matrix inverse
			local inverseMatrix = 1 / divisor;

			-- Solve for a and b
			local a = ((-X * XY) * inverseMatrix) + ((X2 * Y) * inverseMatrix);
			local b = ((numSamples * XY) * inverseMatrix) - ((X * Y) * inverseMatrix);

			if b ~= 0 then
				-- Use best fit line to calculate estimated time to reach target health
				returnValue = (healthPercent - a) / b;

				-- Subtract current time to obtain "time remaining"
				returnValue = max(returnValue - currentTime, 0);
			end
		end
	end

	return returnValue;
end

-- Internal function to get the percent health a unit "Dies" at if it doesn't actually die.
local function getTTDPercentage(Object)
	local Objects = addon.Core.Objects;

	if Objects.IsUnit(Object) then
		local npcID = Object.NPCID() or 0;

		-- Get the unit TTD Percentage
		local minHealthPercent = {
			--- Legion
			----- Open World  -----
			--- Stormheim Invasion (7.2 Patch)
			-- Lord Commander Alexius
			[118566] = 85,
			----- Dungeons (7.0 Patch) -----
			--- Halls of Valor
			-- Hymdall leaves the fight at 10%.
			[94960] = 10,
			-- Fenryr leaves the fight at 60%. We take 50% as check value since it doesn't get immune at 60%.
			[95674] = function(self)
				return (self.HealthPercent() > 50 and 60)
					or 0;
			end,
			-- Odyn leaves the fight at 80%.
			[95676] = 80,
			-- Maw of Souls
			-- Helya leaves the fight at 70%.
			[96759] = 70,
			----- Trial of Valor (T19 - 7.1 Patch) -----
			--- Odyn
			-- Hyrja & Hymdall leaves the fight at 25% during first stage and 85%/90% during second stage (HM/MM).
			[114360] = function(self)
				return (not self.IsBoss(114263, 99) and 25)
					or (addon.Units.Player.InstanceDifficulty() == addonTable.Enum.InstanceDifficulty.MYTHIC_RAID_20MAN and 85)
					or 90;
			end,
			[114361] = function(self)
				return (not self.IsBoss(114263, 99) and 25)
					or (addon.Units.Player.InstanceDifficulty() == addonTable.Enum.InstanceDifficulty.MYTHIC_RAID_20MAN and 85)
					or 90;
			end,
			-- Odyn leaves the fight at 10%.
			[114263] = 10,
			----- Nighthold (T19 - 7.1.5 Patch) -----
			--- Elisande
			-- She leaves the fight two times at 10% then she normally dies.
			-- She looses 50% power per stage (100 -> 50 -> 0).
			[106643] = function(self)
				return (self.Power() > 0 and 10)
					or 0;
			end,
			--- Warlord of Draenor (WoD)
			----- HellFire Citadel (T18 - 6.2 Patch) -----
			--- Hellfire Assault
			-- Mar'Tak doesn't die and leave fight at 50% (blocked at 1hp anyway).
			[93023] = 50,
			----- Dungeons (6.0 Patch) -----
			--- Shadowmoon Burial Grounds
			-- Carrion Worm : They doesn't die but leave the area at 10%.
			[88769] = 10,
			[76057] = 10,
		};

		local type = type;
		local healthPercent = minHealthPercent[npcID] or 0;

		if type(healthPercent) == "function" then
			return healthPercent(Object);
		else
			return healthPercent;
		end
	end

	return 0;
end

local function validateCheck(Object)
	local Objects = addon.Core.Objects;

	if Objects.IsUnit(Object) then
		return Object.Exists()
		   and not Object.IsDummy()
		   and addon.Units.Player.CanAttack(Object);
	else
		return false;
	end
end

addonTable.TimeToDie = {
	AllDamageInX = function(seconds, minSamples)
		local Cache = addonTable.Cache;

		return (calculateAmountInX(Cache.Persistant.IncomingDamage.All, seconds, minSamples) / addon.Units.Player.Health.Max()) * 100;
	end,

	MagicDamageInX = function(seconds, minSamples)
		local Cache = addonTable.Cache;

		return (calculateAmountInX(Cache.Persistant.IncomingDamage.Magic, seconds, minSamples) / addon.Units.Player.Health.Max()) * 100;
	end,

	PhysicalDamageInX = function(seconds, minSamples)
		local Cache = addonTable.Cache;

		return (calculateAmountInX(Cache.Persistant.IncomingDamage.Physical, seconds, minSamples) / addon.Units.Player.Health.Max()) * 100;
	end,

	TimeToX = function(Object, healthPercent, minSamples)
		local huge = math.huge;

		if validateCheck(Object) then
			return calculateTimeToX(Object, healthPercent, minSamples);
		end

		return huge;
	end,

	TimeToDie = function(Object, minSamples)
		local huge = math.huge;

		if validateCheck(Object) then
			return calculateTimeToX(Object, getTTDPercentage(Object), minSamples);
		end

		return huge;
	end,
};

local function pruneTimeToDie()
	local huge, pairs, tremove = math.huge, pairs, tremove;
	local Cache = addonTable.Cache;
	local Settings = addon.Core.Settings;
	local Tables = addon.Core.Tables;

	-- Get the current time, don't use the cache because we are responding to events in real time.
	local currentTime = addon.API.Blizzard.GetTime();
	local maxHistoryTime = Settings.GetAccountValue("TimeToDieSettings", "maxHistoryTime");
	local timeElapsed;

	-- Loop through the last refresh time for all units.
	for GUID, refreshTime in pairs(Cache.Persistant.TimeToDie.Refresh) do
		timeElapsed = (refreshTime and currentTime - refreshTime) or huge;

		-- Check if the last refresh is older then the max history age.
		if timeElapsed > maxHistoryTime then
			-- Remove the unit from the refresh table
			Cache.Persistant.TimeToDie.Refresh[GUID] = nil;
			-- Remove the unit samples from the Time to Die table.
			Cache.Persistant.TimeToDie.Units[GUID] = nil;
		end
	end

	-- Loop through the samples for all units
	for _, unitData in pairs(Cache.Persistant.TimeToDie.Units) do
		-- Check to make sure that we actually have a Time to Die table to prune.
		if not Tables.IsEmpty(unitData) then
			-- Loop through the Time to Die table from the beginning to the end
			for counter = 1, #unitData do
				timeElapsed = currentTime - Tables.GetValue(unitData[1], "TimeStamp", 0);

				-- Check if the sample is older then the max history age.
				if timeElapsed > maxHistoryTime then
					-- Remove the sample from the Time to Die table.
					tremove(unitData, 1);
				else
					-- Exit the loop because all the rest of the data should be fine.
					break;
				end
			end
		end
	end
end

local function pruneIncomingDamage()
	local pairs, tremove = pairs, tremove;
	local Cache = addonTable.Cache;
	local Settings = addon.Core.Settings;
	local Tables = addon.Core.Tables;

	-- Get the current time, don't use the cache because we are responding to events in real time.
	local currentTime = addon.API.Blizzard.GetTime();
	local maxHistoryTime = Settings.GetAccountValue("IncomingDamageSettings", "maxHistoryTime");
	local timeElapsed;

	-- Loop through the samples for all units
	for _, damageData in pairs(Cache.Persistant.IncomingDamage) do
		-- Check to make sure that we actually have a Time to Die table to prune.
		if not Tables.IsEmpty(damageData) then
			-- Loop through the Incoming Damage table from the beginning to the end
			for counter = 1, #damageData do
				timeElapsed = currentTime - Tables.GetValue(damageData[1], "TimeStamp", 0);

				-- Check if the sample is older then the max history age.
				if timeElapsed > maxHistoryTime then
					-- Remove the sample from the Time to Die table.
					tremove(damageData, 1);
				else
					-- Exit the loop because all the rest of the data should be fine.
					break;
				end
			end
		end
	end
end

local function processDamage(...)
	local Cache = addonTable.Cache;
	local Tables = addon.Core.Tables;

	local _, unitID, action, descriptor, damage, damageType = ...;
	local currentUnit = addon.Units[unitID];

	if currentUnit and currentUnit.Exists() then
		local tinsert = tinsert;
		local currentTime = addon.API.Blizzard.GetTime();
		local GUID = addon.API.Blizzard.UnitGUID(unitID);

		-- Make sure we only add entries if we haven't added on for that GUID less then one refresh cycle ago, some units will have multiple UnitIDs.
		if currentTime > Tables.GetValue(Cache.Persistant.TimeToDie.Refresh, GUID, 0) + addon.refreshInterval then
			-- Get the unit sample table, tf the unit does not have sample table, create it.
			local unitCache = Cache.Persistant.TimeToDie.Units[GUID] or {};

			-- Create the sample data which is HealthPercent and TimeStamp.
			local healthSample = {
				TimeStamp = currentTime,
				HealthPercent = currentUnit.Health.Percent(),
			};

			-- Insert the sample data into the Time to Die table.
			tinsert(unitCache, healthSample);

			-- Put the data back into the Cache.
			Cache.Persistant.TimeToDie.Units[GUID] = unitCache;
			Cache.Persistant.TimeToDie.Refresh[GUID] = currentTime;
		end

		if unitID == "player" and action == "WOUND" then
			Cache.Persistant.IncomingDamage.All = Cache.Persistant.IncomingDamage.All or {};
			Cache.Persistant.IncomingDamage.Magic = Cache.Persistant.IncomingDamage.Magic or {};
			Cache.Persistant.IncomingDamage.Physical = Cache.Persistant.IncomingDamage.Physical or {};

			local damageSample = {
				Action = action,
				Descriptor = descriptor,
				Damage = damage,
				TimeStamp = currentTime,
			};

			if MAGIC_DAMAGE[damageType] then
				tinsert(Cache.Persistant.IncomingDamage.Magic, damageSample);
			elseif PHYSICAL_DAMAGE[damageType] then
				tinsert(Cache.Persistant.IncomingDamage.Physical, damageSample);
			end

			tinsert(Cache.Persistant.IncomingDamage.All, damageSample);
		end
	end
end

local function timerEvent()
	local Maths = addon.Core.Maths;

	-- Prune any time to die samples that are too old.
	pruneTimeToDie();

	-- Prune any incoming damage samples that are too old.
	pruneIncomingDamage();

	-- Set the addon value for predicted damage incoming for the next 3 seconds.
	addon.DamagePredicted = Maths.round(addon.Units.Player.DamagePredicted(3));
	addon.MagicDamagePredicted = Maths.round(addon.Units.Player.MagicDamagePredicted(3));
	addon.PhysicalDamagePredicted = Maths.round(addon.Units.Player.PhysicalDamagePredicted(3));

	-- Set the addon value for time to die to current target's time to die.
	addon.TTD = Maths.round(addon.Units.Target.TimeToDie());
end

local moduleEvents = {
	Disabled = function()
		local wipe = wipe;

		wipe(addonTable.Cache.Persistant.IncomingDamage.All);
		wipe(addonTable.Cache.Persistant.IncomingDamage.Magic);
		wipe(addonTable.Cache.Persistant.IncomingDamage.Physical);

		addon.System.Timers.TimerRemove(timerEvent);
	end,

	Enabled = function()
		-- Make sure the timer event is called immediately before starting the timer so it doesn't need to wait 1 second to get enemy data.
		timerEvent();

		addon.System.Timers.TimerAdd(timerEvent, 1);
	end,
};

local tableEvents = {
	UNIT_COMBAT = processDamage,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);