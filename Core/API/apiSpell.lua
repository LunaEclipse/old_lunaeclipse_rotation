-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- This is stored on the addon so its publically available to all other addons.
addon.API.Spell = {
	GetSpellCooldown = function(spellID)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local currentTime = addon.API.Blizzard.GetTime();
		local returnValue;

		if spellID then
			returnValue = Tables.GetValue(Cache.Temporary.Cooldowns.Spell, spellID);

			if Tables.IsEmpty(returnValue) or currentTime - Tables.GetValue(returnValue, "CacheTime", 0) >= addon.refreshInterval then
				local cooldownStart, cooldownDuration, enabled, cooldownModRate = addon.API.Blizzard.GetSpellCooldown(spellID);

				if cooldownStart then
					local GCDStart, GCDDuration = addon.API.Blizzard.GetSpellCooldown(addonTable.GLOBAL_COOLDOWN);
					local currentCharges, maxCharges, chargeStart, chargeDuration, chargeModRate = addon.API.Blizzard.GetSpellCharges(spellID);
					local chargeEnd, cooldownEnd;

					-- Check to see if the spell is actually on cooldown or the information is just the global cooldown.
					if spellID ~= addonTable.GLOBAL_COOLDOWN and cooldownStart == GCDStart and cooldownDuration == GCDDuration then
						cooldownStart = 0;
						cooldownDuration = 0;
					end

					cooldownEnd = cooldownStart + cooldownDuration;

					if not maxCharges then
						currentCharges = cooldownEnd - currentTime <= 0 and 1 or 0;
						maxCharges = 1;
						chargeStart = cooldownStart;
						chargeDuration = cooldownDuration;
						chargeEnd = cooldownEnd;
						chargeModRate = cooldownModRate;
					else
						chargeEnd = chargeStart + chargeDuration;
					end

					returnValue = {
						Charges = currentCharges,
						MaxCharges = maxCharges,
						ChargesStart = chargeStart,
						ChargesEnd = chargeEnd,
						ChargesDuration = chargeDuration,
						ChargesModRate = chargeModRate,
						Start = cooldownStart,
						End = cooldownEnd,
						Duration = cooldownDuration,
						ModRate = cooldownModRate,
						Locked = enabled == 0,
						CacheTime = currentTime,
					};

					Cache.Temporary.Cooldowns.Spell[spellID] = returnValue;
				end
			end
		end

		return returnValue;
	end,

	GetSpellInfo = function(spellID)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local currentTime = addon.API.Blizzard.GetTime();
		local returnValue;

		if spellID then
			returnValue = Tables.GetValue(Cache.Temporary.SpellInfo, spellID);

			if Tables.IsEmpty(returnValue) or currentTime - Tables.GetValue(returnValue, "CacheTime", 0) >= addon.refreshInterval then
				local spellName, _, spellIcon, spellCastingTime, spellMinRange, spellMaxRange = addon.API.Blizzard.GetSpellInfo(spellID);

				if spellName then
					returnValue = {
						ID = spellID,
						Name = spellName,
						IconID = spellIcon,
						CastingTime = spellCastingTime,
						BaseCooldown = addon.API.Blizzard.GetSpellBaseCooldown(spellID),
						UseRange = addon.API.Blizzard.SpellHasRange(spellName) ~= nil,
						MinRange = spellMinRange,
						MaxRange = spellMaxRange,
						CacheTime = currentTime,
					};

					Cache.Temporary.SpellInfo[spellID] = returnValue;
				end
			end
		end

		return returnValue;
	end,

	GetSpellPowerCost = function(spellID)
		local Tables = addon.Core.Tables;

		if spellID then
			local costInfo = addon.API.Blizzard.GetSpellPowerCost(spellID)[1];

			if not Tables.IsEmpty(costInfo) then
				return {
					HasRequiredAura = costInfo.hasRequiredAura,
					Type = costInfo.type,
					Name = costInfo.name,
					Cost = costInfo.cost,
					MinCost = costInfo.minCost,
					RequiredAuraID = costInfo.requiredAuraID,
					CostPercent = costInfo.costPercent,
					CostPerSec = costInfo.costPerSec,
				};
			end
		end

		return nil;
	end,
};