-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Any potion that shares the cooldown will work, this is just to see if its locked without relying on a potion the player has.
local POTION = 127844;

-- Variable to store whether potions are currently locked down
local potionLockedDown = false;
local inCombat = false;

addonTable.PotionTracker = {
	CombatLocked = function()
		return potionLockedDown;
	end,
};

local function bagItemCooldown()
	local Tables = addon.Core.Tables;
	local apiItem = addon.API.Item;

	local returnValue = potionLockedDown;

	if inCombat and not potionLockedDown then
		local itemInfo = apiItem.GetItemCooldown(POTION);

		if Tables.GetValue(itemInfo, "Locked", false) then
			potionLockedDown = true;
			returnValue = true;
		end
	end

	return returnValue;
end

local moduleEvents = {
	enterCombat = function()
		inCombat = true;
		potionLockedDown = bagItemCooldown();
	end,

	leaveCombat = function()
		inCombat = false;
		potionLockedDown = false;
	end,
};

local tableEvents = {
	BAG_UPDATE_COOLDOWN = bagItemCooldown,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);