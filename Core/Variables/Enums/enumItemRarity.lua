local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Enum.ItemRarity = {
	POOR = 0,
	COMMON = 1,
	UNCOMMON = 2,
	RARE = 3,
	EPIC = 4,
	LEGENDARY = 5,
	ARTIFACT = 6,
	HEIRLOOM = 7,
	WOW_TOKEN = 8,
};