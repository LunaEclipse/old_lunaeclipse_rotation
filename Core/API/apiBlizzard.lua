-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local BlizzardAPI = {
	CancelUnitBuff = CancelUnitBuff,
	FindBaseSpellNameByName = FindBaseSpellNameByName,
	FindSpellOverrideNameByName = FindSpellOverrideNameByName,
	GetItemSpell = GetItemSpell,
	GetMacroSpell = GetMacroSpell,
	GetPetActionInfo = GetPetActionInfo,
	GetPossessInfo = GetPossessInfo,
	GetShapeshiftFormInfo = GetShapeshiftFormInfo,
	GetSpellBaseCooldown = GetSpellBaseCooldown,
	GetSpellInfo = GetSpellInfo,
	GetSpellLink = GetSpellLink,
	GetTrainerServiceInfo = GetTrainerServiceInfo,
	SearchGuildRecipes = SearchGuildRecipes,
	UnitAura = UnitAura,
	UnitBuff = UnitBuff,
	UnitCastingInfo = UnitCastingInfo,
	UnitChannelInfo = UnitChannelInfo,
	UnitDebuff = UnitDebuff,
}; -- Local copies of the API functions for faster access.
local error, select, setmetatable, tremove, type, unpack = error, select, setmetatable, tremove, type, unpack;

local Prototype = {
	-- API Version History
	-- 8.0 - No longer supports canceling by spell name.
	CancelUnitBuff = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			local unitID, ID = ...;

			if type(ID) == "string" then
				for counter = 1, 40 do
					local auraName = BlizzardAPI["UnitBuff"](unitID, counter);

					if ID == auraName then
						return BlizzardAPI[API](unitID, counter);
					end
				end
			else
				return BlizzardAPI[API](...);
			end
		else
			return BlizzardAPI[API](...);
		end
	end,

	-- API Version History
	-- 8.0 - Function Removed
	FindBaseSpellNameByName = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			error("FindBaseSpellNameByName API Function has been removed.  Please report this error!");
		else
			return BlizzardAPI[API](...);
		end
	end,

	-- API Version History
	-- 8.0 - Function Removed
	FindSpellOverrideNameByName = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			error("FindSpellOverrideNameByName API Function has been removed.  Please report this error!");
		else
			return BlizzardAPI[API](...);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	GetItemSpell = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			return BlizzardAPI[API](...);
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped first two parameters (name, and nameSubtext).
	GetMacroSpell = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			return BlizzardAPI[API](...);
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);
			tremove(parameters, 1);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	GetPetActionInfo = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			return BlizzardAPI[API](...);
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Second parameter changed from spell name to spell ID.
	GetPossessInfo = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			return BlizzardAPI[API](...);
		else
			local parameters = { BlizzardAPI[API](...) };
			local spellID = select(7, BlizzardAPI["GetSpellInfo"](parameters[2]));

			parameters[2] = spellID;

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (name).
	GetShapeshiftFormInfo = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			return BlizzardAPI[API](...);
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - No changes.
	--     - Convert cooldown to seconds for addon standardization.
	GetSpellBaseCooldown = function(API, ...)
		local parameters = { BlizzardAPI[API](...) };

		parameters[1] = parameters[1] ~= nil and parameters[1] / 1000;

		return unpack(parameters);
	end,

	-- API Version History
	-- 8.0 - No changes.
	--     - Convert castTime to seconds for addon standardization.
	GetSpellInfo = function(API, ...)
		local parameters = { BlizzardAPI[API](...) };

		parameters[4] = parameters[4] ~= nil and parameters[4] / 1000;

		return unpack(parameters);
	end,

	-- API Version History
	-- 8.0 - No longer returns trade skill link as second parameter (use GetSpellTradeSkillLink)
	GetSpellLink = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			return BlizzardAPI[API](...);
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	GetTrainerServiceInfo = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			return BlizzardAPI[API](...);
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Function Removed
	SearchGuildRecipes = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			error("SearchGuildRecipes API Function has been removed.  Please report this error!");
		else
			return BlizzardAPI[API](...);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	--     - Also, no longer supports querying by spell name.
	UnitAura = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			local unitID, ID = ...;

			if type(ID) == "string" then
				for counter = 1, 40 do
					local auraName = BlizzardAPI[API](unitID, counter);

					if ID == auraName then
						return BlizzardAPI[API](unitID, counter);
					end
				end
			else
				return BlizzardAPI[API](...);
			end
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	--     - Also, no longer supports querying by spell name.
	UnitBuff = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			local unitID, ID = ...;

			if type(ID) == "string" then
				for counter = 1, 40 do
					local auraName = BlizzardAPI[API](unitID, counter);

					if ID == auraName then
						return BlizzardAPI[API](unitID, counter);
					end
				end
			else
				return BlizzardAPI[API](...);
			end
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	--     - Convert castStartTime and castEndTime to seconds for addon standardization.
	UnitCastingInfo = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			local parameters = { BlizzardAPI[API](...) };

			parameters[4] = parameters[4] ~= nil and parameters[4] / 1000;
			parameters[5] = parameters[5] ~= nil and parameters[5] / 1000;

			return unpack(parameters);
		else
			local parameters = { BlizzardAPI[API](...) };

			parameters[5] = parameters[5] ~= nil and parameters[5] / 1000;
			parameters[6] = parameters[6] ~= nil and parameters[6] / 1000;

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	--     - Convert channelStartTime and channelEndTime to seconds for addon standardization.
	UnitChannelInfo = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			local parameters = { BlizzardAPI[API](...) };

			parameters[4] = parameters[4] ~= nil and parameters[4] / 1000;
			parameters[5] = parameters[5] ~= nil and parameters[5] / 1000;

			return unpack(parameters);
		else
			local parameters = { BlizzardAPI[API](...) };

			parameters[5] = parameters[5] ~= nil and parameters[5] / 1000;
			parameters[6] = parameters[6] ~= nil and parameters[6] / 1000;

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,

	-- API Version History
	-- 8.0 - Dropped second parameter (nameSubtext).
	--     - Also, no longer supports querying by spell name.
	UnitDebuff = function(API, ...)
		if addon.CURRENT_WOW_VERSION >= 80000 then
			local unitID, ID = ...;

			if type(ID) == "string" then
				for counter = 1, 40 do
					local auraName = BlizzardAPI[API](unitID, counter);

					if ID == auraName then
						return BlizzardAPI[API](unitID, counter);
					end
				end
			else
				return BlizzardAPI[API](...);
			end
		else
			local parameters = { BlizzardAPI[API](...) };

			tremove(parameters, 2);

			return unpack(parameters);
		end
	end,
};

local metaTable = {
	__index = function(table, key)
		local classPrototype = Prototype[key];

		BlizzardAPI[key] = BlizzardAPI[key] or _G[key];

		if classPrototype then
			if type(classPrototype) == "function" then
				return function(...)
					return classPrototype(key, ...);
				end
			else
				return classPrototype;
			end
		else
			return function(...)
				return BlizzardAPI[key](...);
			end
		end
	end,

	__metatable = "This table is protected!",
};

addon.API.Blizzard = setmetatable({}, metaTable);