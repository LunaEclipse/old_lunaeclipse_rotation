local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local Summons = addonTable.Summons;
-- Objects

local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "lunaeclipse_shaman_elemental";

local Guardians = {
	FireElemental = 15438,
	StormElemental = 77936,
};

-- AOE Rotation
local function AOE(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.aoe+=/ascendance,if=talent.storm_elemental.enabled&cooldown.storm_elemental.remains<120&cooldown.storm_elemental.remains>15|!talent.storm_elemental.enabled
		Ascendance = function()
			return Talent.StormElemental.Enabled()
			   and Talent.StormElemental.Cooldown.Remains() < 120
			   and Talent.StormElemental.Cooldown.Remains() > 15
			    or not Talent.StormElemental.Enabled();
		end,

		-- actions.aoe+=/elemental_blast,if=spell_targets.chain_lightning<4
		ElementalBlast = function(numEnemies)
			return numEnemies < 4;
		end,

		FlameShock = {
			-- actions.aoe+=/flame_shock,moving=1,target_if=refreshable
			Moving = function(numEnemies, Target)
				return Player.IsMoving()
				   and Target.Debuff(Aura.FlameShock).Refreshable();
			end,

			-- actions.aoe+=/flame_shock,if=spell_targets.chain_lightning<4,target_if=refreshable
			Use = function(numEnemies, Target)
				return numEnemies < 4
				   and Target.Debuff(Aura.FlameShock).Refreshable();
			end,
		},

		-- actions.aoe+=/frost_shock,moving=1
		FrostShock = function()
			return Player.IsMoving();
		end,

		LavaBurst = {
			-- actions.aoe+=/lava_burst,moving=1
			Moving = function()
				return Player.IsMoving();
			end,

			-- Only cast Lava Burst on three targets if it is an instant.
			-- actions.aoe+=/lava_burst,if=(buff.lava_surge.up|buff.ascendance.up)&spell_targets.chain_lightning<4
			Use = function(numEnemies)
				return (Player.Buff(Aura.LavaSurge).Up() or Player.Buff(Aura.Ascendance).Up())
				   and numEnemies < 4;
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.aoe=stormkeeper
		action.EvaluateAction(Talent.Stormkeeper, true);
		action.EvaluateAction(Talent.Ascendance, self.Requirements.Ascendance);
		-- actions.aoe+=/liquid_magma_totem
		action.EvaluateAction(Talent.LiquidMagmaTotem, true);
		action.EvaluateCycleAction(Spell.FlameShock, self.Requirements.FlameShock.Use, Enemies.GetEnemies(Spell.ChainLightning));
		-- actions.aoe+=/earthquake
		action.EvaluateAction(Spell.Earthquake, true);
		action.EvaluateAction(Spell.LavaBurst, self.Requirements.LavaBurst.Use, Enemies.GetEnemies(Spell.ChainLightning));
		action.EvaluateAction(Talent.ElementalBlast, self.Requirements.ElementalBlast, Enemies.GetEnemies(Spell.ChainLightning));
		-- actions.aoe+=/lava_beam
		action.EvaluateAction(Spell.LavaBeam, true);
		-- actions.aoe+=/chain_lightning
		action.EvaluateAction(Spell.ChainLightning, true);
		action.EvaluateAction(Spell.LavaBurst, self.Requirements.LavaBurst.Moving);
		action.EvaluateCycleAction(Spell.FlameShock, self.Requirements.FlameShock.Moving);
		action.EvaluateAction(Spell.FrostShock, self.Requirements.FrostShock);
	end

	-- actions+=/run_action_list,name=aoe,if=active_enemies>2&(spell_targets.chain_lightning>2|spell_targets.lava_beam>2)
	function self.Use(numEnemies)
		return numEnemies > 2
		   and (Enemies.GetEnemies(Spell.ChainLightning) > 2 or Enemies.GetEnemies(Spell.LavaBeam) > 2);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationAOE = AOE("AOE");

-- SingleTarget Rotation
local function SingleTarget(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		Ascendance = {
			-- SimCraft doesn't specify it, but this line is only if you have selected Storm Elemental, so add a check for this.
			-- actions.single_target+=/ascendance,if=(time>=60|buff.bloodlust.up)&cooldown.lava_burst.remains>0&cooldown.storm_elemental.remains<=120
			StormElemental = function()
				return (coreGeneral.CombatTime() >= 60 or Player.HasBloodlust())
				   and Spell.LavaBurst.Cooldown.Remains() > 0
				   and Talent.StormElemental.Enabled()
				   and Talent.StormElemental.Cooldown.Remains() <= 120;
			end,

			-- actions.single_target+=/ascendance,if=(time>=60|buff.bloodlust.up)&cooldown.lava_burst.remains>0&!talent.storm_elemental.enabled
			Use = function()
				return (coreGeneral.CombatTime() >= 60 or Player.HasBloodlust())
				   and Spell.LavaBurst.Cooldown.Remains() > 0
				   and not Talent.StormElemental.Enabled();
			end,
		},

		-- The addon can't check both spell targets and active targets, however if spell targets is more than 1, active targets must be more than 1 as well.
		-- actions.single_target+=/chain_lightning,if=active_enemies>1&spell_targets.chain_lightning>1
		ChainLightning = function(numEnemies)
			return numEnemies > 1;
		end,

		-- If possible, use Earth Shock with Master of the Elements.
		-- actions.single_target+=/earth_shock,if=talent.master_of_the_elements.enabled&(buff.master_of_the_elements.up|maelstrom>=92)|!talent.master_of_the_elements.enabled
		EarthShock = function()
			return (Talent.MasterOfTheElements.Enabled() and (Player.Buff(Aura.MasterOfTheElements).Up() or Player.Maelstrom() >= 92))
			    or not Talent.MasterOfTheElements.Enabled();
		end,

		-- Don't use Elemental Blast if you could cast a Master of the Elements empowered Earth Shock instead.
		-- actions.single_target+=/elemental_blast,if=talent.master_of_the_elements.enabled&buff.master_of_the_elements.up&maelstrom<60|!talent.master_of_the_elements.enabled
		ElementalBlast = function()
			return (Talent.MasterOfTheElements.Enabled() and Player.Buff(Aura.MasterOfTheElements).Up() and Player.Maelstrom() < 60)
			    or not Talent.MasterOfTheElements.Enabled();
		end,

		FlameShock = {
			-- actions.single_target+=/flame_shock,moving=1,target_if=refreshable
			Moving = function(numEnemies, Target)
				return Player.IsMoving()
				   and Target.Debuff(Aura.FlameShock).Refreshable();
			end,

			-- actions.single_target=flame_shock,if=!ticking|dot.flame_shock.remains<=gcd
			Refresh = function()
				return not Target.Debuff(Aura.FlameShock).Ticking()
				    or Target.Debuff(Aura.FlameShock).Remains() <= Player.GCD();
			end,

			-- actions.single_target+=/flame_shock,target_if=refreshable
			Use = function(numEnemies, Target)
				return Target.Debuff(Aura.FlameShock).Refreshable();
			end,
		},

		FrostShock = {
			-- Frost Shock is our movement filler.
			-- actions.single_target+=/frost_shock,moving=1
			Moving = function()
				return Player.IsMoving();
			end,

			-- actions.single_target+=/frost_shock,if=buff.icefury.up
			Use = function()
				return Player.Buff(Aura.Icefury).Up();
			end,
		},

		-- The addon can't check both spell targets and active targets, however if spell targets is more than 1, active targets must be more than 1 as well.
		-- actions.single_target+=/lava_beam,if=active_enemies>1&spell_targets.lava_beam>1
		LavaBeam = function(numEnemies)
			return numEnemies > 1;
		end,

		LavaBurst = function()
			return (Spell.LavaBurst.Cooldown.React() or (Player.Buff(Aura.LavaSurge).Up() and Spell.LavaBurst.Cooldown.Up()))
			    or Player.Buff(Aura.Ascendance).Up();
		end,

		-- Use the debuff before casting Earth Shock again.
		-- actions.single_target+=/lightning_bolt,if=debuff.exposed_elements.up&maelstrom>=60&!buff.ascendance.up
		LightningBolt = function()
			return Target.Debuff(Aura.ExposedElements).Up()
			   and Player.Maelstrom() >= 60
			   and not Player.Buff(Aura.Ascendance).Up();
		end,

		-- actions.single_target+=/totem_mastery,if=buff.resonance_totem.remains<6|(buff.resonance_totem.remains<(buff.ascendance.duration+cooldown.ascendance.remains)&cooldown.ascendance.remains<15)
		TotemMastery = function()
			return Player.Buff(Aura.ResonanceTotem).Remains() < 6
			    or (Player.Buff(Aura.ResonanceTotem).Remains() < (Player.Buff(Aura.Ascendance).Duration() + Talent.Ascendance.Cooldown.Remains()) and Talent.Ascendance.Cooldown.Remains() < 15);
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.FlameShock, self.Requirements.FlameShock.Refresh);
		action.EvaluateAction(Talent.Ascendance, self.Requirements.Ascendance.Use);
		action.EvaluateAction(Talent.Ascendance, self.Requirements.Ascendance.StormElemental);
		action.EvaluateAction(Talent.ElementalBlast, self.Requirements.ElementalBlast);
		-- Keep SK for large or soon add waves, we can't do raid events for adds, so just skip the requirements and suggest when available
		-- actions.single_target+=/stormkeeper,if=raid_event.adds.count<3|raid_event.adds.in>50
		action.EvaluateAction(Talent.Stormkeeper, true);
		-- We can't do raid events for adds, so just skip the requirements and suggest when available
		-- actions.single_target+=/liquid_magma_totem,if=raid_event.adds.count<3|raid_event.adds.in>50
		action.EvaluateAction(Talent.LiquidMagmaTotem, true);
		action.EvaluateAction(Spell.LightningBolt, self.Requirements.LightningBolt);
		action.EvaluateAction(Spell.EarthShock, self.Requirements.EarthShock);
		-- Storm, Earth and Lava guide says to use LavaBurst whenever you can, not just on proc.
		-- actions.single_target+=/lava_burst,if=cooldown_react|buff.ascendance.up
		action.EvaluateAction(Spell.LavaBurst, true);
		action.EvaluateCycleAction(Spell.FlameShock, self.Requirements.FlameShock.Use);
		action.EvaluateAction(Talent.TotemMastery, self.Requirements.TotemMastery);
		action.EvaluateAction(Spell.FrostShock, self.Requirements.FrostShock.Use);
		-- actions.single_target+=/icefury
		action.EvaluateAction(Talent.Icefury, true);
		action.EvaluateAction(Spell.LavaBeam, self.Requirements.LavaBeam, Enemies.GetEnemies(Spell.LavaBeam));
		action.EvaluateAction(Spell.ChainLightning, self.Requirements.ChainLightning, Enemies.GetEnemies(Spell.ChainLightning));
		-- actions.single_target+=/lightning_bolt
		action.EvaluateAction(Spell.LightningBolt, true);
		action.EvaluateCycleAction(Spell.FlameShock, self.Requirements.FlameShock.Moving);
		action.EvaluateAction(Spell.FrostShock, self.Requirements.FrostShock.Moving);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSingleTarget = SingleTarget("SingleTarget");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Gistwiki",
		GuideLink = "http://www.wowhead.com/elemental-shaman-guide",
		WoWVersion = 80001,
	};

	self.Defensives = {
		AstralShift = function()
			return Player.DamagePredicted(3) >= 20;
		end,

		HealingSurge = function()
			return Player.Health.Percent() < 60;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/berserking,if=!talent.ascendance.enabled|buff.ascendance.up
		Berserking = function()
			return not Talent.Ascendance.Enabled()
			    or Player.Buff(Aura.Ascendance).Up();
		end,

		-- actions+=/blood_fury,if=!talent.ascendance.enabled|buff.ascendance.up|cooldown.ascendance.remains>50
		BloodFury = function()
			return not Talent.Ascendance.Enabled()
			    or Player.Buff(Aura.Ascendance).Up()
			    or Talent.Ascendance.Cooldown.Remains() > 50;
		end,

		-- actions+=/earth_elemental,if=cooldown.fire_elemental.remains<120&!talent.storm_elemental.enabled|cooldown.storm_elemental.remains<120&talent.storm_elemental.enabled
		EarthElemental = function()
			return (Spell.FireElemental.Cooldown.Remains() < 120 and not Talent.StormElemental.Enabled())
			    or (Talent.StormElemental.Cooldown.Remains() < 120 and Talent.StormElemental.Enabled());
		end,

		-- In-combat potion is preferentially linked to your Elemental, unless combat will end shortly
		-- actions+=/potion
		ProlongedPower = function()
			return Summons.Active(Guardians.FireElemental)
			    or Summons.Active(Guardians.StormElemental)
			    or Target.TimeToDie() < 60;
		end,

		-- actions+=/totem_mastery,if=buff.resonance_totem.remains<2
		TotemMastery = function()
			return Player.Buff(Aura.ResonanceTotem).Remains() < 2;
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			ProlongedPower = Objects.newItem(142117),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.Berserking, Racial.BloodFury, Racial.BullRush, Racial.GiftOfTheNaaru, Racial.RocketBarrage, Racial.RocketJump, Racial.Stoneform);
		config.AOEOptions(options, Spell.ChainLightning, Spell.Earthquake, Talent.Stormkeeper);
		config.BuffOptions(options, Talent.TotemMastery);
		config.CooldownOptions(options, Talent.Ascendance, Talent.ElementalBlast, Talent.Icefury, Talent.LiquidMagmaTotem);
		config.DefensiveOptions(options, Spell.AstralShift, Spell.HealingSurge);
		config.UtilityOptions(options, Spell.EarthELemental, Spell.FireElemental, Talent.StormElemental);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.AstralShift, self.Defensives.AstralShift);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Spell.HealingSurge, self.Defensives.HealingSurge);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- SimCraft doesn't specify it, but use the same conditions as in combat to remove the suggestion once its used.
		-- actions.precombat+=/totem_mastery
		action.EvaluateAction(Talent.TotemMastery, self.Requirements.TotemMastery);
		-- actions.precombat+=/fire_elemental
		action.EvaluateAction(Spell.FireElemental, true);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.ProlongedPower, true);
		-- actions.precombat+=/elemental_blast
		action.EvaluateAction(Talent.ElementalBlast, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		action.EvaluateAction(Consumable.ProlongedPower, self.Requirements.ProlongedPower);
		action.EvaluateAction(Talent.TotemMastery, self.Requirements.TotemMastery);
		-- actions+=/fire_elemental
		action.EvaluateAction(Spell.FireElemental, true);
		-- actions+=/storm_elemental
		action.EvaluateAction(Talent.StormElemental, true);
		action.EvaluateAction(Spell.EarthElemental, self.Requirements.EarthElemental);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);

		action.RunActionList(rotationAOE);
		-- actions+=/run_action_list,name=single_target
		action.RunActionList(rotationSingleTarget);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Elemental Shaman", addonTable.Enum.SpecID.SHAMAN_ELEMENTAL);