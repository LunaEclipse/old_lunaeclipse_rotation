-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local format, ipairs, strsub, strsplit, tostring, type = format, ipairs, strsub, strsplit, tostring, type;

local function addSetting(object)
	local Objects = addon.Core.Objects;

	if Objects.IsSpell(object) then
		return object.IsKnown();
	elseif Objects.IsItem(object) then
		return object.Equipped() and object.IsUsable();
	elseif type(object) == "string" then
		return true;
	end

	return false;
end

local function convertVersionNumber(versionNumber)
	if versionNumber then
		local Core = addon.Core.General;
		local majorVersion, minorVersion, revisionNumber;

		majorVersion = Core.ToNumber(strsub(versionNumber, 1, 1));
		minorVersion = Core.ToNumber(strsub(versionNumber, 2, 3));
		revisionNumber = Core.ToNumber(strsub(versionNumber, 4, 5));

		if revisionNumber == 0 then
			return format("%s.%s", tostring(majorVersion), tostring(minorVersion));
		else
			return format("%s.%s.%s", tostring(majorVersion), tostring(minorVersion), tostring(revisionNumber));
		end
	else
		return nil;
	end
end

local function createRotationTooltip(object, tristate)
	local Objects = addon.Core.Objects;
	local cooldownIcon, objectName;

	if Objects.IsSpell(object) then
		objectName = object.Name();
		cooldownIcon = object.IsOffGCD() and "Off GCD" or "CD";
	elseif Objects.IsItem(object) then
		objectName = object.Name();
		cooldownIcon = "CD";
	else
		return "";
	end

	if objectName ~= "" then
		if tristate then
			return format("Display %s on the main rotation icons, this will delay the rotation until the ability is used.\n\nIf this is checked with a grey tick it will display on the %s icon instead!\n\nIf this is unchecked it will not be suggested at all!", objectName, cooldownIcon);
		else
			return format("Display %s on the %s icon.\n\nIf this is unchecked it will not be suggested at all!", objectName, cooldownIcon);
		end
	else
		return "";
	end
end

local function getActiveObject(object)
	local Objects = addon.Core.Objects;
	local Tables = addon.Core.Tables;

	if Tables.IsTable(object) and not Objects.IsObject(object) then
		for _, rotationObject in ipairs(object) do
			if Objects.IsSpell(rotationObject) and rotationObject.IsKnown() then
				return rotationObject;
			elseif Objects.IsItem(rotationObject) and rotationObject.Equipped() and rotationObject.IsUsable() then
				return rotationObject;
			end
		end
	end

	return object;
end

local function addCheckboxes(optionsTable, groupName, objectTable, tristate)
	local apiGeneral = addon.API.General;
	local coreObjects = addon.Core.Objects;
	local coreTables = addon.Core.Tables;
	local optionsCore = addon.Options.Core;

	local specInfo = apiGeneral.GetSpecializationInfo();
	local specName = coreTables.GetValue(specInfo, "Name");
	local defaultValue = optionsCore.ConvertToValue(true, tristate);

	if specName and not coreTables.IsEmpty(objectTable) then
		local settingName, settingText, settingTooltip;
		local optionCount = 0;

		addonTable.characterSettings.RotationSettings[specName] = addonTable.characterSettings.RotationSettings[specName] or {};

		for _, object in ipairs(objectTable) do
			object = getActiveObject(object);

			if coreObjects.IsObject(object) then
				settingName = object.CheckBox();
				settingText = object.Name();
				settingTooltip = createRotationTooltip(object, tristate);
			elseif type(object) == "string" then
				settingName, settingText, settingTooltip = strsplit("|", object);
			end

			if settingName and settingText and addSetting(object) then
				optionCount = optionCount + 1;

				optionsTable.args[groupName].args[settingName] = {
					type = "toggle",
					name = settingText,
					desc = settingTooltip,
					tristate = tristate,
					width = "full",
					order = optionCount * 10,
				};

				addonTable.characterSettings.RotationSettings[specName][settingName] = addonTable.characterSettings.RotationSettings[specName][settingName] or defaultValue;
			end
		end
	end
end

local rotationOptions = {
	type = "group",
	name = "Rotation",
	get = addon.Options.Settings.GetRotationValue,
	set = addon.Options.Settings.SetRotationValue,
	childGroups = "tree",
	args = {
		rotationInfo = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Rotation Information"),
			inline = true,
			order = 10,
			args = {},
		},
		settingsRacial = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Racial"),
			inline = true,
			order = 20,
			args = {},
		},
		settingsAOE = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("AOE"),
			inline = true,
			order = 30,
			args = {},
		},
		settingsBuff = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Buff"),
			inline = true,
			order = 40,
			args = {},
		},
		settingsCooldowns = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Cooldowns"),
			inline = true,
			order = 50,
			args = {},
		},
		settingsDefensive = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Defensive"),
			inline = true,
			order = 60,
			args = {},
		},
		settingsPet = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Pet"),
			inline = true,
			order = 70,
			args = {},
		},
		settingsUtility = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Utility"),
			inline = true,
			order = 80,
			args = {},
		},
		settingsSpecial = {
			type = "group",
			name = addon.Options.Core.CreaterHeader("Special"),
			inline = true,
			order = 90,
			args = {},
		},
	},
};

addon.Options.rotationOptions = {
	type = "group",
	name = "Rotation Settings",
	get = addon.Options.Settings.GetRotationValue,
	set = addon.Options.Settings.SetRotationValue,
	childGroups = "tree",
	args = {},
};

addon.Options.Rotation = {
	AOEOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsAOE", { ... }, true);
	end,

	BuffOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsBuff", { ... }, false);
	end,

	CooldownOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsCooldowns", { ... }, true);
	end,

	CreateRotationOptions = function()
		local coreTables = addon.Core.Tables;

		local returnValue = coreTables.Duplicate(rotationOptions);
		local rotationInfo = coreTables.GetValue(addon.currentRotation, "scriptInfo") or {};

		returnValue.args.rotationInfo.args = {
			guideAuthor = {
				type = "description",
				name = function()
					local guideAuthor = coreTables.GetValue(rotationInfo, "GuideAuthor");

					if guideAuthor then
						return format("%sGuide Author:%s %s", addonTable.COLORS.TAGS.HIGHLIGHT, addonTable.TAGS.END, guideAuthor);
					end

					return "";
				end,
				width = "full",
				order = 10,
			},
			guideLink = {
				type = "description",
				name = function()
					local apiUnit = addon.API.Unit;
					local guideLink = coreTables.GetValue(rotationInfo, "GuideLink");

					if guideLink then
						local unitClass = apiUnit.UnitClass("player");

						if not coreTables.IsEmpty(unitClass) then
							local URL = format("%s|Hurl:%s|h[%s]|h%s", addonTable.COLORS.CLASS[unitClass.ID], guideLink, guideLink, addonTable.TAGS.END)

							return format("%sGuide Link:%s %s", addonTable.COLORS.TAGS.HIGHLIGHT, addonTable.TAGS.END, URL);
						end
					end

					return "";
				end,
				width = "full",
				order = 20,
			},
			rotationAuthor = {
				type = "description",
				name = function()
					local rotationAuthor = coreTables.GetValue(rotationInfo, "ScriptAuthor");

					if rotationAuthor then
						return format("%sRotation Author:%s %s", addonTable.COLORS.TAGS.HIGHLIGHT, addonTable.TAGS.END, rotationAuthor);
					end

					return "";
				end,
				width = "full",
				order = 30,
			},
			rotationCredits = {
				type = "description",
				name = function()
					local rotationCredits = coreTables.GetValue(rotationInfo, "ScriptCredits");

					if rotationCredits then
						return format("%sRotation Credits:%s %s", addonTable.COLORS.TAGS.HIGHLIGHT, addonTable.TAGS.END, rotationCredits);
					end

					return "";
				end,
				width = "full",
				order = 40,
			},
			rotationVersion = {
				type = "description",
				name = function()
					local rotationVersion = coreTables.GetValue(rotationInfo, "WoWVersion");

					if rotationVersion then
						if rotationVersion < addon.CURRENT_WOW_VERSION then
							return format("%sWoW Version:%s %s%s (Outdated)%s", addonTable.COLORS.TAGS.HIGHLIGHT, addonTable.TAGS.END, addonTable.COLORS.TAGS.INACTIVE, convertVersionNumber(rotationVersion), addonTable.TAGS.END);
						else
							return format("%sWoW Version:%s %s%s (Current)%s", addonTable.COLORS.TAGS.HIGHLIGHT, addonTable.TAGS.END, addonTable.COLORS.TAGS.ACTIVE, convertVersionNumber(rotationVersion), addonTable.TAGS.END);
						end
					end

					return "";
				end,
				width = "full",
				order = 50,
			},
			rotationNotes = {
				type = "description",
				name = function()
					local rotationNotes = coreTables.GetValue(rotationInfo, "ImportantNotes");

					if rotationNotes then
						return format("\n%sImportant Notes:%s %s%s%s", addonTable.COLORS.TAGS.HIGHLIGHT, addonTable.TAGS.END, addonTable.COLORS.TAGS.DEFAULT, rotationNotes, addonTable.TAGS.END);
					end

					return "";
				end,
				width = "full",
				order = 60,
			},
		};

		return returnValue;
	end,

	DefensiveOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsDefensive", { ... }, false);
	end,

	PetOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsPet", { ... }, false);
	end,

	RacialOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsRacial", { ... }, false);
	end,

	SpecialOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsSpecial", { ... }, false);
	end,

	UtilityOptions = function(optionsTable, ...)
		addCheckboxes(optionsTable, "settingsUtility", { ... }, false);
	end,
};
