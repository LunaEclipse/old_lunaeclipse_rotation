local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

--localization file for german/Germany
local L = LibStub("AceLocale-3.0"):NewLocale(addonName, "deDE");

if L then
	-- Tooltip Scanning
	L["Dummy"] = "Trainingsattrappe";
	L["For"] = "f�r";
	L["Generates"] = "erzeugt";
	L["Lasts"] = "halten";
	L["Next"] = "n�chster";
	L["Over"] = "im Verlauf von";
	L["Potion"] = "Trank";
end