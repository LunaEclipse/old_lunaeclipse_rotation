local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Enum.VersatilityType = {
	DAMAGE_BONUS = 0,
	DAMAGE_REDUCTION = 1,
};