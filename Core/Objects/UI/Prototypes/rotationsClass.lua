-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local error, format, pairs, rawset, setmetatable, type = error, format, pairs, rawset, setmetatable, type;

local privateVariables = {};

local Alias; -- These are unused features of OOP by this class.
local Cache = setmetatable({}, { __mode = "kv", __metatable = "This table is protected!", });
local Empty = {
	Rotation = true,
};
local Overridable = {
	Use = true,
};
local Prototype = {
	APL = function(self)
		return privateVariables[self.Key].APL;
	end,

	ClassType = function(self)
		return "SubRotation";
	end,

	Name = function(self)
		return privateVariables[self.Key].Name;
	end,

	-- Returns true by default, you only need to override this if there is special requirements for the rotation to be usable,
	-- such as number of enemies, or player buffs or chosen talents, etc...
	Use = function(self, numEnemies)
		return true;
	end,

	-- Function for processing conditional checks, should NEVER be overridden by the inherited rotations.
	ProcessCondition = function(self, useObject, numEnemies, evaluateTime, target)
		local Enemies = addon.Modules.Enemies;
		local Target = addon.Units.Target;

		numEnemies = numEnemies or Enemies.GetEnemies();
		target = target or Target;

		addonTable.EvaluateTime = evaluateTime;

		local returnValue = (self.Use(numEnemies) == nil or self.Use(numEnemies) == true)
						and (useObject == true or (type(useObject) == "function" and useObject(numEnemies, target)));

		addonTable.EvaluateTime = nil;

		return returnValue;
	end,

	-- Function for processing rotation, should NEVER be overridden by the inherited rotations.
	ProcessRotation = function(self, previousRotation)
		local Rotation = addon.Rotation;

		self.Rotation(addon.Rotation);
		Rotation.SetRotationObject(previousRotation);
	end,
};

local InstanceMT = {
	__index = function(table, key)
		local Tables = addon.Core.Tables;

		local classKey = Tables.GetValue(Alias, key, key);
		local prototypeEntry = Prototype[classKey];

		if prototypeEntry then
			if type(prototypeEntry) == "function" then
				return function(...)
					return prototypeEntry(table, ...);
				end
			else
				return prototypeEntry;
			end
		elseif Tables.GetValue(Empty, classKey, false) then
			return function()
			end
		end

		return nil;
	end,

	__newindex = function(table, key, value)
		local Tables = addon.Core.Tables;

		local classKey = Tables.GetValue(Alias, key, key);
		local prototypeEntry = Prototype[classKey];

		if Tables.GetValue(Overridable, key, false) or not prototypeEntry then
			rawset(table, key, value);
		else
			local nameAPL = Tables.GetValue(table, "APL", "");
			local nameRotation = Tables.GetValue(table, "Name", "");

			error(format("Attempted to overwrite protected function %s in %s sub-rotation of %s rotation!", key, nameRotation, nameAPL));
		end
	end,

	__metatable = "This table is protected!",
};
local ClassMT = {
	__call = function(table, nameAPL, nameRotation)
		nameAPL = nameAPL or "";
		nameRotation = nameRotation or ""

		local key = format("%s-%s", nameAPL, nameRotation);

		if Cache[key] then
			return Cache[key];
		else
			local self = setmetatable({ Key = key }, InstanceMT);

			privateVariables[key] = {
				APL = nameAPL,
				Name = nameRotation,
			};

			Cache[key] = self;
			return self;
		end
	end,

	__metatable = "This table is protected!",
};

addonTable.rotationsClass = setmetatable({}, ClassMT);