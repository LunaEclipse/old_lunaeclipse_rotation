## Title: LunaEclipse: Rotation Helper
## Notes: Rotation Helper Addon.
##
## Author: Adrian Hamilton (LunaEclipse)
## Version: 8.0.1.6
##
## Interface: 80000
## OptionalDeps: LunaEclipse_Debug, WeakAuras
##
## SavedVariables: LERH_AccountSettings
## SavedVariablesPerCharacter: LERH_CharacterSettings

## Load the embedded libraries.
Libraries\load_libraries.xml
Localization\load_localization.xml

## Load the main file to create the addon.
LunaEclipse_Rotation.lua

## Load the core addon files.
Core\load_core.xml

## Load the rotation data files.
Data\load_data.xml

## Load the rotation files.
Rotations\Rotations\load_rotations.xml