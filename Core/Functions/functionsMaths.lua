-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
-- local math = math;

-- This is stored on the addon so its publically available to all other addons.
addon.Core.Maths = {
	--[[---------------------------------------------------------
		Name: clamp(value, low, high)
		Desc: Clamp value between 2 defined values.
	------------------------------------------------------------]]
	clamp = function(value, low, high)
		if value and low and high then
			if (value < low) then
				return low;
			end

			if (value > high) then
				return high;
			end

			return value;
		end

		return nil;
	end,

	--[[---------------------------------------------------------
		Name: mod(a, b)
		Desc: Performs modulo operation on two numbers giving
			  the remainder.
	-----------------------------------------------------------]]
	mod = function(a, b)
		local floor = floor;

		if a and b then
			return a - floor(a / b) * b;
		end

		return nil;
	end,

	--[[---------------------------------------------------------
		Name: round(number, numDecimalPlaces)
		Desc: Rounds a number to specified decimal places
			  Default is 2 decimal places.
	-----------------------------------------------------------]]
	round = function(number, numDecimalPlaces)
		if number then
			local floor = floor;
			local multiplier = 10 ^ (numDecimalPlaces or 2);

			return floor(number * multiplier + 0.5) / multiplier;
		end

		return nil;
	end,

	--[[---------------------------------------------------------
		Name: truncate(number, numDecimalPlaces)
		Desc: Rounds a number towards 0 with the
			  specified decimal places.
			  Default is 0 decimal places.
	-----------------------------------------------------------]]
	truncate = function(number, numDecimalPlaces)
		if number then
			local ceil, floor = ceil, floor;
			local multiplier = 10 ^ (numDecimalPlaces or 0);
			local roundFunction = number < 0 and ceil or floor;

			return roundFunction(number * multiplier) / multiplier;
		end

		return nil;
	end,
};