local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local pairs = pairs;

--- Localize Vars
local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local Summons = addonTable.Summons;
local val = coreGeneral.ToNumber;

-- Objects
local Pet = addon.Units.Pet;
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Creature IDs for Undead Tracker
local Guardians = {
	Infernal = 89,
};

local function remainingHavoc()
	for _, currentUnit in pairs(addon.NameplateUnits) do
		if currentUnit.GUID() ~= Target.GUID() and currentUnit.Debuff(Aura.Havoc).Up() then
			return currentUnit.Debuff(Aura.Havoc).Remains();
		end
	end

	return 0;
end

-- Rotation Variables
local nameAPL = "lunaeclipse_warlock_destruction";

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cds+=/dark_soul_instability,if=target.time_to_die>=140|pet.infernal.active|target.time_to_die<=20+gcd
		DarkSoulInstability = function()
			return Target.TimeToDie() >= 140
			    or Summons.Active(Guardians.Infernal)
			    or Target.TimeToDie() <= 20 + Player.GCD();
		end,

		-- actions.cds+=/potion,if=pet.infernal.active|target.time_to_die<65
		ProlongedPower = function()
			return Summons.Active(Guardians.Infernal)
			    or Target.TimeToDie() < 65;
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.cds=summon_infernal,if=target.time_to_die>=210|!cooldown.dark_soul_instability.remains|target.time_to_die<=30+gcd|!talent.dark_soul_instability.enabled
		SummonInfernal = function()
			return Target.TimeToDie() >= 210
			    or not Talent.DarkSoulInstability.Cooldown.Up()
			    or Target.TimeToDie() <= 30 + Player.GCD()
			    or not Talent.DarkSoulInstability.Enabled();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.SummonInfernal, self.Requirements.SummonInfernal);
		action.EvaluateAction(Talent.DarkSoulInstability, self.Requirements.DarkSoulInstability);
		action.EvaluateAction(Consumable.ProlongedPower, self.Requirements.ProlongedPower);
		-- actions.cds+=/berserking
		action.EvaluateAction(Racial.Berserking, true);
		-- actions.cds+=/blood_fury
		action.EvaluateAction(Racial.BloodFury, true);
		-- actions.cds+=/fireblood
		action.EvaluateAction(Racial.Fireblood, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Cataclysm Rotation
local function Cataclysm(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		ChaosBolt = {
			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.cata+=/chaos_bolt,cycle_targets=1,if=!debuff.havoc.remains&talent.grimoire_of_supremacy.enabled&pet.infernal.remains>execute_time&active_enemies<=8&((108*spell_targets.rain_of_fire%3)<(240*(1+0.08*buff.grimoire_of_supremacy.stack)%2*(1+buff.active_havoc.remains>execute_time)))
			GrimoireOfSupremacy = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up()
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.LastDespawn(Guardians.Infernal) > Spell.ChaosBolt.ExecuteTime()
				   and numEnemies <= 8
				   and ((108 * numEnemies / 3) < (240 * (1 + 0.08 * Player.Buff(Aura.GrimoireOfSupremacy).Stack()) / 2 * (1 + val(remainingHavoc() > Spell.ChaosBolt.ExecuteTime()))));
			end,

			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.cata+=/chaos_bolt,cycle_targets=1,if=!debuff.havoc.remains&buff.active_havoc.remains>execute_time&spell_targets.rain_of_fire<=4
			Use = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up()
				   and remainingHavoc() > Spell.ChaosBolt.ExecuteTime()
				   and numEnemies <= 4
			end,
		},

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.cata+=/conflagrate,cycle_targets=1,if=!debuff.havoc.remains
		Conflagrate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,

		Havoc = {
			-- actions.cata+=/havoc,cycle_targets=1,if=!(target=sim.target)&target.time_to_die>10&spell_targets.rain_of_fire<=8&talent.grimoire_of_supremacy.enabled&pet.infernal.active&pet.infernal.remains<=10
			AOECycle = function(numEnemies, Target)
				return Target.TimeToDie() > 10
				   and numEnemies <= 8
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.Active(Guardians.Infernal)
				   and Summons.LastDespawn(Guardians.Infernal) <= 10;
			end,

			-- actions.cata+=/havoc,cycle_targets=1,if=!(target=sim.target)&target.time_to_die>10&spell_targets.rain_of_fire<=4
			CleaveCycle = function(numEnemies, Target)
				return Target.TimeToDie() > 10
				   and numEnemies <= 4;
			end,

			-- actions.cata+=/havoc,if=spell_targets.rain_of_fire<=8&talent.grimoire_of_supremacy.enabled&pet.infernal.active&pet.infernal.remains<=10
			GrimoireOfSupremacy = function(numEnemies)
				return numEnemies <= 8
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.Active(Guardians.Infernal)
				   and Summons.LastDespawn(Guardians.Infernal) <= 10;
			end,

			-- actions.cata+=/havoc,if=spell_targets.rain_of_fire<=4
			Use = function(numEnemies)
				return numEnemies <= 4;
			end,
		},

		Immolate = {
			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.cata+=/immolate,if=talent.channel_demonfire.enabled&!remains&cooldown.channel_demonfire.remains<=action.chaos_bolt.execute_time
			ChannelDemonfire = function()
				return Talent.ChannelDemonfire.Enabled()
				   and not Target.Debuff(Aura.Immolate).Up()
				   and Talent.ChannelDemonfire.Cooldown.Remains() <= Spell.ChaosBolt.ExecuteTime();
			end,

			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.cata+=/immolate,cycle_targets=1,if=!debuff.havoc.remains&refreshable&remains<=cooldown.cataclysm.remains
			Use = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up()
				   and Target.Debuff(Aura.Immolate).Refreshable()
				   and Target.Debuff(Aura.Immolate).Remains() <= Talent.Cataclysm.Cooldown.Remains();
			end,
		},

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.cata+=/incinerate,cycle_targets=1,if=!debuff.havoc.remains
		Incinerate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,

		-- actions.cata+=/rain_of_fire,if=soul_shard>=4.5
		RainOfFire = function()
			return Player.SoulShards() >= 4.5;
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.cata+=/shadowburn,cycle_targets=1,if=!debuff.havoc.remains&((charges=2|!buff.backdraft.remains|buff.backdraft.remains>buff.backdraft.stack*action.incinerate.execute_time))
		Shadowburn = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and ((Talent.Shadowburn.Charges() == 2 or not Player.Buff(Aura.Backdraft).Up() or Player.Buff(Aura.Backdraft).Remains() > Player.Buff(Aura.Backdraft).Stack() * Spell.Incinerate.ExecuteTime()));
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.cata+=/soul_fire,cycle_targets=1,if=!debuff.havoc.remains
		SoulFire = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.cata=call_action_list,name=cds
		action.CallActionList(rotationCooldowns);

		action.EvaluateAction(Spell.RainOfFire, self.Requirements.RainOfFire);
		-- actions.cata+=/cataclysm
		action.EvaluateAction(Talent.Cataclysm, true);
		action.EvaluateAction(Spell.Immolate, self.Requirements.Immolate.ChannelDemonfire);
		-- actions.cata+=/channel_demonfire
		action.EvaluateAction(Talent.ChannelDemonfire, true);
		action.EvaluateCycleAction(Spell.Havoc, self.Requirements.Havoc.AOECycle);
		action.EvaluateAction(Spell.Havoc, self.Requirements.Havoc.GrimoireOfSupremacy);
		action.EvaluateCycleAction(Spell.ChaosBolt, self.Requirements.ChaosBolt.GrimoireOfSupremacy);
		action.EvaluateCycleAction(Spell.Havoc, self.Requirements.Havoc.CleaveCycle);
		action.EvaluateAction(Spell.Havoc, self.Requirements.Havoc.Use);
		action.EvaluateCycleAction(Spell.ChaosBolt, self.Requirements.ChaosBolt.Use);
		action.EvaluateCycleAction(Spell.Immolate, self.Requirements.Immolate.Use);
		-- actions.cata+=/rain_of_fire
		action.EvaluateAction(Spell.RainOfFire, true);
		action.EvaluateCycleAction(Talent.SoulFire, self.Requirements.SoulFire);
		action.EvaluateCycleAction(Spell.Conflagrate, self.Requirements.Conflagrate);
		action.EvaluateCycleAction(Talent.Shadowburn, self.Requirements.Shadowburn);
		action.EvaluateCycleAction(Spell.Incinerate, self.Requirements.Incinerate);
	end

	-- actions=run_action_list,name=cata,if=spell_targets.infernal_awakening>=3&talent.cataclysm.enabled
	function self.Use(numEnemies)
		return numEnemies >= 3
		   and Talent.Cataclysm.Enabled();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCataclysm = Cataclysm("Cataclysm");

-- FireAndBrimstone Rotation
local function FireAndBrimstone(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.fnb+=/chaos_bolt,cycle_targets=1,if=!debuff.havoc.remains&talent.grimoire_of_supremacy.enabled&pet.infernal.remains>execute_time&active_enemies<=4&((108*spell_targets.rain_of_fire%3)<(240*(1+0.08*buff.grimoire_of_supremacy.stack)%2*(1+buff.active_havoc.remains>execute_time)))
		ChaosBolt = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and Talent.GrimoireOfSupremacy.Enabled()
			   and Summons.LastDespawn(Guardians.Infernal) > Spell.ChaosBolt.ExecuteTime()
			   and numEnemies <= 4
			   and ((108 * numEnemies / 3) < (240 * (1 + 0.08 * Player.Buff(Aura.GrimoireOfSupremacy).Stack()) / 2 * (1 + val(remainingHavoc() > Spell.ChaosBolt.ExecuteTime()))));
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.fnb+=/conflagrate,cycle_targets=1,if=!debuff.havoc.remains&(talent.flashover.enabled&buff.backdraft.stack<=2|spell_targets.incinerate<=7|talent.roaring_blaze.enabled&spell_targets.incinerate<=9)
		Conflagrate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and (Talent.Flashover.Enabled() and Player.Buff(Aura.Backdraft).Stack() <= 2 or numEnemies <=7 or Talent.RoaringBlaze.Enabled() and numEnemies <= 9);
		end,

		Havoc = {
			-- actions.fnb+=/havoc,cycle_targets=1,if=!(target=sim.target)&target.time_to_die>10&spell_targets.rain_of_fire<=4&talent.grimoire_of_supremacy.enabled&pet.infernal.active&pet.infernal.remains<=10
			CleaveCycle = function(numEnemies, Target)
				return Target.TimeToDie() > 10
				   and numEnemies <= 4
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.Active(Guardians.Infernal)
				   and Summons.LastDespawn(Guardians.Infernal) <= 10;
			end,

			-- actions.fnb+=/havoc,if=spell_targets.rain_of_fire<=4&talent.grimoire_of_supremacy.enabled&pet.infernal.active&pet.infernal.remains<=10
			Use = function(numEnemies)
				return numEnemies <= 4
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.Active(Guardians.Infernal)
				   and Summons.LastDespawn(Guardians.Infernal) <= 10;
			end,
		},

		Immolate = {
			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.fnb+=/immolate,if=talent.channel_demonfire.enabled&!remains&cooldown.channel_demonfire.remains<=action.chaos_bolt.execute_time
			ChannelDemonfire = function()
				return Talent.ChannelDemonfire.Enabled()
				   and not Target.Debuff(Aura.Immolate).Up()
				   and Talent.ChannelDemonfire.Cooldown.Remains() <= Spell.ChaosBolt.ExecuteTime();
			end,

			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.fnb+=/immolate,cycle_targets=1,if=!debuff.havoc.remains&refreshable&spell_targets.incinerate<=8
			Use = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up()
				   and numEnemies <= 8;
			end,
		},

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.fnb+=/incinerate,cycle_targets=1,if=!debuff.havoc.remains
		Incinerate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,

		-- actions.fnb+=/rain_of_fire,if=soul_shard>=4.5
		RainOfFire = function()
			return Player.SoulShards() >= 4.5;
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.fnb+=/soul_fire,cycle_targets=1,if=!debuff.havoc.remains&spell_targets.incinerate=3
		SoulFire = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and numEnemies == 3;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.fnb=call_action_list,name=cds
		action.CallActionList(rotationCooldowns);

		action.EvaluateAction(Spell.RainOfFire, self.Requirements.RainOfFire);
		action.EvaluateAction(Spell.Immolate, self.Requirements.Immolate.ChannelDemonfire);
		-- actions.fnb+=/channel_demonfire
		action.EvaluateAction(Talent.ChannelDemonfire, true);
		action.EvaluateCycleAction(Spell.Havoc, self.Requirements.Havoc.CleaveCycle);
		action.EvaluateAction(Spell.Havoc, self.Requirements.Havoc.Use);
		action.EvaluateCycleAction(Spell.ChaosBolt, self.Requirements.ChaosBolt);
		action.EvaluateCycleAction(Spell.Immolate, self.Requirements.Immolate.Use, Enemies.GetEnemies(Spell.Incinerate));
		-- actions.fnb+=/rain_of_fire
		action.EvaluateAction(Spell.RainOfFire, true);
		action.EvaluateCycleAction(Talent.SoulFire, self.Requirements.SoulFire, Enemies.GetEnemies(Spell.Incinerate));
		action.EvaluateCycleAction(Spell.Conflagrate, self.Requirements.Conflagrate, Enemies.GetEnemies(Spell.Incinerate));
		action.EvaluateCycleAction(Spell.Incinerate, self.Requirements.Incinerate);
	end

	-- actions+=/run_action_list,name=fnb,if=spell_targets.infernal_awakening>=3&talent.fire_and_brimstone.enabled
	function self.Use(numEnemies)
		return numEnemies >= 3
		   and Talent.FireAndBrimstone.Enabled();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationFireAndBrimstone = FireAndBrimstone("FireAndBrimstone");

-- Infernal Rotation
local function Infernal(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		ChaosBolt = {
			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.inf+=/chaos_bolt,cycle_targets=1,if=!debuff.havoc.remains&talent.grimoire_of_supremacy.enabled&pet.infernal.remains>execute_time&spell_targets.rain_of_fire<=4+talent.internal_combustion.enabled&((108*spell_targets.rain_of_fire%(3-0.16*spell_targets.rain_of_fire))<(240*(1+0.08*buff.grimoire_of_supremacy.stack)%2*(1+buff.active_havoc.remains>execute_time)))
			GrimoireOfSupremacy = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up()
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.LastDespawn(Guardians.Infernal) > Spell.ChaosBolt.ExecuteTime()
				   and numEnemies <= 4 + val(Talent.InternalCombustion.Enabled())
				   and ((108 * numEnemies / (3 - 0.16 * numEnemies)) < (240 * (1 + 0.08 * Player.Buff(Aura.GrimoireOfSupremacy).Stack()) / 2 * (1 + val(remainingHavoc() > Spell.ChaosBolt.ExecuteTime()))));
			end,

			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.inf+=/chaos_bolt,cycle_targets=1,if=!debuff.havoc.remains&buff.active_havoc.remains>execute_time&spell_targets.rain_of_fire<=3&(talent.eradication.enabled|talent.internal_combustion.enabled)
			Use = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up()
				   and remainingHavoc() > Spell.ChaosBolt.ExecuteTime()
				   and numEnemies <= 3
				   and (Talent.Eradication.Enabled() or Talent.InternalCombustion.Enabled());
			end,
		},

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.inf+=/conflagrate,cycle_targets=1,if=!debuff.havoc.remains
		Conflagrate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,

		Havoc = {
			-- actions.inf+=/havoc,cycle_targets=1,if=!(target=sim.target)&target.time_to_die>10&spell_targets.rain_of_fire<=4+talent.internal_combustion.enabled&talent.grimoire_of_supremacy.enabled&pet.infernal.active&pet.infernal.remains<=10
			AOECycle = function(numEnemies, Target)
				return Target.TimeToDie() > 10
				   and numEnemies <= 4 + val(Talent.InternalCombustion.Enabled())
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.Active(Guardians.Infernal)
				   and Summons.LastDespawn(Guardians.Infernal) <= 10;
			end,

			-- actions.inf+=/havoc,cycle_targets=1,if=!(target=sim.target)&target.time_to_die>10&spell_targets.rain_of_fire<=3&(talent.eradication.enabled|talent.internal_combustion.enabled)
			CleaveCycle = function(numEnemies, Target)
				return Target.TimeToDie() > 10
				   and numEnemies <= 3
				   and (Talent.Eradication.Enabled() or Talent.InternalCombustion.Enabled());
			end,

			-- actions.inf+=/havoc,if=spell_targets.rain_of_fire<=4+talent.internal_combustion.enabled&talent.grimoire_of_supremacy.enabled&pet.infernal.active&pet.infernal.remains<=10
			GrimoireOfSupremacy = function(numEnemies)
				return numEnemies <= 4
				   and Talent.InternalCombustion.Enabled()
				   and Talent.GrimoireOfSupremacy.Enabled()
				   and Summons.Active(Guardians.Infernal)
				   and Summons.LastDespawn(Guardians.Infernal) <= 10;
			end,

			-- actions.inf+=/havoc,if=spell_targets.rain_of_fire<=3&(talent.eradication.enabled|talent.internal_combustion.enabled)
			Use = function(numEnemies)
				return numEnemies <= 3
				   and (Talent.Eradication.Enabled() or Talent.InternalCombustion.Enabled());
			end,
		},

		Immolate = {
			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.inf+=/immolate,if=talent.channel_demonfire.enabled&!remains&cooldown.channel_demonfire.remains<=action.chaos_bolt.execute_time
			ChannelDemonfire = function()
				return Talent.ChannelDemonfire.Enabled()
				   and not Target.Debuff(Aura.Immolate).Up()
				   and Talent.ChannelDemonfire.Cooldown.Remains() <= Spell.ChaosBolt.ExecuteTime();
			end,

			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions.inf+=/immolate,cycle_targets=1,if=!debuff.havoc.remains&refreshable
			Use = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up()
				   and Target.Debuff(Aura.Immolate).Refreshable();
			end,
		},

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.inf+=/incinerate,cycle_targets=1,if=!debuff.havoc.remains
		Incinerate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,

		-- actions.inf+=/rain_of_fire,if=soul_shard>=4.5
		RainOfFire = function()
			return Player.SoulShards() >= 4.5;
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.inf+=/shadowburn,cycle_targets=1,if=!debuff.havoc.remains&((charges=2|!buff.backdraft.remains|buff.backdraft.remains>buff.backdraft.stack*action.incinerate.execute_time))
		Shadowburn = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and ((Talent.Shadowburn.Charges() == 2 or not Player.Buff(Aura.Backdraft).Up() or Player.Buff(Aura.Backdraft).Remains() > Player.Buff(Aura.Backdraft).Stack() * Spell.Incinerate.ExecuteTime()));
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions.inf+=/soul_fire,cycle_targets=1,if=!debuff.havoc.remains
		SoulFire = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.inf=call_action_list,name=cds
		action.CallActionList(rotationCooldowns);

		action.EvaluateAction(Spell.RainOfFire, self.Requirements.RainOfFire);
		-- actions.inf+=/cataclysm
		action.EvaluateAction(Talent.Cataclysm, true);
		action.EvaluateAction(Spell.Immolate, self.Requirements.Immolate.ChannelDemonfire);
		-- actions.inf+=/channel_demonfire
		action.EvaluateAction(Talent.ChannelDemonfire, true);
		action.EvaluateCycleAction(Spell.Havoc, self.Requirements.Havoc.AOECycle);
		action.EvaluateAction(Spell.Havoc, self.Requirements.Havoc.GrimoireOfSupremacy);
		action.EvaluateCycleAction(Spell.ChaosBolt, self.Requirements.ChaosBolt.GrimoireOfSupremacy);
		action.EvaluateCycleAction(Spell.Havoc, self.Requirements.Havoc.CleaveCycle);
		action.EvaluateAction(Spell.Havoc, self.Requirements.Havoc.Use);
		action.EvaluateCycleAction(Spell.ChaosBolt, self.Requirements.ChaosBolt.Use);
		action.EvaluateCycleAction(Spell.Immolate, self.Requirements.Immolate.Use);
		-- actions.inf+=/rain_of_fire
		action.EvaluateAction(Spell.RainOfFire, true);
		action.EvaluateCycleAction(Talent.SoulFire, self.Requirements.SoulFire);
		action.EvaluateCycleAction(Spell.Conflagrate, self.Requirements.Conflagrate);
		action.EvaluateCycleAction(Talent.Shadowburn, self.Requirements.Shadowburn);
		action.EvaluateCycleAction(Spell.Incinerate, self.Requirements.Incinerate);
	end

	-- actions+=/run_action_list,name=inf,if=spell_targets.infernal_awakening>=3&talent.inferno.enabled
	function self.Use(numEnemies)
		return numEnemies >= 3
		   and Talent.Inferno.Enabled();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationInfernal = Infernal("Infernal");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Loozy and SimCraft",
		GuideLink = "https://www.wowhead.com/destruction-warlock-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		DarkPact = function()
			return Player.DamagePredicted(5) >= 40;
		end,

		UnendingResolve = function()
			return Player.DamagePredicted(4) >= 20;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions+=/chaos_bolt,cycle_targets=1,if=!debuff.havoc.remains&execute_time+travel_time<target.time_to_die&(talent.internal_combustion.enabled|!talent.internal_combustion.enabled&soul_shard>=4|(talent.eradication.enabled&debuff.eradication.remains<=cast_time)|buff.dark_soul_instability.remains>cast_time|pet.infernal.active&talent.grimoire_of_supremacy.enabled)
		ChaosBolt = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and Spell.ChaosBolt.ExecuteTime() + Spell.ChaosBolt.TravelTime() < Target.TimeToDie()
			   and (Talent.InternalCombustion.Enabled() or not Talent.InternalCombustion.Enabled() and Player.SoulShards() >= 4 or (Talent.Eradication.Enabled() and Target.Debuff(Aura.Eradication).Remains() <= Spell.ChaosBolt.CastTime()) or Player.Buff(Aura.DarkSoulInstability).Remains() > Spell.ChaosBolt.CastTime() or Summons.Active(Guardians.Infernal) and Talent.GrimoireOfSupremacy.Enabled());
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions+=/conflagrate,cycle_targets=1,if=!debuff.havoc.remains&((talent.flashover.enabled&buff.backdraft.stack<=2)|(!talent.flashover.enabled&buff.backdraft.stack<2))
		Conflagrate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and ((Talent.Flashover.Enabled() and Player.Buff(Aura.Backdraft).Stack() <= 2) or (not Talent.Flashover.Enabled() and Player.Buff(Aura.Backdraft).Stack() < 2));
		end,

		-- SimCraft doesn't specify this, but only suggest if you have a pet active.
		-- actions.precombat+=/grimoire_of_sacrifice,if=talent.grimoire_of_sacrifice.enabled
		GrimoireOfSacrifice = function()
			return Talent.GrimoireOfSacrifice.Enabled()
			   and Pet.IsAction();
		end,

		-- Merge these two lines into one condition check, if more than one enemy and the target has more than 10 seconds to die.
		-- actions+=/havoc,cycle_targets=1,if=!(target=sim.target)&target.time_to_die>10
		-- actions+=/havoc,if=active_enemies>1
		Havoc = function(numEnemies, Target)
			return numEnemies > 1
			   and Target.TimeToDie() > 10;
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions+=/immolate,cycle_targets=1,if=!debuff.havoc.remains&(refreshable|talent.internal_combustion.enabled&action.chaos_bolt.in_flight&remains-action.chaos_bolt.travel_time-5<duration*0.3)
		Immolate = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and (Target.Debuff(Aura.Immolate).Refreshable() or Talent.InternalCombustion.Enabled() and Spell.ChaosBolt.IsInFlight() and Target.Debuff(Aura.Immolate).Remains() - Spell.ChaosBolt.TravelTime() - 5 < Spell.Immolate.BaseDuration() * 0.3);
		end,

		Incinerate = {
			-- actions.precombat+=/incinerate,if=!talent.soul_fire.enabled
			Precombat = function()
				return not Talent.SoulFire.Enabled();
			end,

			-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
			-- actions+=/incinerate,cycle_targets=1,if=!debuff.havoc.remains
			Use = function(numEnemies, Target)
				return not Target.Debuff(Aura.Havoc).Up();
			end,
		},

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions+=/shadowburn,cycle_targets=1,if=!debuff.havoc.remains&((charges=2|!buff.backdraft.remains|buff.backdraft.remains>buff.backdraft.stack*action.incinerate.execute_time))
		Shadowburn = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up()
			   and ((Talent.Shadowburn.Charges() == 2 or not Player.Buff(Aura.Backdraft).Up() or Player.Buff(Aura.Backdraft).Remains() > Player.Buff(Aura.Backdraft).Stack() * Spell.Incinerate.ExecuteTime()));
		end,

		-- LUA can't do a true/false comparison with a number like SimCraft so we need to use Up() which returns true if greater than 0 and false if 0.
		-- actions+=/soul_fire,cycle_targets=1,if=!debuff.havoc.remains
		SoulFire = function(numEnemies, Target)
			return not Target.Debuff(Aura.Havoc).Up();
		end,

		-- SimCraft does not specify, but use this only if you don't have a pet summoned or don't have the sacrifice buff if talented.
		-- actions.precombat+=/summon_pet
		SummonImp = function()
			return (Talent.GrimoireOfSacrifice.Enabled() and not Player.Buff(Aura.GrimoireOfSacrifice).Up())
			    or (not Talent.GrimoireOfSacrifice.Enabled() and not Pet.Family("Imp"));
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			ProlongedPower = Objects.newItem(142117),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood);
		config.AOEOptions(options, Talent.Cataclysm, Talent.ChannelDemonfire, Spell.RainOfFire);
		config.BuffOptions(options, Talent.GrimoireOfSacrifice);
		config.CooldownOptions(options, Talent.DarkSoulInstability, Spell.Havoc, Talent.Shadowburn, Talent.SoulFire, Spell.SummonInfernal);
		config.DefensiveOptions(options, Talent.DarkPact, Spell.UnendingResolve);
		config.PetOptions(options, Spell.SummonImp);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Talent.DarkPact, self.Defensives.DarkPact);
		action.EvaluateDefensiveAction(Spell.UnendingResolve, self.Defensives.UnendingResolve);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.SummonImp, self.Requirements.SummonImp);
		action.EvaluateAction(Talent.GrimoireOfSacrifice, self.Requirements.GrimoireOfSacrifice);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.ProlongedPower, true);
		-- actions.precombat+=/soul_fire
		action.EvaluateAction(Talent.SoulFire, true);
		action.EvaluateAction(Spell.Incinerate, self.Requirements.Incinerate.Precombat);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		action.RunActionList(rotationCataclysm);
		action.RunActionList(rotationFireAndBrimstone);
		action.RunActionList(rotationInfernal);

		action.EvaluateCycleAction(Spell.Immolate, self.Requirements.Immolate);

		-- actions+=/call_action_list,name=cds
		action.CallActionList(rotationCooldowns);

		action.EvaluateCycleAction(Spell.Havoc, self.Requirements.Havoc);
		-- actions+=/channel_demonfire
		action.EvaluateAction(Talent.ChannelDemonfire, true);
		-- actions+=/cataclysm
		action.EvaluateAction(Talent.Cataclysm, true);
		action.EvaluateCycleAction(Talent.SoulFire, self.Requirements.SoulFire);
		action.EvaluateCycleAction(Spell.ChaosBolt, self.Requirements.ChaosBolt);
		action.EvaluateCycleAction(Spell.Conflagrate, self.Requirements.Conflagrate);
		action.EvaluateCycleAction(Talent.Shadowburn, self.Requirements.Shadowburn);
		action.EvaluateCycleAction(Spell.Incinerate, self.Requirements.Incinerate.Use);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Destruction Warlock", addonTable.Enum.SpecID.WARLOCK_DESTRUCTION);
