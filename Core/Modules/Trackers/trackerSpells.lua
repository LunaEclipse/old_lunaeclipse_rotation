-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard, systemEvents, systemTimers = addon.API.Blizzard, addon.System.Events, addon.System.Timers;
local format, ipairs, max, pairs, tinsert, type, unpack = format, ipairs, max, pairs, tinsert, type, unpack;

-- CooldownTracking Variables
local cooldowns = {};
local recentCasts = {};

-- PreviousSpell Variables
local LastRecord = 15;

-- Init all the records at 0, so it saves one check on PrevGCD method.
for counter = 1, LastRecord do
	for _, Table in pairs(addonTable.PreviousSpell) do
		tinsert(Table, 0);
	end
end

-- SpellCasting Variables
local CLEU_CHANNEL_EVENTS = {
	SPELL_PERIODIC_DAMAGE = true,
	SPELL_PERIODIC_HEAL = true,
	SPELL_PERIODIC_LEECH = true,
};

local channelTickData = {
	-- Abilities
	[198013] = 0.2, -- Eye Beam
	[209525] = 0.5, -- Soothing Mist
	[117952] = 1, -- Crackling Jade Lightning
	[755] = 1, -- Health Funnel
	[135029] = 1, -- Water Jet
	-- Artifact Traits
	[203415] = 0.5, -- Fury of the Eagle
	[205065] = 1, -- Void Torrent
	-- Honor Talents
	[204437] = 1, -- Lightning Lasso
	[212640] = 1, -- Mending Mandage
	-- Specialization
	[5143] = 0.5, -- Arcane Missiles
	[193440] = 1, -- Demonwrath
	[64843] = 2, -- Divine Hymm
	[234153] = 1, -- Drain Life
	[198590] = 1, -- Drain Soul
	[191837] = 0.167, -- Essence Font
	[113656] = 0.166, -- Fists of Fury
	[15407] = 0.75, -- Mind Flay
	[101546] = 0.5, -- Spinning Crane Kick
	[740] = 2, -- Tranquility
	-- Talents
	[120360] = 0.2, -- Barrage
	[206931] = 1, -- Blooddrinker
	[196447] = 0.2, -- Channel Demonfire
	[211053] = 0.25, -- Fel Barrage
	[212084] = 0.2, -- Fel Devastation
	[205021] = 1, -- Ray of Frost
};
addon.Core.Objects.FinalizeSpellIDTable(channelTickData);

-- Variables for the sub-sections functions
local Events, CooldownTracking, PreviousSpell, ReplacementSpell, SpellCasting;

local function getCooldown(spellID)
	local GCDStart, GCDDuration = Blizzard.GetSpellCooldown(addonTable.GLOBAL_COOLDOWN);
	local cooldownStart, cooldownDuration = Blizzard.GetSpellCooldown(spellID);

	-- Check to make sure the cooldown is not just the standard GCD Cooldown.
	if cooldownStart ~= 0 and cooldownStart ~= GCDStart and cooldownDuration ~= 0 and cooldownDuration ~= GCDDuration then
		return cooldownStart, cooldownDuration;
	end

	return nil;
end

CooldownTracking = {
	CastSuccess = function(...)
		local _, unitID, _, spellID = ...;

		if unitID == "player" then
			local baseCooldown = Blizzard.GetSpellBaseCooldown(spellID) or 0;

			-- Check the spell has a cooldown before queuing it.
			if baseCooldown > 0 then
				recentCasts[spellID] = true;
			end
		end
	end,

	TimerEvent = function()
		local Tables = addon.Core.Tables;
		local currentTime = Blizzard.GetTime();

		if not Tables.IsEmpty(cooldowns) then
			local cooldownStart;

			-- Check spells on cooldown
			for spellID, endTime in pairs(cooldowns) do
				-- Check if cooldown finished normally
				if currentTime >= endTime then
					cooldowns[spellID] = nil;
				else
					-- Check to see if the spell is still on cooldown.
					cooldownStart = getCooldown(spellID);

					-- Cooldown finished sooner than expected, add it to the react list
					if not cooldownStart then
						cooldowns[spellID] = nil;

						addonTable.ReactSpells[spellID] = currentTime + addon.reactTime;
					end
				end
			end
		end

		if not Tables.IsEmpty(addonTable.ReactSpells) then
			-- Check spells that finished early to determine if react time has passed
			for spellID, endTime in pairs(addonTable.ReactSpells) do
				-- Check if reaction time has passed
				if currentTime >= endTime then
					addonTable.ReactSpells[spellID] = nil;
				end
			end
		end
	end,

	UpdateCooldown = function()
		for spellID in pairs(recentCasts) do
			local cooldownStart, cooldownDuration = getCooldown(spellID);

			-- Check to make sure its on cooldown.
			if cooldownStart then
				cooldowns[spellID] = cooldownStart + cooldownDuration;
				recentCasts[spellID] = nil;
			end
		end
	end,
};

PreviousSpell = {
	CastSuccess = function(...)
		local coreObjects = addon.Core.Objects;
		local coreTables = addon.Core.Tables;

		local _, unitID, _, spellID = ...;

		if unitID == "player" or unitID == "pet" then
			local currentSpell = addonTable.getSpell(spellID);

			if coreObjects.IsSpell(currentSpell) then
				local offGCD = coreTables.GetValue(currentSpell, "IsOffGCD", false);
				local unitText = unitID == "pet" and "Pet" or "";

				local tableKey = format("%sAll", unitText);
				local tableSubKey = format("%s%s", unitText, offGCD and "OffGCD" or "GCD");

				tinsert(addonTable.PreviousSpell[tableKey], 1, spellID);
				tinsert(addonTable.PreviousSpell[tableSubKey], 1, spellID);

				PreviousSpell.TrimRecords();
			end
		end
	end,

	-- Clear Old Records
	TrimRecords = function()
		for _, Table in pairs(addonTable.PreviousSpell) do
			local tableCount = #Table;

			while tableCount > LastRecord do
				Table[tableCount] = nil;
				tableCount = tableCount - 1;
			end
		end
	end,
};

ReplacementSpell = {
	CastEnd = function(...)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local _, unitID, lineID, spellID = ...;
		local replacementSpellData = Tables.GetValue(addonTable.ReplacementSpellsData, spellID);

		if replacementSpellData then
			if not Tables.IsEmpty(replacementSpellData) then
				local originalID, replacementID;

				for _, spellData in ipairs(replacementSpellData) do
					originalID, replacementID = unpack(spellData);

					Cache.Persistant.ReplacedSpells[originalID] = nil;
					Cache.Persistant.ReplacedSpells[replacementID] = nil;
				end
			else
				Cache.Persistant.ReplacedSpells[spellID] = nil;
				Cache.Persistant.ReplacedSpells[replacementSpellData] = nil;
			end
		end
	end,

	CastStart = function(...)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local _, unitID, lineID, spellID = ...;
		local replacementSpellData = Tables.GetValue(addonTable.ReplacementSpellsData, spellID);

		if replacementSpellData then
			if not Tables.IsEmpty(replacementSpellData) then
				local originalID, replacementID;

				for _, spellData in ipairs(replacementSpellData) do
					originalID, replacementID = unpack(spellData);

					Cache.Persistant.ReplacedSpells[originalID] = false;
					Cache.Persistant.ReplacedSpells[replacementID] = true;
				end
			else
				Cache.Persistant.ReplacedSpells[spellID] = false;
				Cache.Persistant.ReplacedSpells[replacementSpellData] = true;
			end
		end
	end,
};

SpellCasting = {
	CastEnd = function(...)
		local Cache = addonTable.Cache;

		local _, unitID = ...;
		local GUID = Blizzard.UnitGUID(unitID);

		Cache.Persistant.CastingInfo[GUID] = nil;
	end,

	CastStart = function(...)
		local Cache = addonTable.Cache;

		local _, unitID, lineID, spellID = ...;
		local GUID = Blizzard.UnitGUID(unitID);

		if GUID then
			local castName, castText, castTexture, castStartTime, castEndTime, castIsTradeSkill, _, castNotInterruptible = Blizzard.UnitCastingInfo(unitID);

			if castName then
				Cache.Persistant.CastingInfo[GUID] = {
					ID = lineID,
					SpellID = spellID,
					Name = castName,
					Text = castText,
					Texture = castTexture,
					Start = castStartTime,
					End = castEndTime,
					IsTradeSkill = castIsTradeSkill,
					NotInterruptible = castNotInterruptible,
					CastType = "CAST",
				};
			end
		end
	end,

	CastSuccess = function(...)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local _, unitID, lineID, spellID = ...;
		local GUID = Blizzard.UnitGUID(unitID);

		local castingInfo = Cache.Persistant.CastingInfo[GUID];

		if Tables.GetValue(castingInfo, "ID", 0) == lineID and Tables.GetValue(castingInfo, "SpellID", 0) == spellID then
			Cache.Persistant.CastingInfo[GUID] = nil;
		end
	end,

	ChannelEnd = function(...)
		local Cache = addonTable.Cache;

		local _, unitID = ...;
		local GUID = Blizzard.UnitGUID(unitID);

		Cache.Persistant.CastingInfo[GUID] = nil;
	end,

	ChannelStart = function(...)
		local Tables = addon.Core.Tables;

		local _, unitID, lineID, spellID = ...;
		local GUID = Blizzard.UnitGUID(unitID);

		if GUID then
			local spellCastInfo = addon.API.Unit.UnitCastingInfo(unitID);
			local castName, castText, castTexture, castStartTime, castEndTime, castIsTradeSkill, castNotInterruptible = Blizzard.UnitChannelInfo(unitID);

			if castName then
				local newCastingInfo = {
					ID = lineID,
					SpellID = spellID,
					Name = castName,
					Text = castText,
					Texture = castTexture,
					Start = castStartTime,
					End = castEndTime,
					IsTradeSkill = castIsTradeSkill,
					NotInterruptible = castNotInterruptible,
					CastType = "CHANNEL",
				};

				if channelTickData[spellID] then
					local tickInfo;
					local hasteModifier = 1 + (addon.Core.Misc.GetHaste() / 100);
					local tickTime = channelTickData[spellID] / hasteModifier;

					if unitID == "player" and tickTime then
						local tickLast = Tables.GetValue(spellCastInfo, "LastTick", castStartTime);
						local tickTotal = addon.Core.Maths.round((castEndTime - tickLast) / tickTime, 0);

						tickInfo = {
							TrackTicks = true,
							TicksTotal = tickTotal,
							TicksCurrent = 0,
							TicksDuration = tickTime,
							TicksRemaining = tickTotal,
							LastTick = tickLast,
						};

						addonTable.Cache.Persistant.CastingInfo[GUID] = Tables.Merge(newCastingInfo, tickInfo);
					end
				end
			end
		end
	end,

	CombatLog = function(...)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local currentTime = Blizzard.GetTime();
		local _, _, combatEvent, _, sourceGUID, _, _, _, destGUID, _, _, _, spellID = ...;

		if sourceGUID == Blizzard.UnitGUID("player") and combatEvent == "SPELL_CAST_SUCCESS" then
			addonTable.LastAction = currentTime;
		end

		if sourceGUID == Blizzard.UnitGUID("player") or sourceGUID == Blizzard.UnitGUID("pet") then
			local castingInfo = Cache.Persistant.CastingInfo[sourceGUID];

			Cache.Persistant.ActionInfo[spellID] = Cache.Persistant.ActionInfo[spellID] or {};

			if combatEvent == "SPELL_CAST_SUCCESS" then
				-- Spell cast was successful, record any persistent multipliers which will be used for all aura queries.
				addonTable.Auras.SetPersistentMultiplier(destGUID, spellID);

				Cache.Persistant.ActionInfo[spellID].LastCastTime = currentTime;
				-- We don't know exactly when it will hit, so lets estimate it.
				Cache.Persistant.ActionInfo[spellID].LastHitTime = currentTime + SpellCasting.TravelTime(spellID);
			elseif combatEvent == "SPELL_DAMAGE" then
				-- The spells actually hit now, so we can store the actual time.
				Cache.Persistant.ActionInfo[spellID].LastHitTime = currentTime;
			elseif combatEvent == "SPELL_AURA_APPLIED" then
				-- Buff or Debuff applied so lets save the time in case we need it in the rotations.
				Cache.Persistant.ActionInfo[spellID].LastAppliedTime = currentTime;
			elseif CLEU_CHANNEL_EVENTS[combatEvent] and Tables.GetValue(castingInfo, "TrackTicks", false) then
				-- The spell cast has ticked so lets store tick information, ticks duration will now be an actual time and not an estimate.
				castingInfo.TicksCurrent = Tables.GetValue(castingInfo, "TicksCurrent", 0) + 1;
				castingInfo.TicksRemaining = max(Tables.GetValue(castingInfo, "TicksRemaining", castingInfo.TicksTotal) - 1, 0);
				castingInfo.TicksDuration = currentTime - Tables.GetValue(castingInfo, "LastTick", 0);
				castingInfo.LastTick = currentTime;

				Cache.Persistant.CastingInfo[sourceGUID] = castingInfo;
			end
		end
	end,

	TimerEvent = function()
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local currentTime = Blizzard.GetTime();

		-- Loop through all the units with cast info.
		for GUID, CastInfo in pairs(Cache.Persistant.CastingInfo) do
			-- Check to see if the cast has already finished and remove it.
			if currentTime > Tables.GetValue(CastInfo, "End", 0) then
				Cache.Persistant.CastingInfo[GUID] = nil;
			end
		end
	end,

	TravelTime = function(spellID)
		local Tables = addon.Core.Tables;
		local Speed = Tables.GetValue(addonTable.Enum.ProjectileSpeed, spellID, 0);

		return (Speed > 0 and addon.Units.Target.InRange.Max() / Speed)
			or 0;
	end,
};

Events = {
	CastFailed = function(...)
		ReplacementSpell.CastEnd(...);
		SpellCasting.CastEnd(...);
	end,

	CastInterrupted = function(...)
		ReplacementSpell.CastEnd(...);
		SpellCasting.CastEnd(...);
	end,

	CastStart = function(...)
		ReplacementSpell.CastStart(...);
		SpellCasting.CastStart(...);
	end,

	CastStop = function(...)
		ReplacementSpell.CastEnd(...);
		SpellCasting.CastEnd(...);
	end,

	CastSuccess = function(...)
		CooldownTracking.CastSuccess(...);
		PreviousSpell.CastSuccess(...);
		ReplacementSpell.CastEnd(...);
		SpellCasting.CastSuccess(...);
	end,

	ChannelStart = function(...)
		PreviousSpell.CastSuccess(...);
		SpellCasting.ChannelStart(...);
	end,

	ChannelStop = function(...)
		SpellCasting.ChannelEnd(...);
	end,

	ChannelUpdate = function(...)
		SpellCasting.ChannelStart(...);
	end,

	CombatLog = function(...)
		SpellCasting.CombatLog(...);
	end,

	UpdateCooldown = function()
		CooldownTracking.UpdateCooldown();
	end,
};

local moduleEvents = {
	playerLogin = function()
		systemTimers.TimerAdd(CooldownTracking.TimerEvent, addon.refreshInterval);
		systemTimers.TimerAdd(SpellCasting.TimerEvent, 1);
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = Events.CombatLog,
	SPELL_UPDATE_COOLDOWN = Events.UpdateCooldown,
	UNIT_SPELLCAST_CHANNEL_START = Events.ChannelStart, 		-- Fires when a unit starts channeling a spell
	UNIT_SPELLCAST_CHANNEL_STOP = Events.ChannelStop, 			-- Fires when a unit stops or cancels a channeled spell
	UNIT_SPELLCAST_CHANNEL_UPDATE = Events.ChannelUpdate, 		-- Fires when a unit's channeled spell is interrupted or delayed
	UNIT_SPELLCAST_FAILED = Events.CastFailed, 					-- Fires when a unit's spell cast fails
	UNIT_SPELLCAST_FAILED_QUIET = Events.CastFailed, 			-- Fires when a unit's spell cast fails and no error message should be displayed
	UNIT_SPELLCAST_INTERRUPTED = Events.CastInterrupted, 		-- Fires when a unit's spell cast is interrupted
	UNIT_SPELLCAST_START = Events.CastStart, 					-- Fires when a unit begins casting a spell
	UNIT_SPELLCAST_STOP = Events.CastStop, 						-- Fires when a unit stops or cancels casting a spell
	UNIT_SPELLCAST_SUCCEEDED = Events.CastSuccess, 				-- Fires when a unit's spell cast succeeds
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
systemEvents.RegisterEvents(tableEvents, true);