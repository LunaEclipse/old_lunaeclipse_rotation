-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local function statsChanged(previousStats, currentStats)
	local Tables = addon.Core.Tables;

	if not Tables.IsEmpty(previousStats) and not Tables.IsEmpty(currentStats) then
		return Tables.GetValue(currentStats, "Download", 0) ~= Tables.GetValue(previousStats, "Download", 0)
			or Tables.GetValue(currentStats, "Upload", 0) ~= Tables.GetValue(previousStats, "Upload", 0)
			or Tables.GetValue(currentStats, "HomeLatency", 0) ~= Tables.GetValue(previousStats, "HomeLatency", 0)
			or Tables.GetValue(currentStats, "WorldLatency", 0) ~= Tables.GetValue(previousStats, "WorldLatency", 0);
	end

	return false;
end

local function timerEvent()
	local Cache = addonTable.Cache;
	local Tables = addon.Core.Tables;
	local apiGeneral = addon.API.General;

	local previousStats = apiGeneral.GetNetStats();
	local currentStats = apiGeneral.GetNetStats(true);

	if not Tables.IsEmpty(previousStats) and statsChanged(previousStats, currentStats) then
		local wipe = wipe;

		-- Wipe the Cache
		wipe(Cache.Persistant.NetStats);

		Cache.Persistant.NetStats = currentStats;

		addon.System.Timers.TimerModify(timerEvent, 30);
	end
end

local moduleEvents = {
	playerLogin = function()
		local Cache = addonTable.Cache;
		local apiGeneral = addon.API.General;

		Cache.Persistant.NetStats = apiGeneral.GetNetStats(true);
	end,
};

local tableTimers = {
	[1] = timerEvent,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Timers.RegisterTimers(tableTimers);