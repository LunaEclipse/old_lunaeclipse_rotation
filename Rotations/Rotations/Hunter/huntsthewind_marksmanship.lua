local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local Objects = addon.Core.Objects;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Enemies = addon.Modules.Enemies;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

local nameAPL = "huntsthewind_hunter_marksmanship";

-- CD Rotation
local function Cooldowns(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions+=/ancestral_call,if=cooldown.trueshot.remains>30
        AncestralCall = function()
            return Spell.Trueshot.Cooldown.Remains() > 30;
        end,

        -- actions+=/blood_fury,if=cooldown.trueshot.remains>30
        BloodFury = function()
            return Spell.Trueshot.Cooldown.Remains() > 30;
        end,

        -- actions+=/berserking,if=cooldown.trueshot.remains>30
        Berserking = function()
            return Spell.Trueshot.Cooldown.Remains() > 30;
        end,

        -- actions+=/double_tap,if=cooldown.rapid_fire.remains<gcd
        DoubleTap = function()
            return Spell.RapidFire.Cooldown.Remains() < Player.GCD();
        end,

        -- actions+=/fireblood,if=cooldown.trueshot.remains>30
        Fireblood = function()
            return Spell.Trueshot.Cooldown.Remains() > 30;
        end,

        -- actions+=/hunters_mark,if=debuff.hunters_mark.down
        HuntersMark = function()
            return Target.Debuff(Aura.HuntersMark).Down();
        end,

        -- add_action( "potion,if=(buff.trueshot.react&buff.bloodlust.react)|((consumable.prolonged_power&target.time_to_die<62)|target.time_to_die<31)" );
        -- note simcraft changed to Rising Death but forgot to change the
        -- consumable in the script line. this also affects the cooldown times
        -- Luna suggested a 15 second window to ensure player gets full benefit
        -- of potion close to boss expected death
        RisingDeath = function()
            return (Player.Buff(Aura.Trueshot).React() and Player.HasBloodlust())
                or (Target.TimeToDie() < 40 and Target.TimeToDie() > 25);
                -- or ((Player.Buff(Aura.RisingDeath) and Target.TimeToDie() < 62) or Target.TimeToDie() < 31);
        end,

        -- actions.cds+=/trueshot,if=cooldown.aimed_shot.charges<1|talent.barrage.enabled&cooldown.aimed_shot.charges_fractional<1.3
        Trueshot = function()
            return Spell.AimedShot.Charges() < 1
                or (Talent.Barrage.Enabled()
                    and Spell.AimedShot.Charges.Fractional() < 1.3);
        end,

    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateAction(Talent.HuntersMark, self.Requirements.HuntersMark);
        action.EvaluateAction(Talent.DoubleTap, self.Requirements.DoubleTap);
        action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
        action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
        action.EvaluateAction(Racial.AncestralCall, self.Requirements.AncestralCall);
        action.EvaluateAction(Racial.Fireblood, self.Requirements.Fireblood);
        -- actions+=/lights_judgment
        action.EvaluateAction(Racial.LightsJudgment, true);
        action.EvaluateAction(Consumable.RisingDeath, self.Requirements.RisingDeath);
        action.EvaluateAction(Spell.Trueshot, self.Requirements.Trueshot);
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Single Target Rotation
local function SingleTarget(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        AimedShot = {
            -- actions.st+=/aimed_shot,if=buff.precise_shots.down&(buff.double_tap.down&full_recharge_time<cast_time+gcd|buff.lethal_shots.up)
            AvoidCapping = function()
                return not Player.Buff(Aura.PreciseShots).Up()
                    and ((not Player.Buff(Aura.DoubleTap).Up()
                            and Spell.AimedShot.Charges.FullRechargeTime() < Spell.AimedShot.CastTime() + Player.GCD())
                        or Player.Buff(Aura.LethalShots).Up())
            end,

            -- actions.st+=/aimed_shot,if=buff.precise_shots.down&(!talent.steady_focus.enabled&focus>70|!talent.lethal_shots.enabled|buff.lethal_shots.up)
            Use = function(numEnemies)
                return not Player.Buff(Aura.PreciseShots).Up()
                    and ((not Talent.SteadyFocus.Enabled()
                            and Player.Focus() > 70)
                        or not Talent.LethalShots.Enabled()
                        or Player.Buff(Aura.SteadyFocus).Up());
            end,
        },

        ArcaneShot = {
            -- actions.st+=/arcane_shot,if=buff.precise_shots.up&(cooldown.aimed_shot.full_recharge_time<gcd*buff.precise_shots.stack+action.aimed_shot.cast_time|buff.lethal_shots.up)
            PreciseShotsAvoidAimedCap = function(numEnemies)
                return Player.Buff(Aura.PreciseShots).Up()
                    and (Spell.AimedShot.Charges.FullRechargeTime() < Player.GCD() * Player.Buff(Aura.PreciseShots).Stack() + Spell.AimedShot.CastTime()
                        or Player.Buff(Aura.LethalShots).Up());
            end,

            -- actions.st+=/arcane_shot,if=buff.precise_shots.up|focus>60&(!talent.lethal_shots.enabled|buff.lethal_shots.up)
            Use = function()
                return Player.Buff(Aura.PreciseShots).Up()
                    or (Player.Focus() > 60
                        and (not Talent.LethalShots.Enabled()
                            or Player.Buff(Aura.LethalShots).Up()));
            end,

        },

        -- actions+=/barrage,if=active_enemies>1
        Barrage = function(numEnemies)
            return numEnemies > 1;
        end,

        RapidFire = {
            -- actions.st+=/rapid_fire,if=(!talent.lethal_shots.enabled|buff.lethal_shots.up)&azerite.focused_fire.enabled|azerite.in_the_rhythm.rank>1
            AzeriteBuffed = function()
                return ((not Talent.LethalShots.Enabled()
                        or Player.Buff(Aura.LethalShots).Up())
                        and Azerite.FocusedFire.Enabled())
                    or Azerite.InTheRhythm.Rank() > 1;
            end,

            -- actions.st+=/rapid_fire,if=!talent.lethal_shots.enabled|buff.lethal_shots.up
            Use = function()
                return not Talent.LethalShots.Enabled()
                    or Player.Buff(Aura.LethalShots).Up();
            end,

        },

        -- actions.st+=/serpent_sting,if=refreshable
        SerpentSting = function()
            return Target.Debuff(Aura.SerpentSting).Refreshable();
        end,

        -- actions.st+=/steady_shot,if=focus+cast_regen<focus.max|(talent.lethal_shots.enabled&buff.lethal_shots.down)
        SteadyShot = function()
            return Player.Focus() + Player.Focus.CastRegen(Spell.SteadyShot) < Player.Focus.Max()
                or (Talent.LethalShots.Enabled()
                    and not Player.Buff(Aura.LethalShots).Up());
        end,

    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        -- actions.st=explosive_shot
        -- NOTE: prioritize detonations
        action.EvaluateAction(Talent.ExplosiveShotDetonate, true);
        action.EvaluateAction(Talent.ExplosiveShot, true);
        action.EvaluateAction(Talent.Barrage, self.Requirements.Barrage);
        action.EvaluateAction(Spell.ArcaneShot, self.Requirements.ArcaneShot.PreciseShotsAvoidAimedCap);
        action.EvaluateAction(Spell.RapidFire, self.Requirements.RapidFire.AzeriteBuffed);
        action.EvaluateAction(Spell.AimedShot, self.Requirements.AimedShot.AvoidCapping);
        action.EvaluateAction(Spell.RapidFire, self.Requirements.RapidFire.Use);
        -- actions.st+=/piercing_shot
        action.EvaluateAction(Talent.PiercingShot, true);
        -- actions.st+=/a_murder_of_crows
        action.EvaluateAction(Talent.AMurderOfCrows, true);
        action.EvaluateAction(Talent.SerpentSting, self.Requirements.SerpentSting);
        action.EvaluateAction(Spell.AimedShot, self.Requirements.AimedShot.Use);
        action.EvaluateAction(Spell.ArcaneShot, self.Requirements.ArcaneShot.Use);
        action.EvaluateAction(Spell.SteadyShot, self.Requirements.SteadyShot);
        -- actions.st+=/arcane_shot
        action.EvaluateAction(Spell.ArcaneShot, true);

    end

    function self.Use(numEnemies)
        return numEnemies < 3
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSingleTarget = SingleTarget("SingleTarget");

-- Trick Shots Rotation
local function TrickShots(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions.trickshots+=/aimed_shot,if=buff.trick_shots.up&buff.precise_shots.down&buff.double_tap.down&(!talent.lethal_shots.enabled|buff.lethal_shots.up|focus>60)
        AimedShot = function()
            return Player.Buff(Aura.TrickShots).Up()
                and not Player.Buff(Aura.PreciseShots).Up()
                and not Player.Buff(Aura.DoubleTap).Up()
                and (not Talent.LethalShots.Enabled()
                    or Player.Buff(Aura.LethalShots).Up()
                    or Player.Focus() > 60);
        end,

        -- actions+=/barrage,if=active_enemies>1
        Barrage = function(numEnemies)
            return numEnemies > 1;
        end,

        -- actions.trickshots+=/multishot,if=buff.trick_shots.down|(buff.precise_shots.up|buff.lethal_shots.up)&(!talent.barrage.enabled&buff.steady_focus.down&focus>45|focus>70)
        MultiShot = function()
            return not Player.Buff(Aura.TrickShots).Up()
                or ((Player.Buff(Aura.PreciseShots).Up()
                        or Player.Buff(Aura.LethalShots).Up())
                    and ((not Talent.Barrage.Enabled()
                            and not Player.Buff(Aura.SteadyFocus).Up()
                            and Player.Focus() > 45)
                        or Player.Focus() > 70));
        end,

        RapidFire = {
            -- actions.trickshots+=/rapid_fire,if=buff.trick_shots.up&!talent.barrage.enabled
            NoBarrage = function()
                return Player.Buff(Aura.TrickShots).Up()
                    and not Talent.Barrage.Enabled();
            end,

            -- actions.trickshots+=/rapid_fire,if=buff.trick_shots.up
            Use = function()
                return Player.Buff(Aura.TrickShots).Up();
            end,

        },

        -- actions.st+=/serpent_sting,if=refreshable
        SerpentSting = function()
            return Target.Debuff(Aura.SerpentSting).Refreshable();
        end,

        -- actions.st+=/steady_shot,if=focus+cast_regen<focus.max|(talent.lethal_shots.enabled&buff.lethal_shots.down)
        SteadyShot = function()
            return Player.Focus() + Player.Focus.CastRegen(Spell.SteadyShot) < Player.Focus.Max()
                or (Talent.LethalShots.Enabled()
                    and not Player.Buff(Aura.LethalShots).Up());
        end,

    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        -- NOTE: prioritize Explosive Shot detonations
        action.EvaluateAction(Talent.ExplosiveShotDetonate, true);
        -- actions.trickshots=barrage
        action.EvaluateAction(Talent.Barrage, true);
        -- actions.trickshots+=/explosive_shot
        action.EvaluateAction(Talent.ExplosiveShot, true);
        action.EvaluateAction(Spell.RapidFire, self.Requirements.RapidFire.NoBarrage);
        action.EvaluateAction(Spell.AimedShot, self.Requirements.AimedShot);
        action.EvaluateAction(Spell.RapidFire, self.Requirements.RapidFire.Use);
        action.EvaluateAction(Spell.MultiShot, self.Requirements.MultiShot);
        -- actions.trickshots+=/piercing_shot
        action.EvaluateAction(Talent.PiercingShot, true);
        -- actions.trickshots+=/a_murder_of_crows
        action.EvaluateAction(Talent.AMurderOfCrows, true);
        action.EvaluateAction(Talent.SerpentSting, self.Requirements.SerpentSting);
        action.EvaluateAction(Spell.SteadyShot, self.Requirements.SteadyShot);

    end

    function self.Use(numEnemies)
        return numEnemies > 2
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationTrickShots = TrickShots("TrickShots");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
    -- Inherits APL Class so get the base class.
    local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

    -- Store the information for the script.
    self.scriptInfo = {
        SpecializationID = self.SpecID,
        ScriptAuthor = "HuntsTheWind",
        ScriptCredits = "LunaEclipse",
        GuideAuthor = "Azortharion and SimCraft",
        GuideLink = "http://www.icy-veins.com/wow/marksmanship-hunter-pve-dps-guide",
        WoWVersion = 80001,
    };

    self.Defensives = {
        AspectOfTheTurtle = function()
            return Player.DamagePredicted(4) >= 40;
        end,

        Exhilaration = function()
            return Player.Health.Percent() < 70
                    and Player.DamagePredicted(5) >= 25;
        end,

        FeignDeath = function()
            return Player.Health.Percent() < 25
                    and Player.DamagePredicted(5) >= 25;
        end,
    };

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        AimedShot = {
			-- actions.precombat+=/aimed_shot,if=active_enemies<3
			Precombat = function(numEnemies)
				return numEnemies < 3;
			end,
        },

        ArcaneShot = {
            -- consider burning off PreciseShot buffs in precombat
            -- recommendation
            PrecombatPreciseShots = function()
                return Player.Buff(Aura.PreciseShots).Up();
            end,
        },

        ExplosiveShot = {
			-- actions.precombat+=/explosive_shot,if=active_enemies>2
			Precombat = function(numEnemies)
				return numEnemies > 2;
			end,
        },

        --actions+=/hunters_mark, if=debuff.hunters_mark.down
        HuntersMark = function()
            return not Target.Debuff(Aura.HuntersMark).Up();
        end,

        MultiShot = {
			-- actions.precombat+=/explosive_shot,if=active_enemies>2
			Precombat = function(numEnemies)
				return not Talent.ExplosiveShot.Enabled()
                    and numEnemies > 2;
			end,
        },

    };

    -- Add meta-table to the requirements table, to enable better debugging and case insensitivity.
    Objects.FinalizeRequirements(self.Defensives, self.Requirements);

    -- Function for setting up action objects such as spells, buffs,
    -- debuffs, and items, called when the rotation becomes the active
    -- rotation.
    function self.Enable(...)
        Racial, Spell, Talent, Aura, Azerite = ...;

        Aura.RisingDeath = Objects.newSpell(152559);

        Item = {};

        Consumable = {
            -- Potions
            RisingDeath = Objects.newItem(152559),
        };

        -- Add meta-table to the various object tables, to enable better debugging and case insensitivity.
        Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
    end

    -- Function for setting up the configuration screen, called when rotation becomes the active rotation.
    function self.SetupConfiguration(config, options)
        config.RacialOptions(options, Racial.AncestralCall, Racial.Berserking, Racial.BloodFury, Racial.Fireblood, Racial.LightsJudgment);
        config.AOEOptions(options, Talent.Barrage, Talent.ExplosiveShot, Spell.MultiShot);
        config.CooldownOptions(options, Talent.AMurderOfCrows, Talent.DoubleTap, Talent.HuntersMark, Talent.PiercingShot, Talent.SerpentSting, Spell.Trueshot);
        config.DefensiveOptions(options, Spell.AspectOfTheTurtle, Spell.Exhilaration, Spell.FeignDeath);
    end

    -- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
    function self.Disable()
        local coreTables = addon.Core.Tables;

        coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
    end

    -- Function for checking the rotation that displays on the Defensives icon.
    function self.Defensive(action)
        -- The abilities here should be listed from highest damage required to suggest to lowest,
        -- Specific damage types before all damage types.

        -- Protects against all types of damage
        action.EvaluateDefensiveAction(Spell.AspectOfTheTurtle, self.Defensives.AspectOfTheTurtle);

        -- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
        action.EvaluateDefensiveAction(Spell.Exhilaration, self.Defensives.Exhilaration);

        -- Feign Death if no other defensives are available
        action.EvaluateDefensiveAction(Spell.FeignDeath, self.Defensives.FeignDeath);
    end

    -- Function for displaying opening rotation.
    function self.Opener(action)
    end

    -- Function for displaying any actions before combat starts.
    function self.Precombat(action)
        -- actions.precombat+=/potion
        action.EvaluateAction(Consumable.RisingDeath, true);
        -- SimCraft doesn't specify it, but we only want to use Hunter's Mark if its not already active, so use normal requirements.
        -- actions.precombat+=/hunters_mark
        action.EvaluateAction(Talent.HuntersMark, self.Requirements.HuntersMark);
        -- actions.precombat+=/double_tap,precast_time=5
        action.EvaluateAction(Talent.DoubleTap, true);
        -- The addon can't determine number of enemies outside combat
        -- use left over PreciseShots buffs first
        action.EvaluateAction(Spell.ArcaneShot, self.Requirements.ArcaneShot.PrecombatPreciseShots);
        -- actions.precombat+=/aimed_shot,if=active_enemies<3
		action.EvaluateAction(Spell.AimedShot, self.Requirements.AimedShot.Precombat, Enemies.GetEnemies(Spell.Multishot));
        -- if Explosive Shot not enabled, recommend MultiShot for AOE opening
		action.EvaluateAction(Spell.MultiShot, self.Requirements.MultiShot.Precombat, Enemies.GetEnemies(Spell.Multishot));
        -- actions.precombat+=/explosive_shot,if=active_enemies>2
		action.EvaluateAction(Talent.ExplosiveShot, self.Requirements.ExplosiveShot.Precombat, Enemies.GetEnemies(Spell.Multishot));
    end

    -- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
    function self.Combat(action)
        action.CallActionList(rotationCooldowns);
        action.CallActionList(rotationSingleTarget);
        action.CallActionList(rotationTrickShots);
    end

    return self;
end

local rotationAPL = APL(nameAPL, "HuntsTheWind: Marksmanship Hunter", addonTable.Enum.SpecID.HUNTER_MARKSMANSHIP);
