local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];

--- Localize Vars
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "lunaeclipse_warrior_arms";

-- AOE Rotation
local function AOE(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.five_target+=/bladestorm,if=buff.sweeping_strikes.down&debuff.colossus_smash.remains>4.5&(prev_gcd.1.mortal_strike|spell_targets.whirlwind>1)&(!buff.deadly_calm.up|!talent.deadly_calm.enabled)
		Bladestorm = function(numEnemies)
			return Player.Buff(Aura.SweepingStrikes).Down()
			   and Target.Debuff(Aura.ColossusSmash).Remains() > 4.5
			   and (Player.PrevGCD(1, Spell.MortalStrike) or numEnemies > 1)
			   and (not Player.Buff(Aura.DeadlyCalm).Up() or not Talent.DeadlyCalm.Enabled());
		end,

		-- actions.five_target+=/colossus_smash,if=debuff.colossus_smash.down
		ColossusSmash = function()
			return Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.five_target+=/deadly_calm,if=cooldown.bladestorm.remains>6&((cooldown.colossus_smash.remains<2|(talent.warbreaker.enabled&cooldown.warbreaker.remains<2))|(equipped.weight_of_the_earth&cooldown.heroic_leap.remains<2))
		DeadlyCalm = function()
			return Spell.Bladestorm.Cooldown.Remains() > 6
			   and (Spell.ColossusSmash.Cooldown.Remains() < 2 or (Talent.Warbreaker.Enabled() and Talent.Warbreaker.Cooldown.Remains() < 2));
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.five_target+=/execute,if=(!talent.cleave.enabled&dot.deep_wounds.remains<2)|(buff.sudden_death.react|buff.stone_heart.react)&(buff.sweeping_strikes.up|cooldown.sweeping_strikes.remains>8)
		Execute = function()
			return (not Talent.Cleave.Enabled() and Target.Debuff(Aura.DeepWounds).Remains() < 2)
			    or Player.Buff(Aura.SuddenDeath).React()
			   and (Player.Buff(Aura.SweepingStrikes).Up() or Spell.SweepingStrikes.Cooldown.Remains() > 8);
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.five_target+=/mortal_strike,if=(!talent.cleave.enabled&dot.deep_wounds.remains<2)|buff.sweeping_strikes.up&buff.overpower.stack=2&(talent.dreadnaught.enabled|equipped.archavons_heavy_hand)
		MortalStrike = function()
			return (not Talent.Cleave.Enabled() and Target.Debuff(Aura.DeepWounds).Remains() < 2)
			    or Player.Buff(Aura.SweepingStrikes).Up()
			   and Player.Buff(Aura.Overpower).Stack() == 2
			   and Talent.Dreadnaught.Enabled();
		end,

		-- actions.five_target+=/ravager,if=debuff.colossus_smash.up&(cooldown.deadly_calm.remains>6|!talent.deadly_calm.enabled)
		Ravager = function()
			return Target.Debuff(Aura.ColossusSmash).Up()
			   and (Talent.DeadlyCalm.Cooldown.Remains() > 6 or not Talent.DeadlyCalm.Enabled());
		end,

		-- actions.five_target=skullsplitter,if=rage<70&(cooldown.deadly_calm.remains>3|!talent.deadly_calm.enabled)
		Skullsplitter = function()
			return Player.Rage() < 70
			   and (Talent.DeadlyCalm.Cooldown.Remains() > 3 or not Talent.DeadlyCalm.Enabled());
		end,

		-- actions.five_target+=/warbreaker,if=debuff.colossus_smash.down
		Warbreaker = function()
			return Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- actions.five_target+=/whirlwind,if=debuff.colossus_smash.up
		Whirlwind = function()
			return Target.Debuff(Aura.ColossusSmash).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.Skullsplitter, self.Requirements.Skullsplitter);
		action.EvaluateAction(Talent.DeadlyCalm, self.Requirements.DeadlyCalm);
		action.EvaluateAction(Spell.ColossusSmash, self.Requirements.ColossusSmash);
		action.EvaluateAction(Talent.Warbreaker, self.Requirements.Warbreaker);
		action.EvaluateAction(Spell.Bladestorm, self.Requirements.Bladestorm, Enemies.GetEnemies(8));
		action.EvaluateAction(Talent.Ravager, self.Requirements.Ravager);
		-- actions.five_target+=/cleave
		action.EvaluateAction(Talent.Cleave, true);
		action.EvaluateAction(Spell.Execute, self.Requirements.Execute);
		action.EvaluateAction(Spell.MortalStrike, self.Requirements.MortalStrike);
		action.EvaluateAction(Spell.Whirlwind, self.Requirements.Whirlwind);
		-- actions.five_target+=/overpower
		action.EvaluateAction(Spell.Overpower, true);
		-- actions.five_target+=/whirlwind
		action.EvaluateAction(Spell.Whirlwind, true);
	end

	-- actions+=/run_action_list,name=five_target,if=spell_targets.whirlwind>4
	function self.Use()
		return Enemies.GetEnemies(8) > 4;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationAOE = AOE("AOE");

-- Execute Rotation
local function Execute(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.execute+=/bladestorm,if=debuff.colossus_smash.remains>4.5&rage<70&(!buff.deadly_calm.up|!talent.deadly_calm.enabled)
		Bladestorm = function()
			return Target.Debuff(Aura.ColossusSmash).Remains() > 4.5
			   and Player.Rage() < 70
			   and (not Player.Buff(Aura.DeadlyCalm).Up() or not Talent.DeadlyCalm.Enabled());
		end,

		-- actions.execute+=/cleave,if=spell_targets.whirlwind>2
		Cleave = function(numEnemies)
			return numEnemies > 2;
		end,

		-- actions.execute+=/colossus_smash,if=debuff.colossus_smash.down
		ColossusSmash = function()
			return Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.execute+=/deadly_calm,if=cooldown.bladestorm.remains>6&((cooldown.colossus_smash.remains<2|(talent.warbreaker.enabled&cooldown.warbreaker.remains<2))|(equipped.weight_of_the_earth&cooldown.heroic_leap.remains<2))
		DeadlyCalm = function()
			return Spell.Bladestorm.Cooldown.Remains() > 6
			   and (Spell.ColossusSmash.Cooldown.Remains() < 2 or (Talent.Warbreaker.Enabled() and Talent.Warbreaker.Cooldown.Remains() < 2));
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.execute+=/execute,if=rage>=40|debuff.colossus_smash.up|buff.sudden_death.react|buff.stone_heart.react
		Execute = function()
			return Player.Rage() >= 40
			    or Target.Debuff(Aura.ColossusSmash).Up()
			    or Player.Buff(Aura.SuddenDeath).React();
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.execute+=/mortal_strike,if=buff.overpower.stack=2&(talent.dreadnaught.enabled|equipped.archavons_heavy_hand)
		MortalStrike = function()
			return Player.Buff(Aura.Overpower).Stack() == 2
			   and Talent.Dreadnaught.Enabled();
		end,

		-- actions.execute+=/ravager,if=debuff.colossus_smash.up&(cooldown.deadly_calm.remains>6|!talent.deadly_calm.enabled)
		Ravager = function()
			return Target.Debuff(Aura.ColossusSmash).Up()
			   and (Talent.DeadlyCalm.Cooldown.Remains() > 6 or not Talent.DeadlyCalm.Enabled());
		end,

		-- remains<=duration*0.3 is the same as Refreshable().
		-- actions.execute=rend,if=remains<=duration*0.3&debuff.colossus_smash.down
		Rend = function()
			return Target.Debuff(Aura.Rend).Refreshable()
			   and Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- actions.execute+=/skullsplitter,if=rage<70&((cooldown.deadly_calm.remains>3&!buff.deadly_calm.up)|!talent.deadly_calm.enabled)
		Skullsplitter = function()
			return Player.Rage() < 70
			   and ((Talent.DeadlyCalm.Cooldown.Remains() > 3 and not Player.Buff(Aura.DeadlyCalm).Up()) or not Talent.DeadlyCalm.Enabled());
		end,

		-- actions.execute+=/warbreaker,if=debuff.colossus_smash.down
		Warbreaker = function()
			return Target.Debuff(Aura.ColossusSmash).Down();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.Rend, self.Requirements.Rend);
		action.EvaluateAction(Talent.Skullsplitter, self.Requirements.Skullsplitter);
		action.EvaluateAction(Talent.DeadlyCalm, self.Requirements.DeadlyCalm);
		action.EvaluateAction(Spell.ColossusSmash, self.Requirements.ColossusSmash);
		action.EvaluateAction(Talent.Warbreaker, self.Requirements.Warbreaker);
		action.EvaluateAction(Spell.Bladestorm, self.Requirements.Bladestorm);
		action.EvaluateAction(Talent.Ravager, self.Requirements.Ravager);
		action.EvaluateAction(Talent.Cleave, self.Requirements.Cleave, Enemies.GetEnemies(8));
		action.EvaluateAction(Spell.MortalStrike, self.Requirements.MortalStrike);
		-- actions.execute+=/overpower
		action.EvaluateAction(Spell.Overpower, true);
		action.EvaluateAction(Spell.Execute, self.Requirements.Execute);
	end

	-- actions+=/run_action_list,name=execute,if=(talent.massacre.enabled&target.health.pct<35)|target.health.pct<20
	function self.Use()
		return (Talent.Massacre.Enabled() and Target.Health.Percent() < 35)
		    or Target.Health.Percent() < 20;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationExecute = Execute("Execute");

-- SingleTarget Rotation
local function SingleTarget(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.single_target+=/bladestorm,if=buff.sweeping_strikes.down&debuff.colossus_smash.remains>4.5&(prev_gcd.1.mortal_strike|spell_targets.whirlwind>1)&(!buff.deadly_calm.up|!talent.deadly_calm.enabled)
		Bladestorm = function(numEnemies)
			return Player.Buff(Aura.SweepingStrikes).Down()
			   and Target.Debuff(Aura.ColossusSmash).Remains() > 4.5
			   and (Player.PrevGCD(1, Spell.MortalStrike) or numEnemies > 1)
			   and (not Player.Buff(Aura.DeadlyCalm).Up() or not Talent.DeadlyCalm.Enabled());
		end,

		-- actions.single_target+=/cleave,if=spell_targets.whirlwind>2
		Cleave = function(numEnemies)
			return numEnemies > 2;
		end,

		-- actions.single_target+=/colossus_smash,if=debuff.colossus_smash.down
		ColossusSmash = function()
			return Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.single_target+=/deadly_calm,if=cooldown.bladestorm.remains>6&((cooldown.colossus_smash.remains<2|(talent.warbreaker.enabled&cooldown.warbreaker.remains<2))|(equipped.weight_of_the_earth&cooldown.heroic_leap.remains<2))
		DeadlyCalm = function()
			return Spell.Bladestorm.Cooldown.Remains() > 6
			   and (Spell.ColossusSmash.Cooldown.Remains() < 2 or (Talent.Warbreaker.Enabled() and Talent.Warbreaker.Cooldown.Remains() < 2));
		end,

		-- We are not doing legion legendaries as they stop working at 115, so just skip those checks.
		-- actions.single_target+=/execute,if=buff.sudden_death.react|buff.stone_heart.react
		Execute = function()
			return Player.Buff(Aura.SuddenDeath).React();
		end,

		-- actions.single_target+=/ravager,if=debuff.colossus_smash.up&(cooldown.deadly_calm.remains>6|!talent.deadly_calm.enabled)
		Ravager = function()
			return Target.Debuff(Aura.ColossusSmash).Up()
			   and (Talent.DeadlyCalm.Cooldown.Remains() > 6 or not Talent.DeadlyCalm.Enabled());
		end,

		-- remains<=duration*0.3 is the same as Refreshable().
		-- actions.single_target=rend,if=remains<=duration*0.3&debuff.colossus_smash.down
		Rend = function()
			return Target.Debuff(Aura.Rend).Refreshable()
			   and Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- actions.single_target+=/skullsplitter,if=rage<70&(cooldown.deadly_calm.remains>3|!talent.deadly_calm.enabled)
		Skullsplitter = function()
			return Player.Rage() < 70
			   and (Talent.DeadlyCalm.Cooldown.Remains() > 3 or not Talent.DeadlyCalm.Enabled());
		end,

		-- actions.single_target+=/slam,if=!talent.fervor_of_battle.enabled&(rage>=40|debuff.colossus_smash.up)
		Slam = function()
			return not Talent.FervorOfBattle.Enabled()
			   and (Player.Rage() >= 40 or Target.Debuff(Aura.ColossusSmash).Up());
		end,

		-- actions.single_target+=/warbreaker,if=debuff.colossus_smash.down
		Warbreaker = function()
			return Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- actions.single_target+=/whirlwind,if=talent.fervor_of_battle.enabled&(rage>=50|debuff.colossus_smash.up)
		Whirlwind = function()
			return Talent.FervorOfBattle.Enabled()
			   and (Player.Rage() >= 50 or Target.Debuff(Aura.ColossusSmash).Up());
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.Rend, self.Requirements.Rend);
		action.EvaluateAction(Talent.Skullsplitter, self.Requirements.Skullsplitter);
		action.EvaluateAction(Talent.DeadlyCalm, self.Requirements.DeadlyCalm);
		action.EvaluateAction(Spell.ColossusSmash, self.Requirements.ColossusSmash);
		action.EvaluateAction(Talent.Warbreaker, self.Requirements.Warbreaker);
		action.EvaluateAction(Spell.Execute, self.Requirements.Execute);
		action.EvaluateAction(Spell.Bladestorm, self.Requirements.Bladestorm, Enemies.GetEnemies(8));
		action.EvaluateAction(Talent.Ravager, self.Requirements.Ravager);
		action.EvaluateAction(Talent.Cleave, self.Requirements.Cleave, Enemies.GetEnemies(8));
		-- actions.single_target+=/mortal_strike
		action.EvaluateAction(Spell.MortalStrike, true);
		-- actions.single_target+=/overpower
		action.EvaluateAction(Spell.Overpower, true);
		action.EvaluateAction(Spell.Whirlwind, self.Requirements.Whirlwind);
		action.EvaluateAction(Spell.Slam, self.Requirements.Slam);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSingleTarget = SingleTarget("SingleTarget");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Archimtiros and SimCraft",
		GuideLink = "https://www.wowhead.com/arms-warrior-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		DefensiveStance = function()
			return Player.DamagePredicted(4) >= 80;
		end,

		DieByTheSword = function()
			return Player.PhysicalDamagePredicted(4) >= 60
			    or Player.MagicDamagePredicted(4) >= 20;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/ancestral_call,if=debuff.colossus_smash.up
		AncestralCall = function()
			return Target.Debuff(Aura.ColossusSmash).Up();
		end,

		-- actions+=/avatar,if=cooldown.colossus_smash.remains<8|(talent.warbreaker.enabled&cooldown.warbreaker.remains<8)
		Avatar = function()
			return Spell.ColossusSmash.Cooldown.Remains() < 8
			    or (Talent.Warbreaker.Enabled() and Talent.Warbreaker.Cooldown.Remains() < 8);
		end,

		-- actions+=/arcane_torrent,if=debuff.colossus_smash.down&cooldown.mortal_strike.remains>1.5&rage<50
		ArcaneTorrent = function()
			return Target.Debuff(Aura.ColossusSmash).Down()
			   and Spell.MortalStrike.Cooldown.Remains() > 1.5
			   and Player.Rage() < 50;
		end,

		-- actions+=/berserking,if=debuff.colossus_smash.up
		Berserking = function()
			return Target.Debuff(Aura.ColossusSmash).Up();
		end,

		-- actions+=/blood_fury,if=debuff.colossus_smash.up
		BloodFury = function()
			return Target.Debuff(Aura.ColossusSmash).Up();
		end,

		-- actions+=/fireblood,if=debuff.colossus_smash.up
		Fireblood = function()
			return Target.Debuff(Aura.ColossusSmash).Up();
		end,

		-- Suggest this when the target is out of melee range but within heroic leap range.
		HeroicLeap = function()
			return Target.InRange.Max() >= 8
			   and Target.InRange.Max() <= 40;
		end,

		-- actions+=/lights_judgment,if=debuff.colossus_smash.down
		LightsJudgment = function()
			return Target.Debuff(Aura.ColossusSmash).Down();
		end,

		-- actions+=/sweeping_strikes,if=spell_targets.whirlwind>1
		SweepingStrikes = function(numEnemies)
			return numEnemies > 1;
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfStrength = Objects.newItem(163224),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.AncestralCall, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood, Racial.LightsJudgment);
		config.AOEOptions(options, Spell.Bladestorm, Talent.Cleave, Talent.Ravager, Spell.SweepingStrikes, Spell.Whirlwind);
		config.CooldownOptions(options, Talent.Avatar, Talent.DeadlyCalm, Spell.Execute, Talent.Rend, Talent.Skullsplitter, Talent.Warbreaker);
		config.DefensiveOptions(options, Talent.DefensiveStance, Spell.DieByTheSword);
		config.UtilityOptions(options, Spell.Charge, Spell.HeroicLeap);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.DieByTheSword, self.Defensives.DieByTheSword);
		action.EvaluateDefensiveAction(Talent.DefensiveStance, self.Defensives.DefensiveStance);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfStrength, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions=charge
		action.EvaluateAction(Spell.Charge, true);
		action.EvaluateAction(Spell.HeroicLeap, self.Requirements.HeroicLeap);
		-- actions+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfStrength, true);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
		action.EvaluateAction(Racial.LightsJudgment, self.Requirements.LightsJudgment);
		action.EvaluateAction(Racial.Fireblood, self.Requirements.Fireblood);
		action.EvaluateAction(Racial.AncestralCall, self.Requirements.AncestralCall);
		action.EvaluateAction(Talent.Avatar, self.Requirements.Avatar);
		action.EvaluateAction(Spell.SweepingStrikes, self.Requirements.SweepingStrikes, Enemies.GetEnemies(8));

		action.RunActionList(rotationAOE);
		action.RunActionList(rotationExecute);
		-- actions+=/run_action_list,name=single_target
		action.RunActionList(rotationSingleTarget);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Arms Warrior", addonTable.Enum.SpecID.WARRIOR_ARMS);
