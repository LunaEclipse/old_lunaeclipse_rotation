local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Data.Azerite.Class[7] = {
	Aura = {},
	Spell = {
		AncestralReach = 263790,
		AncestralResonance = 277666,
		AnduinsDedication = 280628,
		ArchiveOfTheTitans = 280555,
		AstralShift = 263786,
		AutoSelfCauterizer = 280172,
		AzeriteEmpowered = 263978,
		AzeriteFortification = 268435,
		AzeriteGlobules = 266936,
		BarrageOfManyBombs = 280163,
		BattlefieldFocus = 280582,
		BattlefieldPrecision = 280627,
		BlessedPortents = 267889,
		BlightborneInfusion = 273823,
		BloodRite = 280407,
		BloodSiphon = 264108,
		BracingChill = 267884,
		BulwarkOfTheMasses = 268595,
		ChampionOfAzeroth = 280710,
		CollectiveWill = 280581,
		CombinedMight = 280580,
		ConcentratedMending = 267882,
		DaggerInTheBack = 280284,
		Earthlink = 279926,
		EbbAndFlow = 273597,
		EchoOfTheElementals = 275381,
		Electropotence = 264121,
		ElementalWhirl = 263984,
		EphemeralRecovery = 267886,
		FilthyTransfusion = 273834,
		FlamesOfTheForefathers = 264113,
		Gemhide = 268596,
		GloryInBattle = 280577,
		Gutripper = 266937,
		Haste = 261582,
		HeedMyCall = 263987,
		IgneousPotential = 279829,
		ImpassiveVisage = 268437,
		InciteThePack = 280410,
		LaserMatrix = 280559,
		LastGift = 280624,
		LavaShock = 273448,
		LiberatorsMight = 280623,
		LightningConduit = 275388,
		Lightningburn = 263792,
		Longstrider = 268594,
		MeticulousScheming = 273682,
		NaturalHarmony = 278697,
		NetherlightFortification = 250879,
		OnMyWay = 267879,
		OverflowingShores = 277658,
		OverwhelmingPower = 266180,
		PackSpirit = 280021,
		PersonalAbsorbOTron = 280181,
		PrimalPrimer = 272992,
		RelationalNormalizationGizmo = 280178,
		ResoundingProtection = 263962,
		RetaliatoryFury = 280579,
		RezansFury = 273790,
		RicochetingInflatablePyrosaw = 280168,
		RoilingStorm = 278719,
		RuinousBolt = 273150,
		RumblingTremors = 278709,
		Savior = 267883,
		SecretsOfTheDeep = 273829,
		SelfReliance = 268600,
		SereneSpirit = 274412,
		SoothingWaters = 272989,
		SpoutingSpirits = 278715,
		StandAsOne = 280626,
		StormsEye = 263795,
		StrengthOfEarth = 273461,
		StrongerTogether = 280625,
		SurgingTides = 278713,
		SwellingStream = 275488,
		SwirlingSands = 280429,
		SylvanasResolve = 280598,
		SynapseShock = 277671,
		SynapticSparkCapacitor = 280174,
		SynergisticGrowth = 267892,
		ThunderousBlast = 280380,
		TidalSurge = 280402,
		Tradewinds = 281841,
		UnstableCatalyst = 281514,
		UnstableFlames = 279899,
		VampiricSpeed = 268599,
		Woundbinder = 267880,
	},
};