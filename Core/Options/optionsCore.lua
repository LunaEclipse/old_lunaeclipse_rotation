-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local function getRotationName()
	local pairs = pairs;
	local Tables = addon.Core.Tables;
	local apiGeneral = addon.API.General;

	local specInfo = apiGeneral.GetSpecializationInfo();

	if not Tables.IsEmpty(specInfo) then
		local numRotations, rotationList = addon.Rotation.GetRegisteredRotations(specInfo.ID);

		if numRotations == 1 then
			for key in pairs(rotationList) do
				return key;
			end
		else
			return "__none";
		end
	end
end

addon.Options.Core = {
	CleanupOptions = function(optionsTable)
		local pairs = pairs;
		local Tables = addon.Core.Tables;

		for key, value in pairs(optionsTable.args) do
			if Tables.IsEmpty(value.args) then
				optionsTable.args[key] = nil;
			end
		end

		return optionsTable;
	end,

	ConvertToValue = function(value, tristate)
		if value == nil then
			return 1;
		elseif value then
			return (tristate and 2) or 1;
		else
			return 0;
		end
	end,

	ConvertToUI = function(value, tristate)
		if value == 2 or (not tristate and value == 1) then
			return true;
		elseif value == 0 then
			return false;
		else
			return nil;
		end
	end,

	CreaterHeader = function(headerText)
		local format = format;

		return format("%s%s%s", addonTable.COLORS.TAGS.HIGHLIGHT, headerText, addonTable.TAGS.END);
	end,
};

addon.Options.Settings = {
	GetAccountValue = function(info)
		local Tables = addon.Core.Tables;
		local returnValue = true;

		if info then
			local sectionName = info[#info - 1];
			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.accountSettings, sectionName, {});

			returnValue = Tables.GetValue(profile, keyName);

			if info.option.type == "toggle" then
				returnValue = addon.Options.Core.ConvertToUI(returnValue, info.option.tristate);
			end
		end

		return returnValue;
	end,

	SetAccountValue = function(info, value)
		if info then
			local Tables = addon.Core.Tables;

			if info.option.type == "toggle" then
				value = addon.Options.Core.ConvertToValue(value, info.option.tristate);
			end

			local sectionName = info[#info - 1];
			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.accountSettings, sectionName, {});

			if keyName == "refreshInterval" then
				addon.refreshInterval = value / 1000;
			elseif keyName == "reactTime" then
				addon.reactTime = value / 1000;
			end

			profile[keyName] = value;

			-- Put the settings into the database
			addonTable.accountSettings[sectionName] = profile;
		end
	end,

	GetActiveRotation = function(info)
		local Tables = addon.Core.Tables;
		local apiGeneral = addon.API.General;

		local returnValue;

		if info then
			local specInfo = apiGeneral.GetSpecializationInfo();

			if not Tables.IsEmpty(specInfo) then
				local profile = Tables.GetValue(addonTable.characterSettings, "ActiveRotations", {});

				returnValue = Tables.GetValue(profile, specInfo.Name);

				if not returnValue then
					returnValue = getRotationName();
					profile[specInfo.Name] = returnValue;

					-- Put the settings into the database
					addonTable.characterSettings.ActiveRotations = profile;
				end
			end
		end

		return returnValue;
	end,

	SetActiveRotation = function(info, value)
		if info then
			local Rotation = addon.Rotation;

			Rotation.SetActiveRotation(value);
		end
	end,

	GetCharacterValue = function(info)
		local Tables = addon.Core.Tables;
		local returnValue = true;

		if info then
			local sectionName = info[#info - 1];
			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.characterSettings, sectionName, {});

			returnValue = Tables.GetValue(profile, keyName);

			if info.option.type == "toggle" then
				returnValue = addon.Options.Core.ConvertToUI(returnValue, info.option.tristate);
			end
		end

		return returnValue;
	end,

	SetCharacterValue = function(info, value)
		if info then
			local Tables = addon.Core.Tables;

			if info.option.type == "toggle" then
				value = addon.Options.Core.ConvertToValue(value, info.option.tristate);
			end

			local sectionName = info[#info - 1];
			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.characterSettings, sectionName, {});

			profile[keyName] = value;

			-- Put the settings into the database
			addonTable.characterSettings[sectionName] = profile;
		end
	end,

	GetNameplatesValue = function(info)
		local Tables = addon.Core.Tables;
		local returnValue = true;

		if info then
			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.accountSettings, "NameplateSettings", {});

			returnValue = Tables.GetValue(profile, keyName);

			if info.option.type == "toggle" then
				returnValue = addon.Options.Core.ConvertToUI(returnValue, info.option.tristate);
			end
		end

		return returnValue;
	end,

	SetNameplatesValue = function(info, value)
		if info then
			local Tables = addon.Core.Tables;

			if info.option.type == "toggle" then
				value = addon.Options.Core.ConvertToValue(value, info.option.tristate);
			end

			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.accountSettings, "NameplateSettings", {});

			profile[keyName] = value;

			if keyName ~= "nameplateShowAlways" then
				addon.API.Blizzard.SetCVar(keyName, profile[keyName]);
			end

			-- Put the settings into the database
			addonTable.accountSettings.NameplateSettings = profile;
		end
	end,

	GetPluginValue = function(info)
		local Tables = addon.Core.Tables;
		local returnValue = false;

		if info then
			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.characterSettings, "Plugins", {});

			returnValue = Tables.GetValue(profile, keyName);

			if info.option.type == "toggle" then
				returnValue = addon.Options.Core.ConvertToUI(returnValue, info.option.tristate);
			end
		end

		return returnValue;
	end,

	SetPluginValue = function(info, value)
		if info then
			local Tables = addon.Core.Tables;

			if info.option.type == "toggle" then
				value = addon.Options.Core.ConvertToValue(value, info.option.tristate);
			end

			local keyName = info[#info];
			local profile = Tables.GetValue(addonTable.characterSettings, "Plugins", {});

			profile[keyName] = value;

			-- Put the settings into the database
			addonTable.characterSettings.Plugins = profile;
		end
	end,

	GetRotationValue = function(info)
		local Tables = addon.Core.Tables;
		local apiGeneral = addon.API.General;

		local returnValue = true;

		if info then
			local specInfo = apiGeneral.GetSpecializationInfo();

			if not Tables.IsEmpty(specInfo) then
				local keyName = info[#info];
				local profile = Tables.GetValue(addonTable.characterSettings.RotationSettings, specInfo.Name, {});

				returnValue = Tables.GetValue(profile, keyName);

				if info.option.type == "toggle" then
					if returnValue == nil then
						returnValue = (info.option.tristate and 2) or 1;

						profile[keyName] = returnValue;

						-- Put the settings into the database
						addonTable.characterSettings.RotationSettings[specInfo.Name] = profile;
					else
						returnValue = addon.Options.Core.ConvertToUI(returnValue, info.option.tristate);
					end
				else
					returnValue = Tables.GetValue(profile, keyName);
				end
			end
		end

		return returnValue;
	end,

	SetRotationValue = function(info, value)
		if info then
			local Tables = addon.Core.Tables;
			local apiGeneral = addon.API.General;

			if info.option.type == "toggle" then
				value = addon.Options.Core.ConvertToValue(value, info.option.tristate);
			end

			local specInfo = apiGeneral.GetSpecializationInfo();

			if not Tables.IsEmpty(specInfo) then
				local keyName = info[#info];
				local profile = Tables.GetValue(addonTable.characterSettings.RotationSettings, specInfo.Name, {});

				profile[keyName] = value;

				-- Put the settings into the database
				addonTable.characterSettings.RotationSettings[specInfo.Name] = profile;
			end
		end
	end,
};
