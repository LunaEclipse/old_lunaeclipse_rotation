-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local format, huge, max, setmetatable = format, math.huge, max, setmetatable;
local Cache = setmetatable({}, { __mode = "kv" });

addonTable.propertiesCooldown = function(Object)
	local Objects = addon.Core.Objects;

	local objectID = Objects.GetID(Object) or Object;
	local objectType = Objects.GetType(Object) or "None";
	local key = format("%s-%s", objectType, objectID);

	local self = Cache[key] or {};

	local function getCache()
		local Item = addon.API.Item;
		local Spell = addon.API.Spell;

		return Objects.IsSpell(Object) and Spell.GetSpellCooldown(Object.SpellID())
		    or Objects.IsItem(Object) and Item.GetItemCooldown(Object.ItemID());
	end

	-- Returns the base cooldown for the spell or item's on use spell.  Will return 0 if no base cooldown information is available.
	self.Base = function()
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local spellID = Object.SpellID();
		local spellInfo = Spell.GetSpellInfo(spellID);

		return Tables.GetValue(spellInfo, "BaseCooldown", 0);
	end

	-- DEPRECATED
	-- cooldown.foo.down should use !cooldown.foo.up
	self.Down = function()
		return not self.Up();
	end

	self.Duration = function()
		local Tables = addon.Core.Tables;
		local cooldownInfo = getCache();

		return (self.Down() and Tables.GetValue(cooldownInfo, "Duration", 0))
			or 0;
	end

	self.End = function()
		local Tables = addon.Core.Tables;
		local cooldownInfo = getCache();

		return (self.Down() and Tables.GetValue(cooldownInfo, "End", 0))
			or 0;
	end

	self.Locked = function()
		local PotionTracker = addonTable.PotionTracker;
		local Tables = addon.Core.Tables;

		if Objects.IsPotion(Object) then
			return PotionTracker.CombatLocked();
		else
			local cooldownInfo = getCache();

			return Tables.GetValue(cooldownInfo, "Locked", false);
		end
	end

	self.Start = function()
		local Tables = addon.Core.Tables;
		local cooldownInfo = getCache();

		return (self.Down() and Tables.GetValue(cooldownInfo, "Start", 0))
			or 0;
	end

	self.React = function()
		local General = addon.API.General;

		local spellID = Object.SpellID() or 0;
		local reactTime = addonTable.ReactSpells[spellID] or 0;

		return General.GetTime() <= reactTime;
	end

	-- cooldown.foo.ready
	self.Ready = function()
		return self.Up();
	end

	-- cooldown.foo.remains
	self.Remains = function()
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local cooldownInfo = getCache();
		local modRate = Tables.GetValue(cooldownInfo, "ModRate", 1);

		if self.Locked() or (Objects.IsSpell(Object) and Object.SpellID() ~= addonTable.GLOBAL_COOLDOWN and not Object.IsKnown() and not Object.Enabled()) then
			return huge;
		elseif self.End() > 0 then
			return max((self.End() - General.GetTime()) / modRate, 0);
		end

		return 0;
	end

	-- cooldown.foo.up
	self.Up = function()
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local cooldownInfo = getCache();
		local endTime = Tables.GetValue(cooldownInfo, "End", 0);

		return endTime == 0 or (endTime > 0 and General.GetTime() > endTime);
	end

	Cache[key] = self;

	return self;
end