-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local function GetHybridPower()
	local Enum = addonTable.Enum;
	local Tables = addon.Core.Tables;
	local apiGeneral = addon.API.General;

	local returnValue = Enum.DamageType.NONE;
	-- Check to see if we can get character information
	local specInfo = apiGeneral.GetSpecializationInfo();

	if not Tables.IsEmpty(specInfo) then
		local specID = Tables.GetValue(specInfo, "ID");

		if specID == Enum.SpecID.HUNTER_BEASTMASTERY or specID == Enum.SpecID.HUNTER_MARKSMANSHIP then
			returnValue = Enum.DamageType.RANGED;
		elseif specID == Enum.SpecID.PALADIN_PROTECTION or specID == Enum.SpecID.PALADIN_RETRIBUTION
			or specID == Enum.SpecID.SHAMAN_ENHANCEMENT
			or specID == Enum.SpecID.MONK_BREWMASTER or specID == Enum.SpecID.MONK_WINDWALKER
			or specID == Enum.SpecID.HUNTER_SURVIVAL
			or specID == Enum.SpecID.DRUID_FERAL or specID == Enum.SpecID.DRUID_GUARDIAN then
				-- Specialization does physical damage so use attack power.
				returnValue = Enum.DamageType.MELEE;
		elseif specID== Enum.SpecID.PALADIN_HOLY
			or specID == Enum.SpecID.SHAMAN_ELEMENTAL or specID == Enum.SpecID.SHAMAN_RESTORATION
			or specID == Enum.SpecID.MONK_MISTWEAVER
			or specID == Enum.SpecID.DRUID_BALANCE or specID == Enum.SpecID.DRUID_RESTORATION then
				-- Specialization does healing or spell damage so use spell power.
				returnValue = Enum.DamageType.SPELL;
		else
			local error = error;

			-- Not one of the current SpecID's, the LunaEclipse_Rotation may need updating.
			error("LunaEclipse_Rotation did not return a valid SpecializationID, this could be because of a bug, or because the game has been updated with a new specialization!");
		end
	end

	return returnValue;
end

local function GetPowerType()
	local Enum = addonTable.Enum;
	local returnValue = Enum.DamageType.NONE;
	-- Check to see if we can get character information
	local specializationID = addon.API.Blizzard.GetSpecialization();

	if specializationID then
		local _, _, classID = addon.API.Blizzard.UnitClass("player");

		if classID == Enum.ClassID.WARRIOR or classID == Enum.ClassID.ROGUE	or classID == Enum.ClassID.DEATHKNIGHT or classID == Enum.ClassID.DEMONHUNTER then
			-- Warriors, Rogues, Death Knights and Demon Hunters all use melee attack power no matter their specialization or lack of it.
			returnValue = Enum.DamageType.MELEE;
		elseif classID == Enum.ClassID.PRIEST or classID == Enum.ClassID.MAGE or classID == Enum.ClassID.WARLOCK then
			-- Priest, Mage and Warlock all use spell power no matter their specialization or lack of it.
			returnValue = Enum.DamageType.SPELL;
		elseif classID == Enum.ClassID.PALADIN or classID == Enum.ClassID.SHAMAN or classID == Enum.ClassID.MONK or classID == Enum.ClassID.HUNTER or classID == Enum.ClassID.DRUID then
			-- Paladin, Shaman, Monk and Druid are Hybrid classes, these can use either attack power or spell power depending on their specialization.
			-- Hunter is also a Hybrid class because they use either melee or ranged attack power depending on their specialization.
			if addon.API.Blizzard.GetSpecialization() == nil then
				-- The player doesn't have a specialization, so chose melee attack power.
				returnValue = Enum.DamageType.MELEE;
			else
				-- Return either melee attack power or spell power based on specialization.
				returnValue = GetHybridPower();
			end
		else
			local error = error;

			-- Not one of the current ClassID's, the LunaEclipse_Rotation may need updating.
			error("LunaEclipse_Rotation did not return a valid ClassID, this could be because of a bug, or because the game has been updated with a new class!");
		end
	end

	return returnValue;
end

local function GetVersatilityType()
	local Enum = addonTable.Enum;
	local Tables = addon.Core.Tables;
	local apiGeneral = addon.API.General;

	local returnValue = Enum.VersatilityType.DAMAGE_BONUS;
	local specInfo = apiGeneral.GetSpecializationInfo();

	if Tables.GetValue(specInfo, "Role", "") == "TANK" then
		returnValue = Enum.VersatilityType.DAMAGE_REDUCTION;
	end

	return returnValue;
end

-- This is stored on the addon so its publically available to all other addons.
addon.Core.Misc = {
	GetCheckBoxName = function(objectName)
		local gsub, strjoin, strtrim, strupper = gsub, strjoin, strtrim, strupper;
		local returnValue = strupper(objectName);

		returnValue = gsub(returnValue, "[%p%c]", "");
		returnValue = gsub(returnValue, "[0-9.]", "");
		returnValue = strtrim(returnValue);
		returnValue = gsub(returnValue, "[%s+]", "_");

		return strjoin("_", "OPT", returnValue);
	end,

	-- Returns:
	--      critPercent - Returns the players critical strike percent.
	--      critRating - Returns the players critical strike rating.
	--      critBonus - Returns the players critical strike bonus.
	GetCriticalStrike = function()
		local Enum = addonTable.Enum;
		local critPercent, critRating, critBonus;
		-- Check to see if we can get character information
		local specializationID = addon.API.Blizzard.GetSpecialization();

		if specializationID then
			local powerType = GetPowerType();

			if powerType == Enum.DamageType.MELEE then
				local CR_CRIT_MELEE = CR_CRIT_MELEE;

				-- Get the players melee critical strike values.
				critPercent = addon.API.Blizzard.GetCritChance();
				critRating = addon.API.Blizzard.GetCombatRating(CR_CRIT_MELEE);
				critBonus = addon.API.Blizzard.GetCombatRatingBonus(CR_CRIT_MELEE);
			elseif powerType == Enum.DamageType.SPELL then
				local CR_CRIT_SPELL = CR_CRIT_SPELL;

				-- Get the players spell critical strike values.
				critPercent = addon.API.Blizzard.GetSpellCritChance(1);
				critRating = addon.API.Blizzard.GetCombatRating(CR_CRIT_SPELL);
				critBonus = addon.API.Blizzard.GetCombatRatingBonus(CR_CRIT_SPELL);
			elseif powerType == Enum.DamageType.RANGED then
				local CR_CRIT_RANGED = CR_CRIT_RANGED;

				-- Get the players ranged critical strike values.
				critPercent = addon.API.Blizzard.GetRangedCritChance();
				critRating = addon.API.Blizzard.GetCombatRating(CR_CRIT_RANGED);
				critBonus = addon.API.Blizzard.GetCombatRatingBonus(CR_CRIT_RANGED);
			end
		end

		return critPercent, critRating, critBonus;
	end,

	-- Returns:
	--      hastePercent - Returns the players haste percent.
	--      hasteRating - Returns the players haste rating.
	--      hasteBonus - Returns the players haste bonus.
	GetHaste = function()
		local Enum = addonTable.Enum;
		local hastePercent, hasteRating, hasteBonus;
		-- Check to see if we can get character information
		local specializationID = addon.API.Blizzard.GetSpecialization();

		if specializationID then
			local powerType = GetPowerType();

			if powerType == Enum.DamageType.MELEE then
				local CR_HASTE_MELEE = CR_HASTE_MELEE;

				-- Get the players melee haste values.
				hastePercent = addon.API.Blizzard.GetMeleeHaste();
				hasteRating = addon.API.Blizzard.GetCombatRating(CR_HASTE_MELEE);
				hasteBonus = addon.API.Blizzard.GetCombatRatingBonus(CR_HASTE_MELEE);
			elseif powerType == Enum.DamageType.SPELL then
				local CR_HASTE_SPELL = CR_HASTE_SPELL;

				-- Get the players spell haste values.
				hastePercent = addon.API.Blizzard.UnitSpellHaste("player");
				hasteRating = addon.API.Blizzard.GetCombatRating(CR_HASTE_SPELL);
				hasteBonus = addon.API.Blizzard.GetCombatRatingBonus(CR_HASTE_SPELL);
			elseif powerType == Enum.DamageType.RANGED then
				local CR_HASTE_RANGED = CR_HASTE_RANGED;

				-- Get the players ranged haste values.
				hastePercent = addon.API.Blizzard.GetRangedHaste();
				hasteRating = addon.API.Blizzard.GetCombatRating(CR_HASTE_RANGED);
				hasteBonus = addon.API.Blizzard.GetCombatRatingBonus(CR_HASTE_RANGED);
			end
		end

		return hastePercent, hasteRating, hasteBonus;
	end,

	-- Returns:
	--      masteryPercent - Returns the mastery percent for the current specialization.
	--      masteryRating - Returns the mastery rating for the current specialization.
	GetMastery = function()
		-- Return Variables
		local masteryPercent, masteryRating;
		-- Check to see if we can get character information
		local specializationID = addon.API.Blizzard.GetSpecialization();

		if specializationID then
			masteryPercent = addon.API.Blizzard.GetMasteryEffect();
			masteryRating = addon.API.Blizzard.GetMastery();
		end

		return masteryPercent, masteryRating;
	end,

	-- Returns:
	--      powerType - Returns the power type.
	--      power - Returns the players attack or spell power.
	GetPower = function()
		local Enum = addonTable.Enum;
		local powerType, power;
		-- Check to see if we can get character information
		local specializationID = addon.API.Blizzard.GetSpecialization();

		if specializationID then
			powerType = GetPowerType();

			if powerType == Enum.DamageType.MELEE then
				-- Get the players melee attack power values.
				local base, positiveBuff, negativeBuff = addon.API.Blizzard.UnitAttackPower("player");

				power = base + positiveBuff + negativeBuff;
			elseif powerType == Enum.DamageType.SPELL then
				local MAX_SPELL_SCHOOLS, min = MAX_SPELL_SCHOOLS, min;
				-- Get the players spell power values.
				power = addon.API.Blizzard.GetSpellBonusDamage(2);

				for counter = 3, MAX_SPELL_SCHOOLS do
					power = min(power, addon.API.Blizzard.GetSpellBonusDamage(counter));
				end
			elseif powerType == Enum.DamageType.RANGED then
				-- Get the players ranged attack power values.
				local base, positiveBuff, negativeBuff = addon.API.Blizzard.UnitRangedAttackPower("player");

				power = base + positiveBuff + negativeBuff;
			end
		end

		return powerType, power;
	end,

	-- Returns:
	--      damageBonus - Returns the players bonus damage percent from versatility.
	--      damageReduction - Returns the players damage reduction percent from versatility.
	--      versatility - Returns the players versatility rating.
	--      versatilityType - Returns whether the main stat is VERSATILITY_DAMAGE_BONUS, or VERSATILITY_DAMAGE_REDUCTION
	GetVersatility = function()
		-- Return Variables
		local versatility, damageBonus, damageReduction, versatilityType;
		-- Check to see if we can get character information
		local specializationID = addon.API.Blizzard.GetSpecialization();

		if specializationID then
			local CR_VERSATILITY_DAMAGE_DONE, CR_VERSATILITY_DAMAGE_TAKEN = CR_VERSATILITY_DAMAGE_DONE, CR_VERSATILITY_DAMAGE_TAKEN;

			-- Get the players versatility values.
			damageBonus = addon.API.Blizzard.GetCombatRatingBonus(CR_VERSATILITY_DAMAGE_DONE) + addon.API.Blizzard.GetVersatilityBonus(CR_VERSATILITY_DAMAGE_DONE);
			damageReduction = addon.API.Blizzard.GetCombatRatingBonus(CR_VERSATILITY_DAMAGE_TAKEN) + addon.API.Blizzard.GetVersatilityBonus(CR_VERSATILITY_DAMAGE_TAKEN);
			versatility = addon.API.Blizzard.GetCombatRating(CR_VERSATILITY_DAMAGE_DONE);
			versatilityType = GetVersatilityType();
		end

		return damageBonus, damageReduction, versatility, versatilityType;
	end,

	ParseGUID = function(GUID)
		local select, strsplit = select, strsplit;
		local Core = addon.Core.General;
		local Enum = addonTable.Enum;

		local unitType = strsplit("-", GUID);
		local serverID, instanceID, zoneID, playerID, creatureID, spawnID;

		if unitType == Enum.GUIDType.PLAYER then
			serverID, playerID = select(2, strsplit("-", GUID));
		elseif unitType == Enum.GUIDType.VIGNETTE then
			serverID, instanceID, zoneID = select(3, strsplit("-", GUID));
			spawnID = select(7, strsplit("-", GUID));
		else
			serverID, instanceID, zoneID, creatureID, spawnID = select(3, strsplit("-", GUID));
		end

		local returnValue = {
			Type = unitType,
			Server = Core.ToNumber(serverID),
			Instance = Core.ToNumber(instanceID),
			Zone = Core.ToNumber(zoneID),
			Player = playerID,
			Creature = Core.ToNumber(creatureID),
			Spawn = spawnID,
		};

		return returnValue;
	end,
};