-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

addon.Options.addonOptions = {
	type = "group",
	name = "Addon Settings",
	get = addon.Options.Settings.GetAccountValue,
	set = addon.Options.Settings.SetAccountValue,
	childGroups = "tree",
	args = {
		AccountSettings = {
			type = "group",
			name = "Account Settings",
			order = 10,
			args = {
				AddonSettings = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("Addon Options"),
					order = 10,
					inline = true,
					args = {
						refreshInterval = {
							type = "range",
							name = "Refresh Interval",
							desc = "Select minimum time between icon updates, the larger the value the lower the impact on FPS as the addon will do less calculations.\n\nThis is measured in milliseconds.",
							min = 50,
							max = 500,
							step = 25,
							width = "full",
							order = 10,
						},
						reactTime = {
							type = "range",
							name = "Max Reaction Time",
							desc = "Select the maximum amount of time allowed to react to a buff or debuff becoming active, or a cooldown becoming available early as a special condition.\n\nAfter this time has passed handling the buff, debuff or cooldown will be done normally.\n\nThis is measured in milliseconds.",
							min = 250,
							max = 2500,
							step = 50,
							width = "full",
							order = 20,
						},
						useMaxWait = {
							type = "toggle",
							name = "Restrict Wait Time",
							desc = "Check this option to restrict suggestions to abilities that require less then max wait time before they can be used.",
							width = "full",
							order = 30,
						},
						maxWaitTime = {
							type = "range",
							name = "Max Wait Time",
							desc = "Select the maximum amount of time in seconds that an ability can wait to be a valid suggestion.\n\nIf you select 10 seconds, any abilities with over 10 seconds remaining on the cooldown, or will require more than 10 seconds to pool the needed resources will not be suggested.",
							min = 5,
							max = 60,
							step = 1,
							width = "full",
							order = 40,
						},
						enemyCount = {
							type = "select",
							name = "Enemy Count Mode",
							desc = "Select which enemies to count for rotation suggestions.\n\nCounting all enemies will count any enemy whether they are in combat or not.\n\nCounting any enemy in combat will count enemies in combat even if they are just fighting other NPCs.\n\nCounting enemies engaged with your group will only count enemies if you or a group member are on their threat table.",
							values = {
								[0] = "Count all enemies.",
								[1] = "Count any enemies currently in combat.",
								[2] = "Count only enemies currently engaged with your group.",
							},
							width = "full",
							order = 50,
						},
						description = {
							type = "description",
							name = "\nThese settings affect how the addon displays information. Here you can adjust how often those suggestions are updated.\n\nThe lower the Refresh Interval the more impact this addon will have on FPS as it will have to do more calculations.",
							width = "full",
							order = 60,
						},
					},
				},
				IncomingDamageSettings = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("Incoming Damage"),
					order = 20,
					inline = true,
					args = {
						maxHistoryTime = {
							type = "range",
							name = "Maximum Incoming Damage History",
							desc = "Select the maximum time that damage data samples can be kept before being discarded.\n\nThis is measured in seconds.",
							min = 5,
							max = 15,
							step = 1,
							width = "full",
							order = 10,
						},
						description = {
							type = "description",
							name = "These settings affect how long incoming damage data is kept before being deleted.\n\nA longer time kept will even out, reducing the impact of spikes, giving a less responsive value.\n\nA smaller value is recommended for best performance.\n",
							width = "full",
							order = 20,
						},
					},
				},
				TimeToDieSettings = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("Time To Die"),
					order = 30,
					inline = true,
					args = {
						maxHistoryTime = {
							type = "range",
							name = "Maximum Health Data Sample Age",
							desc = "Select the maximum time that health data can be kept before being discarded.\n\nThis is measured in seconds.",
							min = 5,
							max = 60,
							step = 5,
							width = "full",
							order = 10,
						},
						description = {
							type = "description",
							name = "These settings affect how long the health data used for calculating Time to Die can be kept before being purged.\n\nA larger time will give a more accurate estimate for the time to die.\n\nA larger value is recommended for best performance.\n",
							width = "full",
							order = 20,
						},
					},
				},
			},
		},
		CharacterSettings = {
			type = "group",
			name = "Character Settings",
			get = addon.Options.Settings.GetCharacterValue,
			set = addon.Options.Settings.SetCharacterValue,
			order = 20,
			args = {
				CooldownOptions = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("Cooldowns Limitation"),
					order = 10,
					inline = true,
					args = {
						useSolo = {
							type = "toggle",
							name = "Disable restrictions when not in a group",
							desc = "Check this to suggest all spells regardless of cooldown length when not in a group.",
							width = "full",
							order = 10,
						},
						useNormal = {
							type = "toggle",
							name = "Regular Enemies",
							desc = "Check this option to limit cooldown usage against regular enemies.",
							width = "full",
							order = 20,
						},
						useNormalMax = {
							type = "range",
							name = "Regular Enemies Maximum Cooldown Length",
							desc = "Select maximum cooldown length that can be used for regular enemies.",
							min = 15,
							max = 600,
							step = 15,
							width = "full",
							order = 30,
						},
						useElite = {
							type = "toggle",
							name = "Elite Enemies",
							desc = "Check this option to limit cooldown usage against elite enemies.",
							width = "full",
							order = 40,
						},
						useEliteMax = {
							type = "range",
							name = "Elite Enemies Maximum Cooldown Length",
							desc = "Select maximum cooldown length that can be used for elite enemies.",
							min = 15,
							max = 600,
							step = 15,
							width = "full",
							order = 50,
						},
						useDungeonBoss = {
							type = "toggle",
							name = "Dungeon Bosses",
							desc = "Check this option to limit cooldown usage against dungeon bosses.",
							width = "full",
							order = 60,
						},
						useDungeonBossMax = {
							type = "range",
							name = "Dungeon Bosses Maximum Cooldown Length",
							desc = "Select maximum cooldown length that can be used for dungeon bosses.",
							min = 15,
							max = 600,
							step = 15,
							width = "full",
							order = 70,
						},
						usePVP = {
							type = "toggle",
							name = "Other Players (PVP)",
							desc = "Check this option to limit cooldown usage against other players.",
							width = "full",
							order = 80,
						},
						usePVPMax = {
							type = "range",
							name = "Actions against other Players Maximum Cooldown Length",
							desc = "Select maximum cooldown length that can be used for attacking other players.",
							min = 15,
							max = 600,
							step = 15,
							width = "full",
							order = 90,
						},
						description = {
							type = "description",
							name = "\nThe cooldown time in the preceding settings are based on the 'Base' cooldown, before any reductions are made.\n\nAll times for cooldown restrictions are measured in seconds.",
							width = "full",
							order = 100,
						},
					},
				},
				ScriptOptions = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("Script Options"),
					order = 20,
					inline = true,
					args = {
						selectedRotation = {
							type = "select",
							name = "Select Rotation",
							desc = "Select which rotation you wish to use for this specialization from the list.",
							get = addon.Options.Settings.GetActiveRotation,
							set = addon.Options.Settings.SetActiveRotation,
							hidden = function()
								local Tables = addon.Core.Tables;
								local apiGeneral = addon.API.General;

								local specInfo = apiGeneral.GetSpecializationInfo();

								return addon.Rotation.GetRegisteredRotations(Tables.GetValue(specInfo, "ID")) <= 1;
							end,
							values = function()
								local pairs = pairs;
								local Tables = addon.Core.Tables;
								local apiGeneral = addon.API.General;

								local specInfo = apiGeneral.GetSpecializationInfo();
								local returnValue = {
									["__none"] = "No Rotation Selected",
								};

								if not Tables.IsEmpty(specInfo) then
									local numRotations, rotationList = addon.Rotation.GetRegisteredRotations(specInfo.ID);

									if numRotations >= 2 then
										for rotationName, rotationData in pairs(rotationList) do
											returnValue[rotationName] = rotationData.description;
										end
									end
								end

								return returnValue;
							end,
							width = "full",
							order = 10,
						},
						OPT_STEALTH_SOLO = {
							type = "toggle",
							name = "Show: Stealth while solo.",
							desc = "Controls whether stealth abilities will be suggested while not in a group.  This only affects suggestion while in combat!\n\nUsing stealth abilities in combat while solo will cause mobs to reset, restoring them to 100% health.\n\nRecommended: Unchecked.",
							width = "full",
							order = 20,
						},
						OPT_MOVING = {
							type = "toggle",
							name = "Use: Movement Check.",
							desc = "Using this option will enable an extra check to see if you are moving and only recommend spells that can be cast while moving!",
							width = "full",
							order = 30,
						},
						OPT_POTION = {
							type = "toggle",
							name = "Use: Potions.",
							desc = "Suggests when to use potions on the cooldown icon.",
							width = "full",
							order = 40,
						},
						OPT_RANGE_CHECK = {
							type = "toggle",
							name = "Use: Range Check.",
							desc = "Using this option will only show spells where the current enemy is in range.\n\nUnchecking this option will show the recommended spell based on priority regards of whether the target is in range or not!",
							width = "full",
							order = 50,
						},
						textDescription = {
							type = "description",
							name = "",
							width = "full",
							order = 60,
						},
					},
				},
			},
		},
		Nameplates = {
			type = "group",
			name = "Nameplate Settings",
			get = addon.Options.Settings.GetNameplatesValue,
			set = addon.Options.Settings.SetNameplatesValue,
			order = 30,
			args = {
				description = {
					type = "description",
					name = "These settings only change the nameplate settings when not in combat, unless the Show nameplates at all times option is checked.\n\nPlease note that enemy nameplate settings below will always be disregarded in combat as all enemy nameplates must be enabled during combat for the addon to work correctly.\n\nIf you use a nameplate addon such as TidyPlates please ensure that you disable any automatc setting changes based on combat otherwise this addon will not process the rotations correctly and provide incorrect suggestions!\n",
					width = "full",
					order = 10,
				},
				NameplateSettings = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("General"),
					order = 20,
					inline = true,
					args = {
						nameplateShowAlways = {
							type = "toggle",
							name = "Show nameplates at all times.",
							desc = "Check this to always show all selected nameplates based on the settings below whether in combat or out of combat.\n\nPlease note that enemy nameplate settings below will always be disregarded in combat as all enemy nameplates must be enabled during combat for the addon to work correctly.",
							width = "full",
							order = 10,
						},
						nameplateMaxDistance = {
							type = "range",
							name = "Maximum Range",
							desc = "Select the maximum range an enemy can be to have a nameplate shown.",
							min = 20,
							max = 100,
							step = 1,
							width = "full",
							order = 20,
						},
					},
				},
				FriendSettings = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("Friendly Units"),
					order = 30,
					inline = true,
					args = {
						nameplateShowFriends = {
							type = "toggle",
							name = "Show friendly nameplates.",
							desc = "Check this option to show friendly unit nameplates.",
							width = "full",
							order = 10,
						},
						nameplateShowFriendlyGuardians = {
							type = "toggle",
							name = "Show friendly guardian nameplates.",
							desc = "Check this option to show nameplates for guardians of friendly units.",
							width = "full",
							order = 20,
						},
						nameplateShowFriendlyMinions = {
							type = "toggle",
							name = "Show friendly minion nameplates.",
							desc = "Check this option to show nameplates for minions of friendly units.",
							width = "full",
							order = 30,
						},
						nameplateShowFriendlyNPCs = {
							type = "toggle",
							name = "Show friendly NPC nameplates.",
							desc = "Check this option to show nameplates for friendly NPCs.",
							width = "full",
							order = 40,
						},
						nameplateShowFriendlyPets = {
							type = "toggle",
							name = "Show friendly pet nameplates.",
							desc = "Check this option to show nameplates for pets of friendly units.",
							width = "full",
							order = 50,
						},
						nameplateShowFriendlyTotems = {
							type = "toggle",
							name = "Show friendly totem nameplates.",
							desc = "Check this option to show nameplates for totems of friendly units.",
							width = "full",
							order = 60,
						},
					},
				},
				EnemySettings = {
					type = "group",
					name = addon.Options.Core.CreaterHeader("Hostile Units"),
					order = 40,
					inline = true,
					args = {
						nameplateShowEnemies = {
							type = "toggle",
							name = "Show hostile nameplates.",
							desc = "Check this option to show hostile unit nameplates.",
							width = "full",
							order = 10,
						},
						nameplateShowEnemyGuardians = {
							type = "toggle",
							name = "Show hostile guardian nameplates.",
							desc = "Check this option to show nameplates for guardians of hostile units.",
							width = "full",
							order = 20,
						},
						nameplateShowEnemyMinions = {
							type = "toggle",
							name = "Show hostile minion nameplates.",
							desc = "Check this option to show nameplates for minions of hostile units.",
							width = "full",
							order = 30,
						},
						nameplateShowEnemyMinus = {
							type = "toggle",
							name = "Show minor hostile nameplates.",
							desc = "Check this option to show nameplates for minor hostile units.",
							width = "full",
							order = 40,
						},
						nameplateShowEnemyPets = {
							type = "toggle",
							name = "Show hostile pet nameplates.",
							desc = "Check this option to show nameplates for pets of hostile units.",
							width = "full",
							order = 50,
						},
						nameplateShowEnemyTotems = {
							type = "toggle",
							name = "Show hostile totem nameplates.",
							desc = "Check this option to show nameplates for totems of hostile units.",
							width = "full",
							order = 60,
						},
					},
				},
			},
		},
	},
};