-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local gsub, setmetatable, strtrim = gsub, setmetatable, strtrim;
local Cache = setmetatable({}, { __mode = "kv" });

addonTable.classAzerite = function(ID)
	local spellID = ID;
	local self = Cache[ID] or {};

	self.ClassID = function()
		return spellID;
	end

	self.ClassType = function()
		return "Azerite";
	end

	self.Enabled = function()
		return addonTable.AzeritePowers.Enabled(spellID);
	end

	self.Name = function()
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local spellInfo = Spell.GetSpellInfo(spellID);
		local returnValue = Tables.GetValue(spellInfo, "Name", "");

		returnValue = gsub(returnValue, "[0-9.]", "");
		returnValue = strtrim(returnValue);

		return returnValue;
	end

	self.Rank = function()
		return addonTable.AzeritePowers.Rank(spellID);
	end

	self.SpellID = function()
		return spellID;
	end

	Cache[ID] = self;

	return self;
end