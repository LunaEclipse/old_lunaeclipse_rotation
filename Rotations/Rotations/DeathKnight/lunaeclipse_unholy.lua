local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local Summons = addonTable.Summons;

-- Objects
local Pet = addon.Units.Pet;
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

-- Rotation Variables
local nameAPL = "lunaeclipse_deathknight_unholy";

-- Creature IDs for Undead Tracker
local Guardians = {
	Apocalypse = 999999, -- Fake creatureID to seperate Army of the Dead Ghouls from Apocalypse Ghouls
	ArmyOfTheDead = 24207,
	Gargoyle = 27829,
};

-- AOE Rotation
local function AOE(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- SimCraft can interpret a 0 times remains as false, LUA can't so use Down() which returns a boolean based on if time remains is 0.
		-- actions.aoe+=/clawing_shadows,if=death_and_decay.ticking&cooldown.apocalypse.remains
		ClawingShadows = function()
			return Player.Buff(Aura.DeathAndDecay).Ticking()
			   and Spell.Apocalypse.Cooldown.Down();
		end,

		-- SimCraft can interpret a 0 times remains as false, LUA can't so use Down() which returns a boolean based on if time remains is 0.
		-- actions.aoe=death_and_decay,if=cooldown.apocalypse.remains
		DeathAndDecay = function()
			return Spell.Apocalypse.Cooldown.Down();
		end,

		DeathCoil = {
			-- actions.aoe+=/death_coil,if=death_and_decay.ticking&rune<2&!talent.epidemic.enabled&!variable.pooling_for_gargoyle
			DeathAndDecay = function()
				return Player.Buff(Aura.DeathAndDecay).Ticking()
				   and Player.Runes() < 2
				   and not Talent.Epidemic.Enabled()
				   and not Variables.pooling_for_gargoyle;
			end,

			-- actions.aoe+=/death_coil,if=buff.sudden_doom.react&rune.deficit>=4
			Use = function()
				return Player.Buff(Aura.SuddenDoom).React()
				   and Player.Runes.Deficit() >= 4;
			end,
		},

		Epidemic = {
			-- actions.aoe+=/epidemic,if=death_and_decay.ticking&rune<2&!variable.pooling_for_gargoyle
			DeathAndDecay = function()
				return Player.Buff(Aura.DeathAndDecay).Ticking()
				   and Player.Runes() < 2
				   and not Variables.pooling_for_gargoyle;
			end,

			-- actions.aoe+=/epidemic,if=!variable.pooling_for_gargoyle
			Use = function()
				return not Variables.pooling_for_gargoyle;
			end,
		},

		-- actions.aoe+=/festering_strike,if=talent.bursting_sores.enabled&spell_targets.bursting_sores>=2&debuff.festering_wound.stack<=1
		FesteringStrike = function(numEnemies)
			return Talent.BurstingSores.Enabled()
			   and numEnemies >= 2
			   and Target.Debuff(Aura.FesteringWound).Stack() <= 1;
		end,

		-- SimCraft can interpret a 0 times remains as false, LUA can't so use Down() which returns a boolean based on if time remains is 0.
		-- actions.aoe+=/scourge_strike,if=death_and_decay.ticking&cooldown.apocalypse.remains
		ScourgeStrike = function()
			return Player.Buff(Aura.DeathAndDecay).Ticking()
			   and Spell.Apocalypse.Cooldown.Down();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.DeathAndDecay, self.Requirements.DeathAndDecay);
		-- actions.aoe+=/defile
		action.EvaluateAction(Talent.Defile, true);
		action.EvaluateAction(Talent.Epidemic, self.Requirements.Epidemic.DeathAndDecay);
		action.EvaluateAction(Spell.DeathCoil, self.Requirements.DeathCoil.DeathAndDecay);
		action.EvaluateAction(Spell.ScourgeStrike, self.Requirements.ScourgeStrike);
		action.EvaluateAction(Talent.ClawingShadows, self.Requirements.ClawingShadows);
		action.EvaluateAction(Talent.Epidemic, self.Requirements.Epidemic.Use);
		action.EvaluateAction(Spell.FesteringStrike, self.Requirements.FesteringStrike, Enemies.GetEnemies(8));
		action.EvaluateAction(Spell.DeathCoil, self.Requirements.DeathCoil.Use);
	end

	-- actions+=/call_action_list,name=aoe,if=active_enemies>=2
	function self.Use(numEnemies)
		return numEnemies >= 2;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationAOE = AOE("AOE");

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cooldowns+=/apocalypse,if=debuff.festering_wound.stack>=4
		Apocalypse = function()
			return Target.Debuff(Aura.FesteringWound).Stack() >= 4;
		end,

		-- actions.cooldowns+=/soul_reaper,target_if=(target.time_to_die<8|rune<=2)&!buff.unholy_frenzy.up
		SoulReaper = function(numEnemies, Target)
			return (Target.TimeToDie() < 8 or Player.Runes() <= 2)
			   and not Player.Buff(Aura.UnholyFrenzy).Up();
		end,

		-- actions.cooldowns+=/summon_gargoyle,if=runic_power.deficit<14
		SummonGargoyle = function()
			return Player.RunicPower.Deficit() < 14;
		end,

		UnholyFrenzy = {
			-- actions.cooldowns+=/unholy_frenzy,if=active_enemies>=2&((cooldown.death_and_decay.remains<=gcd&!talent.defile.enabled)|(cooldown.defile.remains<=gcd&talent.defile.enabled))
			AOE = function(numEnemies)
				return numEnemies >= 2
				   and ((Spell.DeathAndDecay.Cooldown.Remains() <= Player.GCD() and not Talent.Defile.Enabled()) or (Talent.Defile.Cooldown.Remains() <= Player.GCD() and Talent.Defile.Enabled()));
			end,

			-- actions.cooldowns+=/unholy_frenzy,if=debuff.festering_wound.stack<4
			Use = function()
				return Target.Debuff(Aura.FesteringWound).Stack() < 4;
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.cooldowns+=/army_of_the_dead
		action.EvaluateAction(Spell.ArmyOfTheDead, true);
		action.EvaluateAction(Spell.Apocalypse, self.Requirements.Apocalypse);
		-- Legendaries will become stat sticks after level 115, so just skip the legendary should check which means this should be suggested without a requirements check.
		-- actions.cooldowns+=/dark_transformation,if=(equipped.137075&cooldown.summon_gargoyle.remains>40)|(!equipped.137075|!talent.summon_gargoyle.enabled)
		action.EvaluateAction(Spell.DarkTransformation, true);
		action.EvaluateAction(Talent.SummonGargoyle, self.Requirements.SummonGargoyle);
		action.EvaluateAction(Talent.UnholyFrenzy, self.Requirements.UnholyFrenzy.Use);
		action.EvaluateAction(Talent.UnholyFrenzy, self.Requirements.UnholyFrenzy.AOE);
		action.EvaluateCycleAction(Talent.SoulReaper, self.Requirements.SoulReaper);
		-- actions.cooldowns+=/unholy_blight
		action.EvaluateAction(Talent.UnholyBlight, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Generic Rotation
local function Generic(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.generic+=/clawing_shadows,if=((debuff.festering_wound.up&cooldown.apocalypse.remains>5)|debuff.festering_wound.stack>4)&cooldown.army_of_the_dead.remains>5
		ClawingShadows = function()
			return ((Target.Debuff(Aura.FesteringWound).Up() and Spell.Apocalypse.Cooldown.Remains() > 5) or Target.Debuff(Aura.FesteringWound).Stack() > 4)
			   and Spell.ArmyOfTheDead.Cooldown.Remains() > 5;
		end,

		-- SimCraft can interpret a 0 times remains as false, LUA can't so use Down() which returns a boolean based on if time remains is 0.
		-- actions.generic+=/death_and_decay,if=talent.pestilence.enabled&cooldown.apocalypse.remains
		DeathAndDecay = function()
			return Talent.Pestilence.Enabled()
			   and Spell.Apocalypse.Cooldown.Down();
		end,

		DeathCoil = {
			-- actions.generic+=/death_coil,if=runic_power.deficit<20&!variable.pooling_for_gargoyle
			HighRunicPower = function()
				return Player.RunicPower.Deficit() < 20
				   and not Variables.pooling_for_gargoyle;
			end,

			-- actions.generic+=/death_coil,if=runic_power.deficit<14&(cooldown.apocalypse.remains>5|debuff.festering_wound.stack>4)&!variable.pooling_for_gargoyle
			RunicPower = function()
				return Player.RunicPower.Deficit() < 14
				   and (Spell.Apocalypse.Cooldown.Remains() > 5 or Target.Debuff(Aura.FesteringWound).Stack() > 4)
				   and not Variables.pooling_for_gargoyle;
			end,

			-- actions.generic=death_coil,if=buff.sudden_doom.react&!variable.pooling_for_gargoyle|pet.gargoyle.active
			SuddenDoom = function()
				return Player.Buff(Aura.SuddenDoom).React()
				   and not Variables.pooling_for_gargoyle
				    or Summons.Active(Guardians.Gargoyle);
			end,

			-- actions.generic+=/death_coil,if=!variable.pooling_for_gargoyle
			Use = function()
				return not Variables.pooling_for_gargoyle;
			end,
		},

		-- SimCraft can interpret a 0 times remains as false, LUA can't so use Down() which returns a boolean based on if time remains is 0.
		-- actions.generic+=/defile,if=cooldown.apocalypse.remains
		Defile = function()
			return Spell.Apocalypse.Cooldown.Down();
		end,

		-- actions.generic+=/festering_strike,if=((((debuff.festering_wound.stack<4&!buff.unholy_frenzy.up)|debuff.festering_wound.stack<3)&cooldown.apocalypse.remains<3)|debuff.festering_wound.stack<1)&cooldown.army_of_the_dead.remains>5
		FesteringStrike = function()
			return ((((Target.Debuff(Aura.FesteringWound).Stack() < 4 and not Player.Buff(Aura.UnholyFrenzy).Up()) or Target.Debuff(Aura.FesteringWound).Stack() < 3) and Spell.Apocalypse.Cooldown.Remains() < 3) or Target.Debuff(Aura.FesteringWound).Stack() < 1)
			   and Spell.ArmyOfTheDead.Cooldown.Remains() > 5;
		end,

		-- actions.generic+=/scourge_strike,if=((debuff.festering_wound.up&cooldown.apocalypse.remains>5)|debuff.festering_wound.stack>4)&cooldown.army_of_the_dead.remains>5
		ScourgeStrike = function()
			return ((Target.Debuff(Aura.FesteringWound).Up() and Spell.Apocalypse.Cooldown.Remains() > 5) or Target.Debuff(Aura.FesteringWound).Stack() > 4)
			   and Spell.ArmyOfTheDead.Cooldown.Remains() > 5;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.DeathCoil, self.Requirements.DeathCoil.SuddenDoom);
		action.EvaluateAction(Spell.DeathCoil, self.Requirements.DeathCoil.RunicPower);
		action.EvaluateAction(Spell.DeathAndDecay, self.Requirements.DeathAndDecay);
		action.EvaluateAction(Talent.Defile, self.Requirements.Defile);
		action.EvaluateAction(Spell.ScourgeStrike, self.Requirements.ScourgeStrike);
		action.EvaluateAction(Talent.ClawingShadows, self.Requirements.ClawingShadows);
		action.EvaluateAction(Spell.DeathCoil, self.Requirements.DeathCoil.HighRunicPower);
		action.EvaluateAction(Spell.FesteringStrike, self.Requirements.FesteringStrike);
		action.EvaluateAction(Spell.DeathCoil, self.Requirements.DeathCoil.Use);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationGeneric = Generic("Generic");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Veiss Versa and SimCraft",
		GuideLink = "http://www.wowhead.com/unholy-death-knight-talent-guide",
		WoWVersion = 80001,
	};

	self.Defensives = {
		AntiMagicShell = function()
			return Player.MagicDamagePredicted(3) >= 30;
		end,

		DeathPact = function()
			return Talent.DeathPact.Enabled()
			   and Player.Health.Percent() < 50;
		end,

		DeathStrike = function()
			return Player.Buff(Aura.DarkSuccor).Up()
			   and Player.Health.Percent() < 90;
		end,

		IceboundFortitude = function()
			return Player.DamagePredicted(4) >= 25;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/arcane_torrent,if=runic_power.deficit>65&(pet.gargoyle.active|!talent.summon_gargoyle.enabled)&rune.deficit>=5
		ArcaneTorrent = function()
			return Player.RunicPower.Deficit() > 65
			   and (Summons.Active(Guardians.Gargoyle) or not Talent.SummonGargoyle.Enabled())
			   and Player.Runes.Deficit() >= 5;
		end,

		-- actions+=/berserking,if=pet.gargoyle.active|!talent.summon_gargoyle.enabled
		Berserking = function()
			return Summons.Active(Guardians.Gargoyle)
			    or not Talent.SummonGargoyle.Enabled();
		end,

		-- actions+=/blood_fury,if=pet.gargoyle.active|!talent.summon_gargoyle.enabled
		BloodFury = function()
			return Summons.Active(Guardians.Gargoyle)
			    or not Talent.SummonGargoyle.Enabled();
		end,

		-- The addon doesn't do tick time for auras yet so we need to skip this part
		-- actions+=/outbreak,target_if=(dot.virulent_plague.tick_time_remains+tick_time<=dot.virulent_plague.remains)&dot.virulent_plague.remains<=gcd
		Outbreak = function()
			return Target.Debuff(Aura.VirulentPlague).Remains() <= Player.GCD();
		end,

		-- actions+=/potion,if=cooldown.army_of_the_dead.ready|pet.gargoyle.active|buff.unholy_frenzy.up
		ProlongedPower = function()
			return Spell.ArmyOfTheDead.Cooldown.Ready()
			    or Summons.Active(Guardians.Gargoyle)
			    or Player.Buff(Aura.UnholyFrenzy).Up();
		end,

		-- SimCraft doesn't mention it, but only suggest this if the player doesn't already have a pet summoned.
		-- actions.precombat+=/raise_dead
		RaiseDead = function()
			return not Pet.IsActive();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			ProlongedPower = Objects.newItem(142117),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.GiftOfTheNaaru, Racial.Shadowmeld);
		config.AOEOptions(options, Spell.DeathAndDecay, Talent.Defile, Talent.Epidemic);
		config.CooldownOptions(options, Spell.Apocalypse, Spell.ArmyOfTheDead, Spell.DarkTransformation, Talent.SoulReaper, Talent.SummonGargoyle, Talent.UnholyBlight, Talent.UnholyFrenzy);
		config.DefensiveOptions(options, Spell.AntiMagicShell, Talent.DeathPact, Spell.IceboundFortitude, Spell.DeathStrike);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against magic damage
		action.EvaluateDefensiveAction(Spell.AntiMagicShell, self.Defensives.AntiMagicShell);

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.IceboundFortitude, self.Defensives.IceboundFortitude);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Talent.DeathPact, self.Defensives.DeathPact);
		action.EvaluateDefensiveAction(Spell.DeathStrike, self.Defensives.DeathStrike);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.ProlongedPower, true);
		action.EvaluateAction(Spell.RaiseDead, self.Requirements.RaiseDead);
		-- actions.precombat+=/army_of_the_dead
		action.EvaluateAction(Spell.ArmyOfTheDead, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- Legendaries will become stat sticks after level 115, so just skip the legendary should check.
		-- actions+=/variable,name=pooling_for_gargoyle,value=(cooldown.summon_gargoyle.remains<5&(cooldown.dark_transformation.remains<5|!equipped.137075))&talent.summon_gargoyle.enabled
		Variables.pooling_for_gargoyle = (Talent.SummonGargoyle.Cooldown.Remains() < 5 and Talent.SummonGargoyle.Enabled());

		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
		action.EvaluateAction(Consumable.ProlongedPower, self.Requirements.ProlongedPower);
		action.EvaluateAction(Spell.Outbreak, self.Requirements.Outbreak);

		-- actions+=/call_action_list,name=cooldowns
		action.CallActionList(rotationCooldowns);
		action.CallActionList(rotationAOE);
		-- actions+=/call_action_list,name=generic
		action.CallActionList(rotationGeneric);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Unholy Death Knight", addonTable.Enum.SpecID.DEATHKNIGHT_UNHOLY);