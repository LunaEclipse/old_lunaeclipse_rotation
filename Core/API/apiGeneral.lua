-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- This is stored on the addon so its publically available to all other addons.
addon.API.General = {
	GetInstanceInfo = function()
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local returnValue = Cache.Persistant.Player.InstanceInfo;

		if Tables.IsEmpty(returnValue) then
			local instanceName, instanceType, instanceDifficultyID, instanceDifficultyName, instanceMaxPlayers, instanceDynamicDifficulty, instanceIsDynamic, instanceMapID, instanceGroupSize = addon.API.Blizzard.GetInstanceInfo();

			if instanceMapID then
				returnValue = {
					Name = instanceName,
					Type = instanceType,
					DifficultyID = instanceDifficultyID,
					DifficultyName = instanceDifficultyName,
					MaxPlayers = instanceMaxPlayers,
					DynamicDifficulty = instanceDynamicDifficulty,
					IsDynamic = instanceIsDynamic,
					MapID = instanceMapID,
					GroupSize = instanceGroupSize,
				};

				Cache.Persistant.Player.InstanceInfo = returnValue;
			end
		end

		return returnValue;
	end,

	GetNetStats = function(ignoreCache)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local returnValue = Cache.Persistant.NetStats;
		local currentTime = addon.API.Blizzard.GetTime();

		if ignoreCache or Tables.IsEmpty(returnValue) then
			local bandwidthIn, bandwidthOut, latencyHome, latencyWorld = addon.API.Blizzard.GetNetStats();

			if latencyWorld then
				returnValue = {
					Download = bandwidthIn,
					Upload = bandwidthOut,
					HomeLatency = latencyHome,
					WorldLatency = latencyWorld,
					UpdateTime = currentTime,
				};

				if not ignoreCache then
					Cache.Persistant.NetStats = returnValue;
				end
			end
		end

		return returnValue;
	end,

	GetSpecializationInfo = function(ignoreCache, specIndex)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local returnValue = Cache.Persistant.Player.Specialization;

		if ignoreCache or specIndex or Tables.IsEmpty(returnValue) then
			specIndex = specIndex or addon.API.Blizzard.GetSpecialization();

			if specIndex then
				local specID, specName, specDescription, specIcon, specBackground, specRole, specPrimaryStat = addon.API.Blizzard.GetSpecializationInfo(specIndex);

				if specID then
					returnValue = {
						Index = specIndex,
						ID = specID,
						Name = specName,
						Description = specDescription,
						IconID = specIcon,
						Background = specBackground,
						Role = specRole,
						PrimaryStat = specPrimaryStat,
						TotalSpecializations = addon.API.Blizzard.GetNumSpecializations(),
					};
				end
			end
		end

		return returnValue;
	end,

	GetTime = function()
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local cacheTime = Tables.GetValue(Cache, "CurrentTime", 0);
		local currentTime = addon.API.Blizzard.GetTime();

		-- Update the cache if it needs to be updated with the current time.
		if currentTime - cacheTime >= addon.refreshInterval then
			cacheTime = currentTime;

			Cache.CurrentTime = cacheTime;
		end

		-- Return the time the addon will use, if there is an evaluation time, return it, otherwise return the cached time.
		return cacheTime;
	end,
};