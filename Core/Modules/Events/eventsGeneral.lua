-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local function pullTimer(...)
	local strfind, strsub = strfind, strsub;
	local Core = addon.Core.General;

	local _, prefix, message = ...;
	local startTime = addon.API.Blizzard.GetTime();
	local duration;

	if prefix == "D4" and strfind(message, "PT") then
		duration = Core.ToNumber(strsub(message, 4, 5));
	elseif prefix == "BigWigs" and strfind(message, "Pull") then
		duration = Core.ToNumber(strsub(message, 8, 9));
	end

	if duration then
		addonTable.PullTimer = {
			Start = startTime,
			Duration = duration,
			End = startTime + duration,
		};
	end
end

local moduleEvents = {
	playerLogin = function()
		local Cache = addonTable.Cache;
		local apiUnit = addon.API.Unit;

		-- Refresh Player
		Cache.Persistant.Player.Race = apiUnit.UnitRace();
		Cache.Persistant.Player.Class = apiUnit.UnitClass();
	end,

	enterCombat = function()
		addonTable.Combat = {
			Start = addon.API.Blizzard.GetTime(),
			End = 0,
		};
	end,

	leaveCombat = function()
		addonTable.Combat = {
			Start = 0,
			End = addon.API.Blizzard.GetTime(),
		};
	end,
};

local tableEvents = {
	CHAT_MSG_ADDON = pullTimer,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);