local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local val = coreGeneral.ToNumber;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

-- Rotation Variables
local nameAPL = "lunaeclipse_rogue_outlaw";

local function rtb_buffs()
	return val(Player.Buff(Aura.Broadside).Up()) + val(Player.Buff(Aura.BuriedTreasure).Up()) + val(Player.Buff(Aura.GrandMelee).Up()) + val(Player.Buff(Aura.SkullAndCrossbones).Up()) + val(Player.Buff(Aura.TrueBearing).Up()) + val(Player.Buff(Aura.RuthlessPrecision).Up());
end

local function rtb_remaining()
	return (Player.Buff(Aura.Broadside).Up() and Player.Buff(Aura.Broadside).Remains())
	    or (Player.Buff(Aura.BuriedTreasure).Up() and Player.Buff(Aura.BuriedTreasure).Remains())
	    or (Player.Buff(Aura.GrandMelee).Up() and Player.Buff(Aura.GrandMelee).Remains())
	    or (Player.Buff(Aura.SkullAndCrossbones).Up() and Player.Buff(Aura.SkullAndCrossbones).Remains())
	    or (Player.Buff(Aura.TrueBearing).Up() and Player.Buff(Aura.TrueBearing).Remains())
	    or (Player.Buff(Aura.RuthlessPrecision).Up() and Player.Buff(Aura.RuthlessPrecision).Remains())
	    or 0;
end

-- Builders Rotation
local function Builders(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.build=pistol_shot,if=combo_points.deficit>=1+buff.broadside.up+talent.quick_draw.enabled&buff.opportunity.up
		PistolShot = function()
			return Player.ComboPoints.Deficit() >= 1 + val(Player.Buff(Aura.Broadside).Up()) + val(Talent.QuickDraw.Enabled())
			   and Player.Buff(Aura.Opportunity).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.PistolShot, self.Requirements.PistolShot);
		-- actions.build+=/sinister_strike
		action.EvaluateAction(Spell.SinisterStrike, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationBuilders = Builders("Builders");

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cds+=/adrenaline_rush,if=!buff.adrenaline_rush.up&energy.time_to_max>1
		AdrenalineRush = function()
			return not Player.Buff(Aura.AdrenalineRush).Up()
			   and Player.Energy.TimeToMax() > 1;
		end,

		-- actions.cds=potion,if=buff.bloodlust.react|target.time_to_die<=60|buff.adrenaline_rush.up
		BattlePotionOfAgility = function()
			return Player.HasBloodlust()
			    or Target.TimeToDie() <= 60
			    or Player.Buff(Aura.AdrenalineRush).Up();
		end,

		-- We can't do raid add events so just skip these and use on max charges to keep it recharging.
		-- actions.cds+=/blade_flurry,if=spell_targets>=2&!buff.blade_flurry.up&(!raid_event.adds.exists|raid_event.adds.remains>8|cooldown.blade_flurry.charges=1&raid_event.adds.in>(2-cooldown.blade_flurry.charges_fractional)*25)
		BladeFlurry = function(numEnemies)
			return numEnemies >= 2
			   and not Player.Buff(Aura.BladeFlurry).Up()
			   and Spell.BladeFlurry.Charges() == 2;
		end,

		-- actions.cds+=/blade_rush,if=variable.blade_flurry_sync&energy.time_to_max>1
		BladeRush = function()
			return Variables.blade_flurry_sync
			   and Player.Energy.TimeToMax() > 1;
		end,

		-- actions.cds+=/ghostly_strike,if=variable.blade_flurry_sync&combo_points.deficit>=1+buff.broadside.up
		GhostlyStrike = function()
			return Variables.blade_flurry_sync
			   and Player.ComboPoints.Deficit() >= 1 + val(Player.Buff(Aura.Broadside).Up());
		end,

		-- actions.cds+=/killing_spree,if=variable.blade_flurry_sync&(energy.time_to_max>5|energy<15)
		KillingSpree = function()
			return Variables.blade_flurry_sync
			   and (Player.Energy.TimeToMax() > 5 or Player.Energy() < 15);
		end,

		-- We can't do raid add events so just skip this.
		-- actions.cds+=/marked_for_death,target_if=min:target.time_to_die,if=target.time_to_die<combo_points.deficit|((raid_event.adds.in>40|buff.true_bearing.remains>15-buff.adrenaline_rush.up*5)&!stealthed.rogue&combo_points.deficit>=cp_max_spend-1)
		MarkedForDeath = function(numEnemies, Target)
			return Target.TimeToDie() < Player.ComboPoints.Deficit()
			    or (Player.Buff(Aura.TrueBearing).Remains() > 15 - val(Player.Buff(Aura.AdrenalineRush).Up()) * 5 and not Player.IsStealthed() and Player.ComboPoints.Deficit() >= Player.ComboPoints.Max() - 1);
		end,

		-- actions.cds+=/shadowmeld,if=!stealthed.all&variable.ambush_condition
		Shadowmeld = function()
			return not Player.IsStealthed()
			   and Variables.ambush_condition;
		end,

		-- actions.cds+=/vanish,if=!stealthed.all&variable.ambush_condition
		Vanish = function()
			return not Player.IsStealthed()
			   and Variables.ambush_condition;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Consumable.BattlePotionOfAgility, self.Requirements.BattlePotionOfAgility);
		-- actions.cds+=/blood_fury
		action.EvaluateAction(Racial.BloodFury, true);
		-- actions.cds+=/berserking
		action.EvaluateAction(Racial.Berserking, true);
		-- actions.cds+=/fireblood
		action.EvaluateAction(Racial.Fireblood, true);
		-- actions.cds+=/ancestral_call
		action.EvaluateAction(Racial.AncestralCall, true);
		action.EvaluateAction(Spell.AdrenalineRush, self.Requirements.AdrenalineRush);
		action.EvaluateCycleAction(Talent.MarkedForDeath, self.Requirements.MarkedForDeath);
		action.EvaluateAction(Spell.BladeFlurry, self.Requirements.BladeFlurry, Enemies.GetEnemies(5));
		action.EvaluateAction(Talent.GhostlyStrike, self.Requirements.GhostlyStrike);
		action.EvaluateAction(Talent.KillingSpree, self.Requirements.KillingSpree);
		action.EvaluateAction(Talent.BladeRush, self.Requirements.BladeRush);
		action.EvaluateAction(Spell.Vanish, self.Requirements.Vanish);
		action.EvaluateAction(Racial.Shadowmeld, self.Requirements.Shadowmeld);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Finishers Rotation
local function Finishers(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.finish+=/between_the_eyes,if=buff.ruthless_precision.up|azerite.ace_up_your_sleeve.enabled|azerite.deadshot.enabled
		BetweenTheEyes = function()
			return Player.Buff(Aura.RuthlessPrecision).Up()
				or Azerite.AceUpYourSleeve.Enabled()
			    or Azerite.Deadshot.Enabled();
		end,

		-- actions.finish+=/roll_the_bones,if=(buff.roll_the_bones.remains<=3|variable.rtb_reroll)&(target.time_to_die>20|buff.roll_the_bones.remains<target.time_to_die)
		RollTheBones = function()
			return (rtb_remaining() <= 3 or Variables.rtb_reroll)
			   and (Target.TimeToDie() > 20 or rtb_remaining() < Target.TimeToDie());
		end,

		-- actions.finish=slice_and_dice,if=buff.slice_and_dice.remains<target.time_to_die&buff.slice_and_dice.remains<(1+combo_points)*1.8
		SliceAndDice = function()
			return Player.Buff(Aura.SliceAndDice).Remains() < Target.TimeToDie()
			   and Player.Buff(Aura.SliceAndDice).Remains() < (1 + Player.ComboPoints()) * 1.8;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.SliceAndDice, self.Requirements.SliceAndDice);
		action.EvaluateAction(Spell.RollTheBones, self.Requirements.RollTheBones);
		action.EvaluateAction(Spell.BetweenTheEyes, self.Requirements.BetweenTheEyes);
		-- actions.finish+=/dispatch
		action.EvaluateAction(Spell.Dispatch, true);
	end

	-- actions+=/call_action_list,name=finish,if=combo_points>=cp_max_spend-(buff.broadside.up+buff.opportunity.up)*(talent.quick_draw.enabled&(!talent.marked_for_death.enabled|cooldown.marked_for_death.remains>1))
	function self.Use()
		return Player.ComboPoints() >= Player.ComboPoints.Max() - (val(Player.Buff(Aura.Broadside).Up()) + val(Player.Buff(Aura.Opportunity).Up())) * val(Talent.QuickDraw.Enabled() and (not Talent.MarkedForDeath.Enabled() or Talent.MarkedForDeath.Cooldown.Remains() > 1));
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationFinishers = Finishers("Finishers");

-- Stealth Rotation
local function Stealth(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.stealth=ambush
		action.EvaluateAction(Spell.Ambush, true);
	end

	-- actions+=/call_action_list,name=stealth,if=stealthed.all
	function self.Use(numEnemies)
		return Player.IsStealthed();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationStealth = Stealth("Stealth");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "GrayHound and SimCraft",
		GuideLink = "https://www.wowhead.com/outlaw-rogue-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		CloakOfShadows = function()
			return Player.MagicDamagePredicted(5) >= 60;
		end,

		CrimsonVial = function()
			return Player.Health.Percent() < 70
			   and Player.DamagePredicted(5) >= 25;
		end,

		Riposte = function()
			return Player.PhysicalDamagePredicted(5) >= 60;
		end,

		Feint = function()
			return Player.DamagePredicted(5) >= 40;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/arcane_torrent,if=energy.deficit>=15+energy.regen
		ArcaneTorrent = function()
			return Player.Energy.Deficit() >= 15 + (Racial.ArcaneTorrent.ExecuteTime() * Player.Energy.Regen());
		end,

		-- SimCraft doesn't specify it, but make sure we are not already stealthed before suggesting stealth.
		-- actions.precombat+=/stealth
		Stealth = function()
			return not Player.IsStealthed();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfAgility = Objects.newItem(163223),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.AncestralCall, Racial.ArcanePulse, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood, Racial.Shadowmeld);
		config.AOEOptions(options, Spell.BladeFlurry, Talent.BladeRush, Talent.KillingSpree);
		config.CooldownOptions(options, Spell.AdrenalineRush, Talent.GhostlyStrike, Talent.MarkedForDeath, Talent.SliceAndDice);
		config.DefensiveOptions(options, Spell.CloakOfShadows, Spell.CrimsonVial, Spell.Feint, Spell.Riposte);
		config.UtilityOptions(options, Spell.Stealth, Spell.Vanish);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against Magic damage
		action.EvaluateDefensiveAction(Spell.CloakOfShadows, self.Defensives.CloakOfShadows);
		-- Protects against Physical damage
		action.EvaluateDefensiveAction(Spell.Riposte, self.Defensives.Riposte);
		-- Protects against All damage (AOE)
		action.EvaluateDefensiveAction(Spell.Feint, self.Defensives.Feint);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Spell.CrimsonVial, self.Defensives.CrimsonVial);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.Stealth, self.Requirements.Stealth);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfAgility, true);
		-- Addon doesn't know how long until combat starts unless there is a pull timer, so just recommend this without restriction.
		-- actions.precombat+=/marked_for_death,precombat_seconds=5,if=raid_event.adds.in>40
		action.EvaluateAction(Talent.MarkedForDeath, true);
		-- Addon doesn't know how long until combat starts unless there is a pull timer, so just recommend this without restriction.
		-- actions.precombat+=/roll_the_bones,precombat_seconds=2
		action.EvaluateAction(Spell.RollTheBones, true);
		-- Addon doesn't know how long until combat starts unless there is a pull timer, so just recommend this without restriction.
		-- actions.precombat+=/slice_and_dice,precombat_seconds=2
		action.EvaluateAction(Talent.SliceAndDice, true);
		-- Addon doesn't know how long until combat starts unless there is a pull timer, so just recommend this without restriction.
		-- actions.precombat+=/adrenaline_rush,precombat_seconds=1
		action.EvaluateAction(Spell.AdrenalineRush, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions=variable,name=rtb_reroll,value=rtb_buffs<2&(buff.loaded_dice.up|!buff.grand_melee.up&!buff.ruthless_precision.up)
		Variables.rtb_reroll = rtb_buffs() < 2 and (Player.Buff(Aura.LoadedDice).Up() or not Player.Buff(Aura.GrandMelee).Up() and not Player.Buff(Aura.RuthlessPrecision).Up());
		-- actions+=/variable,name=ambush_condition,value=combo_points.deficit>=2+2*(talent.ghostly_strike.enabled&cooldown.ghostly_strike.remains<1)+buff.broadside.up&energy>60&!buff.skull_and_crossbones.up
		Variables.ambush_condition = Player.ComboPoints.Deficit() >= 2 + 2 * val(Talent.GhostlyStrike.Enabled() and Talent.GhostlyStrike.Cooldown.Remains() < 1) + val(Player.Buff(Aura.Broadside).Up()) and Player.Energy() > 60 and not Player.Buff(Aura.SkullAndCrossbones).Up();
		-- We can't do raid add events so just skip this.
		-- actions+=/variable,name=blade_flurry_sync,value=spell_targets.blade_flurry<2&raid_event.adds.in>20|buff.blade_flurry.up
		Variables.blade_flurry_sync = Enemies.GetEnemies(5) < 2 or Player.Buff(Aura.BladeFlurry).Up();

		action.CallActionList(rotationStealth);
		-- actions+=/call_action_list,name=cds
		action.CallActionList(rotationCooldowns);
		action.CallActionList(rotationFinishers);
		-- actions+=/call_action_list,name=build
		action.CallActionList(rotationBuilders);

		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
		-- actions+=/arcane_pulse
		action.EvaluateAction(Racial.ArcanePulse, true);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Outlaw Rogue", addonTable.Enum.SpecID.ROGUE_OUTLAW);
