-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local format, huge, max, pairs, setmetatable = format, math.huge, max, pairs, setmetatable;
local Cache = setmetatable({}, { __mode = "kv" });

-- Todo: Add tick properties for auras to handle debuff tick conditions.
addonTable.propertiesAura = function(Unit, Aura, AnyCaster)
	local Objects = addon.Core.Objects;

	local unitID = Objects.GetID(Unit) or Unit;
	local auraID = Objects.GetID(Aura) or Aura;
	local anyCaster = AnyCaster;
	local key = format("%s-%s", unitID, auraID);

	local self = Cache[key] or { UnitID = unitID, AuraID = auraID, AnyCaster = anyCaster };

	-- Reference to cache function to use depending on if this object is for a buff or debuff.
	-- A count of the number of units with the aura cast by the player.
	self.Count = function()
		local returnValue = 0;

		for _, currentUnit in pairs(addon.NameplateUnits) do
			if currentUnit.Aura(auraID).Up() then
				returnValue = returnValue + 1;
			end
		end

		return returnValue;
	end

	-- The dipel type for this aura.
	self.DispelType = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);

		return Tables.GetValue(unitAura, "Type", "");
	end

	-- DEPRECATED
	-- aura.foo.down should use !aura.foo.up
	self.Down = function()
		return not self.Up();
	end

	-- aura.foo.duration
	self.Duration = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);

		return (self.Up() and Tables.GetValue(unitAura, "Duration", 0))
			or 0;
	end

	-- Time when aura will end, nil if the aura does not exist or is a permanent aura.
	self.End = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);

		return self.Up()
		   and Tables.GetValue(unitAura, "End");
	end

	self.PersistentMultiplier = function()
		local Auras = addonTable.Auras;

		return self.Up() and Auras.GetPersistentMultiplier(unitID, auraID)
		    or 1;
	end

	self.React = function()
		local General = addon.API.General;

		return General.GetTime() - self.Start() <= addon.reactTime;
	end

	self.Refreshable = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);

		return not self.Up()
			or (self.Up() and self.Remains() <= Tables.GetValue(unitAura, "PandemicTime", 0));
	end

	-- aura.foo.remains
	self.Remains = function()
		local General =  addon.API.General;
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);

		if self.Up() then
			if not self.End() or self.End() == 0 then
				return huge;
			else
				return max((self.End() - General.GetTime()) / Tables.GetValue(unitAura, "TimeMod", 1), 0);
			end
		end

		return 0;
	end

	-- buff.foo.stack
	self.Stack = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);

		return (self.Up() and Tables.GetValue(unitAura, "Stacks", 0))
			or 0;
	end

	-- Time when aura started.
	self.Start = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);

		return (self.Up() and Tables.GetValue(unitAura, "Start", 0))
			or 0;
	end

	self.Ticking = function()
		return self.Up();
	end

	-- buff.foo.up
	self.Up = function()
		local General = addon.API.General;
		local Player = addon.Units.Player;
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local unitAura = apiUnit.UnitAura(unitID, auraID, anyCaster);
		local endTime = Tables.GetValue(unitAura, "End");

		if unitAura then
			if not endTime or endTime == 0 then
				return true;
			else
				return endTime > General.GetTime();
			end
		elseif Player.Casting.ID() == auraID then
			return true;
		end

		return false;
	end

	Cache[key] = self;

	return self;
end