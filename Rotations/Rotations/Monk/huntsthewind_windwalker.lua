local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local Objects = addon.Core.Objects;
local coreGeneral = addon.Core.General;
local coreSettings = addon.Core.Settings;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "huntsthewind_monk_windwalker";

local trashMobs = {
    minus = true,
    normal = true,
    trivial = true,
    elite = true,
};

-- CD Rotation
local function Cooldowns(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions.cd+=/arcane_torrent,if=chi.max-chi>=1&energy.time_to_max>=0.5
        ArcaneTorrent = function()
            return Player.Chi.Deficit() >= 1
                    and Player.Energy.TimeToMax() >= 0.5;
        end,

    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        -- actions.cd=invoke_xuen_the_white_tiger
        action.EvaluateAction(Talent.InvokeXuenTheWhiteTiger, true);
        -- actions.cd+=/blood_fury
        action.EvaluateAction(Racial.BloodFury, true);
        -- actions.cd+=/berserking
        action.EvaluateAction(Racial.Berserking, true);
        action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
        -- actions.cd+=/lights_judgment
        action.EvaluateAction(Racial.LightsJudgment, true);
        action.EvaluateAction(Racial.Fireblood, true);
        action.EvaluateAction(Racial.AncestralCall, true);
        action.EvaluateAction(Spell.TouchOfDeath, true);
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- AOE Rotation
local function AOE(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions.aoe+=/arcane_torrent,if=chi.max-chi>=1&energy.time_to_max>=0.5
        ArcaneTorrent = function()
            return Player.Chi.Deficit() >= 1
            and Player.Energy.TimeToMax() >= 0.5;
        end,

        -- actions.aoe+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=(chi>1|buff.bok_proc.up|(talent.energizing_elixir.enabled&cooldown.energizing_elixir.remains<cooldown.fists_of_fury.remains))&((cooldown.rising_sun_kick.remains>1&(!talent.fist_of_the_white_tiger.enabled|cooldown.fist_of_the_white_tiger.remains>1)|chi>4)&(cooldown.fists_of_fury.remains>1|chi>2)|prev_gcd.1.tiger_palm)&!prev_gcd.1.blackout_kick
        -- Removed lines that used previous tier bonuses
        -- Ignore the target_if clause, since we can't do it
        BlackoutKick = function()
            return (Player.Chi() > 1
                    or Player.Buff(Aura.BlackoutKick).Up()
                    or (Talent.EnergizingElixir.Enabled()
                        and Talent.EnergizingElixir.Cooldown.Remains() < Spell.FistsOfFury.Cooldown.Remains()))
                and ((((Spell.RisingSunKick.Cooldown.Remains() > 1
                        and (not Talent.FistOfTheWhiteTiger.Enabled()
                            or Talent.FistOfTheWhiteTiger.Cooldown.Remains() > 1))
                        or Player.Chi() > 4)
                    and (Spell.FistsOfFury.Cooldown.Remains() > 1
                        or Player.Chi() > 2))
                    or Player.PrevGCD(1, Spell.TigerPalm))
                and Player.PrevGCD(1, Spell.BlackoutKick);
        end,

        -- actions.aoe+=/chi_burst,if=chi<=3&(cooldown.rising_sun_kick.remains>=5|cooldown.whirling_dragon_punch.remains>=5)&energy.time_to_max>1
        -- LUNA: do I need to check for talents to be enabled, or do you handle
        --       that upstream?
        ChiBurst = function()
            return Talent.ChiBurst.Enabled()
                and Player.Chi() <= 3
                and (Spell.RisingSunKick.Cooldown.Remains() >= 5
                    or (Talent.WhirlingDragonPunch.Enabled()
                        and Talent.WhirlingDragonPunch.Cooldown.Remains() >= 5))
                and Player.Energy.TimeToMax() > 1;
        end,

        -- actions.aoe+=/chi_wave,if=chi<=3&(cooldown.rising_sun_kick.remains>=5|cooldown.whirling_dragon_punch.remains>=5)&energy.time_to_max>1
        ChiWave = function()
            return Player.Chi() <= 3
                and (Spell.RisingSunKick.Cooldown.Remains() >= 5
                    or (Talent.WhirlingDragonPunch.Enabled()
                        and Talent.WhirlingDragonPunch.Cooldown.Remains() >= 5))
                and Player.Energy.TimeToMax() > 1;
        end,

        -- actions.aoe+=/energizing_elixir,if=!prev_gcd.1.tiger_palm&chi<=1&(cooldown.rising_sun_kick.remains=0|(talent.fist_of_the_white_tiger.enabled&cooldown.fist_of_the_white_tiger.remains=0)|energy<50)
        EnergizingElixir = function()
            return not Player.PrevGCD(1, Spell.TigerPalm)
                and Player.Chi() <= 1
                and (Spell.RisingSunKick.Cooldown.Remains() == 0
                    or (Talent.FistOfTheWhiteTiger.Enabled()
                        and Talent.FistOfTheWhiteTiger.Cooldown.Remains() == 0)
                    or Player.Energy() < 50);
        end,

        FistsOfFury = {
            -- actions.aoe+=/fists_of_fury,if=cooldown.rising_sun_kick.remains>=3.5&chi<=5
            Use = function()
                return Spell.RisingSunKick.Cooldown.Remains() >= 3.5
                    and Player.Chi() <= 5;
            end,

            -- took out special handling for Drinking Horn
            -- actions.aoe+=/fists_of_fury,if=talent.serenity.enabled&cooldown.serenity.remains>=5&energy.time_to_max>2
            WithSerenity = function()
                return Talent.Serenity.Enabled()
                    and Talent.Serenity.Cooldown.Remains() >= 5
                    and Player.Energy.TimeToMax() > 2;
            end,

            -- actions.aoe+=/fists_of_fury,if=!talent.serenity.enabled&energy.time_to_max>2
            WithoutSerenity = function()
                return not Talent.Serenity.Enabled()
                    and Player.Energy.TimeToMax() > 2;
            end,
        },

        -- actions.aoe+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains,if=(talent.whirling_dragon_punch.enabled&cooldown.whirling_dragon_punch.remains<gcd)&!prev_gcd.1.rising_sun_kick&cooldown.fists_of_fury.remains>gcd
        RisingSunKick = function()
            return Talent.WhirlingDragonPunch.Enabled()
                and Talent.WhirlingDragonPunch.Cooldown.Remains() < Player.GCD()
                and not Player.PrevGCD(1, Spell.RisingSunKick)
                and Spell.FistsOfFury.Cooldown.Remains() > Player.GCD();
        end,

        -- actions.aoe+=/spinning_crane_kick,if=active_enemies>=3&!prev_gcd.1.spinning_crane_kick&cooldown.fists_of_fury.remains>gcd
        SpinningCraneKick = function(numEnemies)
            return numEnemies >= 3
                and not Player.PrevGCD(1, Spell.SpinningCraneKick)
                and Spell.FistsOfFury.Cooldown.Remains() > Player.GCD();
        end,

        TigerPalm = {
            -- actions.aoe+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&!prev_gcd.1.energizing_elixir&energy.time_to_max<=1&chi.max-chi>=2
            MaxEnergy = function()
                return not Player.PrevGCD(1, Spell.TigerPalm)
                    and not Player.PrevGCD(1, Talent.EnergizingElixir)
                    and Player.Energy.TimeToMax() <= 1
                    and Player.Chi.Deficit() >= 2;
            end,

            -- actions.aoe+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&!prev_gcd.1.energizing_elixir&(chi.max-chi>=2|energy.time_to_max<3)
            Use = function()
                return not Player.PrevGCD(1, Spell.TigerPalm)
                    and not Player.PrevGCD(1, Talent.EnergizingElixir)
                    and (Player.Chi.Deficit() >= 2
                        or Player.Energy.TimeToMax() < 3);
            end,
        },

    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        -- Simcraft file says this still needs to be optimized
        action.CallActionList(rotationCooldowns);
        action.EvaluateAction(Talent.EnergizingElixir, self.Requirements.EnergizingElixir);
        action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.WithSerenity);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.WithoutSerenity);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.Use);
        -- actions.aoe+=/whirling_dragon_punch
        action.EvaluateAction(Talent.WhirlingDragonPunch, true);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick);
        action.EvaluateAction(Talent.ChiBurst, self.Requirements.ChiBurst);
        action.EvaluateAction(Talent.ChiBurst, true);
        action.EvaluateAction(Spell.SpinningCraneKick, self.Requirements.SpinningCraneKick);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.Use);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.MaxEnergy);
        action.EvaluateAction(Talent.ChiWave, self.Requirements.ChiWave);
        action.EvaluateAction(Talent.ChiWave, true);
    end

    function self.Use(numEnemies)
        return numEnemies > 3
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationAOE = AOE("AOE");

-- Serenity Rotation
local function Serenity(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        BlackoutKick = {
            -- actions.serenity+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.blackout_kick&cooldown.rising_sun_kick.remains>=2&cooldown.fists_of_fury.remains>=2
            CooldownsActive = function()
                return not Player.PrevGCD(1, Spell.BlackoutKick)
                    and Spell.RisingSunKick.Cooldown.Remains() >= 2
                    and Spell.FistsOfFury.Cooldown.Remains() >= 2;
            end,

            -- actions.serenity+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.blackout_kick
            Use = function()
                return not Player.PrevGCD(1, Spell.BlackoutKick);
            end,
        },

        FistsOfFury = {
            -- actions.serenity+=/fists_of_fury,if=prev_gcd.1.rising_sun_kick&prev_gcd.2.serenity
            AfterRisingSunKickAndSerenity = function()
                return Player.PrevGCD(1, Spell.RisingSunKick)
                    and Player.PrevGCD(2, Talent.Serenity);
            end,

            -- actions.serenity+=/fists_of_fury,if=buff.serenity.remains<=1.05
            SerenityEnding = function()
                return Player.Buff(Aura.Serenity).Remains() < 1.05;
            end,

        },

        -- actions.serenity+=/fist_of_the_white_tiger,if=prev_gcd.1.blackout_kick&prev_gcd.2.rising_sun_kick&chi.max-chi>2
        FistOfTheWhiteTiger = function()
            return Player.PrevGCD(1, Spell.BlackoutKick)
                and Player.PrevGCD(2, Spell.RisingSunKick)
                and Player.Chi.Deficit() > 2;
        end,

        RisingSunKick = {
            -- actions.serenity+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains
            Use = function(numEnemies)
                return not Target.Debuff(Aura.MarkOfTheCrane).Up();
            end,
        },

        -- actions.serenity+=/rushing_jade_wind,if=talent.rushing_jade_wind.enabled&!prev_gcd.1.rushing_jade_wind&buff.rushing_jade_wind.down
        RushingJadeWind = function()
            return Talent.RushingJadeWind.Enabled()
                and not Player.PrevGCD(1, Talent.RushingJadeWind)
                and Player.Buff(Aura.RushingJadeWind).Down();
        end,

        -- actions.serenity+=/serenity,if=cooldown.rising_sun_kick.remains<=2&cooldown.fists_of_fury.remains<=4
        Serenity = function()
            return Spell.RisingSunKick.Cooldown.Remains() <= 2
                and Spell.FistsOfFury.Cooldown.Remains() <= 4;
        end,

        -- actions.serenity+=/spinning_crane_kick,if=active_enemies>=3&!prev_gcd.1.spinning_crane_kick
        SpinningCraneKick = function(numEnemies)
            return numEnemies >= 3
                and not Player.PrevGCD(1, Spell.SpinningCraneKick);
        end,

        TigerPalm = {
            -- actions.serenity=tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&!prev_gcd.1.energizing_elixir&energy=energy.max&chi<1&!buff.serenity.up
            NoDoubleTigerPalm = function()
                return not Player.PrevGCD(1, Spell.TigerPalm)
                    and not Player.PrevGCD(1, Talent.EnergizingElixir)
                    and Player.Energy() == Player.Energy.Max()
                    and Player.Chi() < 1
                    and not Player.Buff(Aura.Serenity).Up();
            end,

            -- actions.serenity+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=prev_gcd.1.blackout_kick&prev_gcd.2.rising_sun_kick&chi.max-chi>1
            AfterBoKAndRSK = function()
                return Player.PrevGCD(1, Spell.BlackoutKick)
                    and Player.PrevGCD(2, Spell.RisingSunKick)
                    and Player.Chi.Deficit() > 1;
            end,
        },
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.NoDoubleTigerPalm);
        -- actions.serenity+=/call_action_list,name=cd
        action.CallActionList(rotationCooldowns);
        action.EvaluateAction(Talent.RushingJadeWind, self.Requirements.RushingJadeWind);
        action.EvaluateAction(Talent.Serenity, self.Requirements.Serenity);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.AfterRisingSunKickAndSerenity);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.SerenityEnding);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick.Use);
        action.EvaluateAction(Talent.FistOfTheWhiteTiger, self.Requirements.FistOfTheWhiteTiger);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.AfterBoKAndRSK);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.CooldownsActive);
        action.EvaluateAction(Spell.SpinningCraneKick, self.Requirements.SpinningCraneKick);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick.Use);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.Use);
    end

    -- actions+=/call_action_list,name=serenity,if=((!azerite.swift_roundhouse.enabled&talent.serenity.enabled&cooldown.serenity.remains<=0)|buff.serenity.up)&time>30
    function self.Use()
        return ((not Azerite.SwiftRoundhouse.Enabled()
                and Talent.Serenity.Enabled()
                and Talent.Serenity.Cooldown.Remains() <= 0)
                or Player.Buff(Aura.Serenity).Up())
            and coreGeneral.CombatTime() > 30;
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSerenity = Serenity("Serenity");

-- Serenity Opener Rotation
local function SerenityOpener(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        BlackoutKick = {
            -- actions.serenity_opener+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.blackout_kick&cooldown.rising_sun_kick.remains>=2&cooldown.fists_of_fury.remains>=2
            CooldownsActive = function()
                return not Player.PrevGCD(1, Spell.BlackoutKick)
                    and Spell.RisingSunKick.Cooldown.Remains() >= 2
                    and Spell.FistsOfFury.Cooldown.Remains() >= 2;
            end,

            -- actions.serenity_opener+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.blackout_kick
            Use = function()
                return not Player.PrevGCD(1, Spell.BlackoutKick);
            end,
        },

        -- actions.serenity_opener+=/call_action_list,name=cd,if=buff.serenity.down
        Cooldowns = function()
            return not Player.Buff(Aura.Serenity).Up();
        end,

        FistsOfFury = {
            -- actions.serenity+=/fists_of_fury,if=prev_gcd.1.rising_sun_kick&prev_gcd.2.serenity
            AfterRisingSunKickAndSerenity = function()
                return Player.PrevGCD(1, Spell.RisingSunKick)
                    and Player.PrevGCD(2, Talent.Serenity);
            end,

            -- actions.serenity+=/fists_of_fury,if=prev_gcd.1.rising_sun_kick&prev_gcd.2.blackout_kick
            AfterRisingSunKickAndBoK = function()
                return Player.PrevGCD(1, Spell.RisingSunKick)
                    and Player.PrevGCD(2, Spell.BlackoutKick);
            end,

        },

        -- actions.serenity_opener=fist_of_the_white_tiger,if=buff.serenity.down
        FistOfTheWhiteTiger = function()
            return not Player.Buff(Aura.Serenity).Up();
        end,

        RisingSunKick = {
            -- actions.serenity+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains
            Use = function(numEnemies)
                return not Target.Debuff(Aura.MarkOfTheCrane).Up();
            end,
        },

        -- actions.serenity_opener+=/call_action_list,name=serenity,if=buff.bloodlust.down
        Serenity = function()
            return not Player.HasBloodlust();
        end,

        -- actions.serenity_opener+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&buff.serenity.down&chi<4
        TigerPalm = function()
            return not Player.PrevGCD(1, Spell.TigerPalm)
                and not Player.Buff(Aura.Serenity).Up()
                and Player.Chi() < 4;
        end,
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateAction(Talent.FistOfTheWhiteTiger, self.Requirements.FistOfTheWhiteTiger);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm);
        action.CallActionList(rotationCooldowns, self.Requirements.Cooldowns);
        action.CallActionList(rotationSerenity, self.Requirements.Serenity);
        action.EvaluateAction(Talent.Serenity, true);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick.Use);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.AfterRisingSunKickAndSerenity);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.AfterRisingSunKickAndBoK);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.CooldownsActive);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.Use);
    end

    -- actions+=/call_action_list,name=serenity_opener,if=(!azerite.swift_roundhouse.enabled&talent.serenity.enabled&cooldown.serenity.remains<=0|buff.serenity.up)&time<30
    function self.Use()
        return ((not Azerite.SwiftRoundhouse.Enabled()
                and Talent.Serenity.Enabled()
                and Talent.Serenity.Cooldown.Remains() <= 0)
                or Player.Buff(Aura.Serenity).Up())
            and coreGeneral.CombatTime() <= 30;
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSerenityOpener = SerenityOpener("SerenityOpener");

-- Serenity with Swift Roundhouse Rotation
local function SerenitySR(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        BlackoutKick = {
            -- actions.serenitySR+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.blackout_kick&cooldown.rising_sun_kick.remains>=2&cooldown.fists_of_fury.remains>=2
            CooldownsActive = function()
                return not Player.PrevGCD(1, Spell.BlackoutKick)
                    and Spell.RisingSunKick.Cooldown.Remains() >= 2
                    and Spell.FistsOfFury.Cooldown.Remains() >= 2;
            end,

            -- actions.serenitySR+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains
            Use = function()
                return not Target.Debuff(Aura.MarkOfTheCrane).Up();
            end,
        },

        -- actions.serenitySR+=/fists_of_fury,if=buff.serenity.remains<=1.05
        FistsOfFury = function()
            return Player.Buff(Aura.Serenity).Remains() <= 1.05;
        end,

        -- actions.serenity+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains
        RisingSunKick = function(numEnemies)
            return not Target.Debuff(Aura.MarkOfTheCrane).Up();
        end,

        -- actions.serenitySR+=/serenity,if=cooldown.rising_sun_kick.remains<=2
        Serenity = function()
            return Spell.RisingSunKick.Cooldown.Remains() <= 2
        end,

        -- actions.serenitySR=tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&!prev_gcd.1.energizing_elixir&energy=energy.max&chi<1&!buff.serenity.up
        TigerPalm = function()
            return not Player.PrevGCD(1, Spell.TigerPalm)
                and not Player.PrevGCD(1, Talent.EnergizingElixir)
                and Player.Energy() == Player.Energy.Max()
                and Player.Chi() < 1
                and not Player.Buff(Aura.Serenity).Up();
        end,

    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.NoDoubleTigerPalm);
        -- actions.serenitySR+=/call_action_list,name=cd
        action.CallActionList(rotationCooldowns);
        action.EvaluateAction(Talent.Serenity, self.Requirements.Serenity);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick);

        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.CooldownsActive);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.Use);
    end

    -- actions+=/call_action_list,name=serenity,if=((!azerite.swift_roundhouse.enabled&talent.serenity.enabled&cooldown.serenity.remains<=0)|buff.serenity.up)&time>30
    -- actions+=/call_action_list,name=serenitySR,if=((talent.serenity.enabled&cooldown.serenity.remains<=0)|buff.serenity.up)&azerite.swift_roundhouse.enabled&time>30
    function self.Use()
        return ((Talent.Serenity.Enabled()
                    and Talent.Serenity.Cooldown.Remains() <= 0)
                or Player.Buff(Aura.Serenity).Up())
            and Azerite.SwiftRoundhouse.Enabled()
            and coreGeneral.CombatTime() > 30;
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSerenitySR = SerenitySR("SerenitySR");

-- Serenity Opener with Swift Roundhouse Rotation
local function SerenityOpenerSR(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        BlackoutKick = {
            -- actions.serenity_openerSR+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.blackout_kick&cooldown.rising_sun_kick.remains>=2&cooldown.fists_of_fury.remains>=2
            CooldownsActive = function()
                return not Player.PrevGCD(1, Spell.BlackoutKick)
                    and Spell.RisingSunKick.Cooldown.Remains() >= 2
                    and Spell.FistsOfFury.Cooldown.Remains() >= 2;
            end,

            -- actions.serenity_openerSR+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains
            Use = function()
                return not Target.Debuff(Aura.MarkOfTheCrane).Up();
            end,
        },

        -- actions.serenity_openerSR+=/call_action_list,name=cd,if=buff.serenity.down
        Cooldowns = function()
            return not Player.Buff(Aura.Serenity).Up();
        end,

        -- actions.serenity_openerSR+=/fists_of_fury,if=buff.serenity.remains<1
        FistsOfFury = function()
            return Player.Buff(Aura.Serenity).Remains() < 1;
        end,

        -- actions.serenity_openerSR=fist_of_the_white_tiger,if=buff.serenity.down
        FistOfTheWhiteTiger = function()
            return not Player.Buff(Aura.Serenity).Up();
        end,

        -- actions.serenitySR+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains
        RisingSunKick = function(numEnemies)
            return not Target.Debuff(Aura.MarkOfTheCrane).Up();
        end,

        -- actions.serenity_openerSR+=/call_action_list,name=serenity,if=buff.bloodlust.down
        Serenity = function()
            return not Player.HasBloodlust();
        end,

        -- actions.serenity_openerSR+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=buff.serenity.down&chi<4
        TigerPalm = function()
            return not Player.Buff(Aura.Serenity).Up()
                and Player.Chi() < 4;
        end,
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateAction(Talent.FistOfTheWhiteTiger, self.Requirements.FistOfTheWhiteTiger);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm);
        action.CallActionList(rotationCooldowns, self.Requirements.Cooldowns);
        action.CallActionList(rotationSerenity, self.Requirements.Serenity);
        action.EvaluateAction(Talent.Serenity, true);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.CooldownsActive);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.Use);
    end

    -- actions+=/call_action_list,name=serenity_openerSR,if=(talent.serenity.enabled&cooldown.serenity.remains<=0|buff.serenity.up)&time<30&azerite.swift_roundhouse.enabled
    function self.Use()
        return Talent.Serenity.Enabled()
            and (Talent.Serenity.Cooldown.Remains() <= 0
                or Player.Buff(Aura.Serenity).Up())
            and coreGeneral.CombatTime() <= 30
            and Azerite.SwiftRoundhouse.Enabled();
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSerenityOpenerSR = SerenityOpenerSR("SerenityOpenerSR");

-- Standard Rotation
local function Standard(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        BlackoutKick = {
            -- actions.st+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=cooldown.fists_of_fury.remains>2&!prev_gcd.1.blackout_kick&energy.time_to_max>1&azerite.swift_roundhouse.rank>2
            BuildChi = function()
                return Spell.FistsOfFury.Cooldown.Remains() > 2
                    and not Player.PrevGCD(1, Spell.BlackoutKick)
                    and Player.Energy.TimeToMax() > 1
                    and Azerite.SwiftRoundhouse.Rank() > 2;
            end,

            -- actions.st+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=buff.swift_roundhouse.stack<2&!prev_gcd.1.blackout_kick
            BuildMasteryBuff = function()
                return Player.Buff(Aura.SwiftRoundhouse).Stack() < 2
                    and not Player.PrevGCD(1, Spell.BlackoutKick);
            end,

            -- actions.st+=/blackout_kick,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.blackout_kick
            Use = function()
                return not Player.PrevGCD(1, Spell.BlackoutKick);
            end,

        },

        ChiBurst = {
            -- actions.st+=/chi_burst,if=energy.time_to_max>1&talent.serenity.enabled
            NoCap = function()
                return Player.Energy.TimeToMax() > 1
                        and Talent.Serenity.Enabled();
            end,

            -- actions.st+=/chi_burst,if=chi.max-chi>=3&energy.time_to_max>1&!talent.serenity.enabled
            Use = function()
                return Player.Chi.Deficit() >= 3
                        and Player.Energy.TimeToMax() > 1
                        and not Talent.Serenity.Enabled();
            end,
        },

        -- actions.st+=/energizing_elixir,if=!prev_gcd.1.tiger_palm
        EnergizingElixir = function()
            return not Player.PrevGCD(1, Spell.TigerPalm);
        end,

        FistsOfFury = {
            -- actions.st+=/fists_of_fury,if=chi>=3&energy.time_to_max>2.5&azerite.swift_roundhouse.rank<3
            SwiftRoundhouse = function()
                return Player.Chi() >= 3
                    and Player.Energy.TimeToMax() > 2.5
                    and Azerite.SwiftRoundhouse.Rank() < 3;
            end,

            -- actions.st+=/fists_of_fury,if=!talent.serenity.enabled&(azerite.swift_roundhouse.rank<3|cooldown.whirling_dragon_punch.remains<13)
            NoSerenity = function()
                return not Talent.Serenity.Enabled()
                    and (Azerite.SwiftRoundhouse.Rank() < 3
                            or (Talent.WhirlingDragonPunch.Enabled()
                                and Talent.WhirlingDragonPunch.Cooldown.Remains() < 13));
            end,


            -- actions.st+=/fists_of_fury,if=!talent.serenity.enabled&energy.time_to_max>2
            Use = function()
                return not Talent.Serenity.Enabled()
                        and Player.Energy.TimeToMax() > 2;
            end,

            -- actions.st+=/fists_of_fury,if=talent.serenity.enabled&cooldown.serenity.remains>=5&energy.time_to_max>2
            WithSerenity = function()
                return Talent.Serenity.Enabled()
                        and Talent.Serenity.Cooldown.Remains() >= 5
                        and Player.Energy.TimeToMax() > 2;
            end,
        },

        -- actions.st+=/fist_of_the_white_tiger,if=(chi<=2)
        FistOfTheWhiteTiger = function()
            return Player.Chi() <= 2
        end,

        -- actions.st+=/flying_serpent_kick,if=prev_gcd.1.blackout_kick&energy.time_to_max>2&chi>1,interrupt=1
        FlyingSerpentKick = function()
            return Player.PrevGCD(1, Spell.BlackoutKick)
                and Player.Energy.TimeToMax() > 2
                and Player.Chi() > 1;
        end,

        RisingSunKick = {
            -- actions.st+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains,if=cooldown.serenity.remains>=5|(!talent.serenity.enabled&!azerite.swift_roundhouse.enabled)
            Use = function()
                return (Talent.Serenity.Enabled()
                        and Talent.Serenity.Cooldown.Remains() >= 5)
                    or (Talent.Serenity.Enabled()
                        and coreSettings.GetRotationValue("OPT_RISINGSUNKICK_TRASH",1) == 1
                        and trashMobs[Target.Classification()] == true)
                    or (not Talent.Serenity.Enabled()
                        and not Azerite.SwiftRoundhouse.Enabled());
            end,

            -- actions.st+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains,if=((chi>=3&energy>=40)|chi>=5)&(talent.serenity.enabled|cooldown.serenity.remains>=6&!azerite.swift_roundhouse.enabled)
            -- NOTE: this line has a bug in it based on the description in the
            -- simcraft file just above it. Note the 'not' in second line below
            -- to cover the case where Serenity is not selected
            MaxResources = function()
                return ((Player.Chi() >= 3 and Player.Energy() >= 40) or Player.Chi() >= 5)
                        and (not Talent.Serenity.Enabled()
                            or Talent.Serenity.Cooldown.Remains() >= 6
                            or (coreSettings.GetRotationValue("OPT_RISINGSUNKICK_TRASH",1) == 1
                                and trashMobs[Target.Classification()] == true))
                        and not Azerite.SwiftRoundhouse.Enabled();
            end,

            -- actions.st+=/rising_sun_kick,target_if=min:debuff.mark_of_the_crane.remains,if=azerite.swift_roundhouse.enabled&buff.swift_roundhouse.stack=2
            SwiftRoundhouse = function()
                return Azerite.SwiftRoundhouse.Enabled()
                    and Player.Buff(Aura.SwiftRoundhouse).Stack() == 2;
            end,
        },

        -- actions.st+=/rushing_jade_wind,if=buff.rushing_jade_wind.down&!prev_gcd.1.rushing_jade_wind
        RushingJadeWind = function()
            return not Player.Buff(Aura.RushingJadeWind).Up()
                    and not Player.PrevGCD(1, Talent.RushingJadeWind);
        end,

        -- actions.st+=/storm_earth_and_fire,if=!buff.storm_earth_and_fire.up
        StormEarthAndFire = function()
            return not Player.Buff(Aura.StormEarthAndFire).Up();
        end,

        TigerPalm = {
            -- actions.st+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&chi<=3&energy.time_to_max<2
            ChiBuilding = function()
                return not Player.PrevGCD(1, Spell.TigerPalm)
                    and Player.Chi() <= 3
                    and Player.Energy.TimeToMax() < 2;
            end,

            -- actions.st+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&chi.max-chi>=2&buff.serenity.down&cooldown.fist_of_the_white_tiger.remains>energy.time_to_max
            WhiteTigerNotReady = function()
                return not Player.PrevGCD(1, Spell.TigerPalm)
                    and Player.Chi.Deficit() >= 2
                    and not Player.Buff(Aura.Serenity).Up()
                    and Talent.FistOfTheWhiteTiger.Enabled()
                    and Talent.FistOfTheWhiteTiger.Cooldown.Remains() > Player.Energy.TimeToMax();
            end,

            -- actions.st+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&!prev_gcd.1.energizing_elixir&(chi.max-chi>=2|energy.time_to_max<3)&!buff.serenity.up
            Use = function()
                return not Player.PrevGCD(1, Spell.TigerPalm)
                        and not Player.PrevGCD(1, Talent.EnergizingElixir)
                        and (Player.Chi.Deficit() >= 2 or Player.Energy.TimeToMax() < 3)
                        and not Player.Buff(Aura.Serenity).Up();
            end,

            -- actions.st+=/tiger_palm,target_if=min:debuff.mark_of_the_crane.remains,if=!prev_gcd.1.tiger_palm&!prev_gcd.1.energizing_elixir&energy.time_to_max<=1&chi.max-chi>=2&!buff.serenity.up
            MaxEnergy = function()
                return not Player.PrevGCD(1, Spell.TigerPalm)
                        and not Player.PrevGCD(1, Talent.EnergizingElixir)
                        and Player.Energy.TimeToMax() <= 1
                        and Player.Chi.Deficit() >= 2
                        and not Player.Buff(Aura.Serenity).Up();
            end,
        },
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateAction(Talent.InvokeXuenTheWhiteTiger, true);
        action.EvaluateAction(Spell.TouchOfDeath, true);
        action.EvaluateAction(Spell.StormEarthAndFire, self.Requirements.StormEarthAndFire);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick.SwiftRoundhouse);
        action.EvaluateAction(Talent.RushingJadeWind, self.Requirements.RushingJadeWind);
        action.EvaluateAction(Talent.EnergizingElixir, self.Requirements.EnergizingElixir);
        action.EvaluateAction(Talent.FistOfTheWhiteTiger, self.Requirements.FistOfTheWhiteTiger);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.ChiBuilding);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.WhiteTigerNotReady);
        -- actions.st+=/whirling_dragon_punch
        action.EvaluateAction(Talent.WhirlingDragonPunch, true);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.SwiftRoundhouse);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick.MaxResources);
        action.EvaluateAction(Spell.FistsOfFury, self.Requirements.FistsOfFury.NoSerenity);
        action.EvaluateCycleAction(Spell.RisingSunKick, self.Requirements.RisingSunKick.Use);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.BuildChi);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.BuildMasteryBuff);
        action.EvaluateCycleAction(Spell.BlackoutKick, self.Requirements.BlackoutKick.Use);
        action.EvaluateAction(Talent.ChiWave, true);
        action.EvaluateAction(Talent.ChiBurst, self.Requirements.ChiBurst.NoCap);
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm.Use);
        action.EvaluateAction(Talent.ChiBurst, self.Requirements.ChiBurst.Use);
    end

    function self.Use(numEnemies)
        return numEnemies <= 3
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationStandard = Standard("Standard");

-- Storm, Earth, and Fire Rotation
local function StormEarthAndFire(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions.sef+=/call_action_list,name=aoe,if=active_enemies>3
        AOE = function(numEnemies)
            return numEnemies > 3;
        end,

        -- actions.sef+=/call_action_list,name=st,if=active_enemies<=3
        Standard = function(numEnemies)
            return numEnemies <= 3;
        end,

        -- actions.sef+=/storm_earth_and_fire,if=!buff.storm_earth_and_fire.up
        StormEarthAndFire = function()
            return not Player.Buff(Aura.StormEarthAndFire).Up();
        end,

        -- actions.sef=tiger_palm,target_if=debuff.mark_of_the_crane.down,if=!prev_gcd.1.tiger_palm&!prev_gcd.1.energizing_elixir&energy=energy.max&chi<1
        TigerPalm = function()
            return not Player.PrevGCD(1, Spell.TigerPalm)
                and not Player.PrevGCD(1, Talent.EnergizingElixir)
                and Player.Energy() == Player.Energy.Max()
                and Player.Chi() < 1;
        end,
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateCycleAction(Spell.TigerPalm, self.Requirements.TigerPalm);
        -- actions.sef+=/call_action_list,name=cd
        action.CallActionList(rotationCooldowns);
        action.EvaluateCycleAction(Spell.StormEarthAndFire, self.Requirements.StormEarthAndFire);
        action.CallActionList(rotationAOE, self.Requirements.AOE);
        action.CallActionList(rotationStandard, self.Requirements.Standard);
    end

    -- actions+=/call_action_list,name=sef,if=!talent.serenity.enabled&(buff.storm_earth_and_fire.up|cooldown.storm_earth_and_fire.charges=2)
    -- actions+=/call_action_list,name=sef,if=(!talent.serenity.enabled&cooldown.fists_of_fury.remains<=12&chi>=3&cooldown.rising_sun_kick.remains<=1)|target.time_to_die<=25|cooldown.touch_of_death.remains>112
    -- actions+=/call_action_list,name=sef,if=(!talent.serenity.enabled&!equipped.drinking_horn_cover&cooldown.fists_of_fury.remains<=6&chi>=3&cooldown.rising_sun_kick.remains<=1)|target.time_to_die<=15|cooldown.touch_of_death.remains>112&cooldown.storm_earth_and_fire.charges=1
    -- actions+=/call_action_list,name=sef,if=(!talent.serenity.enabled&cooldown.fists_of_fury.remains<=12&chi>=3&cooldown.rising_sun_kick.remains<=1)|target.time_to_die<=25|cooldown.touch_of_death.remains>112&cooldown.storm_earth_and_fire.charges=1
    function self.Use()
        return (not Talent.Serenity.Enabled() and (Player.Buff(Aura.StormEarthAndFire).Up() or Spell.StormEarthAndFire.Charges() == 2))
            or ((not Talent.Serenity.Enabled() and Spell.FistsOfFury.Cooldown.Remains() <= 12 and Player.Chi() >= 3 and Spell.RisingSunKick.Cooldown.Remains() <= 1) or Target.TimeToDie() <= 25 or Spell.TouchOfDeath.Cooldown.Remains() > 112)
            or (((not Talent.Serenity.Enabled() and Spell.FistsOfFury.Cooldown.Remains() <= 6 and Player.Chi() >= 3 and Spell.RisingSunKick.Cooldown.Remains() <= 1) or Target.TimeToDie() <= 15 or Spell.TouchOfDeath.Cooldown.Remains() > 112) and Spell.StormEarthAndFire.Charges() == 1)
            or (((not Talent.Serenity.Enabled() and Spell.FistsOfFury.Cooldown.Remains() <= 12 and Player.Chi() >= 3 and Spell.RisingSunKick.Cooldown.Remains() <= 1) or Target.TimeToDie() <= 25 or Spell.TouchOfDeath.Cooldown.Remains() > 112) and Spell.StormEarthAndFire.Charges() == 1);
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local rotationStormEarthAndFire = StormEarthAndFire("StormEarthAndFire");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
    -- Inherits APL Class so get the base class.
    local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

    -- Store the information for the script.
    self.scriptInfo = {
        SpecializationID = addonTable.MONK_WINDWALKER,
        ScriptAuthor = "HuntsTheWind",
        GuideAuthor = "Babylonius and SimCraft 9e33dfa",
        GuideLink = "http://www.icy-veins.com/wow/windwalker-monk-pve-dps-guide",
        WoWVersion = 80001,
    };

    self.Defensives = {
        DampenHarm = function()
            return Player.PhysicalDamagePredicted(5) >= 25;
        end,

        DiffuseMagic = function()
            return Player.MagicDamagePredicted(3) >= 30;
        end,

        TouchOfKarma = function()
            return Player.DamagePredicted(5) >= 25;
        end,
    };

    self.Interrupts = {
        LegSweep = function()
            return Target.InRange(5);
        end,

        WarStomp = function()
            return Target.InRange(5);
        end,
    };

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- trinket.proc.agility.react can't be done without long lists of hard coded trinket data, so just skip trinket.proc conditions.
        -- actions+=/potion,if=buff.serenity.up|buff.storm_earth_and_fire.up|(!talent.serenity.enabled&trinket.proc.agility.react)|buff.bloodlust.react|target.time_to_die<=60
        BurstingBlood = function()
            return Player.Buff(Aura.Serenity).Up()
                or Player.Buff(Aura.StormEarthAndFire).Up()
                or not Talent.Serenity.Enabled()
                or Player.HasBloodlust()
                or Target.TimeToDie() <= 60;
        end,

        -- actions+=/touch_of_death,if=target.time_to_die<=9
        TouchOfDeath = function()
            return Target.TimeToDie() <= 9;
        end,
    };

    Objects.FinalizeRequirements(self.Defensives, self.Interrupts, self.Requirements);

    -- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
    function self.Enable(...)
        -- Spells
        Racial, Spell, Talent, Aura, Azerite = ...;

        Aura.SwiftRoundhouse = Objects.newSpell(277669);

        Item = {};

        Consumable = {
            -- Potions
            BurstingBlood = Objects.newItem(152560),
        };

        Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
    end

    -- Function for setting up the configuration screen, called when rotation becomes the active rotation.
    function self.SetupConfiguration(config, options)
        config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.GiftOfTheNaaru, Racial.Shadowmeld);
        config.AOEOptions(options, Spell.FistsOfFury, Talent.RushingJadeWind, Spell.SpinningCraneKick, Talent.ChiBurst);
        config.CooldownOptions(options, Talent.ChiWave, Talent.EnergizingElixir, Talent.InvokeXuenTheWhiteTiger, Talent.Serenity, Spell.StormEarthAndFire, Spell.TouchOfDeath,
                                     Talent.WhirlingDragonPunch);
        config.DefensiveOptions(options, Talent.DampenHarm, Talent.DiffuseMagic, Spell.TouchOfKarma);
        config.SpecialOptions(options, "OPT_RISINGSUNKICK_TRASH|Suggest Rising Sun Kick on trash without Serenity|When this option is checked then Rising Sun Kick will be suggested against normal and elite mobs without regard to whether the Serenity talent is taken.")    end

    -- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
    function self.Disable()
        local coreTables = addon.Core.Tables;

        coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
    end

    -- Function for checking the rotation that displays on the Defensives icon.
    function self.Defensive(action)
        -- The abilities here should be listed from highest damage required to suggest to lowest,
        -- Specific damage types before all damage types.

        -- Redirect damage back to target
        action.EvaluateDefensiveAction(Spell.TouchOfKarma, self.Defensives.TouchOfKarma);

        -- Protects against magical damage
        action.EvaluateDefensiveAction(Talent.DiffuseMagic, self.Defensives.DiffuseMagic);

        -- Protects against physical damage
        action.EvaluateDefensiveAction(Talent.DampenHarm, self.Defensives.DampenHarm);
    end

    -- Function for displaying interrupts when target is casting an interruptible spell.
    function self.Interrupt(action)
        action.EvaluateInterruptAction(Spell.SpearHandStrike, true);

        -- Stuns
        if Target.IsStunnable() then
            action.EvaluateInterruptAction(Racial.QuakingPalm, true);
            action.EvaluateInterruptAction(Spell.LegSweep, self.Interrupts.LegSweep);
            action.EvaluateInterruptAction(Racial.WarStomp, self.Interrupts.WarStomp);
        end

        -- Crowd Control
        action.EvaluateInterruptAction(Spell.Paralysis, true);
    end

    -- Function for displaying opening rotation.
    function self.Opener(action)
    end

    -- Function for displaying any actions before combat starts.
    function self.Precombat(action)
        -- actions.precombat+=/potion
        action.EvaluateAction(Consumable.BurstingBlood, true);
        -- actions.precombat+=/chi_burst
        action.EvaluateAction(Talent.ChiBurst, true);
        -- actions.precombat+=/chi_wave
        action.EvaluateAction(Talent.ChiWave, true);
        -- in case player has chosen Eye of the Tiger at lvl 15
        action.EvaluateAction(Spell.TouchOfDeath, true);
        -- in case other spells are on cooldown and target is in melee range
        action.EvaluateAction(Spell.TigerPalm, Target.InRange(5));
        -- in case other spells are on cooldown and target out of melee range
        action.EvaluateAction(Spell.CracklingJadeLightning, true);
    end

    -- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
    function self.Combat(action)
        action.EvaluateAction(Consumable.BurstingBlood, self.Requirements.BurstingBlood);
        action.EvaluateAction(Spell.TouchOfDeath, self.Requirements.TouchOfDeath);

        action.CallActionList(rotationSerenitySR);
        action.CallActionList(rotationSerenity);
        action.CallActionList(rotationSerenityOpenerSR);
        action.CallActionList(rotationSerenityOpener);
        action.CallActionList(rotationStormEarthAndFire);
        -- actions+=/call_action_list,name=aoe,if=active_enemies>3
        action.CallActionList(rotationAOE);
        -- actions+=/call_action_list,name=st,if=active_enemies<=3
        action.CallActionList(rotationStandard);
    end

    return self;
end

local rotationAPL = APL(nameAPL, "HuntsTheWind: Windwalker Monk", addonTable.Enum.SpecID.MONK_WINDWALKER);
