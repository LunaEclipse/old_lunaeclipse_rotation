-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Aura IDs to track drain stacks
local CLEU_TRACK_AURA = {
	[194249] = true, -- Void Form
};

-- Aura IDs to pause drain stacks
local CLEU_AURA_PAUSE_DRAIN = {
	[47585] = true, -- Dispersion
	[205065] = true, -- Void Torrent
};

-- List of CLEU events for applying auras
local CLEU_AURA_ADDED = {
	SPELL_AURA_APPLIED = true,
};

-- List of CLEU events for aura ticks
local CLEU_AURA_PERIODIC_EVENT = {
	SPELL_AURA_APPLIED_DOSE = true,
};

-- List of CLEU events for removing auras
local CLEU_AURA_REMOVED = {
	SPELL_AURA_REMOVED = true,
};

-- Table of CLEU events for when a unit is removed from combat.
local CLEU_UNIT_REMOVED = {
	UNIT_DESTROYED = true,
	UNIT_DIED = true,
	UNIT_DISSIPATES = true,
};

--- Insanity variables
local voidForm = false;
local insanityDrainStacks = 0;
local pauseInsanityDrain = false;

addonTable.Insanity = {
	InsanityDrain = function()
		local surrenderToMadness = 193223;
		local reduceInsanityRate = 1.00;

		if addonTable.Tier20_4PC then
			reduceInsanityRate = addon.Units.Player.Aura(surrenderToMadness).Up() and 0.95 or 0.90;
		end

		return (voidForm and (6.0 + (insanityDrainStacks * (2 / 3))) * reduceInsanityRate) or 0;
	end,

	InsanityDrainStacks = function()
		return (voidForm and insanityDrainStacks) or 0;
	end,
};

local function combatLog(...)
	local playerGUID = addon.API.Blizzard.UnitGUID("player");
	local _, _, combatEvent, _, sourceGUID, _, _, _, destGUID, _, _, _, spellID = ...;

	if CLEU_UNIT_REMOVED[combatEvent] and destGUID == playerGUID then
		voidForm = false;
		pauseInsanityDrain = false;
		insanityDrainStacks = 0;
	elseif sourceGUID == playerGUID then
		if CLEU_AURA_ADDED[combatEvent] then
			if CLEU_TRACK_AURA[spellID] then
				voidForm = true;
				pauseInsanityDrain = false;
				insanityDrainStacks = 0;
			elseif CLEU_AURA_PAUSE_DRAIN[spellID] then
				pauseInsanityDrain = true;
			end
		elseif CLEU_AURA_PERIODIC_EVENT[combatEvent] then
			if CLEU_TRACK_AURA[spellID] and not pauseInsanityDrain then
				voidForm = true;
				insanityDrainStacks = insanityDrainStacks + 1;
			end
		elseif CLEU_AURA_REMOVED[combatEvent] then
			if CLEU_TRACK_AURA[spellID] then
				voidForm = false;
				pauseInsanityDrain = false;
				insanityDrainStacks = 0;
			elseif CLEU_AURA_PAUSE_DRAIN[spellID] then
				pauseInsanityDrain = false;
			end
		end
	end
end

local function resetVariables()
	voidForm = false;
	insanityDrainStacks = 0;
	pauseInsanityDrain = false;
end

local moduleEvents = {
	Disabled = function()
		resetVariables();
	end,

	Enabled = function()
		resetVariables();
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = combatLog,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);