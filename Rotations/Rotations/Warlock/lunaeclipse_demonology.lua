local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local Objects = addon.Core.Objects;
local Summons = addonTable.Summons;

-- Objects
local Pet = addon.Units.Pet;
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Creature IDs
local Guardians = {
	DemonicTyrant = 135002,
	Dreadstalkers = 98035,
	Felguard = 888888, -- Fake ID to seperate Grimoire Felguard from regular felguard.
	WildImps = 55659,
};

-- Rotation Variables
local nameAPL = "lunaeclipse_warlock_demonology";

-- Builder Rotation
local function Builder(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.build_a_shard=demonbolt,if=azerite.forbidden_knowledge.enabled&buff.forbidden_knowledge.react&!buff.demonic_core.react&cooldown.summon_demonic_tyrant.remains>20
		Demonbolt = function()
			return Azerite.ForbiddenKnowledge.Enabled()
			   and Player.Buff(Aura.ForbiddenKnowledge).React()
			   and not Player.Buff(Aura.DemonicCore).React()
			   and Spell.SummonDemonicTyrant.Cooldown.Remains() > 20;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Demonbolt, self.Requirements.Demonbolt);
		-- actions.build_a_shard+=/soul_strike
		action.EvaluateAction(Talent.SoulStrike, true);
		-- actions.build_a_shard+=/shadow_bolt
		action.EvaluateAction(Spell.ShadowBolt, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationBuilder = Builder("Builder");

-- Implosion Rotation
local function Implosion(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.implosion+=/bilescourge_bombers,if=cooldown.summon_demonic_tyrant.remains>9
		BilescourgeBombers = function()
			return Spell.SummonDemonicTyrant.Cooldown.Remains() > 9;
		end,

		-- LUA can't interpet numbers as boolean, so lets use Up() which returns a boolean.
		-- actions.implosion+=/call_dreadstalkers,if=(cooldown.summon_demonic_tyrant.remains<9&buff.demonic_calling.remains)|(cooldown.summon_demonic_tyrant.remains<11&!buff.demonic_calling.remains)|cooldown.summon_demonic_tyrant.remains>14
		CallDreadstalkers = function()
			return (Spell.SummonDemonicTyrant.Cooldown.Remains() < 9 and Player.Buff(Aura.DemonicCalling).Up())
			    or (Spell.SummonDemonicTyrant.Cooldown.Remains() < 11 and not Player.Buff(Aura.DemonicCalling).Up())
			    or Spell.SummonDemonicTyrant.Cooldown.Remains() > 14;
		end,

		Demonbolt = {
			-- actions.implosion+=/demonbolt,if=prev_gcd.1.hand_of_guldan&soul_shard>=1&(buff.wild_imps.stack<=3|prev_gcd.3.hand_of_guldan)&soul_shard<4&buff.demonic_core.up
			HandOfGuldan = function()
				return Player.PrevGCD(1, Spell.HandOfGuldan)
				   and Player.SoulShards() >= 1
				   and (Summons.Count(Guardians.WildImps) <= 3 or Player.PrevGCD(3, Spell.HandOfGuldan))
				   and Player.SoulShards() < 4
				   and Player.Buff(Aura.DemonicCore).Up();
			end,

			-- actions.implosion+=/demonbolt,if=soul_shard<=3&buff.demonic_core.up&(buff.demonic_core.stack>=3|buff.demonic_core.remains<=gcd*5.7)
			Use = function()
				return Player.SoulShards() <= 3
				   and Player.Buff(Aura.DemonicCore).Up()
				   and (Player.Buff(Aura.DemonicCore).Stack() >= 3 or Player.Buff(Aura.DemonicCore).Remains() <= Player.GCD() * 5.7);
			end,
		},

		-- actions.implosion+=/doom,cycle_targets=1,max_cycle_targets=7,if=refreshable
		Doom = function(numEnemies, Target)
			return Target.Debuff(Aura.Doom).Refreshable();
		end,

		HandOfGuldan = {
			-- actions.implosion+=/hand_of_guldan,if=soul_shard>=5
			SoulShards = function()
				return Player.SoulShards() >= 5;
			end,

			-- actions.implosion+=/hand_of_guldan,if=soul_shard>=3&(((prev_gcd.2.hand_of_guldan|buff.wild_imps.stack>=3)&buff.wild_imps.stack<9)|cooldown.summon_demonic_tyrant.remains<=gcd*2|buff.demonic_power.remains>gcd*2)
			Use = function()
				return Player.SoulShards() >= 3
				   and (((Player.PrevGCD(2, Spell.HandOfGuldan) or Summons.Count(Guardians.WildImps) >= 3) and Summons.Count(Guardians.WildImps) < 9) or Spell.SummonDemonicTyrant.Cooldown.Remains() <= Player.GCD() * 2 or Player.Buff(Aura.DemonicPower).Remains() > Player.GCD() * 2);
			end,
		},

		-- actions.implosion=implosion,if=(buff.wild_imps.stack>=6&(soul_shard<3|prev_gcd.1.call_dreadstalkers|buff.wild_imps.stack>=9|prev_gcd.1.bilescourge_bombers|(!prev_gcd.1.hand_of_guldan&!prev_gcd.2.hand_of_guldan))&!prev_gcd.1.hand_of_guldan&!prev_gcd.2.hand_of_guldan&buff.demonic_power.down)|(time_to_die<3&buff.wild_imps.stack>0)|(prev_gcd.2.call_dreadstalkers&buff.wild_imps.stack>2&!talent.demonic_calling.enabled)
		Implosion = function()
			return (Summons.Count(Guardians.WildImps) >= 6 and (Player.SoulShards() < 3 or Player.PrevGCD(1, Spell.CallDreadstalkers) or Summons.Count(Guardians.WildImps) >= 9 or Player.PrevGCD(1, Talent.BilescourgeBombers) or (not Player.PrevGCD(2, Spell.HandOfGuldan))) and not Player.PrevGCD(1, Spell.HandOfGuldan) and not Player.PrevGCD(2, Spell.HandOfGuldan) and Player.Buff(Aura.DemonicPower).Down())
			    or (Target.TimeToDie() < 3 and Summons.Count(Guardians.WildImps) > 0)
				or (Player.PrevGCD(2, Spell.CallDreadstalkers) and Summons.Count(Guardians.WildImps) > 2 and not Talent.DemonicCalling.Enabled());
		end,

		-- actions.implosion+=/soul_strike,if=soul_shard<5&buff.demonic_core.stack<=2
		SoulStrike = function()
			return Player.SoulShards() < 5
			   and Player.Buff(Aura.DemonicCore).Stack() <= 2;
		end,

		-- actions.implosion+=/summon_vilefiend,if=(cooldown.summon_demonic_tyrant.remains>40&spell_targets.implosion<=2)|cooldown.summon_demonic_tyrant.remains<12
		SummonVilefiend = function(numEnemies)
			return (Spell.SummonDemonicTyrant.Cooldown.Remains() > 40 and numEnemies <= 2)
			    or Spell.SummonDemonicTyrant.Cooldown.Remains() < 12;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Implosion, self.Requirements.Implosion);
		-- Legion legendaries cease to work after level 115, so this means legendary equipped will always be false so skip the condition check completely.
		-- actions.implosion+=/grimoire_felguard,if=cooldown.summon_demonic_tyrant.remains<13|!equipped.132369
		action.EvaluateAction(Talent.GrimoireFelguard, true);
		-- The addon can't actually tell if the enemies are stacked which this subrotation needs,
		-- so lets move this up the priority list so Demonic Tyrant is only summoned after Vilefiend as per non-stacked conditions.
		action.EvaluateAction(Talent.SummonVilefiend, self.Requirements.SummonVilefiend);
		action.EvaluateAction(Spell.CallDreadstalkers, self.Requirements.CallDreadstalkers);
		-- actions.implosion+=/summon_demonic_tyrant
		action.EvaluateAction(Spell.SummonDemonicTyrant, true);
		action.EvaluateAction(Spell.HandOfGuldan, self.Requirements.HandOfGuldan.SoulShards);
		action.EvaluateAction(Spell.HandOfGuldan, self.Requirements.HandOfGuldan.use);
		action.EvaluateAction(Spell.Demonbolt, self.Requirements.Demonbolt.HandOfGuldan);
		action.EvaluateAction(Talent.BilescourgeBombers, self.Requirements.BilescourgeBombers);
		action.EvaluateAction(Talent.SoulStrike, self.Requirements.SoulStrike);
		action.EvaluateAction(Spell.Demonbolt, self.Requirements.Demonbolt.Use);
		action.EvaluateCycleAction(Talent.Doom, self.Requirements.Doom);

		-- actions.implosion+=/call_action_list,name=build_a_shard
		action.CallActionList(rotationBuilder);
	end

	-- actions+=/call_action_list,name=implosion,if=spell_targets.implosion>1
	function self.Use(numEnemies)
		return numEnemies > 1;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationImplosion = Implosion("Implosion");

-- NetherPortalActive Rotation
local function NetherPortalActive(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.nether_portal_active+=/call_action_list,name=build_a_shard,if=soul_shard=1&(cooldown.call_dreadstalkers.remains<action.shadow_bolt.cast_time|(talent.bilescourge_bombers.enabled&cooldown.bilescourge_bombers.remains<action.shadow_bolt.cast_time))
		Builder = function()
			return Player.SoulShards() == 1
			   and (Spell.CallDreadstalkers.Cooldown.Remains() < Spell.ShadowBolt.CastTime() or (Talent.BilescourgeBombers.Enabled() and Talent.BilescourgeBombers.Cooldown.Remains() < Spell.ShadowBolt.CastTime()));
		end,

		-- LUA can't interpret numbers as true or false, so use Up() which returns a boolean.
		-- actions.nether_portal_active+=/call_dreadstalkers,if=(cooldown.summon_demonic_tyrant.remains<9&buff.demonic_calling.remains)|(cooldown.summon_demonic_tyrant.remains<11&!buff.demonic_calling.remains)|cooldown.summon_demonic_tyrant.remains>14
		CallDreadstalkers = function()
			return (Spell.SummonDemonicTyrant.Cooldown.Remains() < 9 and Player.Buff(Aura.DemonicCalling).Up())
			    or (Spell.SummonDemonicTyrant.Cooldown.Remains() < 11 and not Player.Buff(Aura.DemonicCalling).Up())
			    or Spell.SummonDemonicTyrant.Cooldown.Remains() > 14;
		end,

		-- actions.nether_portal_active+=/demonbolt,if=buff.demonic_core.up
		Demonbolt = function()
			return Player.Buff(Aura.DemonicCore).Up();
		end,

		-- actions.nether_portal_active+=/hand_of_guldan,if=((cooldown.call_dreadstalkers.remains>action.demonbolt.cast_time)&(cooldown.call_dreadstalkers.remains>action.shadow_bolt.cast_time))&cooldown.nether_portal.remains>(160+action.hand_of_guldan.cast_time)
		HandOfGuldan = function()
			return ((Spell.CallDreadstalkers.Cooldown.Remains() > Spell.Demonbolt.CastTime()) and (Spell.CallDreadstalkers.Cooldown.Remains() > Spell.ShadowBolt.CastTime()))
			   and Talent.NetherPortal.Cooldown.Remains() > 160 + Spell.HandOfGuldan.CastTime();
		end,

		SummonDemonicTyrant = {
			-- actions.nether_portal_active+=/summon_demonic_tyrant,if=buff.nether_portal.remains<10&soul_shard=0
			NoSoulShards = function()
				return Player.Buff(Aura.NetherPortal).Remains() < 10
				   and Player.SoulShards() == 0;
			end,

			-- actions.nether_portal_active+=/summon_demonic_tyrant,if=buff.nether_portal.remains<action.summon_demonic_tyrant.cast_time+5.5
			Use = function()
				return Player.Buff(Aura.NetherPortal).Remains() < Spell.SummonDemonicTyrant.CastTime() + 5.5;
			end,
		},

		-- actions.nether_portal_active+=/summon_vilefiend,if=cooldown.summon_demonic_tyrant.remains>40|cooldown.summon_demonic_tyrant.remains<12
		SummonVilefiend = function()
			return Spell.SummonDemonicTyrant.Cooldown.Remains() > 40
			    or Spell.SummonDemonicTyrant.Cooldown.Remains() < 12;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- Legion legendaries cease to work after level 115, so this means legendary equipped will always be false so skip the condition check completely.
		-- actions.nether_portal_active=grimoire_felguard,if=cooldown.summon_demonic_tyrant.remains<13|!equipped.132369
		action.EvaluateAction(Talent.GrimoireFelguard, true);
		action.EvaluateAction(Talent.SummonVilefiend, self.Requirements.SummonVilefiend);
		action.EvaluateAction(Spell.CallDreadstalkers, self.Requirements.CallDreadstalkers);

		action.CallActionList(rotationBuilder, self.Requirements.Builder);

		action.EvaluateAction(Spell.HandOfGuldan, self.Requirements.HandOfGuldan);
		action.EvaluateAction(Spell.SummonDemonicTyrant, self.Requirements.SummonDemonicTyrant.NoSoulShards);
		action.EvaluateAction(Spell.SummonDemonicTyrant, self.Requirements.SummonDemonicTyrant.Use);
		action.EvaluateAction(Spell.Demonbolt, self.Requirements.Demonbolt);

		-- actions.nether_portal_active+=/call_action_list,name=build_a_shard
		action.CallActionList(rotationBuilder);
	end

	-- actions.nether_portal+=/call_action_list,name=nether_portal_active,if=cooldown.nether_portal.remains>160
	function self.Use()
		return Talent.NetherPortal.Cooldown.Remains() > 160;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationNetherPortalActive = NetherPortalActive("NetherPortalActive");

-- NetherPortalBuilder Rotation
local function NetherPortalBuilder(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		HandOfGuldan = {
			-- actions.nether_portal_building+=/hand_of_guldan,if=soul_shard>=5
			SoulShards = function()
				return Player.SoulShards() >= 5;
			end,

			-- actions.nether_portal_building+=/hand_of_guldan,if=cooldown.call_dreadstalkers.remains>18&soul_shard>=3
			Use = function()
				return Spell.CallDreadstalkers.Cooldown.Remains() > 18
				   and Player.SoulShards() >= 3;
			end,
		},

		-- actions.nether_portal_building=nether_portal,if=soul_shard>=5&(!talent.power_siphon.enabled|buff.demonic_core.up)
		NetherPortal = function()
			return Player.SoulShards() >= 5
			   and (not Talent.PowerSiphon.Enabled() or Player.Buff(Aura.DemonicCore).Up());
		end,

		-- actions.nether_portal_building+=/power_siphon,if=buff.wild_imps.stack>=2&buff.demonic_core.stack<=2&buff.demonic_power.down&soul_shard>=3
		PowerSiphon = function()
			return Summons.Count(Guardians.WildImps) >= 2
			   and Player.Buff(Aura.DemonicCore).Stack() <= 2
			   and Player.Buff(Aura.DemonicPower).Down()
			   and Player.SoulShards() >= 3;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.NetherPortal, self.Requirements.NetherPortal);
		-- actions.nether_portal_building+=/call_dreadstalkers
		action.EvaluateAction(Spell.CallDreadstalkers, true);
		action.EvaluateAction(Spell.HandOfGuldan, self.Requirements.HandOfGuldan.Use);
		action.EvaluateAction(Talent.PowerSiphon, self.Requirements.PowerSiphon);
		action.EvaluateAction(Spell.HandOfGuldan, self.Requirements.HandOfGuldan.SoulShards);

		-- actions.nether_portal_building+=/call_action_list,name=build_a_shard
		action.CallActionList(rotationBuilder);
	end

	-- actions.nether_portal=call_action_list,name=nether_portal_building,if=cooldown.nether_portal.remains<20
	function self.Use()
		return Talent.NetherPortal.Cooldown.Remains() < 20;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationNetherPortalBuilder = NetherPortalBuilder("NetherPortalBuilder");

-- NetherPortal Rotation
local function NetherPortal(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.CallActionList(rotationNetherPortalBuilder);
		action.CallActionList(rotationNetherPortalActive);
	end

	-- actions+=/call_action_list,name=nether_portal,if=talent.nether_portal.enabled&spell_targets.implosion<=2
	function self.Use(numEnemies)
		return Talent.NetherPortal.Enabled()
		   and numEnemies <= 2;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationNetherPortal = NetherPortal("NetherPortal");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Not and SimCraft",
		GuideLink = "https://www.wowhead.com/demonology-warlock-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		DarkPact = function()
			return Player.DamagePredicted(5) >= 40;
		end,

		UnendingResolve = function()
			return Player.DamagePredicted(4) >= 20;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions=potion,if=pet.demonic_tyrant.active|target.time_to_die<30
		BattlePotionOfIntellect = function()
			return Summons.Active(Guardians.DemonicTyrant)
			    or Target.TimeToDie() < 30;
		end,

		-- actions+=/berserking,if=pet.demonic_tyrant.active|target.time_to_die<=15
		Berserking = function()
			return Summons.Active(Guardians.DemonicTyrant)
			    or Target.TimeToDie() <= 15;
		end,

		-- actions+=/blood_fury,if=pet.demonic_tyrant.active|target.time_to_die<=15
		BloodFury = function()
			return Summons.Active(Guardians.DemonicTyrant)
			    or Target.TimeToDie() <= 15;
		end,

		-- Legion legendaries cease to work after level 115, so just skip this, LUA can't interpret a number a true, so use Up() instead of Remains().
		-- actions+=/call_dreadstalkers,if=equipped.132369|(cooldown.summon_demonic_tyrant.remains<9&buff.demonic_calling.remains)|(cooldown.summon_demonic_tyrant.remains<11&!buff.demonic_calling.remains)|cooldown.summon_demonic_tyrant.remains>14
		CallDreadstalkers = function()
			return (Spell.SummonDemonicTyrant.Cooldown.Remains() < 9 and Player.Buff(Aura.DemonicCalling).Up())
			    or (Spell.SummonDemonicTyrant.Cooldown.Remains() < 11 and not Player.Buff(Aura.DemonicCalling).Up())
			    or Spell.SummonDemonicTyrant.Cooldown.Remains() > 14;
		end,

		-- actions+=/demonbolt,if=soul_shard<=3&buff.demonic_core.up&((cooldown.summon_demonic_tyrant.remains<10|cooldown.summon_demonic_tyrant.remains>22)|buff.demonic_core.stack>=3|buff.demonic_core.remains<5|time_to_die<25)
		Demonbolt = function()
			return Player.SoulShards() <= 3
			   and Player.Buff(Aura.DemonicCore).Up()
			   and ((Spell.SummonDemonicTyrant.Cooldown.Remains() < 10 or Spell.SummonDemonicTyrant.Cooldown.Remains() > 22) or Player.Buff(Aura.DemonicCore).Stack() >= 3 or Player.Buff(Aura.DemonicCore).Remains() < 5 or Target.TimeToDie() < 25);
		end,

		-- actions+=/demonic_strength,if=(buff.wild_imps.stack<6|buff.demonic_power.up)|spell_targets.implosion<2
		DemonicStrength = function(numEnemies)
			return (Summons.Count(Guardians.WildImps) < 6 or Player.Buff(Aura.DemonicPower).Up())
			    or numEnemies < 2;
		end,

		Doom = {
			-- actions+=/doom,if=!ticking&time_to_die>30&spell_targets.implosion<2
			Refresh = function(numEnemies)
				return not Target.Debuff(Aura.Doom).Ticking()
				   and Target.TimeToDie() > 30
				   and numEnemies < 2;
			end,

			-- actions+=/doom,if=talent.doom.enabled&refreshable&time_to_die>(dot.doom.remains+30)
			Use = function()
				return Talent.Doom.Enabled()
				   and Target.Debuff(Aura.Doom).Refreshable()
				   and Target.TimeToDie() > Target.Debuff(Aura.Doom).Remains() + 30;
			end,
		},

		-- actions+=/fireblood,if=pet.demonic_tyrant.active|target.time_to_die<=15
		Fireblood = function()
			return Summons.Active(Guardians.DemonicTyrant)
			    or Target.TimeToDie() <= 15;
		end,

		-- Legion legendaries cease to work after level 115, so just skip this.
		-- actions+=/grimoire_felguard,if=cooldown.summon_demonic_tyrant.remains<13|!equipped.132369
		GrimoireFelguard = function()
			return Spell.SummonDemonicTyrant.Cooldown.Remains() < 13;
		end,

		-- actions+=/hand_of_guldan,if=soul_shard>=5|(soul_shard>=3&cooldown.call_dreadstalkers.remains>4&(!talent.summon_vilefiend.enabled|cooldown.summon_vilefiend.remains>3))
		HandOfGuldan = function()
			return Player.SoulShards() >= 5
			    or (Player.SoulShards() >= 3 and Spell.CallDreadstalkers.Cooldown.Remains() > 4 and (not Talent.SummonVilefiend.Enabled() or Talent.SummonVilefiend.Cooldown.Remains() > 3));
		end,

		-- actions+=/power_siphon,if=buff.wild_imps.stack>=2&buff.demonic_core.stack<=2&buff.demonic_power.down&spell_targets.implosion<2
		PowerSiphon = function(numEnemies)
			return Summons.Count(Guardians.WildImps) >= 2
			   and Player.Buff(Aura.DemonicCore).Stack() <= 2
			   and Player.Buff(Aura.DemonicPower).Down()
			   and numEnemies < 2;
		end,

		-- actions+=/soul_strike,if=soul_shard<5&buff.demonic_core.stack<=2
		SoulStrike = function()
			return Player.SoulShards() < 5
			   and Player.Buff(Aura.DemonicCore).Stack() <= 2;
		end,

		-- Legion legendaries cease to work after level 115, so just skip this.
		-- actions+=/summon_demonic_tyrant,if=equipped.132369|(buff.dreadstalkers.remains>cast_time&(buff.wild_imps.stack>=3|prev_gcd.1.hand_of_guldan)&(soul_shard<3|buff.dreadstalkers.remains<gcd*2.7|buff.grimoire_felguard.remains<gcd*2.7))
		SummonDemonicTyrant = function()
			return Summons.LastDespawn(Guardians.Dreadstalkers) > Spell.SummonDemonicTyrant.CastTime()
			   and (Summons.Count(Guardians.WildImps) >= 3 or Player.PrevGCD(1, Spell.HandOfGuldan))
			   and (Player.SoulShards() < 3 or Summons.LastDespawn(Guardians.Dreadstalkers) < Player.GCD() * 2.7 or Summons.LastDespawn(Guardians.Felguard) < Player.GCD() * 2.7);
		end,

		-- SimCraft doesn't specify this, but only summon if you don't already have a pet summoned
		-- actions.precombat+=/summon_pet
		SummonFelguard = function()
			return not Pet.Family("Felguard");
		end,

		-- Legion legendaries cease to work after level 115, so just skip this.
		-- actions+=/summon_vilefiend,if=equipped.132369|cooldown.summon_demonic_tyrant.remains>40|cooldown.summon_demonic_tyrant.remains<12
		SummonVilefiend = function()
			return Spell.SummonDemonicTyrant.Cooldown.Remains() > 40
			    or Spell.SummonDemonicTyrant.Cooldown.Remains() < 12;
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Aura.ForbiddenKnowledge = Objects.newSpell(279666);

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfIntellect = Objects.newItem(163222),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood);
		config.AOEOptions(options, Talent.BilescourgeBombers, Spell.HandOfGuldan, Spell.Implosion);
		config.CooldownOptions(options, Talent.DemonicStrength, Talent.Doom, Talent.GrimoireFelguard, Talent.NetherPortal, Talent.PowerSiphon, Talent.SoulStrike, Spell.SummonDemonicTyrant, Talent.SummonVilefiend);
		config.DefensiveOptions(options, Talent.DarkPact, Spell.UnendingResolve);
		config.PetOptions(options, Spell.SummonFelguard);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Talent.DarkPact, self.Defensives.DarkPact);
		action.EvaluateDefensiveAction(Spell.UnendingResolve, self.Defensives.UnendingResolve);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.SummonFelguard, self.Requirements.SummonFelguard);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfIntellect, true);
		-- actions.precombat+=/demonbolt
		action.EvaluateAction(Spell.Demonbolt, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		action.EvaluateAction(Consumable.BattlePotionOfIntellect, self.Requirements.BattlePotionOfIntellect);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Fireblood, self.Requirements.Fireblood);
		action.EvaluateAction(Talent.Doom, self.Requirements.Doom.Refresh);
		action.EvaluateAction(Talent.DemonicStrength, self.Requirements.DemonicStrength);

		action.CallActionList(rotationNetherPortal);
		action.CallActionList(rotationImplosion);

		action.EvaluateAction(Talent.GrimoireFelguard, self.Requirements.GrimoireFelguard);
		action.EvaluateAction(Talent.SummonVilefiend, self.Requirements.SummonVilefiend);
		action.EvaluateAction(Spell.CallDreadstalkers, self.Requirements.CallDreadstalkers);
		action.EvaluateAction(Spell.SummonDemonicTyrant, self.Requirements.SummonDemonicTyrant);
		action.EvaluateAction(Talent.PowerSiphon, self.Requirements.PowerSiphon);
		action.EvaluateAction(Talent.Doom, self.Requirements.Doom.Use);
		action.EvaluateAction(Spell.HandOfGuldan, self.Requirements.HandOfGuldan);
		action.EvaluateAction(Talent.SoulStrike, self.Requirements.SoulStrike);
		action.EvaluateAction(Spell.Demonbolt, self.Requirements.Demonbolt);

		-- actions+=/call_action_list,name=build_a_shard
		action.CallActionList(rotationBuilder);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Demonology Warlock", addonTable.Enum.SpecID.WARLOCK_DEMONOLOGY);
