-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local StaticPopupDialogs = StaticPopupDialogs;
local pluginOptions;

StaticPopupDialogs["LERH_PLUGIN_CHANGES"] = {
	text = "You have made changes to which plugins are enabled.\n\nA reload of the user interface is required for the changes to take effect.\n\nPlease confirm you wish to Reload the UI!\n\n",
	button1	= "Yes",
	button2	= "No",
	OnAccept = function()
		addon.API.Blizzard.ReloadUI();
	end,
	whileDead = true,
	hideOnEscape = true,
};

local function createConfigIcon(optionsFrame)
	local format = format;
	local AddonLogo = addon.API.Blizzard.CreateFrame("Frame", nil, optionsFrame);

	AddonLogo:SetFrameLevel(4);
	AddonLogo:SetSize(64, 64);
	AddonLogo:SetPoint("TOPRIGHT", 8, 24);
	AddonLogo:SetBackdrop({ bgFile = format("Interface\\AddOns\\%s\\Textures\\Logo", addonName) });
end

local function createSplashImage(optionsFrame)
	local InterfaceOptionsFramePanelContainer, format, min = InterfaceOptionsFramePanelContainer, format, min;

	if InterfaceOptionsFramePanelContainer then
		local height = InterfaceOptionsFramePanelContainer:GetHeight() - 24;
		local width = InterfaceOptionsFramePanelContainer:GetWidth();
		local ratio = min(height / 1024, width / 1024);

		local SplashScreen = addon.API.Blizzard.CreateFrame("Frame", nil, optionsFrame);

		SplashScreen:SetFrameLevel(4);
		SplashScreen:SetSize(1024 * ratio, 1024 * ratio);
		SplashScreen:SetPoint("CENTER", 0, 0);
		SplashScreen:SetBackdrop({ bgFile = format("Interface\\AddOns\\%s\\Textures\\SplashScreen", addonName) });
	end
end

local function closePlugins()
	local Settings = addon.Core.Settings;
	local Tables = addon.Core.Tables;

	if not Tables.Equate(Settings.GetCharacterValue("Plugins"), pluginOptions) then
		-- Enabled plugins have changed and the player has moved to another config screen, so now prompt to reload UI.
		addon.API.Blizzard.StaticPopup_Show("LERH_PLUGIN_CHANGES");
	end
end

local function openPlugins()
	local Settings = addon.Core.Settings;
	local Tables = addon.Core.Tables;

	pluginOptions = Tables.Duplicate(Settings.GetCharacterValue("Plugins"));
end

addon.Options.mainOptions = {
	type = "group",
	name = "",
	childGroups = "tree",
	args = {},
};

local moduleEvents = {
	optionsChanged = function(optionsPage)
		local LibStub = LibStub;

		LibStub("AceConfigRegistry-3.0"):NotifyChange(optionsPage);
	end,

	playerLogin = function()
		local LibStub = LibStub;

		LibStub("AceConfig-3.0"):RegisterOptionsTable(addonName, addon.Options.mainOptions);
		LibStub("AceConfig-3.0"):RegisterOptionsTable("LERH_AddonSettings", addon.Options.addonOptions);
		LibStub("AceConfig-3.0"):RegisterOptionsTable("LERH_RotationSettings", addon.Options.rotationOptions);
		LibStub("AceConfig-3.0"):RegisterOptionsTable("LERH_PluginSettings", addon.Options.pluginOptions)

		addonTable.mainOptionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions(addonName, "LunaEclipse: Rotation Helper");
		addonTable.addonSettingsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("LERH_AddonSettings", "Addon Settings", "LunaEclipse: Rotation Helper");
		addonTable.rotationSettingsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("LERH_RotationSettings", "Rotation Settings", "LunaEclipse: Rotation Helper");
		addonTable.pluginSettingsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("LERH_PluginSettings", "Plugins", "LunaEclipse: Rotation Helper");

		addonTable.pluginSettingsFrame:HookScript("OnShow", openPlugins);
		addonTable.pluginSettingsFrame:HookScript("OnHide", closePlugins);

		-- Add logo to the main page of the config frame.
		createSplashImage(addonTable.mainOptionsFrame);
		-- Add icon to each configuration page.
		createConfigIcon(addonTable.addonSettingsFrame);
		createConfigIcon(addonTable.rotationSettingsFrame);
		createConfigIcon(addonTable.pluginSettingsFrame);
	end,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);