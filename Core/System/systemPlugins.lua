-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local ipairs, sort, strlower, tinsert = ipairs, sort, strlower, tinsert;

local registeredPlugins = {
	Descriptions = {},
	Options = {},
	Plugins = {},
};

local Register = {};

Register = {
	Description = function(pluginName, pluginDescription)
		if pluginDescription then
			local optionsPlugins = addon.Options.Plugins;

			local key = optionsPlugins.CovertPluginName(pluginName);
			local descriptionsTable = registeredPlugins.Descriptions or {};

			descriptionsTable[key] = pluginDescription;

			registeredPlugins.Descriptions = descriptionsTable;
		end
	end,

	IsRegistered = function(pluginName)
		if pluginName then
			local pluginTable = registeredPlugins.Plugins or {};

			for _, plugin in ipairs(pluginTable) do
				if strlower(plugin) == strlower(pluginName) then
					return true;
				end
			end
		end

		return false;
	end,

	Name = function(pluginName)
		local pluginTable = registeredPlugins.Plugins or {};

		tinsert(pluginTable, pluginName);
		sort(pluginTable);

		registeredPlugins.Plugins = pluginTable;
	end,

	Options = function(pluginName, optionTable)
		if optionTable then
			local optionsPlugins = addon.Options.Plugins;
			local key = optionsPlugins.CovertPluginName(pluginName);
			local optionsTable = registeredPlugins.Options or {};

			optionsTable[key] = optionTable;

			registeredPlugins.Options = optionsTable;
		end
	end,
};

addon.System.Plugins = {
	--- Retrieve a table of all plugins that have been registered.
	-- @return registeredPlugins: table - The table containing all the plugins that have been registered.
	GetRegisteredPlugins = function()
		return registeredPlugins;
	end,

	--- Retrieve whether a plugin has been set to load or not.
	-- @param pluginName: string - The name of the plugin that should be checked if it is active.
	-- @return pluginActive: boolean - Will return true if the plugin is set to load, otherwise it will return false.
	IsActive = function(pluginName)
		local coreSettings = addon.Core.Settings;
		local optionsPlugins = addon.Options.Plugins;

		return coreSettings.GetCharacterValue("Plugins", optionsPlugins.CovertPluginName(pluginName), 0) == 1;
	end,

	--- Retrieve whether a plugin has been registered or not.
	-- @param pluginName: string - The name of the plugin that should be checked if it is registered.
	-- @return pluginRegistered: boolean - Will return true if the plugin has been registered, otherwise it will return false.
	IsRegistered = function(pluginName)
		return Register.IsRegistered(pluginName);
	end,

	--- Register a plugin for use with the addon.
	-- @param pluginName: string - The name of the plugin to register.
	-- @param pluginDescription: string = The description of the plugin, this is used in the options table for tooltips etc...
	-- @param optionsTable: table - This is a WoWAce options table, see WoWAce documentation for layout information.
	RegisterPlugin = function(pluginName, pluginDescription, optionTable)
		if pluginName and not Register.IsRegistered(pluginName) then
			Register.Name(pluginName);
			Register.Description(pluginName, pluginDescription);
			Register.Options(pluginName, optionTable);
		end
	end,
};