local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Enum.DamageType = {
	NONE = 0,
	MELEE = 1,
	SPELL = 2,
	RANGED = 3,
};