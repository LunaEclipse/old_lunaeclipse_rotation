-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local ceil, huge, max, min, setmetatable = ceil, math.huge, max, min, setmetatable;
local Cache = setmetatable({}, { __mode = "kv" });

addonTable.propertiesCharges = function(Spell)
	local Objects = addon.Core.Objects;
	local key = Objects.GetID(Spell) or Spell;

	local self = Cache[key] or {};

	self.Charges = function()
		local Tables = addon.Core.Tables;
		local apiSpell = addon.API.Spell;

		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;
		local cooldownInfo = apiSpell.GetSpellCooldown(spellID);

		return Tables.GetValue(cooldownInfo, "Charges", 0);
	end

	self.Duration = function()
		local Tables = addon.Core.Tables;
		local apiSpell = addon.API.Spell;

		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;
		local cooldownInfo = apiSpell.GetSpellCooldown(spellID);

		return Tables.GetValue(cooldownInfo, "ChargesDuration", 0);
	end

	self.End = function()
		local Tables = addon.Core.Tables;
		local apiSpell = addon.API.Spell;

		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;
		local cooldownInfo = apiSpell.GetSpellCooldown(spellID);

		return Tables.GetValue(cooldownInfo, "ChargesEnd", 0);
	end

	-- action.foo.charges_fractional or cooldown.foo.charges_fractional
	self.Fractional = function()
		return (self.Charges() < self.Max() and self.Charges() + (self.Recharge() / self.Duration()))
			or self.Charges();
	end

	-- action.foo.full_recharge_time or cooldown.foo.charges_full_recharge_time
	self.FullRechargeTime = function()
		return self.Recharge() + (max(self.Max() - (self.Charges() + 1), 0) * self.Duration());
	end

	self.Locked = function()
		local Tables = addon.Core.Tables;
		local apiSpell = addon.API.Spell;

		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;
		local cooldownInfo = apiSpell.GetSpellCooldown(spellID);

		return Tables.GetValue(cooldownInfo, "Locked", false);
	end

	-- action.foo.max_charges or cooldown.foo.max_charges
	self.Max = function()
		local Tables = addon.Core.Tables;
		local apiSpell = addon.API.Spell;

		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;
		local cooldownInfo = apiSpell.GetSpellCooldown(spellID);

		return Tables.GetValue(cooldownInfo, "MaxCharges", 0);
	end

	-- action.foo.recharge_time or cooldown.foo.recharge_time
	self.Recharge = function()
		local General = addon.API.General;
		local Tables = addon.Core.Tables;
		local apiSpell = addon.API.Spell;

		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;
		local cooldownInfo = apiSpell.GetSpellCooldown(spellID);
		local modRate = Tables.GetValue(cooldownInfo, "ChargesModRate", 1);

		if self.Locked() or (spellID ~= addonTable.GLOBAL_COOLDOWN and not Spell.IsKnown() and not Spell.Enabled()) then
			return huge;
		elseif self.End() == 2 ^ 32 / 1000 then
			return 0;
		elseif self.End() > 0 then
			return max((self.End() - General.GetTime()) / modRate, 0);
		end

		return 0;
	end

	self.Start = function()
		local Tables = addon.Core.Tables;
		local apiSpell = addon.API.Spell;

		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;
		local cooldownInfo = apiSpell.GetSpellCooldown(spellID);

		return Tables.GetValue(cooldownInfo, "ChargesStart", 0);
	end

	setmetatable(self, { __call = function(table, ...) return self.Charges(...); end });
	Cache[key] = self;

	return self;
end