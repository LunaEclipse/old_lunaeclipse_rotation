local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Data.Racial = {
	Aura = {
		-- Alliance Races
		Darkflight = 68992,
		Fireblood = 273104,
		GiftOfTheNaaru = 28880,
		RunningWild = 87840,
		Shadowmeld = 58984,
		SpatialRift = 256948,
		StoneForm = 65116,
		-- Horde Races
		ArcanePulse = 260369,
		Berserking = 26297,
		BloodFury = 20572,
		BullRush = 255723,
		Cannibalize = 20578,
		FerocityOfTheFrostwolf = 274741,
		MightOfTheBlackrock = 274742,
		RictusOfTheLaughingSkull = 274739,
 		WarStomp = 20549,
 		ZealOfTheBurningBlade = 274740,
		-- Neutral Races
		QuakingPalm = 107079,
	},
	Spell = {
		-- Alliance Races
		Darkflight = { 68992, 0, false },
		EscapeArtist = { 20589, 0, false },
		EveryManForHimself = { 59752, 0, false },
		Fireblood = { 265221, 0, false },
		GiftOfTheNaaru = { 28880, 0, false },
		LightsJudgment = { 255647, 1.5, false },
		Shadowmeld = { 58984, 0, false },
		SpatialRift = { 256948, 1, false },
		Stoneform = { 20594, 0, false },
		-- Horde Races
		AncestralCall = { 274738, 0, false },
		ArcanePulse = { 260364, 1, false },
		ArcaneTorrent = { 25046, 1.5, false },
		Berserking = { 26297, 0, false },
		BloodFury = { 20572, 0, false },
		BullRush = { 255654, 1.5, false	},
		Cannibalize = { 20577, 1.5, false },
		RocketBarrage = { 69041, 1.5, false },
		RocketJump = { 69070, 1.5, false },
		WarStomp = { 20549, 0, false },
		WillOfTheForsaken = { 7744, 0, false },
		-- Neutral Races
		QuakingPalm = { 107079, 1, false },
	},
};