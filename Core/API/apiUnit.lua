-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- This is stored on the addon so its publically available to all other addons.
addon.API.Unit = {
	GetPowerInfo = function(unitID, powerType)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local currentTime = addon.API.Blizzard.GetTime();
		local returnValue;

		local GUID = addon.API.Blizzard.UnitGUID(unitID);

		if GUID and powerType ~= addonTable.Enum.PowerType.None then
			local powerInfo = Tables.GetValue(Cache.Temporary.PowerInfo, GUID) or {};

			returnValue = Tables.GetValue(powerInfo, powerType);

			if Tables.IsEmpty(returnValue) or currentTime - Tables.GetValue(powerInfo, "CacheTime", 0) >= addon.refreshInterval then
				local currentPower = addon.API.Blizzard.UnitPower(unitID, powerType);
				local maxPower = addon.API.Blizzard.UnitPowerMax(unitID, powerType);

				if currentPower then
					if unitID == "player" then
						local inactiveRegen, activeRegen = addon.API.Blizzard.GetPowerRegenForPowerType(powerType);

						returnValue = {
							Current = currentPower,
							Max = maxPower,
							CombatRegen = activeRegen,
							NoCombatRegen = inactiveRegen,
							CacheTime = currentTime,
						};
					else
						returnValue = {
							Current = currentPower,
							Max = maxPower,
							CacheTime = currentTime,
						};
					end

					Cache.Temporary.PowerInfo[GUID] = Cache.Temporary.PowerInfo[GUID] or {};
					Cache.Temporary.PowerInfo[GUID][powerType] = returnValue;
				end
			end
		end

		return returnValue;
	end,

	GetUnitInfo = function(unitID)
		local Cache = addonTable.Cache;
		local Misc = addon.Core.Misc;
		local Tables = addon.Core.Tables;

		local currentTime = addon.API.Blizzard.GetTime();
		local returnValue;

		local GUID = addon.API.Blizzard.UnitGUID(unitID);

		if GUID then
			returnValue = Tables.GetValue(Cache.Temporary.UnitInfo, GUID);

			if Tables.IsEmpty(returnValue) or currentTime - Tables.GetValue(returnValue, "CacheTime", 0) >= addon.refreshInterval then
				local GUIDInfo = Misc.ParseGUID(GUID);
				local unitName = addon.API.Blizzard.UnitName(unitID);

				if unitName then
					returnValue = {
						NPCID = GUIDInfo.Creature,
						Name = unitName,
						Classification = addon.API.Blizzard.UnitClassification(unitID),
						UnitLevel = addon.API.Blizzard.UnitLevel(unitID),
						IsPlayer = addon.API.Blizzard.UnitIsPlayer(unitID),
						Exists = addon.API.Blizzard.UnitExists(unitID) and addon.API.Blizzard.UnitIsVisible(unitID),
						Health = addon.API.Blizzard.UnitHealth(unitID),
						MaxHealth = addon.API.Blizzard.UnitHealthMax(unitID),
						IsDeadOrGhost = addon.API.Blizzard.UnitIsDeadOrGhost(unitID),
						AffectingCombat = addon.API.Blizzard.UnitAffectingCombat(unitID),
						IsMoving = addon.API.Blizzard.GetUnitSpeed(unitID) ~= 0,
						PowerType = addon.API.Blizzard.UnitPowerType(unitID),
						CacheTime = currentTime,
					};

					Cache.Temporary.UnitInfo[GUID] = returnValue;
				end
			end
		end

		return returnValue;
	end,

	InCombat = function()
		return addon.API.Blizzard.InCombatLockdown() or addon.API.Blizzard.UnitAffectingCombat("player");
	end,

	UnitAura = function(unitID, spellID, anyCaster)
		local Auras = addonTable.Auras;

		return Auras.UnitAura(unitID, spellID, anyCaster);
	end,

	UnitBuff = function(unitID, spellID, anyCaster)
		local Auras = addonTable.Auras;

		return Auras.UnitBuff(unitID, spellID, anyCaster);
	end,

	UnitCastingInfo = function(unitID)
		local Cache = addonTable.Cache;
		local GUID = addon.API.Blizzard.UnitGUID(unitID);

		return GUID and Cache.Persistant.CastingInfo[GUID]
		    or nil;
	end,

	UnitClass = function(unitID)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local returnValue = Cache.Persistant.Player.Class;

		if Tables.IsEmpty(returnValue) then
			unitID = unitID or "player";

			local classDisplayName, className, classID = addon.API.Blizzard.UnitClass(unitID);

			if classID then
				returnValue = {
					ID = classID,
					Name = className,
					DisplayName = classDisplayName,
				};

				Cache.Persistant.Player.Classes = returnValue;
			end
		end

		return returnValue;
	end,

	UnitDebuff = function(unitID, spellID, anyCaster)
		local Auras = addonTable.Auras;

		return Auras.UnitDebuff(unitID, spellID, anyCaster);
	end,

	UnitRace = function()
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local returnValue = Cache.Persistant.Player.Race;

		if Tables.IsEmpty(returnValue) then
			local raceName, raceID = addon.API.Blizzard.UnitRace("player");

			if raceID then
				returnValue = {
					ID = raceID,
					Name = raceName,
				};

				Cache.Persistant.Player.Race = returnValue;
			end
		end

		return returnValue;
	end,
};