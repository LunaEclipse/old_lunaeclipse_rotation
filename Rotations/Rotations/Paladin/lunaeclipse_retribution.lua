local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

-- Rotation Variables
local nameAPL = "lunaeclipse_paladin_retribution";

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cooldowns+=/avenging_wrath,if=buff.inquisition.up|!talent.inquisition.enabled
		AvengingWrath = function()
			return Player.Buff(Aura.Inquisition).Up()
			    or not Talent.Inquisition.Enabled();
		end,

		-- actions.cooldowns+=/crusade,if=holy_power>=4
		Crusade = function()
			return Player.HolyPower() >= 4;
		end,

		-- We can't do raid adds events so just skip these.
		-- actions.cooldowns+=/lights_judgment,if=spell_targets.lights_judgment>=2|(!raid_event.adds.exists|raid_event.adds.in>75)
		LightsJudgment = function(numEnemies)
			return numEnemies >= 2;
		end,

		-- actions.cooldowns=potion,if=(buff.bloodlust.react|buff.avenging_wrath.up|buff.crusade.up&buff.crusade.remains<25|target.time_to_die<=40)
		OldWar = function()
			return Player.HasBloodlust()
			    or Player.Buff(Aura.AvengingWrath).Up()
			    or (Player.Buff(Aura.Crusade).Up() and Player.Buff(Aura.Crusade).Remains() < 25)
			    or Target.TimeToDie() <=  40;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Consumable.OldWar, self.Requirements.OldWar);
		action.EvaluateAction(Racial.LightsJudgment, self.Requirements.LightsJudgment, Enemies.GetEnemies(Racial.LightsJudgment));
		-- Technically this is a defensive cooldown that does damage, this is suggested on the defensives icon, so don't show it here.
		-- actions.cooldowns+=/shield_of_vengeance

		action.EvaluateAction(Spell.AvengingWrath, self.Requirements.AvengingWrath);
		action.EvaluateAction(Talent.Crusade, self.Requirements.Crusade);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Finishers Rotation
local function Finishers(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		DivineStorm = {
			-- actions.finishers+=/divine_storm,if=variable.ds_castable&buff.divine_purpose.react
			DivinePurpose = function()
				return Variables.ds_castable
				   and Player.Buff(Aura.DivinePurpose).React();
			end,

			-- actions.finishers+=/divine_storm,if=variable.ds_castable&(!talent.crusade.enabled|cooldown.crusade.remains>gcd*2)
			Use = function()
				return Variables.ds_castable
				   and (not Talent.Crusade.Enabled() or Talent.Crusade.Cooldown.Remains() > Player.GCD() * 2);
			end,
		},

		-- actions.finishers+=/execution_sentence,if=spell_targets.divine_storm<=3&(!talent.crusade.enabled|cooldown.crusade.remains>gcd*2)
		ExecutionSentence = function(numEnemies)
			return numEnemies <= 3
			   and (not Talent.Crusade.Enabled() or Talent.Crusade.Cooldown.Remains() > Player.GCD() * 2);
		end,

		-- actions.finishers+=/inquisition,if=buff.inquisition.down|buff.inquisition.remains<5&holy_power>=3|talent.execution_sentence.enabled&cooldown.execution_sentence.remains<10&buff.inquisition.remains<15|cooldown.avenging_wrath.remains<15&buff.inquisition.remains<20&holy_power>=3
		Inquisition = function()
			return Player.Buff(Aura.Inquisition).Down()
			    or (Player.Buff(Aura.Inquisition).Remains() < 5 and Player.HolyPower() >= 3)
			    or (Talent.ExecutionSentence.Enabled() and Talent.ExecutionSentence.Cooldown.Remains() < 10 and Player.Buff(Aura.Inquisition).Remains() < 15)
			    or (Spell.AvengingWrath.Cooldown.Remains() < 15 and Player.Buff(Aura.Inquisition).Remains() < 20 and Player.HolyPower() >= 3);
		end,

		TemplarsVerdict = {
			-- actions.finishers+=/templars_verdict,if=buff.divine_purpose.react&(!talent.execution_sentence.enabled|cooldown.execution_sentence.remains>gcd)
			DivinePurpose = function()
				return Player.Buff(Aura.DivinePurpose).React()
				   and (not Talent.ExecutionSentence.Enabled() or Talent.ExecutionSentence.Cooldown.Remains() > Player.GCD());
			end,

			-- actions.finishers+=/templars_verdict,if=(!talent.crusade.enabled|cooldown.crusade.remains>gcd*2)&(!talent.execution_sentence.enabled|buff.crusade.up&buff.crusade.stack<10|cooldown.execution_sentence.remains>gcd*2)
			Use = function()
				return (not Talent.Crusade.Enabled() or Talent.Crusade.Cooldown.Remains() > Player.GCD() * 2)
				   and (not Talent.ExecutionSentence.Enabled() or (Player.Buff(Aura.Crusade).Up() and Player.Buff(Aura.Crusade).Stack() < 10) or Talent.ExecutionSentence.Cooldown.Remains() > Player.GCD() * 2);
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.finishers=variable,name=ds_castable,value=spell_targets.divine_storm>=3|talent.divine_judgment.enabled&spell_targets.divine_storm>=2|azerite.divine_right.enabled&target.health.pct<=20&buff.divine_right.down
		Variables.ds_castable = Enemies.GetEnemies(8) >= 3 or (Talent.DivineJudgment.Enabled() and Enemies.GetEnemies(8) >= 2 or Azerite.DivineRight.Enabled() and Target.Health.Percent() <= 20 and Player.Buff(Aura.DivineRight).Down());

		action.EvaluateAction(Talent.Inquisition, self.Requirements.Inquisition);
		action.EvaluateAction(Talent.ExecutionSentence, self.Requirements.ExecutionSentence, Enemies.GetEnemies(8));
		action.EvaluateAction(Spell.DivineStorm, self.Requirements.DivineStorm.DivinePurpose);
		action.EvaluateAction(Spell.DivineStorm, self.Requirements.DivineStorm.Use);
		action.EvaluateAction(Spell.TemplarsVerdict, self.Requirements.TemplarsVerdict.DivinePurpose);
		action.EvaluateAction(Spell.TemplarsVerdict, self.Requirements.TemplarsVerdict.Use);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationFinishers = Finishers("Finishers");

-- Generators Rotation
local function Generators(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.generators+=/arcane_torrent,if=(debuff.execution_sentence.up|(talent.hammer_of_wrath.enabled&(target.health.pct>=20|buff.avenging_wrath.down|buff.crusade.down))|!talent.execution_sentence.enabled|!talent.hammer_of_wrath.enabled)&holy_power<=4
		ArcaneTorrent = function()
			return (Target.Debuff(Aura.ExecutionSentence).Up() or (Talent.HammerOfWrath.Enabled() and (Target.Health.Percent() >= 20 or Player.Buff(Aura.AvengingWrath).Down() or Player.Buff(Aura.Crusade).Down())) or Talent.ExecutionSentence.Enabled() or not Talent.HammerOfWrath.Enabled())
			   and Player.HolyPower() <= 4;
		end,

		-- actions.generators+=/blade_of_justice,if=holy_power<=2|(holy_power=3&(cooldown.hammer_of_wrath.remains>gcd*2|variable.HoW))
		BladeOfJustice = function()
			return Player.HolyPower() <= 2
			    or (Player.HolyPower() == 3 and (Talent.HammerOfWrath.Cooldown.Remains() > Player.GCD() * 2 or Variables.HoW));
		end,

		-- actions.generators+=/consecration,if=holy_power<=2|holy_power<=3&cooldown.blade_of_justice.remains>gcd*2|holy_power=4&cooldown.blade_of_justice.remains>gcd*2&cooldown.judgment.remains>gcd*2
		Consecration = function()
			return Player.HolyPower() <= 2
			    or (Player.HolyPower() <= 3 and Spell.BladeOfJustice.Cooldown.Remains() > Player.GCD() * 2)
			    or (Player.HolyPower() == 4 and Spell.BladeOfJustice.Cooldown.Remains() > Player.GCD() * 2 and Spell.Judgment.Cooldown.Remains() > Player.GCD() * 2);
		end,

		CrusaderStrike = {
			-- actions.generators+=/crusader_strike,if=cooldown.crusader_strike.charges_fractional>=1.75&(holy_power<=2|holy_power<=3&cooldown.blade_of_justice.remains>gcd*2|holy_power=4&cooldown.blade_of_justice.remains>gcd*2&cooldown.judgment.remains>gcd*2&cooldown.consecration.remains>gcd*2)
			Charges = function()
				return Spell.CrusaderStrike.Charges.Fractional() >= 1.75
				   and (Player.HolyPower() <= 2 or Player.HolyPower() <= 3 and Spell.BladeOfJustice.Cooldown.Remains() > Player.GCD() * 2 or Player.HolyPower == 4 and Spell.BladeOfJustice.Cooldown.Remains() > Player.GCD() * 2 and Spell.Judgment.Cooldown.Remains() > Player.GCD() * 2 and Talent.Consecration.Cooldown.Remains() > Player.GCD() * 2);
			end,

			-- actions.generators+=/crusader_strike,if=holy_power<=4
			Use = function()
				return Player.HolyPower() <= 4;
			end,
		},

		Finishers = {
			-- actions.generators+=/call_action_list,name=finishers,if=talent.hammer_of_wrath.enabled&(target.health.pct<=20|buff.avenging_wrath.up|buff.crusade.up)&(buff.divine_purpose.up|buff.crusade.stack<10)
			HammerOfWrath = function()
				return Talent.HammerOfWrath.Enabled()
				   and (Target.Health.Percent() <= 20 or Player.Buff(Aura.AvengingWrath).Up() or Player.Buff(Aura.Crusade).Up())
				   and (Player.Buff(Aura.DivinePurpose).Up() or Player.Buff(Aura.Crusade).Stack() < 10);
			end,

			-- actions.generators+=/call_action_list,name=finishers,if=holy_power>=5
			HolyPower = function()
				return Player.HolyPower() >= 5;
			end,
		},

		-- actions.generators+=/hammer_of_wrath,if=holy_power<=4
		HammerOfWrath = function()
			return Player.HolyPower() <= 4;
		end,

		-- actions.generators+=/judgment,if=holy_power<=2|(holy_power<=4&(cooldown.blade_of_justice.remains>gcd*2|variable.HoW))
		Judgment = function()
			return Player.HolyPower() <= 2
			    or (Player.HolyPower() <= 4 and (Spell.BladeOfJustice.Cooldown.Remains() > Player.GCD() * 2 or Variables.HoW));
		end,

		-- We can't do raid adds events so just skip this.
		-- actions.generators+=/wake_of_ashes,if=(!raid_event.adds.exists|raid_event.adds.in>20)&(holy_power<=0|holy_power=1&cooldown.blade_of_justice.remains>gcd)
		WakeOfAshes = function()
			return Player.HolyPower() <= 0
			    or (Player.HolyPower() == 1 and Spell.BladeOfJustice.Cooldown.Remains() > Player.GCD())
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.generators=variable,name=HoW,value=(!talent.hammer_of_wrath.enabled|target.health.pct>=20&(buff.avenging_wrath.down|buff.crusade.down))
		Variables.HoW = (not Talent.HammerOfWrath.Enabled() or Target.Health.Percent() >= 20 and (Player.Buff(Aura.AvengingWrath).Down() or Player.Buff(Aura.Crusade).Down()));

		action.CallActionList(rotationFinishers, self.Requirements.Finishers.HolyPower);

		action.EvaluateAction(Talent.WakeOfAshes, self.Requirements.WakeOfAshes);
		action.EvaluateAction(Spell.BladeOfJustice, self.Requirements.BladeOfJustice);
		action.EvaluateAction(Spell.Judgment, self.Requirements.Judgment);
		action.EvaluateAction(Talent.HammerOfWrath, self.Requirements.HammerOfWrath);
		action.EvaluateAction(Talent.Consecration, self.Requirements.Consecration);

		action.CallActionList(rotationFinishers, self.Requirements.Finishers.HammerOfWrath);

		action.EvaluateAction(Spell.CrusaderStrike, self.Requirements.CrusaderStrike.Charges);

		-- actions.generators+=/call_action_list,name=finishers
		action.CallActionList(rotationFinishers);

		action.EvaluateAction(Spell.CrusaderStrike, self.Requirements.CrusaderStrike.Use);
		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationGenerators = Generators("Generators");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Rebdull and SimCraft",
		GuideLink = "http://www.wowhead.com/retribution-paladin-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		BlessingOfProtection = function()
			return not Player.Debuff(Aura.Forbearance).Up()
			   and Player.PhysicalDamagePredicted(5) >= 50;
		end,

		DivineShield = function()
			return not Player.Debuff(Aura.Forbearance).Up()
			   and Player.DamagePredicted(5) >= 70;
		end,

		FlashOfLight = function()
			return Talent.SelflessHealer.Enabled()
			   and Player.Buff(Aura.SelflessHealer).Stack() == 4
			   and Player.Health.Percent() < 80;
		end,

		LayOnHands = function()
			return Player.Health.Percent() < 40
			   and Player.DamagePredicted(5) >= 50;
		end,

		ShieldOfVengeance = function()
			return not Player.Debuff(Aura.Forbearance).Up()
			   and Player.DamagePredicted(7) >= 30;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Aura.DivineRight = Objects.newSpell(278523);

		Item = {};

		Consumable = {
			-- Potions
			OldWar = Objects.newItem(127844),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.LightsJudgment);
		config.AOEOptions(options, Talent.Consecration, Spell.DivineStorm);
		config.CooldownOptions(options, Spell.AvengingWrath, Talent.Crusade, Talent.ExecutionSentence, Talent.HammerOfWrath, Talent.Inquisition, Spell.TemplarsVerdict, Talent.WakeOfAshes);
		config.DefensiveOptions(options, Spell.BlessingOfProtection, Spell.DivineShield, Spell.FlashOfLight, Spell.LayOnHands, Spell.ShieldOfVengeance);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.
		action.EvaluateDefensiveAction(Spell.BlessingOfProtection, self.Defensives.BlessingOfProtection);

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.DivineShield, self.Defensives.DivineShield);
		action.EvaluateDefensiveAction(Spell.ShieldOfVengeance, self.Defensives.ShieldOfVengeance);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Spell.LayOnHands, self.Defensives.LayOnHands);
		action.EvaluateDefensiveAction(Spell.FlashOfLight, self.Defensives.FlashOfLight);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.OldWar, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions+=/call_action_list,name=cooldowns
		action.CallActionList(rotationCooldowns);
		-- actions+=/call_action_list,name=generators
		action.CallActionList(rotationGenerators);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Retribution Paladin", addonTable.Enum.SpecID.PALADIN_RETRIBUTION);
