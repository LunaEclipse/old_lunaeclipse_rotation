-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local Blizzard = addon.API.Blizzard;
local MELEE_RANGE, SPELL_CAST_CHANNELED, SPELL_CAST_TIME_INSTANT, gsub, huge, max, pairs, select, setmetatable, strjoin, strtrim, strupper = MELEE_RANGE, SPELL_CAST_CHANNELED, SPELL_CAST_TIME_INSTANT, gsub, math.huge, max, pairs, select, setmetatable, strjoin, strtrim, strupper;
local Cache = setmetatable({}, { __mode = "kv" });

-- Buffs that allow moving while casting.
local CAST_MOVING_BUFFS = {
	[171375] = {
		[5143] = true,
		[30451] = true,
	},
	[108839] = true,
	[236380] = true,
	[236430] = true,
	[236431] = true,
	[79206] = true,
	[202461] = true,
	[193223] = true,
	[229884] = true,
};

-- Talents that allow moving while casting.
local CAST_MOVING_TALENTS = {
	[22443] = {
		[5143] = true,
		[12051] = true,
	},
	[22126] = {
		[209525] = true,
	},
};

-- Spells castable while moving.
local SPELLS_CASTABLE_MOVING = {
	[2948] = true,
	[19386] = true,
	[120360] = true,
	[135029] = true,
	[191837] = true,
	[113656] = true,
	[47540] = true,
	[193440] = true,
};

-- Internal function for checking buffs that allow casting while moving.
local function checkMovementBuffs(self)
	local General = addon.API.General;
	local Player = addon.Units.Player;
	local Tables = addon.Core.Tables;

	local spellID = self.ClassID();

	for ID, value in pairs(CAST_MOVING_BUFFS) do
		local affectsSpell = (not Tables.IsTable(value) and value) or value[spellID] or false;
		local unitBuff = Player.Aura(ID);

		if unitBuff.Up() and affectsSpell and (unitBuff.End() == 0 or self.CastTime() < unitBuff.End() - General.GetTime()) then
			return true;
		end
	end

	return false;
end

-- Internal function for checking talents that allow spell casting while moving without using a buff.
local function checkMovementTalents(self)
	local Tables = addon.Core.Tables;
	local spellID = self.ClassID();

	for ID, value in pairs(CAST_MOVING_TALENTS) do
		local affectsSpell = (not Tables.IsTable(value) and value) or value[spellID] or false;
		local selected = select(4, Blizzard.GetTalentInfoByID(ID, Blizzard.GetActiveSpecGroup()));

		if selected and affectsSpell then
			return true;
		end
	end

	return false;
end

-- Check to see if the spell can be cast based on movement and buffs.
local function checkMovement(self)
	local Player = addon.Units.Player;
	local Settings = addon.Core.Settings;
	local Tables = addon.Core.Tables;

-- Disable movement check until it can be updated for Battle For Azeroth, instead simply return true so it will suggest any spell regardless of movement.
--	return Settings.GetCharacterValue("ScriptOptions", "OPT_MOVING") == 0
--		or not Player.IsMoving()
--		or self.IsInstantCast()
--		or Tables.GetValue(SPELLS_CASTABLE_MOVING, self.ClassID(), false)
--		or checkMovementTalents(self)
--		or checkMovementBuffs(self);
	return true;
end

--- Get whether the player meets the reactive requirements to cast the spell.
local function checkRequirements(self)
	local usable, noMana = Blizzard.IsUsableSpell(self.SpellID());

	-- Will return true if spell is usable, or noMana is true.  nomana seems to specify that
	-- the spell only thing stopping the spell from being cast is resources.
	return usable or noMana;
end

local function checkMaxRange(self)
	if self.IsKnown() then
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local spellID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(spellID);
		local useRange = Tables.GetValue(spellInfo, "UseRange", false);
		local spellRange = self.InRange.Max();

		if useRange and spellRange <= 60 then
			addonTable.maxRange = max(addonTable.maxRange, spellRange);
		end
	end
end

addonTable.getSpell = function(ID)
	local coreTables = addon.Core.Tables;
	local returnValue;

	if not coreTables.IsEmpty(Cache) then
		returnValue = Cache[ID];

		if not returnValue then
			for _, currentSpell in pairs(Cache) do
				if coreTables.GetValue(currentSpell, "SpellID", 0) == ID then
					return currentSpell;
				end
			end
		end
	end

	return returnValue;
end

addonTable.classSpell = function(ID, GCD, CheckBox)
	local spellID = ID;

	local self = Cache[ID] or {};

	self.ClassID = function()
		return spellID;
	end

	self.ClassType = function()
		return "Spell";
	end

	-- Get spell duration.
	self.BaseDuration = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "BaseDuration") == nil then
			local seconds = addon.Core.Tooltip.GetSpellBaseDuration(spellID)

			spellInfo.BaseDuration = seconds;
			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "BaseDuration", 0);
	end

	-- Get the spell BookIndex along with BookType.
	self.BookIndex = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "BookIndex") == nil then
			spellInfo.BookIndex = Blizzard.FindSpellBookSlotBySpellID(currentID);

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "BookIndex", 0), self.SpellType();
	end

	-- action.foo.cast_time
	self.CastTime = function()
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		return Tables.GetValue(spellInfo, "CastingTime", 0);
	end

	self.Charges = addonTable.propertiesCharges(self);

	self.CheckBox = function()
		local coreMisc = addon.Core.Misc;

		return CheckBox == nil and coreMisc.GetCheckBoxName(self.Name()) or CheckBox;
	end

	self.Cooldown = addonTable.propertiesCooldown(self);

	-- action.foo.cost
	self.Cost = function()
		local PowerType = addonTable.Enum.PowerType;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellCost = Spell.GetSpellPowerCost(currentID);

		return Tables.GetValue(spellCost, "Cost", 0), Tables.GetValue(spellCost, "Type", PowerType.None);
	end

	-- Check if the spell is known or not, will return true if the spell has been replaced by another spell.
	self.Enabled = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "IsAvailable") == nil then
			if self.SpellType() == "pet" then
				spellInfo.IsAvailable = self.IsKnown();
			else
				spellInfo.IsAvailable = Blizzard.IsPlayerSpell(currentID) or self.IsKnown();
			end

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "IsAvailable", false);
	end

	self.Evaluate = function()
		local addonCache = addonTable.Cache;
		local Player = addon.Units.Player;
		local PowerType = addonTable.Enum.PowerType;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentTime = Blizzard.GetTime();
		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		local returnValue = Tables.GetValue(spellInfo, "Evaluate");

		local defaultReturn = {
			Use = false,
			Start = 0,
			End = 0,
			Duration = 0,
			Text = "",
		};

		checkMaxRange(self);

		if not Tables.IsEmpty(spellInfo) then
			if returnValue == nil and self.IsKnown()
								  and checkRequirements(self)
								  and checkMovement(self)
								  and not self.Cooldown.Locked() then
				local cost, costType = self.Cost(currentID);
				local powerType = Tables.GetKey(PowerType, costType);

				if costType == PowerType.None or cost <= Player[powerType]() then
					-- No cost or we have enough resources so we don't have to worry about calculating time to resources.
					returnValue = {
						Use = true,
						Start = self.Cooldown.Start(),
						End = self.Cooldown.End(),
						Duration = self.Cooldown.Duration(),
						Text = self.Cooldown.End() > currentTime and "wait" or "",
					};
				else
					local resourceDuration = Player[powerType].TimeToX(cost);

					if resourceDuration ~= huge then
						local resourceStart = addonTable.LastAction or currentTime;
						local resourceEnd = resourceStart + resourceDuration;

						if resourceEnd > self.Cooldown.End() then
							-- It will take longer to collect resources then the spell cooldown so use the resource information.

							returnValue = {
								Use = true,
								Start = resourceStart,
								End = resourceEnd,
								Duration = resourceDuration,
								Text = resourceEnd > currentTime and "pool" or "",
							};
						else
							-- We will have the resources before the spell cooldown is up, so use the spell cooldown information.
							returnValue = {
								Use = true,
								Start = self.Cooldown.Start(),
								End = self.Cooldown.End(),
								Duration = self.Cooldown.Duration(),
								Text = self.Cooldown.End() > currentTime and "wait" or "",
							};
						end
					end
				end
			end

			spellInfo.Evaluate = returnValue or defaultReturn;

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return returnValue or defaultReturn;
	end

	-- action.foo.execute_time
	self.ExecuteTime = function()
		local addonCache = addonTable.Cache;
		local Player = addon.Units.Player;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "ExecuteTime") == nil then
			spellInfo.ExecuteTime = (not self.IsOffGCD() and max(self.CastTime(), Player.GCD()))
								 or self.CastTime();

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "ExecuteTime", self.CastTime());
	end

	self.InRange = addonTable.propertiesRange(self);

	-- Get whether the spell is a channeled spell.
	self.IsChanneled = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;
		local Tooltip = addon.Core.Tooltip;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "IsChanneled") == nil then
			spellInfo.IsChanneled = Tooltip.IsSpellType(currentID, SPELL_CAST_CHANNELED);

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "IsChanneled", false);
	end

	-- action.foo.in_flight
	self.IsInFlight = function()
		local addonCache = addonTable.Cache;
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local lastHitTime = Tables.GetValue(addonCache.Persistant.ActionInfo[currentID], "LastHitTime", 0);

		return General.GetTime() < lastHitTime;
	end

	-- Get whether the spell is an instant cast spell.
	self.IsInstantCast = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;
		local Tooltip = addon.Core.Tooltip;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "IsInstantCast") == nil then
			spellInfo.IsInstantCast = Tooltip.IsSpellType(currentID, SPELL_CAST_TIME_INSTANT);

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "IsInstantCast", false);
	end

	-- Check to see if the spell is known or not, will return false if the spell is replaced by another spell.
	self.IsKnown = function()
		local addonCache = addonTable.Cache;

		local currentID = self.SpellID();
		local spellReplaced = addonCache.Persistant.ReplacedSpells[currentID];

		if spellReplaced ~= nil then
			return spellReplaced;
		end

		if not Blizzard.IsPassiveSpell(currentID) then
			return addonCache.Persistant.SpellLearned[self.SpellType()][currentID] or false;
		end

		return false;
	end

	-- Check if the spell is a melee range spell or not.
	self.IsMelee = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;
		local Tooltip = addon.Core.Tooltip;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "IsMelee") == nil then
			spellInfo.IsMelee = Tooltip.IsSpellType(currentID, MELEE_RANGE);

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "IsMelee", false);
	end

	-- Get whether the spell does not trigger the global cooldown.
	self.IsOffGCD = function()
		return GCD == 0;
	end

	-- Check if the spell is both castable and usable or not.
	self.IsReady = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "IsReady") == nil then
			spellInfo.IsReady = self.IsKnown()
							and self.IsUsable()
							and self.Charges() >= 1;

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "IsReady", false);
	end

	-- Check if the spell is usable or not.
	self.IsUsable = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "IsUsable") == nil then
			spellInfo.IsUsable = Blizzard.IsUsableSpell(currentID);

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "IsUsable", false);
	end

	-- Get the spell Name.
	self.Name = function()
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);
		local returnValue = Tables.GetValue(spellInfo, "Name", "");

		returnValue = gsub(returnValue, "[0-9.]", "");
		returnValue = strtrim(returnValue);

		return returnValue;
	end

	-- Get spell duration.
	self.PowerGenerated = function()
		local addonCache = addonTable.Cache;
		local Core = addon.Core.General;
		local PowerType = addonTable.Enum.PowerType;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;
		local Tooltip = addon.Core.Tooltip;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "PowerGenerated") == nil then
			local powerType = Tables.GetKey(PowerType, Blizzard.UnitPowerType("player"));

			spellInfo.PowerGenerated = Core.ToNumber(Tooltip.GetSpellValue(currentID, strjoin(" ", strupper(L["Generates"]),"(%d*%.?%d+)", strupper(powerType))));
			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "PowerGenerated", 0);
	end

	-- Get the spell ID.
	self.SpellID = function()
		local Core = addon.Core.General;

		return Core.GetSpellReplacementID(spellID);
	end

	-- Get the spell Type.
	self.SpellType = function()
		local addonCache = addonTable.Cache;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local spellInfo = Spell.GetSpellInfo(currentID);

		if not Tables.IsEmpty(spellInfo) and Tables.GetValue(spellInfo, "SpellType") == nil then
			if Blizzard.GetSpellBookItemInfo(self.Name()) == "PETACTION" then
				spellInfo.SpellType = "Pet";
			else
				spellInfo.SpellType = "Player";
			end

			addonCache.Temporary.SpellInfo[currentID] = spellInfo;
		end

		return Tables.GetValue(spellInfo, "SpellType", "Player");
	end

	self.TimeSinceLastBuff = function()
		local addonCache = addonTable.Cache;
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local lastBuffTime = Tables.GetValue(addonCache.Persistant.ActionInfo[currentID], "LastBuffTime", 0);

		return General.GetTime() - lastBuffTime;
	end

	-- Get the Time since Last spell Cast.
	self.TimeSinceLastUsed = function()
		local addonCache = addonTable.Cache;
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();
		local lastCastTime = Tables.GetValue(addonCache.Persistant.ActionInfo[currentID], "LastCastTime", 0);

		return General.GetTime() - lastCastTime;
	end

	-- action.foo.travel_speed
	self.TravelSpeed = function()
		local ProjectileSpeed = addonTable.Enum.ProjectileSpeed;
		local Tables = addon.Core.Tables;

		local currentID = self.SpellID();

		return Tables.GetValue(ProjectileSpeed, currentID, 22);
	end

	-- action.foo.travel_time
	self.TravelTime = function()
		local Target = addon.Units.Target;
		local Speed = self.TravelSpeed();

		return (Speed > 0 and Target.InRange.Max() / Speed)
			or 0;
	end

	Cache[ID] = self;

	return self;
end