-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local assert, format, ipairs, pairs, print, type = assert, format, ipairs, pairs, print, type;

local function debugAction(Object, ...)
	local Enemies = addon.Modules.Enemies;
	local Target = addon.Units.Target;

	local args = {...};
	-- Debuging rotation so we want to run all sub rotations regardless of requirements.
	-- First check that the rotation is a valid object
	assert(Object, "Action Object not valid!");

	for _, callFunction in ipairs(args) do
		assert(callFunction ~= nil, "Requirement must contain either true, or a pointer to a function that returns true/false!");

		if type(callFunction) == "function" then
			assert(callFunction(Enemies.GetEnemies(), Target) == true or callFunction(Enemies.GetEnemies(), Target) == false, "Requirement function's must return true/false!");
		end
	end
end

local function debugActionList(Object, callFunction)
	local Action = addon.RotationDebug;
	local Enemies = addon.Modules.Enemies;
	local Target = addon.Units.Target;

	-- Debuging rotation so we want to run all sub rotations regardless of requirements.
	-- First check that the rotation is a valid object
	assert(Object, "Rotation not valid!");

	if type(callFunction) == "function" then
		assert(callFunction(Enemies.GetEnemies(), Target) ~= nil, "Requirement functions must return true/false!");
	end

	assert(Object.Use(Enemies.GetEnemies()) ~= nil, "Requirement functions must return true/false!");

	-- We are not limiting rotations as we want to process them all so just call it now.
	Object.Rotation(Action);
end

local function debugRotation(rotationObject, rotationName)
	local Action = addon.RotationDebug;

	if rotationName then
		print(format("Debugging rotation %q!", rotationName));
	else
		print("Debugging active rotation!");
	end

	rotationObject.ProcessEnable(Action);
	rotationObject.Precombat(Action);
	rotationObject.Opener(Action);
	rotationObject.Interrupt(Action);
	rotationObject.Combat(Action);
	rotationObject.Defensive(Action);
	rotationObject.ProcessDisable();

	if rotationName then
		print(format("Debugging rotation %q completed without errors!", rotationName));
	else
		print("Debugging active rotation completed without errors!");
	end
end

addon.RotationDebug = {
	DebugAll = function()
		local Rotation = addon.Rotation;
		local currentRotation = addon.currentRotation;

		addonTable.Debug = true;

		if currentRotation then
			currentRotation.ProcessDisable();
		end

		for _, specRotations in pairs(addonTable.RegisteredRotations) do
			for key, rotation in pairs(specRotations) do
				debugRotation(rotation.callback, key);
			end
		end

		addonTable.Debug = nil;

		if currentRotation then
			currentRotation.ProcessEnable(Rotation);
		end
	end,

	DebugCurrent = function()
		local Rotation = addon.Rotation;
		local currentRotation = addon.currentRotation;

		addonTable.Debug = true;

		debugRotation(currentRotation);

		addonTable.Debug = nil;

		if currentRotation then
			currentRotation.ProcessEnable(Rotation);
		end
	end,

	EvaluateAction = function(Object, useObject)
		-- Code for debugging rotations to trigger any errors.
		debugAction(Object, useObject);
	end,

	EvaluateCycleAction = function(Object, useObject)
		-- Code for debugging rotations to trigger any errors.
		debugAction(Object, useObject);
	end,

	EvaluateDefensiveAction = function(Object, useObject)
		-- Code for debugging rotations to trigger any errors.
		debugAction(Object, useObject);
	end,

	EvaluateInterruptAction = function(Object, useObject)
		-- Code for debugging rotations to trigger any errors.
		debugAction(Object, useObject);
	end,

	EvaluateInterruptCondition = function(Object, useObject, interruptCheck)
		-- Code for debugging rotations to trigger any errors.
		debugAction(Object, useObject, interruptCheck);
	end,

	EvaluatePoolAction = function(Object, useObject)
		-- Code for debugging rotations to trigger any errors.
		debugAction(Object, useObject);
	end,

	EvaluateWaitCondition = function(Object, waitCheck)
		-- Code for debugging rotations to trigger any errors.
		debugAction(Object, waitCheck);
	end,

	CallActionList = function(Object, callFunction)
		debugActionList(Object, callFunction);
	end,

	RunActionList = function(Object, callFunction)
		debugActionList(Object, callFunction);
	end,
};