-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Variables for module
local isLoggedIn = false;
local ACTIVATE_SPEC = 200749;

local function changeSpecFailed(...)
	local _, unitID, _, spellID = ...;

	if isLoggedIn and unitID == "player" and spellID == ACTIVATE_SPEC then
		local specInfo = addon.API.General.GetSpecializationInfo();

		addon.System.Callbacks.Callback("changeSpecFailed", specInfo, ...);
	end
end

local function changeSpecFinish(...)
	local _, unitID = ...;

	if isLoggedIn and unitID == "player" then
		local specInfo = addon.API.General.GetSpecializationInfo(true);

		addon.System.Callbacks.Callback("changeSpecFinish", specInfo, ...);
	end
end

local function changeSpecStart(...)
	local _, unitID, _, spellID = ...;

	if isLoggedIn and unitID == "player" and spellID == ACTIVATE_SPEC then
		local specInfo = addon.API.General.GetSpecializationInfo();

		addon.System.Callbacks.Callback("changeSpecStart", specInfo, ...);
	end
end

local moduleEvents = {
	playerLogin = function()
		isLoggedIn = true;
	end,
};

local tableEvents = {
	PLAYER_SPECIALIZATION_CHANGED = changeSpecFinish,
	UNIT_SPELLCAST_START = changeSpecStart,
	UNIT_SPELLCAST_FAILED = changeSpecFailed,
	UNIT_SPELLCAST_INTERRUPTED = changeSpecFailed,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);