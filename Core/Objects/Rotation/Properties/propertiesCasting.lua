-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local format, huge, max, pairs, setmetatable = format, math.huge, max, pairs, setmetatable;
local Cache = setmetatable({}, { __mode = "kv" });

addonTable.propertiesCasting = function(Unit)
	local Objects = addon.Core.Objects;
	local unitID = Objects.GetID(Unit) or Unit;

	local self = Cache[unitID] or { UnitID = unitID };

	self.Casting = function()
		return self.IsCasting()
			or self.IsChanneling();
	end

	-- Get the full duration, in seconds, of the current cast, if there is any.
	self.Duration = function()
		return (self.Casting() and max(self.End() - self.Start(), 0))
			or 0;
	end

	-- Get when the cast, if there is any, will end (in seconds).
	self.End = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

		return (self.Casting() and Tables.GetValue(spellCastInfo, "End", 0))
			or 0;
	end

	-- Get the unit cast's id if there is any.
	self.ID = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

		return self.Casting()
		   and Tables.GetValue(spellCastInfo, "SpellID");
	end

	-- Get if the unit is casting or not.
	self.IsCasting = function()
		local General = addon.API.General;
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

		return Tables.GetValue(spellCastInfo, "CastType", "") == "CAST"
		   and Tables.GetValue(spellCastInfo, "End", 0) > General.GetTime();
	end

	-- Get if the unit is channeling or not.
	self.IsChanneling = function()
		local General = addon.API.General;
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

		return Tables.GetValue(spellCastInfo, "CastType", "") == "CHANNEL"
		   and Tables.GetValue(spellCastInfo, "End", 0) > General.GetTime();
	end

	-- Get if the unit cast is interruptible if there is any.
	self.IsInterruptible = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

		return self.Casting()
		   and not Tables.GetValue(spellCastInfo, "NotInterruptible", true);
	end

	-- Get the unit cast's name if there is any.
	self.Name = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

		return (self.Casting() and Tables.GetValue(spellCastInfo, "Name", ""))
			or "";
	end

	-- Get the progression of the cast in percentage if there is any.
	-- By default for channeling, it returns total - progress, if ReverseChannel is true it'll return only progress.
	self.Percentage = function(ReverseChannel)
		local castPercent = 100 - ((self.Remains() / self.Duration()) * 100);

		return (self.IsCasting() and castPercent)
			or (self.IsChanneling() and ReverseChannel and castPercent)
			or (self.IsChanneling() and 100 - castPercent)
			or 0;
	end

	-- Get the remaining cast time, if there is any.
	self.Remains = function()
		local General = addon.API.General;

		return (self.Casting() and self.End() - General.GetTime())
			or 0;
	end

	-- Get whether casting the specified spell object
	self.Spell = function(Spell)
		local spellID = Objects.IsSpell(Spell) and Spell.SpellID() or Spell;

		return self.Casting()
		   and self.ID() == spellID;
	end

	-- Get when the cast, if there is any, started (in seconds).
	self.Start = function()
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

		return (self.Casting() and Tables.GetValue(spellCastInfo, "Start", 0))
			or 0;
	end

	self.Tick = {
		-- Todo:  Add allowance for ticks at evaluate time, not current time.
		Current = function()
			local Tables = addon.Core.Tables;
			local apiUnit = addon.API.Unit;

			local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

			return (Tables.GetValue(spellCastInfo, "CastType", "") == "CHANNEL" and Tables.GetValue(spellCastInfo, "TicksCurrent", 0))
				or 0;
		end,

		Max = function()
			local Tables = addon.Core.Tables;
			local apiUnit = addon.API.Unit;

			local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

			return (Tables.GetValue(spellCastInfo, "CastType", "") == "CHANNEL" and Tables.GetValue(spellCastInfo, "TicksTotal", 0))
				or 0;
		end,

		Remains = function()
			local Tables = addon.Core.Tables;
			local apiUnit = addon.API.Unit;

			local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

			return (Tables.GetValue(spellCastInfo, "CastType", "") == "CHANNEL" and Tables.GetValue(spellCastInfo, "TicksRemaining", 0))
				or 0;
		end,

		TimeSinceLastTick = function()
			local General = addon.API.General;
			local Tables = addon.Core.Tables;
			local apiUnit = addon.API.Unit;

			local spellCastInfo = apiUnit.UnitCastingInfo(unitID);

			return (Tables.GetValue(spellCastInfo, "CastType", "") == "CHANNEL" and General.GetTime() - Tables.GetValue(spellCastInfo, "LastTick", General.GetTime()))
				or 0;
		end,
	};

	setmetatable(self.Tick, { __call = function(table, ...) return self.Tick.Current(...); end });
	setmetatable(self, { __call = function(table, ...) return self.Casting(...); end });
	Cache[unitID] = self;

	return self;
end