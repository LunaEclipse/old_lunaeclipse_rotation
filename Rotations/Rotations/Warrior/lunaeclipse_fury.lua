local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local coreSettings = addon.Core.Settings;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "lunaeclipse_warrior_fury";

local trashMobs = {
	minus = true,
	normal = true,
	trivial = true,
	elite = true,
};

-- SingleTarget Rotation
local function SingleTarget(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- The PrevGCD Ramage and debuff up is not practical for a human being, its almost never suggested and when it is it flashes for one refresh cycle only.
		-- Change this to buff.enrage.up instead to allow more time for a human being to use it.
		-- actions.single_target+=/bladestorm,if=prev_gcd.1.rampage&(debuff.siegebreaker.up|!talent.siegebreaker.enabled)
		Bladestorm = function()
			return Player.Buff(Aura.Enrage).Up()
			   and (Target.Debuff(Aura.Siegebreaker).Up() or not Talent.Siegebreaker.Enabled());
		end,

		-- actions.single_target+=/bloodthirst,if=buff.enrage.down
		Bloodthirst = function()
			return Player.Buff(Aura.Enrage).Down();
		end,

		-- actions.single_target+=/dragon_roar,if=buff.enrage.up&(debuff.siegebreaker.up|!talent.siegebreaker.enabled)
		DragonRoar = function()
			return Player.Buff(Aura.Enrage).Up()
			   and (Target.Debuff(Aura.Siegebreaker).Up() or not Talent.Siegebreaker.Enabled());
		end,

		-- actions.single_target+=/execute,if=buff.enrage.up
		Execute = function()
			return Player.Buff(Aura.Enrage).Up();
		end,

		-- actions.single_target+=/furious_slash,if=talent.furious_slash.enabled
		FuriousSlash = function()
			return Talent.FuriousSlash.Enabled();
		end,

		RagingBlow = {
			-- actions.single_target+=/raging_blow,if=charges=2
			Charges = function()
				return Spell.RagingBlow.Charges() == 2;
			end,

			-- actions.single_target+=/raging_blow,if=talent.carnage.enabled|(talent.massacre.enabled&rage<80)|(talent.frothing_berserker.enabled&rage<90)
			Use = function()
				return Talent.Carnage.Enabled()
				    or (Talent.Massacre.Enabled() and Player.Rage() < 80)
				    or (Talent.FrothingBerserker.Enabled() and Player.Rage() < 90);
			end,
		},

		-- actions.single_target+=/rampage,if=buff.recklessness.up|(talent.frothing_berserker.enabled|talent.carnage.enabled&(buff.enrage.remains<gcd|rage>90)|talent.massacre.enabled&(buff.enrage.remains<gcd|rage>90))
		Rampage = function()
			return Player.Buff(Aura.Recklessness).Up()
			    or (Talent.FrothingBerserker.Enabled() or Talent.Carnage.Enabled() and (Player.Buff(Aura.Enrage).Remains() < Player.GCD() or Player.Rage() > 90) or Talent.Massacre.Enabled() and (Player.Buff(Aura.Enrage).Remains() < Player.GCD() or Player.Rage() > 90));
		end,

		-- SimCraft doesn't specify it, but add a check for trash mobs so it can be used without the recklessness buff on trash.
		-- actions.single_target=siegebreaker,if=buff.recklessness.up|cooldown.recklessness.remains>28
		Siegebreaker = function()
			return (coreSettings.GetRotationValue("OPT_SIEGBREAKER_TRASH", 1) == 1 and trashMobs[Target.Classification()] == true)
			    or Player.Buff(Aura.Recklessness).Up()
			    or Spell.Recklessness.Cooldown.Remains() > 28;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.Siegebreaker, self.Requirements.Siegebreaker);
		action.EvaluateAction(Spell.Rampage, self.Requirements.Rampage);
		action.EvaluateAction(Spell.Execute, self.Requirements.Execute);
		action.EvaluateAction(Spell.Bloodthirst, self.Requirements.Bloodthirst);
		action.EvaluateAction(Spell.RagingBlow, self.Requirements.RagingBlow.Charges);
		-- actions.single_target+=/bloodthirst
		action.EvaluateAction(Spell.Bloodthirst, true);
		action.EvaluateAction(Talent.Bladestorm, self.Requirements.Bladestorm);
		action.EvaluateAction(Talent.DragonRoar, self.Requirements.DragonRoar);
		action.EvaluateAction(Spell.RagingBlow, self.Requirements.RagingBlow.Use);
		action.EvaluateAction(Talent.FuriousSlash, self.Requirements.FuriousSlash);
		-- actions.single_target+=/whirlwind
		action.EvaluateAction(Spell.Whirlwind, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSingleTarget = SingleTarget("SingleTarget");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Archimtiros and SimCraft",
		GuideLink = "https://www.wowhead.com/fury-warrior-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		Bloodthirst = function()
			return Player.Health.Percent() < 90
			   and Player.Buff(Aura.EnragedRegeneration).Up();
		end,

		EnragedRegeneration = function()
			return Player.DamagePredicted(4) >= 30;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/ancestral_call,if=buff.recklessness.up
		AncestralCall = function()
			return Player.Buff(Aura.Recklessness).Up();
		end,

		-- actions+=/arcane_torrent,if=rage<40&!buff.recklessness.up
		ArcaneTorrent = function()
			return Player.Rage() < 40
			   and not Player.Buff(Aura.Recklessness).Up();
		end,

		-- actions+=/berserking,if=buff.recklessness.up
		Berserking = function()
			return Player.Buff(Aura.Recklessness).Up();
		end,

		-- actions+=/blood_fury,if=buff.recklessness.up
		BloodFury = function()
			return Player.Buff(Aura.Recklessness).Up();
		end,

		-- actions+=/fireblood,if=buff.recklessness.up
		Fireblood = function()
			return Player.Buff(Aura.Recklessness).Up();
		end,

		-- actions+=/furious_slash,if=talent.furious_slash.enabled&(buff.furious_slash.stack<3|buff.furious_slash.remains<3|(cooldown.recklessness.remains<3&buff.furious_slash.remains<9))
		FuriousSlash = function()
			return Talent.FuriousSlash.Enabled()
			   and (Player.Buff(Aura.FuriousSlash).Stack() < 3 or Player.Buff(Aura.FuriousSlash).Remains() < 3 or (Spell.Recklessness.Cooldown.Remains() < 3 and Player.Buff(Aura.FuriousSlash).Remains() < 9));
		end,

		-- We can't do raid raid movement events, suggest this when the target is out of melee range but within heroic leap range.
		-- actions+=/heroic_leap,if=(raid_event.movement.distance>25&raid_event.movement.in>45)|!raid_event.movement.exists
		HeroicLeap = function()
			return Target.InRange.Max() >= 8
			   and Target.InRange.Max() <= 40;
		end,

		-- actions+=/lights_judgment,if=cooldown.recklessness.remains<3
		LightsJudgment = function()
			return Spell.Recklessness.Cooldown.Remains() < 3;
		end,

		-- actions+=/rampage,if=cooldown.recklessness.remains<3
		Rampage = function()
			return Spell.Recklessness.Cooldown.Remains() < 3;
		end,

		-- SimCraft doesn't specify it, but only check Meat Cleaver buff if you chose Meat Cleaver talent, otherwise only suggest if you don't already have the buff.
		-- actions+=/whirlwind,if=spell_targets.whirlwind>1&!buff.meat_cleaver.up
		Whirlwind = function(numEnemies)
			return numEnemies > 1
			   and ((Talent.MeatCleaver.Enabled() and not Player.Buff(Aura.MeatCleaver).Up()) or Player.Buff(Aura.Whirlwind).Down());
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfStrength = Objects.newItem(163224),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.AncestralCall, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood, Racial.LightsJudgment);
		config.AOEOptions(options, Talent.Bladestorm, Spell.Whirlwind);
		config.CooldownOptions(options, Talent.DragonRoar, Talent.FuriousSlash, Spell.Rampage, Spell.RagingBlow, Spell.Recklessness, Talent.Siegebreaker);
		config.DefensiveOptions(options, "Bloodthirst|Use Bloodthirst when you have the Enraged Regeneration buff for self healing.", Spell.EnragedRegeneration);
		config.UtilityOptions(options, Spell.Charge, Spell.HeroicLeap);
		config.SpecialOptions(options, "OPT_SIEGBREAKER_TRASH|Suggest Siegebreaker on trash without Recklessness|When this option is checked then Siegebreaker will be suggested against normal and elite mobs without needing Recklessness buff.")
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.EnragedRegeneration, self.Defensives.EnragedRegeneration);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Spell.Bloodthirst, self.Defensives.Bloodthirst);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfStrength, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions+=/charge
		action.EvaluateAction(Spell.Charge, true);
		action.EvaluateAction(Spell.HeroicLeap, self.Requirements.HeroicLeap);
		-- actions+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfStrength, true);
		action.EvaluateAction(Talent.FuriousSlash, self.Requirements.FuriousSlash);
		action.EvaluateAction(Spell.Rampage, self.Requirements.Rampage);
		-- actions+=/recklessness
		action.EvaluateAction(Spell.Recklessness, true);
		action.EvaluateAction(Spell.Whirlwind, self.Requirements.Whirlwind, Enemies.GetEnemies(8));
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
		action.EvaluateAction(Racial.LightsJudgment, self.Requirements.LightsJudgment);
		action.EvaluateAction(Racial.Fireblood, self.Requirements.Fireblood);
		action.EvaluateAction(Racial.AncestralCall, self.Requirements.AncestralCall);

		-- actions+=/run_action_list,name=single_target
		action.RunActionList(rotationSingleTarget);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Fury Warrior", addonTable.Enum.SpecID.WARRIOR_FURY);
