local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local Objects = addon.Core.Objects;
-- Objects

local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "huntsthewind_shaman_enhancement";

local function Variables(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Function to set variables that change in combat.
    function self.Rotation()
        -- actions+=/variable,name=furyCheck80,value=(!talent.fury_of_air.enabled|(talent.fury_of_air.enabled&((maelstrom>35&cooldown.lightning_bolt.remains>=3*gcd)|maelstrom>80)))
        self.fury_check_80 = not Talent.FuryOfAir.Enabled()
                                or (Talent.FuryOfAir.Enabled()
                                    and ((Player.Maelstrom() > 35
                                            and Spell.LightningBolt.Cooldown.Remains() >= 3 * Player.GCD())
                                        or Player.Maelstrom() > 80))

        -- actions+=/variable,name=furyCheck45,value=(!talent.fury_of_air.enabled|(talent.fury_of_air.enabled&maelstrom>45))
        self.fury_check_45 = not Talent.FuryOfAir.Enabled()
                            or (Talent.FuryOfAir.Enabled()
                                and Player.Maelstrom() > 45)

        -- actions+=/variable,name=furyCheck35,value=(!talent.fury_of_air.enabled|(talent.fury_of_air.enabled&maelstrom>35))
        self.fury_check_35 = not Talent.FuryOfAir.Enabled()
                            or (Talent.FuryOfAir.Enabled()
                                and Player.Maelstrom() > 35)

        -- actions+=/variable,name=furyCheck25,value=(!talent.fury_of_air.enabled|(talent.fury_of_air.enabled&maelstrom>25))
        self.fury_check_25 = not Talent.FuryOfAir.Enabled()
                            or (Talent.FuryOfAir.Enabled()
                                and Player.Maelstrom() > 25)

        -- actions+=/variable,name=OCPool70,value=(!talent.overcharge.enabled|(talent.overcharge.enabled&maelstrom>70))
        self.oc_pool_70 = not Talent.Overcharge.Enabled()
                            or (Talent.Overcharge.Enabled()
                                and Player.Maelstrom() > 70)

        -- actions+=/variable,name=OCPool60,value=(!talent.overcharge.enabled|(talent.overcharge.enabled&maelstrom>60))
        self.oc_pool_60 = not Talent.Overcharge.Enabled()
                            or (Talent.Overcharge.Enabled()
                                and Player.Maelstrom() > 60)
    end

    function self.Use()
        return true;
    end

    return self;
end

-- Create a variable so we can call the functions to set rotation variables.
local Variables = Variables("Variables");

-- Ascendance Rotation
local function Ascendance(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions.asc+=/crash_lightning,if=!buff.crash_lightning.up&active_enemies>1
        CrashLightning = function(numEnemies)
            return not Player.Buff(Aura.CrashLightning).Up()
                and numEnemies > 1;
        end,

        -- actions.asc+=/rockbiter,if=talent.landslide.enabled&!buff.landslide.up&charges_fractional>1.7
        Rockbiter = function()
            return Talent.Landslide.Enabled()
                and Player.Buff(Aura.Landslide).Up()
                and Spell.Rockbiter.Charges.Fractional() > 1.7;
        end,
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        -- actions.asc=earthen_spike
        action.EvaluateAction(Talent.EarthenSpike, true);
        action.EvaluateAction(Spell.CrashLightning, self.Requirements.CrashLightning);
        action.EvaluateAction(Spell.Rockbiter, self.Requirements.Rockbiter);
        -- actions.asc+=/windstrike
        action.EvaluateAction(Talent.Windstrike, true);
    end

    -- actions+=/call_action_list,name=asc,if=buff.ascendance.up
    function self.Use(numEnemies)
        return Player.Buff(Aura.Ascendance).Up();
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local Ascendance = Ascendance("Ascendance");

-- Buffs Rotation
local function Buffs(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions.buffs=crash_lightning,if=!buff.crash_lightning.up&active_enemies>1
        CrashLightning = function(numEnemies)
            return not Player.Buff(Aura.CrashLightning).Up()
                and numEnemies > 1;
        end,

        Flametongue = {
            -- actions.buffs+=/flametongue,if=!buff.flametongue.up
            BuffDown = function()
                return not Player.Buff(Aura.Flametongue).Up();
            end,

            -- actions.buffs+=/flametongue,if=buff.flametongue.remains<6+gcd
            BuffRefresh = function()
                return Player.Buff(Aura.Flametongue).Up()
                    and  Player.Buff(Aura.Flametongue).Remains() < 6 + Player.GCD();
            end,
        };

        Frostbrand = {
            -- actions.buffs+=/frostbrand,if=talent.hailstorm.enabled&!buff.frostbrand.up&variable.furyCheck45
            BuffDown = function()
                return Talent.Hailstorm.Enabled()
                    and not Player.Buff(Aura.Frostbrand).Up()
                    and Variables.fury_check_45;
            end,

            -- actions.buffs+=/frostbrand,if=talent.hailstorm.enabled&buff.frostbrand.remains<6+gcd
            BuffRefresh = function()
                return Talent.Hailstorm.Enabled()
                    and Player.Buff(Aura.Frostbrand).Up()
                    and  Player.Buff(Aura.Frostbrand).Remains() < 6 + Player.GCD();
            end,
        };

        -- actions.buffs+=/fury_of_air,if=!ticking&maelstrom>22
        FuryOfAir = function()
            return not Player.Buff(Aura.FuryOfAir).Up()
                and Player.Maelstrom() > 22;
        end,

        -- actions.asc+=/rockbiter,if=talent.landslide.enabled&!buff.landslide.up&charges_fractional>1.7
        Rockbiter = function()
            return Talent.Landslide.Enabled()
                and Player.Buff(Aura.Landslide).Up()
                and Spell.Rockbiter.Charges.Fractional() > 1.7;
        end,

        -- actions.buffs+=/totem_mastery,if=buff.resonance_totem.remains<2
        TotemMastery = function()
            return Player.Buff(Aura.ResonanceTotem).Up()
                and Player.Buff(Aura.ResonanceTotem).Remains() < 2
        end,
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateAction(Spell.CrashLightning, self.Requirements.CrashLightning);
        action.EvaluateAction(Spell.Rockbiter, self.Requirements.Rockbiter);
        action.EvaluateAction(Talent.FuryOfAir, self.Requirements.FuryOfAir);
        action.EvaluateAction(Spell.Flametongue, self.Requirements.Flametongue.BuffDown);
        action.EvaluateAction(Spell.Frostbrand, self.Requirements.Frostbrand.BuffDown);
        action.EvaluateAction(Spell.Flametongue, self.Requirements.Flametongue.BuffRefresh);
        action.EvaluateAction(Spell.Frostbrand, self.Requirements.Frostbrand.BuffRefresh);
        action.EvaluateAction(Talent.TotemMastery, self.Requirements.TotemMastery);
    end

    function self.Use(numEnemies)
        return true;
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local Buffs = Buffs("Buffs");

-- Core Rotation
local function Core(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        CrashLightning = {
            
            -- actions.core+=/crash_lightning,if=active_enemies>3
            AOE = function(numEnemies)
                return numEnemies > 3;
            end,
            
            -- actions.core+=/crash_lightning,if=active_enemies>1
            Use = function(numEnemies)
                return numEnemies > 1;
            end,
            
            -- actions.core+=/crash_lightning,if=talent.forceful_winds.enabled&active_enemies>1
            WithForecefulWinds = function(numEnemies)
                return Talent.ForcefulWinds.Enabled()
                    and numEnemies > 1;
            end,
        },

        -- actions.core=earthen_spike,if=variable.furyCheck25
        EarthenSpike = function()
            return Variables.fury_check_25;
        end,

        -- actions.core+=/flametongue,if=talent.searing_assault.enabled
        Flametongue = function()
            return Talent.SearingAssault.Enabled();
        end,

        -- actions.core+=/lava_lash,if=buff.hot_hand.react
        LavaLash = function()
            return Player.Buff(Aura.HotHand).Up();
        end,

        -- actions.core+=/lightning_bolt,if=talent.overcharge.enabled&variable.furyCheck45&maelstrom>=40
        LightningBolt = function()
            return Talent.Overcharge.Enabled()
                and Variables.fury_check_45
                and Player.Maelstrom() >= 40;
        end,

        Stormstrike = {
            -- actions.core+=/stormstrike,if=buff.stormbringer.up|buff.gathering_storms.up
            -- suspect they forgot to remove gathering storms artifact trait
            Buffed = function()
                return Player.Buff(Aura.Stormbringer).Up();
            end,
            
            -- actions.core+=/stormstrike,if=(!talent.overcharge.enabled&variable.furyCheck35)|(talent.overcharge.enabled&variable.furyCheck80)
            Use = function()
                return (not Talent.Overcharge.Enabled()
                        and Variables.fury_check_25)
                    or (Talent.Overcharge.Enabled()
                        and Variables.fury_check_80);
            end,
        },

        Sundering = {
            -- actions.core+=/sundering,if=active_enemies>=3
            AOE = function(numEnemies)
                return numEnemies >= 3;
            end,

            -- actions.core+=/sundering
            Use = function()
                return true;
            end,
        },
    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateAction(Talent.EarthenSpike, self.Requirements.EarthenSpike);
        action.EvaluateAction(Talent.Sundering, self.Requirements.Sundering.AOE);
        action.EvaluateAction(Spell.Stormstrike, self.Requirements.Stormstrike.Buffed);
        action.EvaluateAction(Spell.CrashLightning, self.Requirements.CrashLightning.AOE);
        action.EvaluateAction(Spell.LightningBolt, self.Requirements.LightningBolt);
        action.EvaluateAction(Spell.Stormstrike, self.Requirements.Stormstrike.Use);
        action.EvaluateAction(Talent.Sundering, self.Requirements.Sundering.Use);
        action.EvaluateAction(Spell.CrashLightning, self.Requirements.CrashLightning.WithForcefulWinds);
        action.EvaluateAction(Spell.Flametongue, self.Requirements.Flametongue);
        action.EvaluateAction(Spell.LavaLash, self.Requirements.LavaLash);
        action.EvaluateAction(Spell.CrashLightning, self.Requirements.CrashLightning.Use);
    end

    function self.Use(numEnemies)
        return true;
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local Core = Core("Core");

-- Filler Rotation
local function Filler(rotationName)
    -- Inherits Rotation Class so get the base class.
    local self = addonTable.rotationsClass(nameAPL, rotationName);

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        CrashLightning = {
            -- actions.filler+=/crash_lightning,if=(maelstrom>=65|talent.crashing_storm.enabled)&variable.OCPool60&variable.furyCheck45
            Use = function()
                return (Player.Maelstrom() >= 65
                        or Talent.CrashingStorm.Enabled())
                    and Variables.oc_pool_60
                    and Variables.fury_check_45;
            end,
            
            -- actions.filler+=/crash_lightning,if=talent.crashing_storm.enabled&debuff.earthen_spike.up&maelstrom>=40&variable.OCPool60
            WithCrashingStorm = function()
                return Talent.CrashingStorm.Enabled()
                    and Target.Debuff(Aura.EarthenSpike).Up()
                    and Player.Maelstrom() >= 40
                    and Variables.oc_pool_60;
            end,
        },

        Flametongue = {
            -- actions.filler+=/flametongue
            Use = function()
                return true;
            end,

            -- actions.filler+=/flametongue,if=talent.searing_assault.enabled|buff.flametongue.remains<4.8
            WithSearingAssult = function()
                return Talent.SearingAssault.Enabled()
                    and Player.Buff(Aura.Flametongue).Remains() < 4.8;
            end,
        },

        -- actions.filler+=/frostbrand,if=talent.hailstorm.enabled&buff.frostbrand.remains<4.8&maelstrom>40
        Frostbrand = function()
            return Talent.Hailstorm.Enabled()
                and Player.Buff(Aura.Frostbrand).Remains() < 4.8
                and Player.Maelstrom() > 40;
        end,

        -- actions.filler+=/lava_lash,if=maelstrom>=50&variable.OCPool70&variable.furyCheck80
        LavaLash = function()
            return Player.Maelstrom() >= 50
                and Variables.oc_pool_70
                and Variables.fury_check_80;
        end,

        Rockbiter = {
            -- actions.filler+=/rockbiter,if=maelstrom<70
            NotCapping = function()
                return Player.Maelstrom() < 70;
            end,

            -- actions.filler+=/rockbiter
            Use = function()
                return true;
            end,
        },

    };

    Objects.FinalizeRequirements(self.Requirements);

    function self.Rotation(action)
        action.EvaluateAction(Spell.Rockbiter, self.Requirements.Rockbiter.NotCapping);
        action.EvaluateAction(Spell.Flametongue, self.Requirements.Flametongue.WithSearingAssault);
        action.EvaluateAction(Spell.CrashLightning, self.Requirements.CrashLightning.WithCrashingStorm);
        action.EvaluateAction(Spell.Frostbrand, self.Requirements.Frostbrand);
        action.EvaluateAction(Spell.LavaLash, self.Requirements.LavaLash);
        action.EvaluateAction(Spell.Rockbiter, self.Requirements.Rockbiter.Use);
        action.EvaluateAction(Spell.CrashLightning, self.Requirements.CrashLightning.Use);
        action.EvaluateAction(Spell.Flametongue, self.Requirements.Flametongue.Use);
    end

    function self.Use(numEnemies)
        return true;
    end

    return self;
end

-- Create a variable so we can call the rotations functions.
local Filler = Filler("Filler");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
    -- Inherits APL Class so get the base class.
    local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

    -- Store the information for the script.
    self.scriptInfo = {
        SpecializationID = self.SpecID,
        ScriptAuthor = "HuntsTheWind",
        GuideAuthor = "Wordup",
        GuideLink = "https://www.icy-veins.com/wow/enhancement-shaman-pve-dps-guide",
        WoWVersion = 80001,
    };

    -- Table to hold requirements to use spells and items listed in the rotation.
    self.Requirements = {
        -- actions.cds+=/ascendance,if=(cooldown.strike.remains>0)&buff.ascendance.down
        -- need to understand the cooldown.strike.remains reference
        Ascendance = function()
            return (Spell.Stormstrike.Cooldown.Remains() > 0
                    or Talent.Windstrike.Cooldown.Remains() > 0)
                and not Player.Buff(Aura.Ascendance).Up();
        end,

        AstralShift = function()
            return Player.DamagePredicted(3) >= 20;
        end,

        -- actions.cds+=/berserking,if=buff.ascendance.up|(feral_spirit.remains>5)|level<100
        Berserking = function()
            return (Talent.Ascendance.Enabled()
                    and Player.Buff(Aura.Ascendance).Up())
                or (addonTable.Summons.Active(100820)
                    and addonTable.Summons.LastDespawn(100820) > 5);
                -- or Spell.FeralSpirit.Remains() > 5;
        end,

        -- actions.cds+=/blood_fury,if=buff.ascendance.up|(feral_spirit.remains>5)|level<100
        BloodFury = function()
            return (Talent.Ascendance.Enabled()
                    and Player.Buff(Aura.Ascendance).Up())
                or (addonTable.Summons.Active(100820)
                    and addonTable.Summons.LastDespawn(100820) > 5);
                -- or Spell.FeralSpirit.Remains() > 5;
        end,

        -- actions.cds=bloodlust,if=target.health.pct<25|time>0.500
        Bloodlust = function()
            return Target.Health.Percent() < 25;
        end,

        BullRush = function()
            return Target.InRange(6);
        end,

        HealingSurge = function()
            return Player.Health.Percent() < 60;
        end,

        LightningShield = function()
            return Talent.LightningShield.Enabled()
                and not Player.Buff(Aura.LightningShield).Up();
        end,

        -- actions.cds+=/potion,if=buff.ascendance.up|!talent.ascendance.enabled&feral_spirit.remains>5|target.time_to_die<=60
        ProlongedPower = function()
            return (Talent.Ascendance.Enabled()
                    and Player.Buff(Aura.Ascendance).Up())
                or (not Talent.Ascendance.Enabled()
                    and (addonTable.Summons.Active(100820)
                        and addonTable.Summons.LastDespawn(100820) > 5))
                    -- and Spell.FeralSpirit.Remains() > 5)
                or Target.TimeToDie() <= 60;
        end,

        -- actions.buffs+=/totem_mastery,if=buff.resonance_totem.remains<2
        TotemMastery = function()
            return Player.Buff(Aura.ResonanceTotem).Up()
                and Player.Buff(Aura.ResonanceTotem).Remains() < 2
        end,

        WarStomp = function()
            return Target.InRange(5);
        end,
    };

    Objects.FinalizeRequirements(self.Requirements);

    -- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
    function self.Enable(...)
        Racial, Spell, Talent, Aura, Azerite = ...;

        Item = {};

        Consumable = {
            -- Potions
            ProlongedPower = Objects.newItem(142117),
        };

        Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
    end


    -- Function for setting up the configuration screen, called when rotation becomes the active rotation.
    function self.SetupConfiguration(config, options)
        config.RacialOptions(options, Racial.Berserking, Racial.BloodFury, Racial.BullRush, Racial.GiftOfTheNaaru, Racial.RocketBarrage, Racial.RocketJump, Racial.Stoneform);
        config.AOEOptions(options, Spell.CrashLightning);
        config.BuffOptions(options, Talent.TotemMastery);
        config.CooldownOptions(options, Talent.Ascendance, Spell.Bloodlust, Spell.EarthElemental, Spell.FeralSpirit, Spell.Heroism, Talent.EarthenSpike);
        config.DefensiveOptions(options, Spell.AstralShift, Talent.EarthShield, Talent.LightningShield);
        -- Setup the Utility options
        config.UtilityOptions(options, Spell.CapacitorTotem, Spell.CleanseSpirit, Spell.EarthbindTotem,
                                    Spell.HealingSurge, Spell.Hex, Spell.Purge, Spell.TremorTotem, Talent.WindRushTotem);
    end

    -- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
    function self.Disable()
        local coreTables = addon.Core.Tables;

        coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
    end

    -- Function for checking the rotation that displays on the Defensives icon.
    function self.Defensive(action)
        -- The abilities here should be listed from highest damage required to suggest to lowest,
        -- Specific damage types before all damage types.

        -- Protects against all types of damage
        action.EvaluateDefensiveAction(Spell.AstralShift, self.Requirements.AstralShift);

        -- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
        action.EvaluateDefensiveAction(Spell.HealingSurge, self.Requirements.HealingSurge);
    end

    -- Function for displaying opening rotation.
    function self.Opener(action)
    end

    -- Function for setting any pre-combat variables, is always called even if you don't have a target.
    function self.PrecombatVariables()
    end

    -- Function for displaying any actions before combat starts.
    function self.Precombat(action)
-- Add the standard use check for totem mastery so it doesn't continue to suggest it if you have already used it.
-- actions.precombat+=/totem_mastery
        action.EvaluateAction(Talent.TotemMastery, self.Requirements.TotemMastery);
-- actions.precombat+=/potion
        action.EvaluateAction(Consumable.ProlongedPower, true);
-- actions.precombat+=/lightning_shield
        action.EvaluateAction(Talent.LightningShield, self.Requirements.LightningShield);
        -- standard opener is Rockbiter, if player has
        -- leftover maelstrom from previous fight, they'll
        -- have to adjust
        action.EvaluateAction(Spell.Rockbiter, true);
    end

    -- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
    function self.Combat(action)
        action.CallActionList(Variables);
        action.CallActionList(Ascendance);
        action.CallActionList(Buffs);
        -- action.EvaluateAction(Racial.Bloodlust, self.Requirements.Bloodlust);
        action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
        action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
        action.EvaluateAction(Consumable.ProlongedPower, self.Requirements.ProlongedPower);
        -- action.cds+=/feral_spirit
        action.EvaluateAction(Spell.FeralSpirit, true);
        action.EvaluateAction(Talent.Ascendance, self.Requirements.Ascendance);
        -- action.cds+=/earth_elemental
        action.EvaluateAction(Spell.EarthElemental, true);
        action.CallActionList(Core);
        action.RunActionList(Filler);
    end

    return self;
end

local APL = APL(nameAPL, "HuntsTheWind: Enhancement Shaman", addonTable.Enum.SpecID.SHAMAN_ENHANCEMENT);
