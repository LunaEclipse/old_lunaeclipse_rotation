local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local sqrt = sqrt;

--- Localize Vars
local coreGeneral = addon.Core.General;
local Objects = addon.Core.Objects;
local val = coreGeneral.ToNumber;

-- Objects
local Pet = addon.Units.Pet;
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

-- Rotation Variables
local nameAPL = "lunaeclipse_warlock_affliction";

local function active_uas()
	local apiUnit = addon.API.Unit;
	local coreTables = addon.Core.Tables;
	local unstableAffliction = { 233490, 233496, 233497, 233498, 233499 };

	for index = #unstableAffliction, 1, -1 do
		local unitAura = apiUnit.UnitAura("target", unstableAffliction[index]);

		if coreTables.GetValue(unitAura, "ID", 0) == unstableAffliction[index] then
			return index;
		end
	end

	return 0;
end

local function time_to_shard()
	local numAgony = Target.Debuff(Aura.Agony).Count();
	local tickTime = 2 * (Talent.CreepingDeath.Enabled() and 0.85 or 1) * (Player.HastePercent() / 100);

	if numAgony == 0 then
		return 3600;
	end

	return 1 / (0.16 / sqrt(numAgony) * (numAgony == 1 and 1.15 or 1) * numAgony / tickTime);
end

-- Fillers Rotation
local function Fillers(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.fillers+=/agony,if=buff.movement.up&!(talent.siphon_life.enabled&(prev_gcd.1.agony&prev_gcd.2.agony&prev_gcd.3.agony)|prev_gcd.1.agony)
		Agony = function()
			return Player.IsMoving()
			   and not(Talent.SiphonLife.Enabled() and (Player.PrevGCD(1, Spell.Agony) and Player.PrevGCD(2, Spell.Agony) and Player.PrevGCD(3, Spell.Agony)) or Player.PrevGCD(1, Spell.Agony));
		end,

		-- actions.fillers+=/corruption,if=buff.movement.up&!prev_gcd.1.corruption&!talent.absolute_corruption.enabled
		Corruption = function()
			return Player.IsMoving()
			   and not Player.PrevGCD(1, Spell.Corruption)
			   and not Talent.AbsoluteCorruption.Enabled();
		end,

		-- actions.fillers+=/drain_life,if=(buff.inevitable_demise.stack>=90&(cooldown.deathbolt.remains>execute_time|!talent.deathbolt.enabled)&(cooldown.phantom_singularity.remains>execute_time|!talent.phantom_singularity.enabled)&(cooldown.dark_soul.remains>execute_time|!talent.dark_soul_misery.enabled)&(cooldown.vile_taint.remains>execute_time|!talent.vile_taint.enabled)&cooldown.summon_darkglare.remains>execute_time+10|buff.inevitable_demise.stack>30&target.time_to_die<=10)
		DrainLife = function()
			return Player.Buff(Aura.InevitableDemise).Stack() >= 90
			   and (Talent.Deathbolt.Cooldown.Remains() > Spell.DrainLife.ExecuteTime() or not Talent.Deathbolt.Enabled())
			   and (Talent.PhantomSingularity.Cooldown.Remains() > Spell.DrainLife.ExecuteTime() or not Talent.PhantomSingularity.Enabled())
			   and (Talent.DarkSoulMisery.Cooldown.Remains() > Spell.DrainLife.ExecuteTime() or not Talent.DarkSoulMisery.Enabled())
			   and (Talent.VileTaint.Cooldown.Remains() > Spell.DrainLife.ExecuteTime() or not Talent.VileTaint.Enabled())
			   and Spell.SummonDarkglare.Cooldown.Remains() > Spell.DrainLife.ExecuteTime() + 10
			    or Player.Buff(Aura.InevitableDemise).Stack() > 30
			   and Target.TimeToDie() <= 10;
		end,

		-- actions.fillers+=/drain_soul,interrupt_global=1,chain=1,cycle_targets=1,if=target.time_to_die<=gcd
		DrainSoul = function(numEnemies, Target)
			return Target.TimeToDie() <= Player.GCD();
		end,

		ShadowBolt = {
			-- LUA can't interpret a number a true and 0 as false so use Up() which returns a boolean.
			-- actions.fillers+=/shadow_bolt,if=buff.movement.up&buff.nightfall.remains
			Nightfall = function()
				return Player.IsMoving()
				   and Player.Buff(Aura.Nightfall).Up();
			end,

			-- LUA can't interpret a number a true and 0 as false so use Up() which returns a boolean.
			-- actions.fillers+=/shadow_bolt,cycle_targets=1,if=talent.shadow_embrace.enabled&talent.absolute_corruption.enabled&active_enemies=2&!debuff.shadow_embrace.remains&!action.shadow_bolt.in_flight
			ShadowEmbrace = function(numEnemies, Target)
				return Talent.ShadowEmbrace.Enabled()
				   and Talent.AbsoluteCorruption.Enabled()
				   and numEnemies == 2
				   and not Target.Debuff(Aura.ShadowEmbrace).Up()
				   and not Spell.ShadowBolt.IsInFlight();
			end,

			-- actions.fillers+=/shadow_bolt,target_if=min:debuff.shadow_embrace.remains,if=talent.shadow_embrace.enabled&talent.absolute_corruption.enabled&active_enemies=2
			Use = function(numEnemies, Target)
				return Talent.ShadowEmbrace.Enabled()
				   and Talent.AbsoluteCorruption.Enabled()
				   and numEnemies == 2;
			end,
		},

		-- actions.fillers+=/siphon_life,if=buff.movement.up&!(prev_gcd.1.siphon_life&prev_gcd.2.siphon_life&prev_gcd.3.siphon_life)
		SiphonLife = function()
			return Player.IsMoving()
			   and not(Player.PrevGCD(1, Talent.SiphonLife) and Player.PrevGCD(2, Talent.SiphonLife) and Player.PrevGCD(3, Talent.SiphonLife));
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.fillers=deathbolt
		action.EvaluateAction(Talent.Deathbolt, true);
		action.EvaluateAction(Spell.ShadowBolt, self.Requirements.ShadowBolt.Nightfall);
		action.EvaluateAction(Spell.Agony, self.Requirements.Agony);
		action.EvaluateAction(Talent.SiphonLife, self.Requirements.SiphonLife);
		action.EvaluateAction(Spell.Corruption, self.Requirements.Corruption);
		action.EvaluateAction(Spell.DrainLife, self.Requirements.DrainLife);
		action.EvaluateCycleAction(Talent.DrainSoul, self.Requirements.DrainSoul);
		action.EvaluateCycleAction(Spell.ShadowBolt, self.Requirements.ShadowBolt.ShadowEmbrace);
		action.EvaluateCycleAction(Spell.ShadowBolt, self.Requirements.ShadowBolt.Use);
		-- actions.fillers+=/shadow_bolt
		action.EvaluateAction(Spell.ShadowBolt, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationFillers = Fillers("Fillers");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Xyronic and SimCraft",
		GuideLink = "https://www.wowhead.com/affliction-warlock-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		DarkPact = function()
			return Player.DamagePredicted(5) >= 40;
		end,

		UnendingResolve = function()
			return Player.DamagePredicted(4) >= 20;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		Agony = {
			-- actions+=/agony,cycle_targets=1,max_cycle_targets=6,if=talent.creeping_death.enabled&target.time_to_die>10&refreshable
			CreepingDeath = function(numEnemies, Target)
				return Talent.CreepingDeath.Enabled()
				   and Target.TimeToDie() > 10
				   and Target.Debuff(Aura.Agony).Refreshable();
			end,

			-- actions+=/agony,cycle_targets=1,if=remains<=gcd
			Expiring = function(numEnemies, Target)
				return Target.Debuff(Aura.Agony).Remains() <= Player.GCD();
			end,

			-- actions+=/agony,cycle_targets=1,max_cycle_targets=8,if=(!talent.creeping_death.enabled)&target.time_to_die>10&refreshable
			Use = function(numEnemies, Target)
				return not Talent.CreepingDeath.Enabled()
				   and Target.TimeToDie() > 10
				   and Target.Debuff(Aura.Agony).Refreshable();
			end,
		},

		-- The SimCraft profile doesn't specify this, but the second part should only be if not Dark Soul Misery chosen, otherwise it makes the first condition pointless.
		-- actions+=/potion,if=(talent.dark_soul_misery.enabled&cooldown.summon_darkglare.up&cooldown.dark_soul.up)|cooldown.summon_darkglare.up|target.time_to_die<30
		BattlePotionOfIntellect = function()
			return (Talent.DarkSoulMisery.Enabled() and Spell.SummonDarkglare.Cooldown.Up() and Talent.DarkSoulMisery.Cooldown.Up())
			    or (not Talent.DarkSoulMisery.Enabled() and Spell.SummonDarkglare.Cooldown.Up())
			    or Target.TimeToDie() < 30;
		end,

		-- actions+=/blood_fury,if=!cooldown.summon_darkglare.up
		BloodFury = function()
			return not Spell.SummonDarkglare.Cooldown.Up();
		end,

		-- actions+=/corruption,cycle_targets=1,if=active_enemies<3+talent.writhe_in_agony.enabled&refreshable&target.time_to_die>10
		Corruption = function(numEnemies, Target)
			return numEnemies < 3 + val(Talent.WritheInAgony.Enabled())
			   and Target.Debuff(Aura.Corruption).Refreshable()
			   and Target.TimeToDie() > 10;
		end,

		-- actions+=/drain_soul,interrupt_global=1,chain=1,cycle_targets=1,if=target.time_to_die<=gcd&soul_shard<5
		DrainSoul = function(numEnemies, Target)
			return Target.TimeToDie() <= Player.GCD()
			   and Player.SoulShards() < 5;
		end,

		-- actions+=/call_action_list,name=fillers,if=(cooldown.summon_darkglare.remains<time_to_shard*(5-soul_shard)|cooldown.summon_darkglare.up)&time_to_die>cooldown.summon_darkglare.remains
		Fillers = function()
			return (Spell.SummonDarkglare.Cooldown.Remains() < time_to_shard() * (5 - Player.SoulShards()) or Spell.SummonDarkglare.Cooldown.Up())
			   and Target.TimeToDie() > Spell.SummonDarkglare.Cooldown.Remains();
		end,

		-- actions+=/fireblood,if=!cooldown.summon_darkglare.up
		Fireblood = function()
			return not Spell.SummonDarkglare.Cooldown.Up();
		end,

		-- SimCraft doesn't specify this, but only suggest if you have a pet active.
		-- actions.precombat+=/grimoire_of_sacrifice,if=talent.grimoire_of_sacrifice.enabled
		GrimoireOfSacrifice = function()
			return Talent.GrimoireOfSacrifice.Enabled()
			   and Pet.IsAction();
		end,

		-- actions+=/phantom_singularity,if=time>40
		PhantomSingularity = function()
			return coreGeneral.CombatTime() > 40;
		end,

		SeedOfCorruption = {
			-- actions.precombat+=/seed_of_corruption,if=spell_targets.seed_of_corruption_aoe>=3
			Precombat = function(numEnemies)
				return numEnemies >= 3;
			end,

			-- LUA can't interpret a number a true and 0 as false so use Up() which returns a boolean.
			-- actions+=/seed_of_corruption,if=dot.corruption.remains<=action.seed_of_corruption.cast_time+time_to_shard+4.2*(1-talent.creeping_death.enabled*0.15)&spell_targets.seed_of_corruption_aoe>=3+talent.writhe_in_agony.enabled&!dot.seed_of_corruption.remains&!action.seed_of_corruption.in_flight
			Refresh = function(numEnemies)
				return Target.Debuff(Aura.Corruption).Remains() <= Spell.SeedOfCorruption.CastTime() + time_to_shard() + 4.2 * (1 - val(Talent.CreepingDeath.Enabled()) * 0.15)
				   and numEnemies >= 3 + val(Talent.WritheInAgony.Enabled())
				   and not Target.Debuff(Aura.SeedOfCorruption).Up()
				   and not Spell.SeedOfCorruption.IsInFlight();
			end,

			-- actions+=/seed_of_corruption,if=variable.spammable_seed
			Use = function()
				return Variables.spammable_seed;
			end,
		},

		ShadowBolt = {
			-- actions.precombat+=/shadow_bolt,if=!talent.haunt.enabled&spell_targets.seed_of_corruption_aoe<3
			Precombat = function(numEnemies)
				return numEnemies < 3
				   and not Talent.Haunt.Enabled();
			end,

			-- LUA can't interpret a number a true and 0 as false so use Up() which returns a boolean.
			-- actions+=/shadow_bolt,target_if=min:debuff.shadow_embrace.remains,if=talent.shadow_embrace.enabled&talent.absolute_corruption.enabled&active_enemies=2&debuff.shadow_embrace.remains&debuff.shadow_embrace.remains<=execute_time*2+travel_time&!action.shadow_bolt.in_flight
			Use = function(numEnemies, Target)
				return Talent.ShadowEmbrace.Enabled()
				   and Talent.AbsoluteCorruption.Enabled()
				   and numEnemies == 2
				   and Target.Debuff(Aura.ShadowEmbrace).Up()
				   and Target.Debuff(Aura.ShadowEmbrace).Remains() <= Spell.ShadowBolt.ExecuteTime() * 2 + Spell.ShadowBolt.TravelTime()
				   and not Spell.ShadowBolt.IsInFlight();
			end,
		},

		SiphonLife = {
			-- actions+=/siphon_life,cycle_targets=1,max_cycle_targets=4,if=refreshable&target.time_to_die>10&((!(cooldown.summon_darkglare.remains<=soul_shard*action.unstable_affliction.execute_time)&active_enemies=5)|active_enemies=4)
			FourTarget = function(numEnemies, Target)
				return Target.Debuff(Aura.SiphonLife).Refreshable()
				   and Target.TimeToDie() > 10
				   and ((not(Spell.SummonDarkglare.Cooldown.Remains() <= Player.SoulShards() * Spell.UnstableAffliction.ExecuteTime()) and numEnemies == 5) or numEnemies == 4);
			end,

			-- actions+=/siphon_life,cycle_targets=1,max_cycle_targets=1,if=refreshable&target.time_to_die>10&((!(cooldown.summon_darkglare.remains<=soul_shard*action.unstable_affliction.execute_time)&active_enemies>=8)|active_enemies=1)
			OneTarget = function(numEnemies, Target)
				return Target.Debuff(Aura.SiphonLife).Refreshable()
				   and Target.TimeToDie() > 10
				   and ((not(Spell.SummonDarkglare.Cooldown.Remains() <= Player.SoulShards() * Spell.UnstableAffliction.ExecuteTime()) and numEnemies >= 8) or numEnemies == 1);
			end,

			-- actions+=/siphon_life,cycle_targets=1,max_cycle_targets=3,if=refreshable&target.time_to_die>10&((!(cooldown.summon_darkglare.remains<=soul_shard*action.unstable_affliction.execute_time)&active_enemies=6)|active_enemies=3)
			ThreeTarget = function(numEnemies, Target)
				return Target.Debuff(Aura.SiphonLife).Refreshable()
				   and Target.TimeToDie() > 10
				   and ((not(Spell.SummonDarkglare.Cooldown.Remains() <= Player.SoulShards() * Spell.UnstableAffliction.ExecuteTime()) and numEnemies == 6) or numEnemies == 3);
			end,

			-- actions+=/siphon_life,cycle_targets=1,max_cycle_targets=2,if=refreshable&target.time_to_die>10&((!(cooldown.summon_darkglare.remains<=soul_shard*action.unstable_affliction.execute_time)&active_enemies=7)|active_enemies=2)
			TwoTarget = function(numEnemies, Target)
				return Target.Debuff(Aura.SiphonLife).Refreshable()
				   and Target.TimeToDie() > 10
				   and ((not(Spell.SummonDarkglare.Cooldown.Remains() <= Player.SoulShards() * Spell.UnstableAffliction.ExecuteTime()) and numEnemies == 7) or numEnemies == 2);
			end,

		},

		-- LUA can't interpret a number a true and 0 as false so use Down() which returns a boolean.
		-- actions+=/summon_darkglare,if=dot.agony.ticking&dot.corruption.ticking&(buff.active_uas.stack=5|soul_shard=0)&(!talent.phantom_singularity.enabled|cooldown.phantom_singularity.remains)
		SummonDarkglare = function()
			return Target.Debuff(Aura.Agony).Ticking()
			   and Target.Debuff(Aura.Corruption).Ticking()
			   and (active_uas() == 5 or Player.SoulShards() == 0)
			   and (not Talent.PhantomSingularity.Enabled() or Talent.PhantomSingularity.Cooldown.Down());
		end,

		-- SimCraft does not specify, but use this only if you don't have a pet summoned or don't have the sacrifice buff if talented.
		-- actions.precombat+=/summon_pet
		SummonImp = function()
			return (Talent.GrimoireOfSacrifice.Enabled() and not Player.Buff(Aura.GrimoireOfSacrifice).Up())
			    or (not Talent.GrimoireOfSacrifice.Enabled() and not Pet.Family("Imp"));
		end,

		UnstableAffliction = {
			-- actions+=/unstable_affliction,cycle_targets=1,if=!variable.spammable_seed&(!talent.deathbolt.enabled|cooldown.deathbolt.remains>time_to_shard|soul_shard>1)&contagion<=cast_time+variable.padding
			Cycle = function()
				return not Variables.spammable_seed
				   and (not Talent.Deathbolt.Enabled() or Talent.Deathbolt.Cooldown.Remains() > time_to_shard() or Player.SoulShards() > 1)
				   and Player.Buff(Aura.Contagion).Remains() <= Spell.UnstableAffliction.CastTime() + Variables.padding;
			end,

			-- actions+=/unstable_affliction,if=!prev_gcd.1.summon_darkglare&!variable.spammable_seed&(talent.deathbolt.enabled&cooldown.deathbolt.remains<=execute_time&!azerite.cascading_calamity.enabled|soul_shard>=2&target.time_to_die>4+execute_time&active_enemies=1|target.time_to_die<=8+execute_time*soul_shard)
			NotSummonDarkglare = function(numEnemies)
				return not Player.PrevGCD(1, Spell.SummonDarkglare)
				   and not Variables.spammable_seed
				   and (Talent.Deathbolt.Enabled() and Talent.Deathbolt.Cooldown.Remains() <= Spell.UnstableAffliction.ExecuteTime() and not Azerite.CascadingCalamity.Enabled() or Player.SoulShards() >= 2 and Target.TimeToDie() > 4 + Spell.UnstableAffliction.ExecuteTime() and numEnemies == 1 or Target.TimeToDie() <= 8 + Spell.UnstableAffliction.ExecuteTime() * Player.SoulShards());
			end,

			-- actions+=/unstable_affliction,if=soul_shard>=5
			SoulShards = function()
				return Player.SoulShards() >= 5;
			end,

			-- actions+=/unstable_affliction,if=cooldown.summon_darkglare.remains<=soul_shard*execute_time
			SummonDarkglare = function()
				return Spell.SummonDarkglare.Cooldown.Remains() <= Player.SoulShards() * Spell.UnstableAffliction.ExecuteTime();
			end,

			-- actions+=/unstable_affliction,if=!variable.spammable_seed&contagion<=cast_time+variable.padding
			Use = function()
				return not Variables.spammable_seed
				   and Player.Buff(Aura.Contagion).Remains() <= Spell.UnstableAffliction.CastTime() + Variables.padding;
			end,
		},

		-- actions+=/vile_taint,if=time>20
		VileTaint = function()
			return coreGeneral.CombatTime() > 20;
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfIntellect = Objects.newItem(163222),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood);
		config.AOEOptions(options, Talent.PhantomSingularity, Spell.SeedOfCorruption);
		config.BuffOptions(options, Talent.GrimoireOfSacrifice);
		config.CooldownOptions(options, Talent.DarkSoulMisery, Talent.Deathbolt, Talent.DrainSoul, Talent.Haunt, Talent.PhantomSingularity, Talent.SiphonLife, Spell.SummonDarkglare, Talent.VileTaint);
		config.DefensiveOptions(options, Talent.DarkPact, Spell.UnendingResolve);
		config.PetOptions(options, Spell.SummonImp);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Talent.DarkPact, self.Defensives.DarkPact);
		action.EvaluateDefensiveAction(Spell.UnendingResolve, self.Defensives.UnendingResolve);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.SummonImp, self.Requirements.SummonImp);
		action.EvaluateAction(Talent.GrimoireOfSacrifice, self.Requirements.GrimoireOfSacrifice);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfIntellect, true);
		action.EvaluateAction(Spell.SeedOfCorruption, self.Requirements.SeedOfCorruption.Precombat);
		-- actions.precombat+=/haunt
		action.EvaluateAction(Talent.Haunt, true);
		action.EvaluateAction(Spell.ShadowBolt, self.Requirements.ShadowBolt.Precombat);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions=variable,name=spammable_seed,value=talent.sow_the_seeds.enabled&spell_targets.seed_of_corruption_aoe>=3|talent.siphon_life.enabled&spell_targets.seed_of_corruption>=5|spell_targets.seed_of_corruption>=8
		Variables.spammable_seed = (Talent.SowTheSeeds.Enabled() and addon.Enemies >= 3) or (Talent.SiphonLife.Enabled() and addon.Enemies >= 5) or addon.Enemies >= 8;
		-- actions+=/variable,name=padding,op=set,value=action.shadow_bolt.execute_time*azerite.cascading_calamity.enabled
		Variables.padding = Spell.ShadowBolt.ExecuteTime() * val(Azerite.CascadingCalamity.Enabled());
		-- actions+=/variable,name=padding,op=reset,value=gcd,if=azerite.cascading_calamity.enabled&(talent.drain_soul.enabled|talent.deathbolt.enabled&cooldown.deathbolt.remains<=gcd)
		if Azerite.CascadingCalamity.Enabled() and (Talent.DrainSoul.Enabled() or Talent.Deathbolt.Enabled() and Talent.Deathbolt.Cooldown.Remains() <= Player.GCD()) then
			Variables.padding = Player.GCD();
		end

		action.EvaluateAction(Consumable.BattlePotionOfIntellect, self.Requirements.BattlePotionOfIntellect);
		action.EvaluateAction(Racial.Fireblood, self.Requirements.Fireblood);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateCycleAction(Talent.DrainSoul, self.Requirements.DrainSoul);
		-- actions+=/haunt
		action.EvaluateAction(Talent.Haunt, true);
		action.EvaluateAction(Spell.SummonDarkglare, self.Requirements.SummonDarkglare);
		action.EvaluateCycleAction(Spell.Agony, self.Requirements.Agony.Expiring);
		action.EvaluateCycleAction(Spell.ShadowBolt, self.Requirements.ShadowBolt.Use);
		action.EvaluateAction(Talent.PhantomSingularity, self.Requirements.PhantomSingularity);
		action.EvaluateAction(Talent.VileTaint, self.Requirements.VileTaint);
		action.EvaluateAction(Spell.SeedOfCorruption, self.Requirements.SeedOfCorruption.Refresh);
		action.EvaluateCycleAction(Spell.Agony, self.Requirements.Agony.CreepingDeath);
		action.EvaluateCycleAction(Spell.Agony, self.Requirements.Agony.Use);
		action.EvaluateCycleAction(Talent.SiphonLife, self.Requirements.SiphonLife.OneTarget);
		action.EvaluateCycleAction(Talent.SiphonLife, self.Requirements.SiphonLife.TwoTarget);
		action.EvaluateCycleAction(Talent.SiphonLife, self.Requirements.SiphonLife.ThreeTarget);
		action.EvaluateCycleAction(Talent.SiphonLife, self.Requirements.SiphonLife.FourTarget);
		action.EvaluateCycleAction(Spell.Corruption, self.Requirements.Corruption);
		-- actions+=/dark_soul
		action.EvaluateAction(Talent.DarkSoulMisery, true);
		-- actions+=/vile_taint
		action.EvaluateAction(Talent.VileTaint, true);
		-- actions+=/berserking
		action.EvaluateAction(Racial.Berserking, true);
		action.EvaluateAction(Spell.UnstableAffliction, self.Requirements.UnstableAffliction.SoulShards);
		action.EvaluateAction(Spell.UnstableAffliction, self.Requirements.UnstableAffliction.SummonDarkglare);
		-- actions+=/phantom_singularity
		action.EvaluateAction(Talent.PhantomSingularity, true);

		action.CallActionList(rotationFillers, self.Requirements.Fillers);

		action.EvaluateAction(Spell.SeedOfCorruption, self.Requirements.SeedOfCorruption.Use);
		action.EvaluateAction(Spell.UnstableAffliction, self.Requirements.UnstableAffliction.NotSummonDarkglare);
		action.EvaluateAction(Spell.UnstableAffliction, self.Requirements.UnstableAffliction.Use);
		action.EvaluateCycleAction(Spell.UnstableAffliction, self.Requirements.UnstableAffliction.Cycle);

		-- actions+=/call_action_list,name=fillers
		action.CallActionList(rotationFillers);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Affliction Warlock", addonTable.Enum.SpecID.WARLOCK_AFFLICTION);
