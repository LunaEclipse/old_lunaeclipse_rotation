-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local Blizzard, systemCallbacks = addon.API.Blizzard, addon.System.Callbacks;
local select, strupper = select, strupper;
local variableError = false;

local defaultAccountSettings = {
	AddonSettings = {
		refreshInterval = 250,
		reactTime = 1000,
		useMaxWait = 0,
		maxWaitTime = 10,
		enemyCount = 2,
	},
	IncomingDamageSettings = {
		maxHistoryTime = 5,
	},
	NameplateSettings = {
		nameplateShowAlways = 0,
		nameplateMaxDistance = 60,
		nameplateShowFriends = 0,
		nameplateShowFriendlyGuardians = 0,
		nameplateShowFriendlyMinions = 0,
		nameplateShowFriendlyNPCs = 0,
		nameplateShowFriendlyPets = 0,
		nameplateShowFriendlyTotems = 0,
		nameplateShowEnemies = 0,
		nameplateShowEnemyGuardians = 0,
		nameplateShowEnemyMinions = 0,
		nameplateShowEnemyMinus = 0,
		nameplateShowEnemyPets = 0,
		nameplateShowEnemyTotems = 0,
	},
	TimeToDieSettings = {
		maxHistoryTime = 30,
	},
};

local defaultCharacterSettings = {
	ActiveRotations = {},
	CooldownOptions = {
		useSolo = 1,
		useNormal = 0,
		useNormalMax = 30,
		useElite = 0,
		useEliteMax = 90,
		useDungeonBoss = 0,
		useDungeonBossMax = 180,
		usePVP = 0,
		usePVPMax = 300,
	},
	Keybinds = {},
	Plugins = {
		InterruptManager = 1,
		WeakAuras = 1,
	},
	RotationSettings = {},
	ScriptOptions = {
		OPT_MOVING = 1,
		OPT_RANGE_CHECK = 1,
		OPT_STEALTH_SOLO = 0,
		OPT_POTION = 1,
	},
};

local function CMDLine(...)
	local Debug = addon.RotationDebug;
	local parameter = ...;

	if strupper(parameter) == "DEBUG_ALL" then
		Debug.DebugAll();
	elseif strupper(parameter) == "DEBUG" then
		Debug.DebugCurrent();
	elseif not parameter or parameter == "" then
		systemCallbacks.Callback("toggleMinimap");
	end
end

local function createTables(specInfo)
	if specInfo then
		local coreObjects = addon.Core.Objects;
		local coreTables = addon.Core.Tables;

		local classID = select(3, Blizzard.UnitClass("player"));
		local specID = coreTables.GetValue(specInfo, "ID");

		local azeriteData = addonTable.Data.Azerite.Class[classID] or {};
		local classData = addonTable.Data.Class[classID] or {};
		local racialData = addonTable.Data.Racial;
		local generalData = coreTables.GetValue(classData, "All", {});
		local specData = coreTables.GetValue(classData, specID, {});

		coreTables.Wipe(addonTable.AuraData, addonTable.AzeriteData, addonTable.CreateAuraData, addonTable.ConsumeAuraData, addonTable.PersistentMultiplierData, addonTable.RacialData, addonTable.SpellData, addonTable.TalentData, addonTable.TotemData);

		addonTable.AuraData = coreTables.Merge(coreTables.GetValue(generalData, "Aura", {}), coreTables.GetValue(specData, "Aura", {}), true);
		addonTable.AuraData = coreTables.Merge(addonTable.AuraData, coreTables.GetValue(racialData, "Aura", {}), true);
		addonTable.AuraData = coreTables.Merge(addonTable.AuraData, coreTables.GetValue(azeriteData, "Aura", {}), true);
		addonTable.AzeriteData = coreTables.Duplicate(coreTables.GetValue(azeriteData, "Spell", {}), true);
		addonTable.CreateAuraData = coreTables.Merge(coreTables.GetValue(generalData, "CreateAura", {}), coreTables.GetValue(specData, "CreateAura", {}), true);
		addonTable.ConsumeAuraData = coreTables.Merge(coreTables.GetValue(generalData, "ConsumeAura", {}), coreTables.GetValue(specData, "ConsumeAura", {}), true);
		addonTable.PersistentMultiplierData = coreTables.Merge(coreTables.GetValue(generalData, "PersistentMultiplier", {}), coreTables.GetValue(specData, "PersistentMultiplier", {}), true);
		addonTable.RacialData = coreTables.Duplicate(coreTables.GetValue(racialData, "Spell", {}), true);
		addonTable.ReplacementSpellsData = coreTables.Merge(coreTables.GetValue(generalData, "ReplacementSpells", {}), coreTables.GetValue(specData, "ReplacementSpells", {}), true);
		addonTable.SpellData = coreTables.Merge(coreTables.GetValue(generalData, "Spell", {}), coreTables.GetValue(specData, "Spell", {}), true);
		addonTable.TalentData = coreTables.Merge(coreTables.GetValue(generalData, "Talent", {}), coreTables.GetValue(specData, "Talent", {}), true);
		addonTable.TotemData = coreTables.Merge(coreTables.GetValue(generalData, "Totem", {}), coreTables.GetValue(specData, "Totem", {}), true);

		coreObjects.FinalizeSpellIDTable(addonTable.CreateAuraData, addonTable.ConsumeAuraData, addonTable.ReplacementSpellsData, addonTable.TotemData);
	end
end

local moduleEvents = {
	addonLoaded = function(name)
		if name == addonName then
			local coreTables = addon.Core.Tables;

			addonTable.accountSettings = coreTables.ApplyDefaults(LERH_AccountSettings, defaultAccountSettings);
			addonTable.characterSettings = coreTables.ApplyDefaults(LERH_CharacterSettings, defaultCharacterSettings);

			addon.refreshInterval = coreTables.GetValue(addonTable.accountSettings.AddonSettings, "refreshInterval") / 1000;
			addon.reactTime = coreTables.GetValue(addonTable.accountSettings.AddonSettings, "reactTime") / 1000;
		end
	end,

	errorSavedVariables = function(name)
		if name == addonName then
			variableError = true;
		end
	end,

	playerLogout = function()
		if not variableError then
			LERH_AccountSettings = addonTable.accountSettings;
			LERH_CharacterSettings = addonTable.characterSettings;
		end
	end,

	playerLogin = function(specInfo)
		createTables(specInfo);
	end,

	changeSpecFinish = function(specInfo)
		createTables(specInfo);
	end,

	rotationChanged = function()
		local apiGeneral = addon.API.General;
		local apiUnit = addon.API.Unit;
		local coreTables = addon.Core.Tables;
		local Rotation = addon.Rotation;

		local specInfo = apiGeneral.GetSpecializationInfo();

		if not coreTables.IsEmpty(specInfo) then
			local enabled = addon.currentRotation ~= nil;

			if enabled then
				addon.currentRotation.ProcessDisable();
			end

			addon.currentRotation = Rotation.GetRotation(specInfo.ID);

			if addon.currentRotation then
				addon.currentRotation.ProcessEnable(addon.Rotation);

				if not enabled then
					systemCallbacks.Callback("Enabled");
				end
			elseif not addon.currentRotation then
				addon.Options.rotationOptions.args = {};
				systemCallbacks.Callback("optionsChanged", "LERH_RotationSettings");

				if enabled then
					systemCallbacks.Callback("Disabled");
				end
			end
		end

		if apiUnit.InCombat() then
			systemCallbacks.Callback("enterCombat");
		else
			systemCallbacks.Callback("leaveCombat");
		end
	end,

	Enabled = function()
		SlashCmdList["LERH_ROTATION"] = CMDLine;
		_G["SLASH_LERH_ROTATION1"] = "/lerh";
	end,

	Disabled = function()
		SlashCmdList["LERH_ROTATION"] = nil;
		_G["SLASH_LERH_ROTATION1"] = nil;
	end,
};

-- Register this as a module of the addon.
systemCallbacks.RegisterAddonCallbacks(moduleEvents);