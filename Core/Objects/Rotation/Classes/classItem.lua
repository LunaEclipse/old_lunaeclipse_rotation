-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard = addon.API.Blizzard;
local gsub, huge, select, setmetatable, strtrim = gsub, huge, select, setmetatable, strtrim;
local Cache = setmetatable({}, { __mode = "kv" });

addonTable.classItem = function(ID, CheckBox)
	local itemID = ID;
	local spellID = select(2, Blizzard.GetItemSpell(ID)) or 0;

	local self = Cache[ID] or {};

	self.ClassID = function()
		return itemID;
	end

	self.ClassType = function()
		return "Item";
	end

	self.CheckBox = function()
		local coreMisc = addon.Core.Misc;
		local coreObjects = addon.Core.Objects;

		return (coreObjects.IsPotion(self) and "OPT_POTION")
		    or (CheckBox == nil and coreMisc.GetCheckBoxName(self.Name()))
		    or CheckBox;
	end

	self.Cooldown = addonTable.propertiesCooldown(self);

	-- equipped.foo
	-- Check if a given item is currently equipped in the given slot.
	self.Equipped = function(SlotID)
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local apiItem = addon.API.Item;

		local itemInfo = apiItem.GetItemInfo(itemID);

		if not Tables.IsEmpty(itemInfo) and Tables.GetValue(itemInfo, "IsEquipped") == nil then
			-- If the item does not equip in a slot, mark it as equipped if the player has any in their possession.
			-- This is for things like potions, which are used without being equippable.
			if Tables.GetValue(itemInfo, "EquipSlot") == "" then
				itemInfo.IsEquipped = Blizzard.GetItemCount(itemID) > 0;
				-- Returns false for Legion Legendaries while in Instanced PvP.
			elseif addon.Units.Player.InstancedPvP() and self.IsLegendary() and self.iLevel() >= 910 then
				itemInfo.IsEquipped = false;
				-- If a slot was given, check if the ID matches the item equipped in that slot.
			elseif SlotID then
				itemInfo.IsEquipped = addonCache.Persistant.Equipment[SlotID] == itemID;
				-- No slot was given just check with API if equipped in any slot.
			else
				itemInfo.IsEquipped = Blizzard.IsEquippedItem(itemID);
			end

			addonCache.Temporary.ItemInfo[itemID] = itemInfo;
		end

		return Tables.GetValue(itemInfo, "IsEquipped", false);
	end

	self.Evaluate = function()
		local addonCache = addonTable.Cache;
		local Core = addon.Core.General;
		local Item = addon.API.Item;
		local Objects = addon.Core.Objects;
		local Settings = addon.Core.Settings;
		local Tables = addon.Core.Tables;

		local usePotion = Settings.GetCharacterValue("ScriptOptions", "OPT_POTION") == 1;
		local itemInfo = Item.GetItemInfo(itemID);
		local returnValue = Tables.GetValue(itemInfo, "Evaluate");

		local defaultReturn = {
			Use = false,
			Start = 0,
			End = 0,
			Duration = 0,
			Text = "",
		};

		if not Tables.IsEmpty(itemInfo) then
			if returnValue == nil and self.Equipped()
								  and self.IsUsable()
								  and not self.Cooldown.Locked()
								  and (not Objects.IsPotion(self) or (usePotion and Core.IsRaidBossFight())) then
				returnValue = {
					Use = true,
					Start = self.Cooldown.Start(),
					End = self.Cooldown.End(),
					Duration = self.Cooldown.Duration(),
					Text = self.Cooldown.End() > Blizzard.GetTime() and "wait" or "",
				};
			end

			itemInfo.Evaluate = returnValue or defaultReturn;

			addonCache.Temporary.ItemInfo[itemID] = itemInfo;
		end

		return returnValue or defaultReturn;
	end

	-- Get the item Level.
	self.iLevel = function()
		local Item = addon.API.Item;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);

		return Tables.GetValue(itemInfo, "Level", 0);
	end

	self.InRange = addonTable.propertiesRange(self);

	-- Get wether an item is legendary.
	self.IsLegendary = function()
		local Item = addon.API.Item;
		local ItemRarity = addonTable.Enum.ItemRarity;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);

		return Tables.GetValue(itemInfo, "Rarity", ItemRarity.COMMON) == ItemRarity.LEGENDARY;
	end

	-- Get wether an item is ready to be used
	self.IsReady = function()
		return self.IsUsable()
		   and self.Cooldown.Up();
	end

	-- Get wether an item is usable currently.
	self.IsUsable = function()
		local Item = addon.API.Item;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);

		return Tables.GetValue(itemInfo, "HasOnUse", false)
		   and Tables.GetValue(itemInfo, "IsAvailable", false);
	end

	-- Get the item ID.
	self.ItemID = function()
		return itemID;
	end

	-- Get the item level requirement.
	self.LevelRequirement = function()
		local Item = addon.API.Item;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);

		return Tables.GetValue(itemInfo, "MinLevel", huge);
	end

	-- Get the item Name.
	self.Name = function()
		local Item = addon.API.Item;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);
		local returnValue = Tables.GetValue(itemInfo, "Name", "");

		returnValue = gsub(returnValue, "[0-9.]", "");
		returnValue = strtrim(returnValue);

		return returnValue;
	end

	-- Get the item Rarity.
	self.Rarity = function()
		local Item = addon.API.Item;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);

		return Tables.GetValue(itemInfo, "Rarity", addonTable.Enum.ItemRarity.COMMON);
	end

	self.SpellID = function()
		return spellID or 0;
	end

	self.SubType = function()
		local Item = addon.API.Item;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);

		return Tables.GetValue(itemInfo, "SubType", "");
	end

	-- Get the Time since last used.
	self.TimeSinceLastUsed = function()
		local addonCache = addonTable.Cache;
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local lastUsedTime = spellID ~= 0 and Tables.GetValue(addonCache.Persistant.ActionInfo[spellID], "LastUsedTime") or 0;

		return General.GetTime() - lastUsedTime;
	end

	self.Type = function()
		local Item = addon.API.Item;
		local Tables = addon.Core.Tables;

		local itemInfo = Item.GetItemInfo(itemID);

		return Tables.GetValue(itemInfo, "Type", "");
	end

	Cache[ID] = self;

	return self;
end