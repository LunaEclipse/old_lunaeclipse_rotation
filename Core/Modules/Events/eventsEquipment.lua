-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local function performScan()
	local wipe = wipe;
	local Cache = addonTable.Cache;

	-- Wipe the Cache
	wipe(Cache.Persistant.Equipment);

	-- Refresh Gear
	for counter = 1, 19 do
		local itemID = addon.API.Blizzard.GetInventoryItemID("player", counter);

		-- If there is an item in that slot
		if itemID then
			Cache.Persistant.Equipment[counter] = itemID;
		end
	end

	addonTable.Tier19_2PC, addonTable.Tier19_4PC = addon.Units.Player.HasTier("player", "T19");
	addonTable.Tier20_2PC, addonTable.Tier20_4PC = addon.Units.Player.HasTier("player", "T20");
	addonTable.Tier21_2PC, addonTable.Tier21_4PC = addon.Units.Player.HasTier("player", "T21");
end

local moduleEvents = {
	Enabled = function()
		performScan();
	end,
};

local tableEvents = {
	PLAYER_EQUIPMENT_CHANGED = performScan,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);