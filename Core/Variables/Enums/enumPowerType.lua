local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Enum.PowerType = {
	HealthCost = -2,
	None = -1,
	Mana = 0,
	Rage = 1,
	Focus = 2,
	Energy = 3,
	ComboPoints = 4,
	Runes = 5,
	RunicPower = 6,
	SoulShards = 7,
	AstralPower = 8,
	HolyPower = 9,
	Alternate = 10,
	Maelstrom = 11,
	Chi = 12,
	Insanity = 13,
	ArcaneCharges = 16,
	Fury = 17,
	Pain = 18,
};