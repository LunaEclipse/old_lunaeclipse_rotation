-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local min, pairs, sort, tinsert, tremove, wipe = min, pairs, sort, tinsert, tremove, wipe;

-- GUIDs used as keys for this table are summoned undead with limited duration.
local summonedCreatures = {};

-- SpellIDs associated with the tracked summons
-- Death Knight
local SPELLID_APOCALYPSE = 275699;
local SPELLID_ARMY_OF_THE_DEAD = 42650;
-- Warlock
local SPELLID_GRIMOIRE_FELGUARD = 111898;

-- Creatures summoned
-- Death Knight
local CREATURE_APOCALYPSE = 999999;  -- Fake creatureID to seperate Army of the Dead Ghouls from Apocalypse Ghouls
local CREATURE_ARMY_OF_THE_DEAD = 24207;
local CREATURE_ARMY_OF_THE_DEAD_SPECIAL = 111101;  -- Special Army of the Dead spawn relating to Apocalypse Hidden Skin
local CREATURE_GARGOYLE = 27829;
-- Shaman
local CREATURE_FERAL_SPIRIT = 100820;
local CREATURE_FIRE_ELEMENTAL = 15438;
local CREATURE_STORM_ELEMENTAL = 77936;
-- Warlock
local CREATURE_DEMONIC_TYRANT = 135002;
local CREATURE_DREADSTALKERS = 98035;
local CREATURE_GRIMOIRE_FELGUARD = 888888;   -- Fake creatureID to seperate Grimoire Felguard from regular Felguard
local CREATURE_INFERNAL = 89;
local CREATURE_WILD_IMPS = 55659;

-- Creature UnitIDs to check for summon events
local CLEU_SUMMON_CREATURES = {
	-- Death Knight
	[CREATURE_ARMY_OF_THE_DEAD] = true,
	[CREATURE_ARMY_OF_THE_DEAD_SPECIAL] = true,
	[CREATURE_GARGOYLE] = true,
	-- Shaman
	[CREATURE_FERAL_SPIRIT] = true,
	[CREATURE_FIRE_ELEMENTAL] = true,
	[CREATURE_STORM_ELEMENTAL] = true,
	-- Warlock
	[CREATURE_DEMONIC_TYRANT] = true,
	[CREATURE_DREADSTALKERS] = true,
	[CREATURE_GRIMOIRE_FELGUARD] = true,
	[CREATURE_INFERNAL] = true,
	[CREATURE_WILD_IMPS] = true,
};

-- Summon duration for removing from the tracker
local SUMMONED_CREATURE_DURATIONS = {
	-- Death Knight
	[CREATURE_APOCALYPSE] = 15,
	[CREATURE_ARMY_OF_THE_DEAD] = 30,
	[CREATURE_ARMY_OF_THE_DEAD_SPECIAL] = 30,
	[CREATURE_GARGOYLE] = 40,
	-- Shaman
	[CREATURE_FERAL_SPIRIT] = 15,
	[CREATURE_FIRE_ELEMENTAL] = 30,
	[CREATURE_STORM_ELEMENTAL] = 30,
	-- Warlock
	[CREATURE_DEMONIC_TYRANT] = 15,
	[CREATURE_DREADSTALKERS] = 12,
	[CREATURE_GRIMOIRE_FELGUARD] = 15,
	[CREATURE_INFERNAL] = 30,
	[CREATURE_WILD_IMPS] = 10,
};

-- CreatureIDs that override the actual summoned creatureID
local SUMMONED_CREATURE_OVERRIDE_CREATUREID = {
	[SPELLID_APOCALYPSE] = CREATURE_APOCALYPSE,
	[SPELLID_ARMY_OF_THE_DEAD] = CREATURE_ARMY_OF_THE_DEAD,
	[SPELLID_GRIMOIRE_FELGUARD] = CREATURE_GRIMOIRE_FELGUARD,
};

local CLEU_UNIT_ADDED = {
	SPELL_SUMMON = true,
};

-- Table of CLEU events for when a unit is removed from combat.
local CLEU_UNIT_REMOVED = {
	UNIT_DESTROYED = true,
	UNIT_DIED = true,
	UNIT_DISSIPATES = true,
};

addon.Core.Objects.FinalizeSpellIDTable(SUMMONED_CREATURE_OVERRIDE_CREATUREID);

local function removeImps(numImps)
	local Tables = addon.Core.Tables;

	local activeSummons = Tables.GetValue(summonedCreatures, CREATURE_WILD_IMPS);
	local currentTime = addon.API.Blizzard.GetTime();
	local counter = 0;

	if not Tables.IsEmpty(activeSummons) then
		for GUID, data in Tables.Sort(activeSummons, function(tableName, a, b) return Tables.GetValue(tableName[a], "despawn", 0) < Tables.GetValue(tableName[b], "despawn", 0) end) do
			local despawnTime = Tables.GetValue(data, "despawn");

			if despawnTime ~= nil and currentTime < despawnTime then
				if not numImps or counter < numImps then
					summonedCreatures[CREATURE_WILD_IMPS][GUID] = nil;
					counter = counter + 1;
				end
			end
		end
	end
end

local Events = {
	CastSuccess = function(...)
		local coreObjects = addon.Core.Objects;

		local _, unitID, _, spellID = ...;
		local currentSpell = addonTable.getSpell(spellID);

		if unitID == "player" and coreObjects.IsSpell(currentSpell) then
			if currentSpell.ClassID() == 264130 then -- Power Siphon, removes 2 wild imps.
				removeImps(2);
			elseif currentSpell.ClassID() == 196277 or (currentSpell.ClassID() == 265187 and addon.API.Blizzard.IsPlayerSpell(267215)) then  -- Implosion or Summon Demonic Tyrant with Demonic Consumption, removes all wild imps.
				removeImps();
			end
		end
	end,

	CombatLog = function(...)
		local Misc = addon.Core.Misc;
		local Tables = addon.Core.Tables;

		local currentTime = addon.API.Blizzard.GetTime();
		local _, _, combatEvent, _, sourceGUID, _, _, _, destGUID, _, _, _, spellID = ...;

		if sourceGUID == addon.API.Blizzard.UnitGUID("player") then
			local unitInfo = Misc.ParseGUID(destGUID);
			local creatureID = Tables.GetValue(SUMMONED_CREATURE_OVERRIDE_CREATUREID, spellID) or Tables.GetValue(unitInfo, "Creature");

			if CLEU_SUMMON_CREATURES[creatureID] then
				if CLEU_UNIT_REMOVED[combatEvent] and not Tables.IsEmpty(summonedCreatures[creatureID]) then
					summonedCreatures[creatureID][destGUID] = nil;
				elseif CLEU_UNIT_ADDED[combatEvent] then
					local spawnDuration = Tables.GetValue(SUMMONED_CREATURE_DURATIONS, creatureID);
					local despawnTime = spawnDuration ~= nil and currentTime + spawnDuration or nil;

					summonedCreatures[creatureID] = summonedCreatures[creatureID] or {};
					summonedCreatures[creatureID][destGUID] = {
						spawn = currentTime,
						despawn = despawnTime,
					};
				end
			end
		end
	end,

	-- Remove summons that have despawned without a combat log event.
	TimerEvent = function()
		local Tables = addon.Core.Tables;

		local activeSummons = {};
		local currentTime = addon.API.Blizzard.GetTime();

		if not Tables.IsEmpty(summonedCreatures) then
			for creatureID in pairs(summonedCreatures) do
				activeSummons = Tables.GetValue(summonedCreatures, creatureID);

				if not Tables.IsEmpty(activeSummons) then
					for GUID, data in Tables.Sort(activeSummons, function(tableName, a, b) return Tables.GetValue(tableName[a], "despawn", 0) < Tables.GetValue(tableName[b], "despawn", 0) end) do
						local despawnTime = Tables.GetValue(data, "despawn");

						if despawnTime and currentTime > despawnTime then
							summonedCreatures[creatureID][GUID] = nil;
						end
					end
				end

				if Tables.IsEmpty(activeSummons) then
					summonedCreatures[creatureID] = nil;
				end
			end
		end
	end,
};

addonTable.Summons = {
	Active = function(ID)
		local Tables = addon.Core.Tables;

		local activeSummons = Tables.GetValue(summonedCreatures, ID);
		local currentTime = addon.API.Blizzard.GetTime();

		if not Tables.IsEmpty(activeSummons) then
			for GUID, data in Tables.Sort(activeSummons, function(tableName, a, b) return Tables.GetValue(tableName[a], "despawn", 0) < Tables.GetValue(tableName[b], "despawn", 0) end) do
				local despawnTime = Tables.GetValue(data, "despawn");

				if despawnTime ~= nil and currentTime < despawnTime then
					return true;
				end
			end
		end

		return false;
	end,

	Count = function(ID)
		local Tables = addon.Core.Tables;

		local activeSummons = Tables.GetValue(summonedCreatures, ID);
		local currentTime = addon.API.Blizzard.GetTime();
		local returnValue = 0;

		if not Tables.IsEmpty(activeSummons) then
			for GUID, data in Tables.Sort(activeSummons, function(tableName, a, b) return Tables.GetValue(tableName[a], "despawn", 0) < Tables.GetValue(tableName[b], "despawn", 0) end) do
				local despawnTime = Tables.GetValue(data, "despawn");

				if despawnTime ~= nil and currentTime < despawnTime then
					returnValue = returnValue + 1;
				end
			end
		end

		return returnValue;
	end,

	FirstDespawn = function(ID)
		local Tables = addon.Core.Tables;

		local activeSummons = Tables.GetValue(summonedCreatures, ID);
		local currentTime = addon.API.Blizzard.GetTime();

		if not Tables.IsEmpty(activeSummons) then
			for GUID, data in Tables.Sort(activeSummons, function(tableName, a, b) return Tables.GetValue(tableName[a], "despawn", 0) < Tables.GetValue(tableName[b], "despawn", 0) end) do
				local despawnTime = Tables.GetValue(data, "despawn");

				if despawnTime ~= nil and currentTime < despawnTime then
					return despawnTime - currentTime;
				end
			end
		end

		return 0;
	end,

	LastDespawn = function(ID)
		local Tables = addon.Core.Tables;

		local activeSummons = Tables.GetValue(summonedCreatures, ID);
		local currentTime = addon.API.Blizzard.GetTime();

		if not Tables.IsEmpty(activeSummons) then
			for GUID, data in Tables.Sort(activeSummons, function(tableName, a, b) return Tables.GetValue(tableName[b], "despawn", 0) < Tables.GetValue(tableName[a], "despawn", 0) end) do
				local despawnTime = Tables.GetValue(data, "despawn");

				if despawnTime ~= nil and currentTime < despawnTime then
					return despawnTime - currentTime;
				end
			end
		end

		return 0;
	end,
};

local moduleEvents = {
	Disabled = function()
		wipe(summonedCreatures);

		addon.System.Timers.TimerRemove(Events.TimerEvent);
	end,

	Enabled = function()
		-- Make sure the timer event is called immediately before starting the timer so it doesn't need to wait 1 second to get enemy data.
		Events.TimerEvent();

		addon.System.Timers.TimerAdd(Events.TimerEvent, 1);
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = Events.CombatLog,
	UNIT_SPELLCAST_SUCCEEDED = Events.CastSuccess,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);