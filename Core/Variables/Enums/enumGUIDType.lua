local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Enum.GUIDType = {
	GAMEOBJECT = "GameObject",
	NPC = "Creature",
	PET = "Pet",
	PLAYER = "Player",
	VEHICLE = "Vehicle",
	VIGNETTE = "Vignette",
};