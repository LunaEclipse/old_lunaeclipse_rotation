-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local Blizzard = addon.API.Blizzard;
local error, format, ipairs, strfind, strupper, pairs, setmetatable = error, format, ipairs, strfind, strupper, pairs, setmetatable;

-- This is stored on the addon so its publically available to all other addons.
addon.Core.Objects = {
	FinalizeActions = function(...)
		local Tables = addon.Core.Tables;
		local args = {...};

		-- Set the metatable to allow keys to be case insensitive.
		local metatable = {
			__mode = "k",

			__index = function(table, key)
				for k in pairs(table) do
					if strlower(k) == strlower(key) then
						return table[k];
					end
				end

				error(format("Action object %q not found!", key));
			end,

			__metatable = "This table is protected!",
		};

		if not Tables.IsEmpty(args) then
			for _, table in ipairs(args) do
				-- Attach the metatable
				setmetatable(table, metatable);
			end
		else
			local returnValue = {};

			setmetatable(returnValue, metatable);

			return returnValue;
		end
	end,

	FinalizeRequirements = function(...)
		local Tables = addon.Core.Tables;
		local args = {...};

		-- Set the metatable to allow keys to be case insensitive.
		local metatable = {
			__mode = "k",

			__index = function(table, key)
				for k in pairs(table) do
					if strlower(k) == strlower(key) then
						return table[k];
					end
				end

				error(format("Action requirements function %q not found!", key));
			end,

			__metatable = "This table is protected!",
		};

		if not Tables.IsEmpty(args) then
			for _, table in ipairs(args) do
				-- Attach the metatable
				setmetatable(table, metatable);
			end
		else
			local returnValue = {};

			setmetatable(returnValue, metatable);

			return returnValue;
		end
	end,

	FinalizeSpellIDTable = function(...)
		local Tables = addon.Core.Tables;
		local args = {...};

		-- Set the metatable to allow keys to be case insensitive.
		local metatable = {
			__index = function(table, key)
				for k in pairs(table) do
					if addon.Core.General.GetSpellReplacementID(k) == key then
						return table[k];
					end
				end

				return nil;
			end,

			__metatable = "This table is protected!",
		};

		if not Tables.IsEmpty(args) then
			for _, table in ipairs(args) do
				-- Attach the metatable
				setmetatable(table, metatable);
			end
		else
			local returnValue = {};

			setmetatable(returnValue, metatable);

			return returnValue;
		end
	end,

	FinalizeUnits = function(...)
		local Tables = addon.Core.Tables;
		local args = {...};

		-- Set the metatable to allow keys to be case insensitive.
		local metatable = {
			__mode = "k",

			__index = function(table, key)
				local unitID = strlower(key);
				local unitGUID = addon.API.Blizzard.UnitGUID(unitID);

				if unitGUID then
					for ID, currentUnit in pairs(table) do
						if unitID == strlower(ID) or unitGUID == currentUnit.GUID() then
							return currentUnit;
						end
					end
				end

				return addonTable.EmptyUnit;
			end,

			__metatable = "This table is protected!",
		};

		if not Tables.IsEmpty(args) then
			for _, table in ipairs(args) do
				-- Attach the metatable
				setmetatable(table, metatable);
			end
		else
			local returnValue = {};

			setmetatable(returnValue, metatable);

			return returnValue;
		end

	end,

	GetID = function(variable)
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;

		return Objects.IsObject(variable)
		   and Tables.GetValue(variable, "ClassID", "");
	end,

	GetType = function(variable)
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;

		return Objects.IsObject(variable)
		   and Tables.GetValue(variable, "ClassType", "");
	end,

	IsAzerite = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "Azerite";
	end,

	IsBaseAPL = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "BaseAPL";
	end,

	IsItem = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "Item";
	end,

	IsObject = function(variable)
		local Objects = addon.Core.Objects;

		return Objects.IsAzerite(variable)
		    or Objects.IsItem(variable)
		    or Objects.IsPassive(variable)
			or Objects.IsSpell(variable)
		    or Objects.IsTalent(variable)
			or Objects.IsUnit(variable);
	end,

	IsPassive = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "Passive";
	end,

	IsPotion = function(variable)
		local Objects = addon.Core.Objects;

		return Objects.IsItem(variable)
		   and strupper(variable.SubType()) == strupper(L["Potion"]);
	end,

	IsSpell = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "Spell";
	end,

	IsSubRotation = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "SubRotation";
	end,

	IsTalent = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "Talent";
	end,

	IsUnit = function(variable)
		local Tables = addon.Core.Tables;

		return not Tables.IsEmpty(variable)
		   and Tables.GetValue(variable, "ClassType", "") == "Unit";
	end,

	newAzerite = function(spellID)
		if spellID then
			return addonTable.classAzerite(spellID);
		end

		return nil;
	end,

	newItem = function(itemID, checkBox)
		if itemID then
			return addonTable.classItem(itemID, checkBox);
		end

		return nil;
	end,

	newSpell = function(spellID, GCD, checkBox)
		local isPassive = Blizzard.IsPassiveSpell(spellID);

		if spellID then
			if isPassive then
				return addonTable.classPassive(spellID);
			else
				return addonTable.classSpell(spellID, GCD, checkBox);
			end
		end

		return nil;
	end,

	newTalent = function(spellID, GCD, checkBox)
		local isPassive = Blizzard.IsPassiveSpell(spellID);

		if spellID then
			if isPassive then
				return addonTable.classPassive(spellID, "Talent");
			else
				return addonTable.classSpell(spellID, GCD, checkBox);
			end
		end

		return nil;
	end,

	newUnit = function(unitID)
		if unitID then
			unitID = strlower(unitID);

			if unitID == "pet" then
				return addonTable.classPet(unitID);
			elseif unitID == "player" then
				return addonTable.classPlayer(unitID);
			else
				return addonTable.classUnit(unitID);
			end
		end

		return nil;
	end,
};