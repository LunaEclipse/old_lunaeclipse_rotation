-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local GetTime = GetTime;

-- Base Mana by level for all classes, casters is multiplied by 5.
addonTable.BASE_MANA = { 31, 34, 36, 42, 71, 101, 104, 137, 140, 173, 176, 212, 220, 252, 256, 292, 298, 334, 362, 400, 420, 460, 480, 520, 580, 620, 620, 660, 740, 780, 780, 840, 880, 940, 980, 1020, 1040, 1080, 1180, 1240, 1240, 1300, 1360, 1420, 1480, 1540, 1600, 1660, 1720, 1780, 1840, 1920, 1940, 2040, 2060, 2140, 2200, 2280, 2360, 2420, 2600, 2760, 2920, 2940, 3020, 3020, 3080, 3100, 3260, 3380, 3520, 3700, 3860, 3880, 4060, 4180, 4360, 4600, 4680, 4880, 5100, 5280, 5520, 5780, 6000, 6400, 6600, 6800, 7200, 7400, 10200, 13000, 15600, 19400, 24000, 27600, 28800, 30000, 31000, 32000, 76000, 86000, 100000, 114000, 128000, 140000, 160000, 180000, 200000, 220000 };

addonTable.Blacklist = {
	-- Custom List (User Defined), must be a valid Lua Boolean or Function as Value and have the npcID as Key
	UserDefined = {},

	-- Custom Cycle List (User Defined), must be a valid Lua Boolean or Function as Value and have the npcID as Key
	CycleUserDefined = {},
};

-- Color Tags
addonTable.COLORS = {
	CLASS = {
		[1] = "|cFFC79C6E",
		[2] = "|cFFF58CBA",
		[3] = "|cFFABD473",
		[4] = "|cFFFFF569",
		[5] = "|cFFFFFFFF",
		[6] = "|cFFC41F3B",
		[7] = "|cFF0070DE",
		[8] = "|cFF69CCF0",
		[9] = "|cFF9482C9",
		[10] = "|cFF00FF96",
		[11] = "|cFFFF7D0A",
		[12] = "|cFFA330C9",
	},

	TAGS = {
		ACTIVE = "|cFF00FF00",
		DEFAULT = "|cFF7FCCFF",
		HIGHLIGHT = "|cFF1784D1",
		INACTIVE = "|cFFFF0000",
		MESSAGE = "|cFFFFFF00",
	},
};

-- Records time combat started and ended.
addonTable.Combat = {
	Start = 0,
	End = 0
};

-- Custom Cycle List (User Defined), must be a valid Lua Boolean or Function as Value and have the npcID as Key
addonTable.CycleUserDefined = {
	--- Legion
	----- Trial of Valor (T19 - 7.1 Patch) -----
	-- Helya
	-- Bilewater Slime
	[114553] = function(self)
		return self.HealthPercent() >= 65;
	end,
	-- Decaying Minion
	[114568] = true,
	-- Helarjar Mistwatcher
	[116335] = true,
	----- Nighthold (T19 - 7.1.5 Patch) -----
	-- Trilliax
	-- Scrubber
	[104596] = true,
	-- Spellblade Aluriel
	-- Fel Soul
	[115905] = true,
	-- Botanist Tel'Arn (Mythic Only)
	-- Naturalist Tel'Arn
	[109041] = function()
		return addon.Units.Player.InstanceDifficulty() == 16;
	end,
	-- Arcanist Tel'Arn
	[109040] = function()
		return addon.Units.Player.InstanceDifficulty() == 16;
	end,
	-- Solarist Tel'Arn
	[109038] = function()
		return addon.Units.Player.InstanceDifficulty() == 16;
	end,
	-- Star Augur Etraeus
	-- Voidling
	[104688] = true,
	----- Tomb of Sargeras (T20 - 7.2.5 Patch) -----
	-- Mistress Sassz'ine
	-- Abyss Stalker
	[115795] = true,
	-- Razorjaw Waverunner
	[115902] = true,
	-- Fallen Avatar
	-- Maiden of Valor
	[120437] = true,
};

-- SpellIDs
addonTable.GLOBAL_COOLDOWN = 61304;

-- Stores the time of last action, this is set to current time by default on startup.
addonTable.LastAction = GetTime();

-- Melee Range is 5 yards so we never want lower then this.
addonTable.maxRange = 5;

-- Distance to use for showing nameplates, and maximum range of spells with range specified.
addonTable.nameplateDistance = 0;

-- Variables for performing Opening Rotations.
addonTable.Opener = {};
addonTable.OpenerRunning = false;

-- Table for spells who came off cooldown early.
addonTable.ReactSpells = {};

-- Table for holding previously cast spells.
addonTable.PreviousSpell = {
	All = {},
	GCD = {},
	OffGCD = {},

	PetAll = {},
	PetGCD = {},
	PetOffGCD = {},
};

-- Stores DBM/BigWigs pull timer information.
addonTable.PullTimer = {};

-- Table for holding registered rotations
addonTable.RegisteredRotations = {};

-- General Tags
addonTable.TAGS = {
	END = "|r",
};