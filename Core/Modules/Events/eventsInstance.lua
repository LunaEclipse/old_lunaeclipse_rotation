-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local function performScan()
	local wipe = wipe;
	local Cache = addonTable.Cache;
	local apiGeneral = addon.API.General;

	-- Wipe the Cache
	wipe(Cache.Persistant.Player.InstanceInfo);

	-- Update Instance information.
	Cache.Persistant.Player.InstanceInfo = apiGeneral.GetInstanceInfo();
end

local moduleEvents = {
	playerLogin = function()
		performScan();
	end,
};

local tableEvents = {
	UPDATE_INSTANCE_INFO = performScan,
	ZONE_CHANGED_NEW_AREA = performScan,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);