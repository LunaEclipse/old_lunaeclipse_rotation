-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard, libRangeCheck = addon.API.Blizzard, LibStub("LibRangeCheck-2.0");
local format, huge, setmetatable = format, math.huge, setmetatable;

local Cache = setmetatable({}, { __mode = "kv" });

local function itemInRange(Item, Unit)
	local Objects = addon.Core.Objects;
	local Tables = addon.Core.Tables;
	local apiItem = addon.API.Item;

	if Objects.IsItem(Item) and Objects.IsUnit(Unit) then
		local itemID = Item.ItemID();
		local unitID = Unit.UnitID();
		local itemInfo = apiItem.GetItemInfo(itemID);
		local useRange = Tables.GetValue(itemInfo, "UseRange", false);

		if useRange then
			local inRange = Blizzard.IsItemInRange(itemID, unitID);

			return inRange == nil or inRange == 1 or inRange == true;
		else
			-- Spell specifies don't use range information so always return as in range.
			return true;
		end
	end

	-- Objects are not a valid so return false.
	return false;
end

local function spellInRange(Spell, Unit)
	local Objects = addon.Core.Objects;
	local Tables = addon.Core.Tables;
	local apiSpell = addon.API.Spell;

	if Objects.IsSpell(Spell) and Objects.IsUnit(Unit) then
		local spellID = Spell.SpellID();
		local unitID = Unit.UnitID();
		local spellInfo = apiSpell.GetSpellInfo(spellID);
		local useRange = Tables.GetValue(spellInfo, "UseRange", false);

		if useRange then
			local bookIndex, bookType = Spell.BookIndex();
			local inRange;

			if bookIndex then
				inRange = Blizzard.IsSpellInRange(bookIndex, bookType, unitID);
			else
				inRange = true;
			end

			return inRange == nil or inRange == 1 or inRange == true;
		else
			-- Spell specifies don't use range information so always return as in range.
			return true;
		end
	end

	-- Objects are not valid so return false.
	return false;
end

local function unitInRange(Unit, Other)
	local Core = addon.Core.General;
	local Objects = addon.Core.Objects;

	if Objects.IsUnit(Unit) then
		local unitID = Unit.UnitID();

		if Objects.IsItem(Other) then
			return itemInRange(Other, Unit);
		elseif Objects.IsSpell(Other) then
			return spellInRange(Other, Unit);
		elseif Core.ToNumber(Other) ~= nil then
			local minRange, maxRange = libRangeCheck:GetRange(unitID);

			if not minRange then
				-- Couldn't get range information so just mark this unit as in range as safety fall back
				return true;
			elseif not maxRange then
				-- Couldn't get max range information so mark the unit is probably far away so return false
				return false;
			else
				-- Range info found so mark the unit as in range only if the distance specified is greater than maximum range
				return Core.ToNumber(Other) >= maxRange;
			end
		end
	end

	-- Not a valid unit or distance specified so return false
	return false;
end

addonTable.propertiesRange = function(Object)
	local Objects = addon.Core.Objects;

	local objectID = Objects.GetID(Object) or Object;
	local objectType = Objects.GetType(Object) or "None";
	local key = format("%s-%s", objectType, objectID);

	local self = Cache[key] or {};

	if objectType ~= "Item" then
		-- Get the spell minimum range.
		self.Min = function()
			local Spell = addon.API.Spell;
			local Tables = addon.Core.Tables;

			if objectType == "Spell" then
				local spellID = Object.SpellID();
				local spellInfo = Spell.GetSpellInfo(spellID);

				return Tables.GetValue(spellInfo, "MinRange", 0);
			elseif objectType == "Unit" then
				local minRange = libRangeCheck:GetRange(objectID);

				return minRange	or 0;
			end
		end

		-- Get the spell maximum range.
		self.Max = function()
			local Spell = addon.API.Spell;
			local Tables = addon.Core.Tables;

			if objectType == "Spell" then
				local spellID = Object.SpellID();
				local spellInfo = Spell.GetSpellInfo(spellID);

				return Tables.GetValue(spellInfo, "MaxRange", huge);
			elseif objectType == "Unit" then
				local _, maxRange = libRangeCheck:GetRange(objectID);

				return maxRange	or huge;
			end
		end
	end

	local metaTable = {
		__call = function(table, Other)
			if objectType == "Item" and Objects.IsUnit(Other) then
				return itemInRange(Object, Other);
			elseif objectType == "Spell" and Objects.IsUnit(Other) then
				return spellInRange(Object, Other);
			elseif objectType == "Unit" then
				return unitInRange(Object, Other);
			end

			-- Objects are not a valid so return false.
			return false;
		end,
	};
	setmetatable(self, metaTable);
	Cache[key] = self;

	return self;
end