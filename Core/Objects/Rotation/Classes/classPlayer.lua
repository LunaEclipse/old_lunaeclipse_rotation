-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard = addon.API.Blizzard;
local CR_VERSATILITY_DAMAGE_DONE, ipairs, max, pairs, select, setmetatable, strupper = CR_VERSATILITY_DAMAGE_DONE, ipairs, max, pairs, select, setmetatable, strupper;

-- buff.bloodlust.up
local BloodlustBuffs = {
	90355, -- Ancient Hysteria
	2825, -- Bloodlust
	146555, -- Drums of Rage
	32182, -- Heroism
	160452, -- Netherwinds
	80353, -- Time Warp
};

-- debuff.bloodlust.up
local BloodlustDebuffs = {
	57723, -- Exhaustion
	160455, -- Fatigued
	95809, -- Insanity
	57724, -- Sated
	80354, -- Temporal Displacement
};

-- Get if the player is on a combat mount or not.
local CombatMountBuffs = {
	--- Classes
	131347, -- Demon Hunter Glide
	783, -- Druid Travel Form
	165962, -- Druid Flight Form
	220509, -- Paladin Divine Steed
	221883, -- Paladin Divine Steed
	221884, -- Paladin Divine Steed
	221886, -- Paladin Divine Steed
	221887, -- Paladin Divine Steed
	--- Legion
	-- Class Order Hall
	220480, -- Death Knight Ebon Blade Deathcharger
	220484, -- Death Knight Nazgrim's Deathcharger
	220488, -- Death Knight Trollbane's Deathcharger
	220489, -- Death Knight Whitemane's Deathcharger
	220491, -- Death Knight Mograine's Deathcharger
	220504, -- Paladin Silver Hand Charger
	220507, -- Paladin Silver Hand Charger
	-- Stormheim PVP Quest (Bareback Brawl)
	221595, -- Storm's Reach Cliffwalker
	221671, -- Storm's Reach Warbear
	221672, -- Storm's Reach Greatstag
	221673, -- Storm's Reach Worg
	218964, -- Stormtalon
	--- Warlord of Draenor (WoD)
	-- Nagrand
	164222, -- Frostwolf War Wolf
	165803, -- Telaari Talbuk
};

-- Get if the player is stealthed or not
local StealthBuffs = {
	-- Rogue
	1784, -- Stealth
	115191, -- Stealth w/ Subterfuge Talent
	-- Feral
	5215, -- Prowl
	-- Night Elf
	58984, -- Shadowmeld
	-- Rogue
	11327, -- Vanish
	115193, -- Vanish w/ Subterfuge Talent
	115192, -- Subterfuge Buff
	185422, -- Stealth from Shadow Dance
	-- Feral
	252071, -- Stealth from Incartation: King of the Jungle
};

-- Check player set bonuses (call addonTable.GetEquipment before to refresh the current gear)
local TierSets = {
	["T18"] = {
		Count = function(Count) return Count >= 2, Count >= 4; end, -- Return Function
		[1] = { [5] = 124319, [10] = 124329, [1] = 124334, [7] = 124340, [3] = 124346 }, -- Warrior:      Chest, Hands, Head, Legs, Shoulder
		[2] = { [5] = 124318, [10] = 124328, [1] = 124333, [7] = 124339, [3] = 124345 }, -- Paladin:      Chest, Hands, Head, Legs, Shoulder
		[3] = { [5] = 124284, [10] = 124292, [1] = 124296, [7] = 124301, [3] = 124307 }, -- Hunter:       Chest, Hands, Head, Legs, Shoulder
		[4] = { [5] = 124248, [10] = 124257, [1] = 124263, [7] = 124269, [3] = 124274 }, -- Rogue:        Chest, Hands, Head, Legs, Shoulder
		[5] = { [5] = 124172, [10] = 124155, [1] = 124161, [7] = 124166, [3] = 124178 }, -- Priest:       Chest, Hands, Head, Legs, Shoulder
		[6] = { [5] = 124317, [10] = 124327, [1] = 124332, [7] = 124338, [3] = 124344 }, -- Death Knight: Chest, Hands, Head, Legs, Shoulder
		[7] = { [5] = 124303, [10] = 124293, [1] = 124297, [7] = 124302, [3] = 124308 }, -- Shaman:       Chest, Hands, Head, Legs, Shoulder
		[8] = { [5] = 124171, [10] = 124154, [1] = 124160, [7] = 124165, [3] = 124177 }, -- Mage:         Chest, Hands, Head, Legs, Shoulder
		[9] = { [5] = 124173, [10] = 124156, [1] = 124162, [7] = 124167, [3] = 124179 }, -- Warlock:      Chest, Hands, Head, Legs, Shoulder
		[10] = { [5] = 124247, [10] = 124256, [1] = 124262, [7] = 124268, [3] = 124273 }, -- Monk:         Chest, Hands, Head, Legs, Shoulder
		[11] = { [5] = 124246, [10] = 124255, [1] = 124261, [7] = 124267, [3] = 124272 }, -- Druid:        Chest, Hands, Head, Legs, Shoulder
		[12] = nil, -- Demon Hunter: Chest, Hands, Head, Legs, Shoulder
	},

	["T18_ClassTrinket"] = {
		Count = function(Count) return Count >= 1; end, -- Return Function
		[1] = { [13] = 124523, [14] = 124523 }, -- Warrior:      Worldbreaker's Resolve
		[2] = { [13] = 124518, [14] = 124518 }, -- Paladin:      Libram of Vindication
		[3] = { [13] = 124515, [14] = 124515 }, -- Hunter:       Talisman of the Master Tracker
		[4] = { [13] = 124520, [14] = 124520 }, -- Rogue:        Bleeding Hollow Toxin Vessel
		[5] = { [13] = 124519, [14] = 124519 }, -- Priest:       Repudiation of War
		[6] = { [13] = 124513, [14] = 124513 }, -- Death Knight: Reaper's Harvest
		[7] = { [13] = 124521, [14] = 124521 }, -- Shaman:       Cache of the Primal Elements
		[8] = { [13] = 124516, [14] = 124516 }, -- Mage:         Tome of Shifting Words
		[9] = { [13] = 124522, [14] = 124522 }, -- Warlock:      Fragment of the Dark Star
		[10] = { [13] = 124517, [14] = 124517 }, -- Monk:         Sacred Draenic Incense
		[11] = { [13] = 124514, [14] = 124514 }, -- Druid:        Seed of Creation
		[12] = { [13] = 139630, [14] = 139630 }, -- Demon Hunter: Etching of Sargeras
	},

	["T19"] = {
		Count = function(Count) return Count >= 2, Count >= 4; end, -- Return Function
		[1] = { [5] = 138351, [15] = 138374, [10] = 138354, [1] = 138357, [7] = 138360, [3] = 138363 }, -- Warrior:      Chest, Back, Hands, Head, Legs, Shoulder
		[2] = { [5] = 138350, [15] = 138369, [10] = 138353, [1] = 138356, [7] = 138359, [3] = 138362 }, -- Paladin:      Chest, Back, Hands, Head, Legs, Shoulder
		[3] = { [5] = 138339, [15] = 138368, [10] = 138340, [1] = 138342, [7] = 138344, [3] = 138347 }, -- Hunter:       Chest, Back, Hands, Head, Legs, Shoulder
		[4] = { [5] = 138326, [15] = 138371, [10] = 138329, [1] = 138332, [7] = 138335, [3] = 138338 }, -- Rogue:        Chest, Back, Hands, Head, Legs, Shoulder
		[5] = { [5] = 138319, [15] = 138370, [10] = 138310, [1] = 138313, [7] = 138316, [3] = 138322 }, -- Priest:       Chest, Back, Hands, Head, Legs, Shoulder
		[6] = { [5] = 138349, [15] = 138364, [10] = 138352, [1] = 138355, [7] = 138358, [3] = 138361 }, -- Death Knight: Chest, Back, Hands, Head, Legs, Shoulder
		[7] = { [5] = 138346, [15] = 138372, [10] = 138341, [1] = 138343, [7] = 138345, [3] = 138348 }, -- Shaman:       Chest, Back, Hands, Head, Legs, Shoulder
		[8] = { [5] = 138318, [15] = 138365, [10] = 138309, [1] = 138312, [7] = 138315, [3] = 138321 }, -- Mage:         Chest, Back, Hands, Head, Legs, Shoulder
		[9] = { [5] = 138320, [15] = 138373, [10] = 138311, [1] = 138314, [7] = 138317, [3] = 138323 }, -- Warlock:      Chest, Back, Hands, Head, Legs, Shoulder
		[10] = { [5] = 138325, [15] = 138367, [10] = 138328, [1] = 138331, [7] = 138334, [3] = 138337 }, -- Monk:         Chest, Back, Hands, Head, Legs, Shoulder
		[11] = { [5] = 138324, [15] = 138366, [10] = 138327, [1] = 138330, [7] = 138333, [3] = 138336 }, -- Druid:        Chest, Back, Hands, Head, Legs, Shoulder
		[12] = { [5] = 138376, [15] = 138375, [10] = 138377, [1] = 138378, [7] = 138379, [3] = 138380 }, -- Demon Hunter: Chest, Back, Hands, Head, Legs, Shoulder
	},

	["T20"] = {
		Count = function(Count) return Count >= 2, Count >= 4; end, -- Return Function
		[1] = { [5] = 147187, [15] = 147188, [10] = 147189, [1] = 147190, [7] = 147191, [3] = 147192 }, -- Warrior:      Chest, Back, Hands, Head, Legs, Shoulder
		[2] = { [5] = 147157, [15] = 147158, [10] = 147159, [1] = 147160, [7] = 147161, [3] = 147162 }, -- Paladin:      Chest, Back, Hands, Head, Legs, Shoulder
		[3] = { [5] = 147139, [15] = 147140, [10] = 147141, [1] = 147142, [7] = 147143, [3] = 147144 }, -- Hunter:       Chest, Back, Hands, Head, Legs, Shoulder
		[4] = { [5] = 147169, [15] = 147170, [10] = 147171, [1] = 147172, [7] = 147173, [3] = 147174 }, -- Rogue:        Chest, Back, Hands, Head, Legs, Shoulder
		[5] = { [5] = 147167, [15] = 147163, [10] = 147164, [1] = 147165, [7] = 147166, [3] = 147168 }, -- Priest:       Chest, Back, Hands, Head, Legs, Shoulder
		[6] = { [5] = 147121, [15] = 147122, [10] = 147123, [1] = 147124, [7] = 147125, [3] = 147126 }, -- Death Knight: Chest, Back, Hands, Head, Legs, Shoulder
		[7] = { [5] = 147175, [15] = 147176, [10] = 147177, [1] = 147178, [7] = 147179, [3] = 147180 }, -- Shaman:       Chest, Back, Hands, Head, Legs, Shoulder
		[8] = { [5] = 147149, [15] = 147145, [10] = 147146, [1] = 147147, [7] = 147148, [3] = 147150 }, -- Mage:         Chest, Back, Hands, Head, Legs, Shoulder
		[9] = { [5] = 147185, [15] = 147181, [10] = 147182, [1] = 147183, [7] = 147184, [3] = 147186 }, -- Warlock:      Chest, Back, Hands, Head, Legs, Shoulder
		[10] = { [5] = 147151, [15] = 147152, [10] = 147153, [1] = 147154, [7] = 147155, [3] = 147156 }, -- Monk:         Chest, Back, Hands, Head, Legs, Shoulder
		[11] = { [5] = 147133, [15] = 147134, [10] = 147135, [1] = 147136, [7] = 147137, [3] = 147138 }, -- Druid:        Chest, Back, Hands, Head, Legs, Shoulder
		[12] = { [5] = 147127, [15] = 147128, [10] = 147129, [1] = 147130, [7] = 147131, [3] = 147132 }, -- Demon Hunter: Chest, Back, Hands, Head, Legs, Shoulder
	},

	["T21"] = {
		Count = function(Count) return Count >= 2, Count >= 4; end, -- Return Function
		[1] = { [5] = 152178, [15] = 152179, [10] = 152180, [1] = 152181, [7] = 152182, [3] = 152183 }, -- Warrior:      Chest, Back, Hands, Head, Legs, Shoulder
		[2] = { [5] = 152148, [15] = 152149, [10] = 152150, [1] = 152151, [7] = 152152, [3] = 152153 }, -- Paladin:      Chest, Back, Hands, Head, Legs, Shoulder
		[3] = { [5] = 152130, [15] = 152131, [10] = 152132, [1] = 152133, [7] = 152134, [3] = 152135 }, -- Hunter:       Chest, Back, Hands, Head, Legs, Shoulder
		[4] = { [5] = 152160, [15] = 152161, [10] = 152162, [1] = 152163, [7] = 152164, [3] = 152165 }, -- Rogue:        Chest, Back, Hands, Head, Legs, Shoulder
		[5] = { [5] = 152158, [15] = 152154, [10] = 152155, [1] = 152156, [7] = 152157, [3] = 152159 }, -- Priest:       Chest, Back, Hands, Head, Legs, Shoulder
		[6] = { [5] = 152112, [15] = 152113, [10] = 152114, [1] = 152115, [7] = 152116, [3] = 152117 }, -- Death Knight: Chest, Back, Hands, Head, Legs, Shoulder
		[7] = { [5] = 152166, [15] = 152167, [10] = 152168, [1] = 152169, [7] = 152170, [3] = 152171 }, -- Shaman:       Chest, Back, Hands, Head, Legs, Shoulder
		[8] = { [5] = 152140, [15] = 152136, [10] = 152137, [1] = 152138, [7] = 152139, [3] = 152141 }, -- Mage:         Chest, Back, Hands, Head, Legs, Shoulder
		[9] = { [5] = 152176, [15] = 152172, [10] = 152173, [1] = 152174, [7] = 152175, [3] = 152177 }, -- Warlock:      Chest, Back, Hands, Head, Legs, Shoulder
		[10] = { [5] = 152142, [15] = 152143, [10] = 152144, [1] = 152145, [7] = 152146, [3] = 152147 }, -- Monk:         Chest, Back, Hands, Head, Legs, Shoulder
		[11] = { [5] = 152124, [15] = 152125, [10] = 152126, [1] = 152127, [7] = 152128, [3] = 152129 }, -- Druid:        Chest, Back, Hands, Head, Legs, Shoulder
		[12] = { [5] = 152118, [15] = 152119, [10] = 152120, [1] = 152121, [7] = 152122, [3] = 152123 }, -- Demon Hunter: Chest, Back, Hands, Head, Legs, Shoulder
	}
};

-- Get if the player is in a vhehicle that is not really a vehicle.
local VehicleWhitelist = {
	Spell = {
		--- Warlord of Draenor (WoD)
		-- Hellfire Citadel (T18 - 6.2 Patch)
		187819, -- Crush (Kormrok's Hands)
		181345, -- Foul Crush (Kormrok's Tank Hand)
		-- Blackrock Foundry (T17 - 6.0 Patch)
		157059, -- Rune of Grasping Earth (Kromog's Hand)
	},

	PetMount = {
		--- Warlord of Draenor (WoD)
		-- Garrison Stables Quest (6.0 Patch)
		87082, -- Silverperlt
		87078, -- Icehoof
		87081, -- Rocktusk
		87080, -- Riverwallow
		87079, -- Meadowstomper
		87076, -- Snarler
	},
};

addonTable.classPlayer = function(ID)
	local PowerType = addonTable.Enum.PowerType;
	local unitID = ID;

	local self = addonTable.classUnit(ID);

	local function calculateBloodtalonsModifier()
		-- Buffs affecting the damage multiplier
		local Bloodtalons = 145152;

		return self.Aura(Bloodtalons).Up() and 1.5 or 1;
	end

	local function calculateGeneralModifier()
		-- Buffs affecting the damage multiplier
		local SavageRoar = 52610;
		local TigerFury = 5217;

		local returnValue = 1;

		returnValue = self.Aura(TigerFury).Up() and returnValue * 1.15 or returnValue;
		returnValue = self.Aura(SavageRoar).Up() and returnValue * 1.25 or returnValue;

		return returnValue;
	end

	local function calculateStealthModifier()
		return (self.IsStealthed() and 2)
			or 1;
	end

	local function getComboPoints()
		local comboPoints = self.ComboPoints();

		if comboPoints == 0 then
			return self.ComboPoints.Previous();
		end

		return comboPoints;
	end

	local function multiplierAssassination(Spell)
		local gsub, strtrim = gsub, strtrim;
		local coreObjects = addon.Core.Objects;

		-- Buffs affecting the damage multiplier
		local Stealth = 1784;
		local Subterfuge = 115192;

		local spellName = coreObjects.IsSpell(Spell) and Spell.Name() or Spell or "";

		spellName = gsub(spellName, "[0-9.]", "");
		spellName = strtrim(spellName);

		if spellName == "Kidney Shot" then
			spellName = "Internal Bleeding";
		end

		local rogueMultiplier = 1;

		if self.Aura(Stealth).Up() or self.Aura(Subterfuge).Up() then
			local talentNightstalker = coreObjects.newSpell(14062);
			local talentSubterfuge =  coreObjects.newSpell(108208);

			if talentNightstalker.Enabled() then
				rogueMultiplier = rogueMultiplier * 2;
			end

			if talentSubterfuge.Enabled() then
				rogueMultiplier = rogueMultiplier * 2;
			end
		end

		local damageModifier = {
			["Crimson Tempest"] = rogueMultiplier,
			["Garrote"] = rogueMultiplier,
			["Internal Bleeding"] = rogueMultiplier,
			["Kidney Shot"] = rogueMultiplier,
			["Rupture"] = rogueMultiplier,
		};

		return damageModifier[spellName]
		    or 1;
	end

	local function multiplierFeral(Spell)
		local Objects = addon.Core.Objects;

		local spellName = Objects.IsSpell(Spell) and Spell.Name() or Spell or "";
		local modifierGeneral = calculateGeneralModifier();
		local modifierBloodtalons = calculateBloodtalonsModifier();
		local modifierStealth = calculateStealthModifier();

		local damageModifier = {
			["Moonfire"] = modifierGeneral, -- Moonfire debuff
			["Rake"] = modifierGeneral * modifierBloodtalons * modifierStealth, -- Rake debuff
			["Rip"] = modifierGeneral * modifierBloodtalons * getComboPoints(), -- Rip debuff, costs combo points
			["Thrash"] = modifierGeneral * modifierBloodtalons, -- Thrash debuff
		};

		return damageModifier[spellName]
		    or 1;
	end

	local function iterateAuraTable(auraTable, AnyCaster, returnDuration)
		-- Loop through the auras in the aura table
		for _, auraID in ipairs(auraTable) do
			if self.Aura(auraID, AnyCaster).Up() then
				return returnDuration and self.Aura(auraID, AnyCaster).Remains() or true;
			end
		end

		return returnDuration and 0 or false;
	end

	-- attack_power, spell_power or ranged_attack_power
	self.AttackPower = function()
		local Misc = addon.Core.Misc;

		return select(2, Misc.GetPower());
	end

	self.BloodlustRemains = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "BloodlustRemains") == nil then
			unitInfo.BloodlustRemains = iterateAuraTable(BloodlustBuffs, true, true);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "BloodlustRemains", 0);
	end

	self.CombatMount = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "CombatMount") == nil then
			unitInfo.CombatMount = iterateAuraTable(CombatMountBuffs);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "CombatMount", false);
	end

	self.CritChancePercent = function()
		local Misc = addon.Core.Misc;

		return Misc.GetCriticalStrike();
	end

	self.CritChanceRating = function()
		local Misc = addon.Core.Misc;

		return select(2, Misc.GetCriticalStrike());
	end

	self.DamagePredicted = function(seconds, minSamples)
		local TimeToDie = addonTable.TimeToDie;

		return TimeToDie.AllDamageInX(seconds, minSamples);
	end

	-- Get whether the player is in a dungeon area.
	self.DungeonInstance = function()
		return self.InstanceType() == "party";
	end

	self.GCD = {
		End = function()
			local General = addon.API.General;
			local Spell = addon.API.Spell;
			local Tables = addon.Core.Tables;

			local GCDInfo = Spell.GetSpellCooldown(addonTable.GLOBAL_COOLDOWN);

			return Tables.GetValue(GCDInfo, "End", General.GetTime());
		end,

		Duration = function()
			local Spell = addon.API.Spell;
			local Tables = addon.Core.Tables;

			local AdrenalineRush = 13750;
			local Voidform = 194249;

			local GCDInfo = Spell.GetSpellCooldown(addonTable.GLOBAL_COOLDOWN);
			local returnValue = Tables.GetValue(GCDInfo, "Duration", 0);

			if returnValue == 0 then
				returnValue = (self.PowerType() == addonTable.Enum.PowerType.Energy) and (self.Buff(AdrenalineRush).Up() and 0.8 or 1)
						   or max(1.5 / (1 + 0.01 * self.HastePercent()), self.Buff(Voidform).Up() and 0.67 or 0.75);
			end

			return returnValue;
		end,

		Max = function()
			return self.GCD.Duration();
		end,

		-- gcd.remains
		Remains = function()
			local General = addon.API.General;
			local Spell = addon.API.Spell;
			local Tables = addon.Core.Tables;

			local cooldownInfo = Spell.GetSpellCooldown(addonTable.GLOBAL_COOLDOWN);
			local endTime = Tables.GetValue(cooldownInfo, "End", 0);
			local modRate = Tables.GetValue(cooldownInfo, "ModRate", 1);

			if endTime > 0 then
				return max((endTime - General.GetTime()) / modRate, 0);
			end

			return 0;
		end,

		Start = function()
			local General = addon.API.General;
			local Spell = addon.API.Spell;
			local Tables = addon.Core.Tables;

			local GCDInfo = Spell.GetSpellCooldown(addonTable.GLOBAL_COOLDOWN);

			return Tables.GetValue(GCDInfo, "Start", General.GetTime());
		end,
	};
	setmetatable(self.GCD, { __call = function(table, ...) return self.GCD.Duration(...); end });

	self.HasBloodlust = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "HasBloodlust") == nil then
			unitInfo.HasBloodlust = iterateAuraTable(BloodlustBuffs, true);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "HasBloodlust", false);
	end

	self.HastePercent = function()
		local Misc = addon.Core.Misc;

		return Misc.GetHaste();
	end

	self.HasteRating = function()
		local Misc = addon.Core.Misc;

		return select(2, Misc.GetHaste());
	end

	self.HasTier = function(Tier)
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		-- Set Bonuses are disabled in Challenge Mode (Diff = 8) and in Proving Grounds (Map = 1148).
		if self.InstanceDifficulty() ~= addonTable.Enum.InstanceDifficulty.CHALLENGE_MODE_DUNGEON and self.InstanceMapID() ~= 1148 then
			local playerClass = Unit.UnitClass(unitID);
			local classID = Tables.GetValue(playerClass, "ID", -1);

			-- Check gear
			if Tables.GetValue(TierSets[Tier], classID) then
				local Count = 0;

				for Slot, ItemID in pairs(TierSets[Tier][classID]) do
					local Item = addonCache.Persistant.Equipment[Slot];

					if Item and Item == ItemID then
						Count = Count + 1;
					end
				end

				return TierSets[Tier].Count(Count);
			else
				return false;
			end
		end
	end

	-- Get whether the player is in a 5 man group.
	self.InGroup = function()
		return Blizzard.IsInGroup()
		   and Blizzard.GetNumGroupMembers() >= 2;
	end

	-- Get whether the player is in a 5 man group.
	self.InParty = function()
		return not Blizzard.IsInRaid()
		   and Blizzard.IsInGroup()
		   and Blizzard.GetNumGroupMembers() >= 2;
	end

	-- Get whether the player is in a raid.
	self.InRaid = function()
		return Blizzard.IsInRaid()
		   and Blizzard.GetNumGroupMembers() >= 2;
	end

	-- Get the player instance difficulty.
	self.InstanceDifficulty = function()
		local General = addon.API.General;
		local InstanceDifficulty = addonTable.Enum.InstanceDifficulty;
		local Tables = addon.Core.Tables;

		local instanceInfo = General.GetInstanceInfo();

		return Tables.GetValue(instanceInfo, "DifficultyID", InstanceDifficulty.NONE);
	end

	self.InstanceMapID = function()
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local instanceInfo = General.GetInstanceInfo();

		return Tables.GetValue(instanceInfo, "MapID", 0);
	end

	-- Get whether the player is in an instanced pvp area.
	self.InstancedPvP = function()
		return self.InstanceType() == "arena"
			or self.InstanceType() == "pvp";
	end

	-- Get the player instance type.
	self.InstanceType = function()
		local General = addon.API.General;
		local Tables = addon.Core.Tables;

		local instanceInfo = General.GetInstanceInfo();

		return Tables.GetValue(instanceInfo, "Type", "none");
	end

	-- Get if the player is in a valid vehicle.
	self.InVehicle = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "InVehicle") == nil then
			unitInfo.InVehicle = Blizzard.UnitInVehicle(unitID) and not self.IsInWhitelistedVehicle();

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "InVehicle", false);
	end

	self.IsInWhitelistedVehicle = function()
		local addonCache = addonTable.Cache;
		local Pet = addon.Units.Pet;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "InSpecialVehicle") == nil then
			unitInfo.InSpecialVehicle = (Pet.IsActive() and iterateAuraTable(VehicleWhitelist.PetMount, true))
								     or iterateAuraTable(VehicleWhitelist.Spell, true)
									 or false;

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "InSpecialVehicle", false);
	end

	-- Get if the player is mounted on a non-combat mount.
	self.IsMounted = function()
		return Blizzard.IsMounted()
		   and not self.CombatMount();
	end

	-- return whether the player currently has the sated debuff
	self.IsSated = function(Duration)
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsSated") == nil then
			unitInfo.IsSated = iterateAuraTable(BloodlustDebuffs, true);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsSated", false);
	end

	-- Get whether the player is solo or not.
	-- Will return true even if technically in a group, such as a solo player scenario.
	self.IsSolo = function()
		return Blizzard.IsInGroup()
		    or Blizzard.GetNumGroupMembers() <= 1;
	end

	-- return whether the player is currently stealthed
	self.IsStealthed = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsStealthed") == nil then
			unitInfo.IsStealthed = iterateAuraTable(StealthBuffs);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsStealthed", false);
	end

	-- Get if we are Tanking the Unit.
	self.IsTanking = function(Other)
		local addonCache = addonTable.Cache;
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		if Objects.IsUnit(Other) then
			local GUID = self.GUID();
			local OtherGUID = Other.GUID();

			if GUID and OtherGUID then
				local unitInfo = Unit.GetUnitInfo(unitID);

				if not Tables.IsEmpty(unitInfo) then
					unitInfo.IsTanking = unitInfo.IsTanking or {};

					if Tables.GetValue(unitInfo.IsTanking, OtherGUID) == nil then
						unitInfo.IsTanking[OtherGUID] = Blizzard.UnitThreatSituation(unitID, Other.ClassID) ~= nil
								                    and Blizzard.UnitThreatSituation(unitID, Other.ClassID) >= 2;

						addonCache.Temporary.UnitInfo[GUID] = unitInfo;
					end

					return Tables.GetValue(unitInfo.IsTanking, OtherGUID, false);
				end
			end
		end

		return false;
	end

	self.MagicDamagePredicted = function(seconds, minSamples)
		local IncomingDamage = addonTable.TimeToDie;

		return IncomingDamage.MagicDamageInX(seconds, minSamples);
	end

	-- mastery
	self.MasteryPercent = function()
		local Misc = addon.Core.Misc;

		return Misc.GetMastery();
	end

	self.MasteryRating = function()
		local Misc = addon.Core.Misc;

		return select(2, Misc.GetMastery());
	end

	self.PersistentMultiplier = function(Spell)
		local apiGeneral = addon.API.General;
		local coreTables = addon.Core.Tables;
		local enumSpecID = addonTable.Enum.SpecID;

		local specInfo = apiGeneral.GetSpecializationInfo();
		local specID = coreTables.GetValue(specInfo, "ID", 0);

		return (specID == enumSpecID.DRUID_FERAL and multiplierFeral(Spell))
		    or (specID == enumSpecID.ROGUE_ASSASSINATION and multiplierAssassination(Spell))
		    or 1;
	end

	self.PhysicalDamagePredicted = function(seconds, minSamples)
		local IncomingDamage = addonTable.TimeToDie;

		return IncomingDamage.PhysicalDamageInX(seconds, minSamples);
	end

	-- These are the various properties for the different power types;
	self.Mana = addonTable.propertiesPower(self, PowerType.Mana);
	self.Rage = addonTable.propertiesPower(self, PowerType.Rage);
	self.Focus = addonTable.propertiesPower(self, PowerType.Focus);
	self.Energy = addonTable.propertiesPower(self, PowerType.Energy);
	self.ComboPoints = addonTable.propertiesPower(self, PowerType.ComboPoints);
	self.RunicPower = addonTable.propertiesPower(self, PowerType.RunicPower);
	self.Runes = addonTable.propertiesPower(self, PowerType.Runes);
	self.SoulShards = addonTable.propertiesPower(self, PowerType.SoulShards);
	self.AstralPower = addonTable.propertiesPower(self, PowerType.AstralPower);
	self.HolyPower = addonTable.propertiesPower(self, PowerType.HolyPower);
	self.Maelstrom = addonTable.propertiesPower(self, PowerType.Maelstrom);
	self.Chi = addonTable.propertiesPower(self, PowerType.Chi);
	self.Insanity = addonTable.propertiesPower(self, PowerType.Insanity);
	self.ArcaneCharges = addonTable.propertiesPower(self, PowerType.ArcaneCharges);
	self.Fury = addonTable.propertiesPower(self, PowerType.Fury);
	self.Pain = addonTable.propertiesPower(self, PowerType.Pain);

	-- prev_gcd.x.foo
	self.PrevGCD = function(index, Spell)
		local Tables = addon.Core.Tables;
		local previousSpell = addonTable.PreviousSpell.GCD;

		return Tables.GetValue(previousSpell, index, 0) == Spell.SpellID()
			or false;
	end

	-- prev_off_gcd.x.foo
	self.PrevOffGCD = function(index, Spell)
		local Tables = addon.Core.Tables;
		local previousSpell = addonTable.PreviousSpell.OffGCD;

		return Tables.GetValue(previousSpell, index, 0) == Spell.SpellID()
			or false;
	end

	-- prev.x.foo
	self.PrevSpell = function(index, Spell)
		local Tables = addon.Core.Tables;
		local previousSpell = addonTable.PreviousSpell.All;

		return Tables.GetValue(previousSpell, index, 0) == Spell.SpellID()
			or false;
	end

	-- Get the player race.
	-- Dwarf, Draenei, Gnome, Human, NightElf, Worgen
	-- BloodElf, Goblin, Orc, Tauren, Troll, Scourge
	-- Pandaren
	self.Race = function(checkRace)
		local Tables = addon.Core.Tables;
		local apiUnit = addon.API.Unit;

		local raceInfo = apiUnit.UnitRace();
		local raceName = Tables.GetValue(raceInfo, "Name", "");

		return (not checkRace and raceName)
			or strupper(raceName) == strupper(checkRace);
	end

	-- Get whether the player is in a raid area.
	self.RaidInstance = function()
		return self.InstanceType() == "raid";
	end

	-- Get the remaining time on the sated debuff.
	self.SatedRemains = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "SatedRemains") == nil then
			unitInfo.SatedRemains = iterateAuraTable(BloodlustDebuffs, true, true);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "SatedRemains", 0);
	end

	-- Get the remaining time on the stealth buff.
	self.StealthedRemains = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "StealthRemains") == nil then
			unitInfo.StealthRemains = iterateAuraTable(StealthBuffs, nil, true);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "StealthRemains", 0);
	end

	-- Get the unit ID.
	self.UnitID = function()
		return unitID;
	end

	-- versatility
	self.VersatilityDamagePercent = function()
		return Blizzard.GetCombatRatingBonus(CR_VERSATILITY_DAMAGE_DONE) + Blizzard.GetVersatilityBonus(CR_VERSATILITY_DAMAGE_DONE);
	end

	return self;
end