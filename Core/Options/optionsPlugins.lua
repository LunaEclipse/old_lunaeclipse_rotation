-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local pluginOptions = {
	type = "group",
	name = "Plugins",
	get = addon.Options.Settings.GetPluginValue,
	set = addon.Options.Settings.SetPluginValue,
	childGroups = "tree",
	args = {
		loadPlugins = {
			type = "group",
			name = "Load Plugins",
			order = 10,
			args = {
				description = {
					type = "description",
					name = "These settings determine which optional components will be loaded into memory.\n\nImportant Note: Changes to these settings will not take effect until AFTER a UI reload!",
					width = "full",
					order = 10,
				},
			},
		},
	},
};

addon.Options.pluginOptions = {
	type = "group",
	name = "Plugins",
	get = addon.Options.Settings.GetPluginValue,
	set = addon.Options.Settings.SetPluginValue,
	childGroups = "tree",
	args = {},
};

addon.Options.Plugins = {
	AddPluginTable = function (optionsTable, pluginName, pluginTable)
		local Tables = addon.Core.Tables;
		local optionCount = Tables.Count(optionsTable.args) + 1;

		optionsTable.args[pluginName] = Tables.Duplicate(pluginTable);
		optionsTable.args[pluginName].order = optionCount * 10;
	end,

	CovertPluginName = function(pluginName)
		local format, gsub, strlower, strupper = format, gsub, strlower, strupper;
		local returnValue = gsub(pluginName, "(%a)([%w_']*)", function(a, b) return format("%s%s", strupper(a), strlower(b)); end);  -- Title Case

		returnValue = gsub(returnValue, "[^%w_]", "") -- Remove puctuation and white space

		return returnValue;
	end,

	CreateCheckbox = function(optionsTable, key, pluginName, pluginDescription)
		local format = format;
		local Tables = addon.Core.Tables;
		local optionCount = Tables.Count(optionsTable.args.loadPlugins) + 1;

		if not pluginDescription then
			pluginDescription = format("Check this option to enable %s, if this option is unchecked %s will not be loaded!", pluginName, pluginName);
		else
			pluginDescription = format("%s\n\nCheck this option to enable %s, if this option is unchecked %s will not be loaded!", pluginDescription, pluginName, pluginName);
		end

		optionsTable.args.loadPlugins.args[key] = {
			type = "toggle",
			name = pluginName,
			desc = pluginDescription,
			width = "full",
			order = optionCount * 10,
		};
	end,

	CreatePluginOptions = function()
		local Tables = addon.Core.Tables;

		return Tables.Duplicate(pluginOptions);
	end,

	FinalizePluginOptions = function(optionsTable)
		local Tables = addon.Core.Tables;

		addon.Options.pluginOptions.args = Tables.Duplicate(optionsTable.args);
		addon.System.Callbacks.Callback("optionsChanged", "LERH_PluginSettings");
	end
};

local moduleEvents = {
	playerLogin = function()
		local ipairs, sort = ipairs, sort;

		local Options = addon.Options.Plugins;
		local Tables = addon.Core.Tables;

		local profile = Tables.GetValue(addonTable.characterSettings, "Plugins", {});
		local registeredPlugins = addon.System.Plugins.GetRegisteredPlugins();

		local pluginTable = registeredPlugins.Plugins or {};
		local descriptionTable = registeredPlugins.Descriptions or {};
		local optionsTable = registeredPlugins.Options or {};

		local createOptions = Options.CreatePluginOptions();
		local key;

		if not Tables.IsEmpty(pluginTable) then
			sort(pluginTable);

			for _, pluginName in ipairs(pluginTable) do
				key = Options.CovertPluginName(pluginName);

				-- If plugin has never been registered before set it to enabled by default.
				if not profile[key] then
					profile[key] = 0;

					-- Put the settings into the database
					addonTable.characterSettings.Plugins = profile;
				end

				Options.CreateCheckbox(createOptions, key, pluginName, descriptionTable[key]);

				if addon.System.Plugins.IsActive(pluginName) and optionsTable[key] then
					Options.AddPluginTable(createOptions, key, optionsTable[key]);
				end
			end
		end

		Options.FinalizePluginOptions(createOptions);
	end,
};

-- Register this as a module of the addon.
addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);