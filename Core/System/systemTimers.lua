-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local activeTimers = {};
local singleTimers = {};

local registeredTimers ={};

local function registerTimers(callbacks)
	local pairs = pairs;

	if callbacks then
		for key, value in pairs(callbacks) do
			registeredTimers[value] = key;
		end
	end
end

local function singleTimerCallback(timerEvent, ...)
	local type = type;

	if timerEvent and type(timerEvent) == "function" then
		singleTimers[timerEvent] = nil;

		timerEvent(...);
	end
end

addon.System.Timers = {
	RegisterTimers = function(timerCallbacks)
		registerTimers(timerCallbacks);
	end,

	TimerAdd = function(timerEvent, interval, repeatable, ...)
		if not addon.System.Timers.TimerExists(timerEvent) then
			if repeatable == nil or repeatable == true then
				activeTimers[timerEvent] = addon:ScheduleRepeatingTimer(timerEvent, interval, ...);
			else
				singleTimers[timerEvent] = addon:ScheduleTimer(singleTimerCallback, interval, timerEvent, ...);
			end
		end
	end,

	TimerExists = function(timerEvent)
		local Tables = addon.Core.Tables;

		return Tables.GetValue(activeTimers, timerEvent) ~= nil or Tables.GetValue(singleTimers, timerEvent) ~= nil;
	end,

	TimerModify = function(timerEvent, interval, ...)
		local Tables = addon.Core.Tables;
		local currentTimer = Tables.GetValue(activeTimers, timerEvent) or Tables.GetValue(singleTimers, timerEvent);

		if currentTimer then
			addon:CancelTimer(currentTimer)
			activeTimers[timerEvent] = addon:ScheduleRepeatingTimer(timerEvent, interval, ...);
		end
	end,

	TimerRemove = function(timerEvent)
		local Tables = addon.Core.Tables;
		local currentTimer = Tables.GetValue(activeTimers, timerEvent) or Tables.GetValue(singleTimers, timerEvent);

		if currentTimer then
			addon:CancelTimer(currentTimer)
			activeTimers[timerEvent] = nil;
		end
	end,
};

local moduleEvents = {
	playerLogin = function()
		local pairs = pairs;
		local Tables = addon.Core.Tables;

		if not Tables.IsEmpty(registeredTimers) then
			for timerEvent, interval in pairs(registeredTimers) do
				addon.System.Timers.TimerAdd(timerEvent, interval);
			end
		end
	end,
};

-- Register this as a module of the addon.
addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);