-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard, systemEvents, systemTimers = addon.API.Blizzard, addon.System.Events, addon.System.Timers;
local gsub, ipairs, pairs, select, strtrim, strupper, type = gsub, ipairs, pairs, select, strtrim, strupper, type;

-- List of CLEU events for applying auras
local CLEU_SPELL_CAST_START = {
	SPELL_CAST_START = true,
	SPELL_CAST_SUCCESS = true,
};

local CLEU_SPELL_CAST_FAIL = {
	SPELL_CAST_FAILED = true,
};

local CLEU_SPELL_HIT = {
	SPELL_DAMAGE = true,
	SPELL_HEAL = true,
	SPELL_MISSED = true,
};

local AuraCache, PredictionCache, persistentMultipliers, prevGUID, TotemTimes = {}, {}, {}, {}, {};
local Auras, Events, Prediction = {}, {}, {};

local function getInfo(auraData)
	if type(auraData) == "table" then
		return auraData[1], auraData[2];
	elseif type(auraData) == "number" then
		return auraData, nil;
	end
end

local function calculatePandemic(spellID)
	return addon.Core.Tooltip.GetSpellBaseDuration(spellID) * 0.3;
end

Auras = {
	AddBuff = function(unitID, index)
		local GUID = Blizzard.UnitGUID(unitID);

		if GUID and AuraCache[GUID] then
			local auraName, auraIcon, auraStacks, auraType, auraDuration, expirationTime, auraCaster, isStealable, _, spellID, canApplyAura, isBossAura, _, _, timeMod = Blizzard.UnitBuff(unitID, index);

			if spellID then
				AuraCache[GUID].Buff[spellID] = {
					ID = spellID,
					Name = auraName,
					IconID = auraIcon,
					Stacks = auraStacks,
					Type = auraType,
					Start = (expirationTime or 0) - (auraDuration or 0),
					Duration = auraDuration or 0,
					End = expirationTime or 0,
					PandemicTime = calculatePandemic(spellID),
					Caster = auraCaster,
					IsStealable = isStealable,
					CanApplyAura = canApplyAura,
					IsBossAura = isBossAura,
					TimeMod = timeMod,
				};
				AuraCache[GUID].Buff.NAMES[auraName] = AuraCache[GUID].Buff.NAMES[auraName] or spellID;

				if PredictionCache[GUID] then
					PredictionCache[GUID][spellID] = nil;
				end

				return true;
			end
		end

		return false;
	end,

	AddDebuff = function(unitID, index)
		local GUID = Blizzard.UnitGUID(unitID);

		if GUID and AuraCache[GUID] then
			local auraName, auraIcon, auraStacks, auraType, auraDuration, expirationTime, auraCaster, isStealable, _, spellID, canApplyAura, isBossAura, _, _, timeMod = Blizzard.UnitDebuff(unitID, index);

			if spellID then
				AuraCache[GUID].Debuff[spellID] = {
					ID = spellID,
					Name = auraName,
					IconID = auraIcon,
					Stacks = auraStacks,
					Type = auraType,
					Start = (expirationTime or 0) - (auraDuration or 0),
					Duration = auraDuration or 0,
					End = expirationTime or 0,
					PandemicTime = calculatePandemic(spellID),
					Caster = auraCaster,
					IsStealable = isStealable,
					CanApplyAura = canApplyAura,
					IsBossAura = isBossAura,
					TimeMod = timeMod,
				};

				AuraCache[GUID].Debuff.NAMES[auraName] = AuraCache[GUID].Debuff.NAMES[auraName] or spellID;

				if addonTable.PersistentMultiplierData[auraName] then
					AuraCache[GUID].Debuff[spellID].PersistentMultiplier = persistentMultipliers[GUID][auraName];
				end

				if PredictionCache[GUID] then
					PredictionCache[GUID][spellID] = nil;
				end

				return true;
			end
		end

		return false;
	end,

	GetAura = function(unitID, spellID, anyCaster)
		local returnValue;

		if unitID and spellID then
			returnValue = Auras.GetBuff(unitID, spellID, anyCaster) or Auras.GetDebuff(unitID, spellID, anyCaster);
		end

		return returnValue;
	end,

	GetBuff = function(unitID, spellID, anyCaster)
		local returnValue;
		local GUID = Blizzard.UnitGUID(unitID);

		if GUID then
			if not AuraCache[GUID] then
				Auras.SetupAuras(GUID);
				Auras.ScanAuras(unitID, GUID);
			end

			if spellID then
				returnValue = AuraCache[GUID].Buff[spellID];

				if not returnValue then
					local spellName = Blizzard.GetSpellInfo(spellID) or "";
					local recordedID = AuraCache[GUID].Buff.NAMES[spellName] or 0;

					returnValue = AuraCache[GUID].Buff[recordedID];

					if returnValue and not anyCaster and returnValue.Caster ~= "player" then
						returnValue = nil;
					end
				end
			end
		end

		return returnValue;
	end,

	GetDebuff = function(unitID, spellID, anyCaster)
		local returnValue;
		local GUID = Blizzard.UnitGUID(unitID);

		if GUID then
			if not AuraCache[GUID] then
				Auras.SetupAuras(GUID);
				Auras.ScanAuras(unitID, GUID);
			end

			if spellID then
				returnValue = AuraCache[GUID].Debuff[spellID];

				if not returnValue then
					local spellName = Blizzard.GetSpellInfo(spellID) or "";
					local recordedID = AuraCache[GUID].Debuff.NAMES[spellName] or 0;

					returnValue = AuraCache[GUID].Debuff[recordedID];

					if returnValue and not anyCaster and returnValue.Caster ~= "player" then
						returnValue = nil;
					end
				end
			end
		end

		return returnValue;
	end,

	ScanAuras = function(unitID, GUID)
		if unitID and GUID and AuraCache[GUID] then
			for counter = 1, 40 do
				if not Auras.AddBuff(unitID, counter) then
					break;
				end
			end

			for counter = 1, 40 do
				if not Auras.AddDebuff(unitID, counter) then
					break;
				end
			end
		end
	end,

	SetupAuras = function(GUID)
		if GUID and not AuraCache[GUID] then
			AuraCache[GUID] = {
				Buff = {
					NAMES = {},
				},
				Debuff = {
					NAMES = {},
				},
			};
		end
	end,
};

Prediction = {
	ConsumeAura = function(GUID, spellID, value)
		local Tables = addon.Core.Tables;
		local consumeAuraData = Tables.GetValue(addonTable.ConsumeAuraData, spellID);

		if consumeAuraData then
			local spellIDs, requiredAura = getInfo(consumeAuraData);

			if spellIDs and (requiredAura == nil or (requiredAura and Prediction.GetAura(GUID, requiredAura))) then
				local aura, spellName;

				if not Tables.IsEmpty(spellIDs) then
					for _, ID in ipairs(spellIDs) do
						spellName = Blizzard.GetSpellInfo(ID);
						aura = Prediction.GetAura(GUID, ID);

						if aura then
							aura.Consumed = value;
							break;
						end
					end
				else
					spellName = Blizzard.GetSpellInfo(spellIDs);
					aura = Prediction.GetAura(GUID, spellIDs);

					if aura then
						aura.Consumed = value;
					end
				end
			end
		end
	end,

	CreateAura = function(GUID, spellID)
		local Tables = addon.Core.Tables;

		local unitAuras = PredictionCache[GUID] or {};
		local createAuraData = Tables.GetValue(addonTable.CreateAuraData, spellID);

		if createAuraData then
			local spellIDs, requiredAura = getInfo(createAuraData);

			if spellIDs and (requiredAura == nil or (requiredAura and Prediction.GetAura(GUID, requiredAura))) then
				local aura, spellName;

				if not Tables.IsEmpty(spellIDs) then
					for _, ID in ipairs(spellIDs) do
						spellName = Blizzard.GetSpellInfo(ID);
						aura = Prediction.GetAura(GUID, ID);

						if not aura then
							unitAuras[ID] = spellID;
							break;
						end
					end
				else
					spellName = Blizzard.GetSpellInfo(spellIDs);
					aura = Prediction.GetAura(GUID, spellIDs);

					if not aura then
						unitAuras[spellIDs] = spellID;
					end
				end

				PredictionCache[GUID] = unitAuras;
			end
		end
	end,

	GetAura = function(GUID, spellID)
		local returnValue;

		if GUID and spellID then
			returnValue = Prediction.GetBuff(GUID, spellID) or Prediction.GetDebuff(GUID, spellID);
		end

		return returnValue;
	end,

	GetBuff = function(GUID, spellID)
		local returnValue;

		if GUID and AuraCache[GUID] and spellID then
			returnValue = AuraCache[GUID].Buff[spellID];

			if not returnValue then
				local spellName = Blizzard.GetSpellInfo(spellID) or "";
				local recordedID = AuraCache[GUID].Buff.NAMES[spellName] or 0;

				returnValue = AuraCache[GUID].Buff[recordedID];

				if returnValue and returnValue.Caster ~= "player" then
					returnValue = nil;
				end
			end
		end

		return returnValue;
	end,

	GetDebuff = function(GUID, spellID)
		local returnValue;

		if GUID and AuraCache[GUID] and spellID then
			returnValue = AuraCache[GUID].Debuff[spellID];

			if not returnValue then
				local spellName = Blizzard.GetSpellInfo(spellID) or "";
				local recordedID = AuraCache[GUID].Debuff.NAMES[spellName] or 0;

				returnValue = AuraCache[GUID].Debuff[recordedID];

				if returnValue and returnValue.Caster ~= "player" then
					returnValue = nil;
				end
			end
		end

		return returnValue;
	end,

	ProcessTotems = function(spellID)
		local coreObjects = addon.Core.Objects;
		local coreTables = addon.Core.Tables;

		local auraTable = coreTables.GetValue(addonTable.TotemData, spellID);
		local currentTime = Blizzard.GetTime();

		if auraTable then
			local currentSpell = coreObjects.newSpell(spellID);
			local endTime = currentTime + currentSpell.BaseDuration();

			if not coreTables.IsEmpty(auraTable) then
				for _, auraID in ipairs(auraTable) do
					TotemTimes[auraID] = endTime;
				end
			else
				TotemTimes[auraTable] = endTime;
			end
		end
	end,

	RemoveAura = function(GUID, spellID)
		local Tables = addon.Core.Tables;

		local unitAuras = PredictionCache[GUID] or {};
		local createAuraData = Tables.GetValue(addonTable.CreateAuraData, spellID);

		if createAuraData then
			local spellIDs, requiredAura = getInfo(createAuraData);

			if spellIDs and (requiredAura == nil or (requiredAura and Prediction.GetAura(GUID, requiredAura))) then
				local aura, spellName;

				if not Tables.IsEmpty(spellIDs) then
					for _, ID in ipairs(spellIDs) do
						spellName = Blizzard.GetSpellInfo(ID);
						aura = unitAuras[ID];

						if aura and aura == spellID then
							unitAuras[ID] = nil;
							break;
						end
					end
				else
					spellName = Blizzard.GetSpellInfo(spellIDs);
					aura = unitAuras[spellIDs];

					if aura and aura == spellID then
						unitAuras[spellIDs] = nil;
					end
				end

				PredictionCache[GUID] = unitAuras;
			end
		end
	end,
};

Events = {
	combatLog = function(...)
		local _, _, combatEvent, _, sourceGUID, _, _, _, destGUID, _, _, _, spellID = ...;

		if sourceGUID == Blizzard.UnitGUID("player") then
			if CLEU_SPELL_CAST_START[combatEvent] then
				-- Process totem duration time to be used as remaining time on buffs when the buff is active.
				Prediction.ProcessTotems(spellID);
				-- Spell cast started, mark any spells consumed, and add any fake temp auras.
				Prediction.ConsumeAura(sourceGUID, spellID, true);
				Prediction.CreateAura(sourceGUID, spellID);
			elseif CLEU_SPELL_CAST_FAIL[combatEvent] or CLEU_SPELL_HIT[combatEvent] then
				-- Spell cast either failed or hit meaning the auras should be up to date so remove temp changes.
				Prediction.ConsumeAura(sourceGUID, spellID);
				Prediction.RemoveAura(sourceGUID, spellID);
			end
		end
	end,

	timerEvent = function()
		local coreTables = addon.Core.Tables;
		local currentTime = Blizzard.GetTime();

		if not coreTables.IsEmpty(TotemTimes) then
			for spellID, endTime in pairs(TotemTimes) do
				if endTime < currentTime then
					TotemTimes[spellID] = nil;
				end
			end
		end
	end,

	unitAura = function(...)
		local _, unitID = ...;
		local GUID = Blizzard.UnitGUID(unitID);

		AuraCache[GUID] = nil;
	end,
};

addonTable.Auras = {
	GetPersistentMultiplier = function(unitID, spellID)
		local apiSpell = addon.API.Spell;
		local coreTables = addon.Core.Tables;

		if unitID and spellID then
			local GUID = Blizzard.UnitGUID(unitID);
			local spellInfo = apiSpell.GetSpellInfo(spellID);
			local spellName = coreTables.GetValue(spellInfo, "Name", "");

			spellName = gsub(spellName, "[0-9.]", "");
			spellName = strtrim(spellName);

			return coreTables.GetValue(coreTables.GetValue(persistentMultipliers, GUID or "None", {}), spellName, 1);
		end

		return 1;
	end,

	SetPersistentMultiplier = function(GUID, spellID)
		local apiSpell = addon.API.Spell;
		local coreTables = addon.Core.Tables;
		local Player = addon.Units.Player;

		if GUID and spellID then
			local spellInfo = apiSpell.GetSpellInfo(spellID);
			local spellName = coreTables.GetValue(spellInfo, "Name", "");

			spellName = gsub(spellName, "[0-9.]", "");
			spellName = strtrim(spellName);

			if spellName ~= "" and addonTable.PersistentMultiplierData[spellName] then
				persistentMultipliers[GUID] = persistentMultipliers[GUID] or {};
				persistentMultipliers[GUID][spellName] = Player.PersistentMultiplier(spellName);
			end
		end
	end,

	UnitAura = function(unitID, spellID, anyCaster)
		if unitID and spellID then
			local GUID = Blizzard.UnitGUID(unitID);
			local returnValue = Auras.GetAura(unitID, spellID, anyCaster);
			local totemEnd = TotemTimes[spellID];

			if returnValue and returnValue.End == 0 and totemEnd then
				returnValue.End = totemEnd;
			end

			if not returnValue and PredictionCache[GUID] then
				returnValue = PredictionCache[GUID][spellID];
			end

			return returnValue;
		end

		return nil;
	end,

	UnitBuff = function(unitID, spellID, anyCaster)
		if unitID and spellID then
			local GUID = Blizzard.UnitGUID(unitID);
			local returnValue = Auras.GetBuff(GUID, spellID, anyCaster);
			local totemEnd = TotemTimes[spellID];

			if returnValue and returnValue.End == 0 and totemEnd then
				returnValue.End = totemEnd;
			end

			if not returnValue and PredictionCache[GUID] then
				returnValue = PredictionCache[GUID][spellID];
			end

			return returnValue;
		end

		return nil;
	end,

	UnitDebuff = function(unitID, spellID, anyCaster)
		if unitID and spellID then
			local GUID = Blizzard.UnitGUID(unitID);
			local returnValue = Auras.GetDebuff(GUID, spellID, anyCaster);
			local totemEnd = TotemTimes[spellID];

			if returnValue and returnValue.End == 0 and totemEnd then
				returnValue.End = totemEnd;
			end

			if not returnValue and PredictionCache[GUID] then
				returnValue = PredictionCache[GUID][spellID];
			end

			return returnValue;
		end

		return nil;
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = Events.combatLog,
	UNIT_AURA = Events.unitAura,
};

local tableTimers = {
	[1] = Events.timerEvent,
};

systemEvents.RegisterEvents(tableEvents, true);
systemTimers.RegisterTimers(tableTimers);