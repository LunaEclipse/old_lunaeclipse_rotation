local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
-- Objects

local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "lunaeclipse_druid_balance";

-- AOE Rotation
local function AOE(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.aoe+=/force_of_nature,if=(buff.celestial_alignment.up|buff.incarnation.up)|(cooldown.celestial_alignment.remains>30|cooldown.incarnation.remains>30)
		ForceOfNature = function()
			return (Player.Buff(Aura.CelestialAlignment).Up() or Player.Buff(Aura.IncarnationChosenOfElune).Up())
			    or (Spell.CelestialAlignment.Cooldown.Remains() > 30 or Talent.IncarnationChosenOfElune.Cooldown.Remains() > 30);
		end,

		-- actions.aoe+=/full_moon,if=astral_power.deficit>42
		FullMoon = function()
			return Player.AstralPower.Deficit() > 42;
		end,

		-- actions.aoe=fury_of_elune,if=(buff.celestial_alignment.up|buff.incarnation.up)|(cooldown.celestial_alignment.remains>30|cooldown.incarnation.remains>30)
		FuryOfElune = function()
			return (Player.Buff(Aura.CelestialAlignment).Up() or Player.Buff(Aura.IncarnationChosenOfElune).Up())
			    or (Spell.CelestialAlignment.Cooldown.Remains() > 30 or Talent.IncarnationChosenOfElune.Cooldown.Remains() > 30);
		end,

		-- actions.aoe+=/half_moon,if=astral_power.deficit>22
		HalfMoon = function()
			return Player.AstralPower.Deficit() > 22;
		end,

		-- actions.aoe+=/lunar_strike,if=(buff.lunar_empowerment.stack=3|buff.solar_empowerment.stack=2&buff.lunar_empowerment.stack=2&astral_power>=40)&astral_power.deficit>14
		LunarStrike = function()
			return (Player.Buff(Aura.LunarEmpowerment).Stack() == 3 or Player.Buff(Aura.SolarEmpowerment).Stack() == 2 and Player.Buff(Aura.LunarEmpowerment).Stack() == 2 and Player.AstralPower() >= 40)
			   and Player.AstralPower.Deficit() > 14;
		end,

		-- actions.aoe+=/moonfire,target_if=refreshable,if=astral_power.deficit>7&target.time_to_die>4
		Moonfire = function(numEnemies, Target)
			return Player.AstralPower.Deficit() > 7
			   and Target.TimeToDie() > 4
			   and Target.Debuff(Aura.Moonfire).Refreshable();
		end,

		-- actions.aoe+=/new_moon,if=astral_power.deficit>12
		NewMoon = function()
			return Player.AstralPower.Deficit() > 12;
		end,

		SolarWrath = {
			-- actions.aoe+=/solar_wrath,if=buff.solar_empowerment.stack=3&astral_power.deficit>10
			SolarEmpowerment = function()
				return Player.Buff(Aura.SolarEmpowerment).Stack() == 3
				   and Player.AstralPower.Deficit() > 10;
			end,

			-- actions.aoe+=/solar_wrath,if=(buff.solar_empowerment.up&!buff.warrior_of_elune.up|buff.solar_empowerment.stack>=3)&buff.lunar_empowerment.stack<3
			Use = function()
				return (Player.Buff(Aura.SolarEmpowerment).Up() and not Player.Buff(Aura.WarriorOfElune).Up() or Player.Buff(Aura.SolarEmpowerment).Stack() >= 3)
				   and Player.Buff(Aura.LunarEmpowerment).Stack() < 3;
			end,
		},

		-- actions.aoe+=/starfall,if=!buff.starlord.up|buff.starlord.remains>=4
		Starfall = function()
			return not Player.Buff(Aura.Starlord).Up()
			    or Player.Buff(Aura.Starlord).Remains() >= 4;
		end,

		-- Legendary items become stat sticks at 115, so we will just skip the legendary buff check.
		-- actions.aoe+=/starsurge,if=buff.oneths_intuition.react|target.time_to_die<=4
		Starsurge = function()
			return Target.TimeToDie() <= 4;
		end,

		-- actions.aoe+=/stellar_flare,target_if=refreshable,if=target.time_to_die>10
		StellarFlare = function(numEnemies, Target)
			return Target.TimeToDie() > 10
			   and Target.Debuff(Aura.StellarFlare).Refreshable();
		end,

		-- actions.aoe+=/sunfire,target_if=refreshable,if=astral_power.deficit>7&target.time_to_die>4
		Sunfire = function(numEnemies, Target)
			return Player.AstralPower.Deficit() > 7
			   and Target.TimeToDie() > 4
			   and Target.Debuff(Aura.Sunfire).Refreshable();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.FuryOfElune, self.Requirements.FuryOfElune);
		action.EvaluateAction(Talent.ForceOfNature, self.Requirements.ForceOfNature);
		action.EvaluateCycleAction(Spell.Sunfire, self.Requirements.Sunfire);
		action.EvaluateCycleAction(Spell.Moonfire, self.Requirements.Moonfire);
		action.EvaluateCycleAction(Talent.StellarFlare, self.Requirements.StellarFlare);
		action.EvaluateAction(Spell.LunarStrike, self.Requirements.LunarStrike);
		action.EvaluateAction(Spell.SolarWrath, self.Requirements.SolarWrath.SolarEmpowerment);
		action.EvaluateAction(Spell.Starsurge, self.Requirements.Starsurge);
		action.EvaluateAction(Spell.Starfall, self.Requirements.Starfall);
		action.EvaluateAction(Talent.NewMoon, self.Requirements.NewMoon);
		action.EvaluateAction(Talent.HalfMoon, self.Requirements.HalfMoon);
		action.EvaluateAction(Talent.FullMoon, self.Requirements.FullMoon);
		action.EvaluateAction(Spell.SolarWrath, self.Requirements.SolarWrath.Use);
		-- actions.aoe+=/lunar_strike
		action.EvaluateAction(Spell.LunarStrike, true);
		-- actions.aoe+=/moonfire
		action.EvaluateAction(Spell.Moonfire, true);
	end

	-- actions+=/call_action_list,name=aoe,if=spell_targets.starfall>=3
	function self.Use()
		return Enemies.GetEnemies(40) >= 3;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationAOE = AOE("AOE");

-- SingleTarget Rotation
local function SingleTarget(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.st+=/force_of_nature,if=(buff.celestial_alignment.up|buff.incarnation.up)|(cooldown.celestial_alignment.remains>30|cooldown.incarnation.remains>30)
		ForceOfNature = function()
			return (Player.Buff(Aura.CelestialAlignment).Up() or Player.Buff(Aura.IncarnationChosenOfElune).Up())
			    or (Spell.CelestialAlignment.Cooldown.Remains() > 30 or Talent.IncarnationChosenOfElune.Cooldown.Remains() > 30);
		end,

		-- actions.st+=/full_moon,if=astral_power.deficit>40
		FullMoon = function()
			return Player.AstralPower.Deficit() > 40;
		end,

		-- actions.st=fury_of_elune,if=(buff.celestial_alignment.up|buff.incarnation.up)|(cooldown.celestial_alignment.remains>30|cooldown.incarnation.remains>30)
		FuryOfElune = function()
			return (Player.Buff(Aura.CelestialAlignment).Up() or Player.Buff(Aura.IncarnationChosenOfElune).Up())
			    or (Spell.CelestialAlignment.Cooldown.Remains() > 30 or Talent.IncarnationChosenOfElune.Cooldown.Remains() > 30);
		end,

		-- actions.st+=/half_moon,if=astral_power.deficit>20
		HalfMoon = function()
			return Player.AstralPower.Deficit() > 20;
		end,

		LunarStrike = {
			-- actions.st+=/lunar_strike,if=buff.lunar_empowerment.stack=3&astral_power.deficit>14
			LunarEmpowerment = function()
				return Player.Buff(Aura.LunarEmpowerment).Stack() == 3
				   and Player.AstralPower.Deficit() > 14;
			end,

			-- actions.st+=/lunar_strike,if=(buff.warrior_of_elune.up|!buff.solar_empowerment.up)&buff.lunar_empowerment.up
			Use = function()
				return (Player.Buff(Aura.WarriorOfElune).Up() or not Player.Buff(Aura.SolarEmpowerment).Up())
				   and Player.Buff(Aura.LunarEmpowerment).Up();
			end,
		},

		-- actions.st+=/moonfire,target_if=refreshable,if=target.time_to_die>8
		Moonfire = function(numEnemies, Target)
			return Target.TimeToDie() > 8
			   and Target.Debuff(Aura.Moonfire).Refreshable();
		end,

		-- actions.st+=/new_moon,if=astral_power.deficit>10
		NewMoon = function()
			return Player.AstralPower.Deficit() > 10;
		end,

		-- actions.st+=/solar_wrath,if=(buff.solar_empowerment.stack=3|buff.solar_empowerment.stack=2&buff.lunar_empowerment.stack=2&astral_power>=40)&astral_power.deficit>10
		SolarWrath = function()
			return (Player.Buff(Aura.SolarEmpowerment).Stack() == 3 or Player.Buff(Aura.SolarEmpowerment).Stack() == 2 and Player.Buff(Aura.LunarEmpowerment).Stack() == 2 and Player.AstralPower() >= 40)
			   and Player.AstralPower.Deficit() > 10;
		end,

		-- actions.st+=/starsurge,if=!buff.starlord.up|buff.starlord.remains>=4|(gcd.max*(astral_power%40))>target.time_to_die
		Starsurge = function()
			return not Player.Buff(Aura.Starlord).Up()
				or Player.Buff(Aura.Starlord).Remains() >= 4
				or (Player.GCD() * (Player.AstralPower() / 40)) > Target.TimeToDie();
		end,

		-- actions.st+=/stellar_flare,target_if=refreshable,if=target.time_to_die>10
		StellarFlare = function(numEnemies, Target)
			return Target.TimeToDie() > 10
			   and Target.Debuff(Aura.StellarFlare).Refreshable();
		end,

		-- actions.st+=/sunfire,target_if=refreshable,if=target.time_to_die>8
		Sunfire = function(numEnemies, Target)
			return Target.TimeToDie() > 8
			   and Target.Debuff(Aura.Sunfire).Refreshable();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.FuryOfElune, self.Requirements.FuryOfElune);
		action.EvaluateAction(Talent.ForceOfNature, self.Requirements.ForceOfNature);
		action.EvaluateCycleAction(Spell.Moonfire, self.Requirements.Moonfire);
		action.EvaluateCycleAction(Spell.Sunfire, self.Requirements.Sunfire);
		action.EvaluateCycleAction(Talent.StellarFlare, self.Requirements.StellarFlare);
		action.EvaluateAction(Spell.SolarWrath, self.Requirements.SolarWrath);
		action.EvaluateAction(Spell.LunarStrike, self.Requirements.LunarStrike.LunarEmpowerment);
		action.EvaluateAction(Spell.Starsurge, self.Requirements.Starsurge);
		action.EvaluateAction(Spell.LunarStrike, self.Requirements.LunarStrike.Use);
		action.EvaluateAction(Talent.NewMoon, self.Requirements.NewMoon);
		action.EvaluateAction(Talent.HalfMoon, self.Requirements.HalfMoon);
		action.EvaluateAction(Talent.FullMoon, self.Requirements.FullMoon);
		-- actions.st+=/solar_wrath
		action.EvaluateAction(Spell.SolarWrath, true);
		-- actions.st+=/moonfire
		action.EvaluateAction(Spell.Moonfire, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSingleTarget = SingleTarget("SingleTarget");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Tettles and SimCraft",
		GuideLink = "http://www.wowhead.com/balance-druid-guide",
		WoWVersion = 80001,
	};

	self.Defensives = {
		Barkskin = function()
			return Player.DamagePredicted(6) >= 30;
		end,

		Renewal = function()
			return Player.Health.Percent() < 70
			   and Player.DamagePredicted(5) >= 25;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/berserking,if=buff.celestial_alignment.up|buff.incarnation.up
		Berserking = function()
			return Player.Buff(Aura.CelestialAlignment).Up()
			    or Player.Buff(Aura.IncarnationChosenOfElune).Up();
		end,

		-- actions+=/blood_fury,if=buff.celestial_alignment.up|buff.incarnation.up
		BloodFury = function()
			return Player.Buff(Aura.CelestialAlignment).Up()
			    or Player.Buff(Aura.IncarnationChosenOfElune).Up();
		end,

		-- actions+=/celestial_alignment,if=astral_power>=40
		CelestialAlignment = function()
			return Player.AstralPower() >= 40;
		end,

		-- actions+=/incarnation,if=astral_power>=40
		IncarnationChosenOfElune = function()
			return Player.AstralPower() >= 40;
		end,

		-- SimCraft doesn't mention it, but make sure we are not already in Moonkin Form before suggesting it.
		-- actions.precombat+=/moonkin_form
		MoonkinForm = function()
			return not Player.Buff(Aura.MoonkinForm).Up();
		end,

		-- actions=potion,name=deadly_grace,if=buff.celestial_alignment.up|buff.incarnation.up
		ProlongedPower = function()
			return Player.Buff(Aura.CelestialAlignment).Up()
			    or Player.Buff(Aura.IncarnationChosenOfElune).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			ProlongedPower = Objects.newItem(142117),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.Berserking, Racial.Shadowmeld);
		config.AOEOptions(options, Spell.Starfall);
		config.BuffOptions(options, Spell.MoonkinForm);
		config.CooldownOptions(options, Spell.CelestialAlignment, Talent.ForceOfNature, Talent.FuryOfElune, Talent.IncarnationChosenOfElune, Talent.NewMoon, Talent.StellarFlare, Talent.WarriorOfElune);
		config.DefensiveOptions(options, Spell.Barkskin, Talent.Renewal);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.Barkskin, self.Defensives.Barkskin);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Talent.Renewal, self.Defensives.Renewal);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.MoonkinForm, self.Requirements.MoonkinForm);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.ProlongedPower, true);
		-- actions.precombat+=/solar_wrath
		action.EvaluateAction(Spell.SolarWrath, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		action.EvaluateAction(Consumable.ProlongedPower, self.Requirements.ProlongedPower);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking)
		-- actions+=/warrior_of_elune
		action.EvaluateAction(Talent.WarriorOfElune, true);
		action.EvaluateAction(Talent.IncarnationChosenOfElune, self.Requirements.IncarnationChosenOfElune);
		action.EvaluateAction(Spell.CelestialAlignment, self.Requirements.CelestialAlignment);

		action.CallActionList(rotationAOE);
		-- actions+=/call_action_list,name=st
		action.CallActionList(rotationSingleTarget);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Balance Druid", addonTable.Enum.SpecID.DRUID_BALANCE);