local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "lunaeclipse_druid_feral";

local function getRange()
	return Talent.BalanceAffinity.Enabled() and 13 or 8;
end

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cooldowns+=/berserk,if=energy>=30&(cooldown.tigers_fury.remains>5|buff.tigers_fury.up)
		Berserk = function()
			return Player.Energy() >= 30
			   and (Spell.TigersFury.Cooldown.Remains() > 5 or Player.Buff(Aura.TigersFury).Up());
		end,

		-- actions.cooldowns=dash,if=!buff.cat_form.up
		Dash = function()
			return not Player.Buff(Aura.CatForm).Up();
		end,

		-- actions.cooldowns+=/feral_frenzy,if=combo_points=0
		FeralFrenzy = function()
			return Player.ComboPoints() == 0;
		end,

		-- actions.cooldowns+=/incarnation,if=energy>=30&(cooldown.tigers_fury.remains>15|buff.tigers_fury.up)
		IncarnationKingOfTheJungle = function()
			return Player.Energy() >= 30
			   and (Spell.TigersFury.Cooldown.Remains() > 15 or Player.Buff(Aura.TigersFury).Up());
		end,

		-- actions.cooldowns+=/potion,name=prolonged_power,if=target.time_to_die<65|(time_to_die<180&(buff.berserk.up|buff.incarnation.up))
		ProlongedPower = function()
			return Target.TimeToDie() < 65
			    or (Target.TimeToDie() < 180 and (Player.Buff(Aura.Berserk).Up() or Player.Buff(Aura.IncarnationKingOfTheJungle).Up()));
		end,

		-- SimCraft doesn't specify it, but make sure we are in cat form.
		-- Increase remains check to 1 second as 0.5 may be too short depending on the players refresh setting.
		-- actions.cooldowns+=/prowl,if=buff.incarnation.remains<0.5&buff.jungle_stalker.up
		Prowl = function()
			return Player.Buff(Aura.CatForm).Up()
			   and Player.Buff(Aura.IncarnationKingOfTheJungle).Remains() < 1
			   and Player.Buff(Aura.JungleStalker).Up();
		end,

		-- actions.cooldowns+=/shadowmeld,if=combo_points<5&energy>=action.rake.cost&dot.rake.pmultiplier<2.1&buff.tigers_fury.up&(buff.bloodtalons.up|!talent.bloodtalons.enabled)&(!talent.incarnation.enabled|cooldown.incarnation.remains>18)&!buff.incarnation.up
		Shadowmeld = function()
			return Player.ComboPoints() < 5
			   and Player.Energy() >= Spell.Rake.Cost()
			   and Target.Debuff(Aura.Rake).PersistentMultiplier() < 2.1
			   and Player.Buff(Aura.TigersFury).Up()
			   and (Player.Buff(Aura.Bloodtalons).Up() or not Talent.Bloodtalons.Enabled())
			   and (not Talent.IncarnationKingOfTheJungle.Enabled() or Talent.IncarnationKingOfTheJungle.Cooldown.Remains() > 18)
			   and not Player.Buff(Aura.IncarnationKingOfTheJungle).Up();
		end,

		-- actions.cooldowns+=/tigers_fury,if=energy.deficit>=60
		TigersFury = function()
			return Player.Energy.Deficit() >= 60;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Dash, self.Requirements.Dash);
		action.EvaluateAction(Spell.Prowl, self.Requirements.Prowl);
		action.EvaluateAction(Spell.Berserk, self.Requirements.Berserk);
		action.EvaluateAction(Spell.TigersFury, self.Requirements.TigersFury);
		-- actions.cooldowns+=/berserking
		action.EvaluateAction(Racial.Berserking, true);
		action.EvaluateAction(Talent.FeralFrenzy, self.Requirements.FeralFrenzy);
		action.EvaluateAction(Talent.IncarnationKingOfTheJungle, self.Requirements.IncarnationKingOfTheJungle);
		action.EvaluateAction(Consumable.ProlongedPower, self.Requirements.ProlongedPower);
		action.EvaluateAction(Racial.Shadowmeld, self.Requirements.Shadowmeld);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Finishers Rotation
local function Finishers(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- max_energy means if you have enough energy for the maximum cost which is 50 energy.
		-- actions.st_finishers+=/ferocious_bite,max_energy=1
		FerociousBite = function()
			return Player.Energy() >= 50;
		end,

		-- remains<=duration*0.3 is handled by the addon as Debuff.Refreshable() as this calculates pandemic range.
		-- actions.st_finishers+=/rip,target_if=!ticking|(remains<=duration*0.3)&(target.health.pct>25&!talent.sabertooth.enabled)|(remains<=duration*0.8&persistent_multiplier>dot.rip.pmultiplier)&target.time_to_die>8
		Rip = function()
			return not Target.Debuff(Aura.Rip).Ticking()
			    or Target.Debuff(Aura.Rip).Refreshable()
			   and (Target.Health.Percent() > 25 and not Talent.Sabertooth.Enabled())
				or (Target.Debuff(Aura.Rip).Remains() <= Target.Debuff(Aura.Rip).Duration() * 0.8 and Player.PersistentMultiplier(Spell.Rip) > Target.Debuff(Aura.Rip).PersistentMultiplier())
			   and Target.TimeToDie() > 8;
		end,

		SavageRoar = {
			-- actions.st_finishers+=/savage_roar,if=buff.savage_roar.down
			Down = function()
				return Player.Buff(Aura.SavageRoar).Down();
			end,

			-- actions.st_finishers+=/savage_roar,if=buff.savage_roar.remains<12
			Use = function()
				return Player.Buff(Aura.SavageRoar).Remains() < 12;
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluatePoolAction(Talent.SavageRoar, self.Requirements.SavageRoar.Down);
		action.EvaluatePoolAction(Spell.Rip, self.Requirements.Rip);
		action.EvaluatePoolAction(Talent.SavageRoar, self.Requirements.SavageRoar.Use);
		action.EvaluateAction(Spell.FerociousBite, self.Requirements.FerociousBite);
	end

	-- actions.single_target+=/run_action_list,name=st_finishers,if=combo_points>4
	function self.Use()
		return Player.ComboPoints() > 4;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationFinishers = Finishers("Finishers");

-- Generators Rotation
local function Generators(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		BrutalSlash = {
			-- We can't do raid add events, as this is based on charges compared to when adds will be available lets just use on max charges
			-- actions.st_generators+=/brutal_slash,if=(buff.tigers_fury.up&(raid_event.adds.in>(1+max_charges-charges_fractional)*recharge_time))
			Charges = function()
				return Player.Buff(Aura.TigersFury).Up()
				   and Talent.BrutalSlash.Charges() == Talent.BrutalSlash.Charges.Max();
			end,

			-- actions.st_generators+=/brutal_slash,if=spell_targets.brutal_slash>desired_targets
			Use = function(numEnemies)
				return numEnemies > coreGeneral.DesiredTargets();
			end,
		},

		-- SimCraft doesn't specify it, but we are actually only using Moonfire with Lunar Inspiration talent chosen.
		-- actions.st_generators+=/moonfire_cat,target_if=refreshable
		Moonfire = function(numEnemies, Target)
			return Talent.LunarInspiration.Enabled()
			   and Target.Debuff(Aura.Moonfire).Refreshable();
		end,

		Rake = {
			-- remains<duration*0.3 is handled by the addon as Debuff.Refreshable() as this calculates pandemic range.
			-- actions.st_generators+=/rake,target_if=!ticking|(!talent.bloodtalons.enabled&remains<duration*0.3)&target.time_to_die>4
			Down = function()
				return not Target.Debuff(Aura.Rake).Ticking()
				    or (not Talent.Bloodtalons.Enabled() and Target.Debuff(Aura.Rake).Refreshable())
				   and Target.TimeToDie() > 4;
			end,

			-- actions.st_generators+=/rake,target_if=talent.bloodtalons.enabled&buff.bloodtalons.up&((remains<=7)&persistent_multiplier>dot.rake.pmultiplier*0.85)&target.time_to_die>4
			Use = function()
				return Talent.Bloodtalons.Enabled()
				   and Player.Buff(Aura.Bloodtalons).Up()
				   and (Target.Debuff(Aura.Rake).Remains() <= 7 and Player.PersistentMultiplier(Spell.Rake) > Target.Debuff(Aura.Rake).PersistentMultiplier() * 0.85)
				   and Target.TimeToDie() > 4;
			end,
		},

		-- actions.st_generators=regrowth,if=talent.bloodtalons.enabled&buff.predatory_swiftness.up&buff.bloodtalons.down&combo_points=4&dot.rake.remains<4
		Regrowth = function()
			return Talent.Bloodtalons.Enabled()
			   and Player.Buff(Aura.PredatorySwiftness).Up()
			   and Player.Buff(Aura.Bloodtalons).Down()
			   and Player.ComboPoints() == 4
			   and Target.Debuff(Aura.Rake).Remains() < 4;
		end,

		-- actions.st_generators+=/shred,if=dot.rake.remains>(action.shred.cost+action.rake.cost-energy)%energy.regen|buff.clearcasting.react
		Shred = function()
			return Target.Debuff(Aura.Rake).Remains() > (Spell.Shred.Cost() + Spell.Rake.Cost() - Player.Energy()) / Player.Energy.Regen()
			    or Player.Buff(Aura.ClearCasting).React();
		end,

		-- actions.st_generators+=/swipe_cat,if=spell_targets.swipe_cat>1
		Swipe = function(numEnemies)
			return numEnemies > 1;
		end,

		-- actions.st_generators+=/thrash_cat,if=refreshable&(spell_targets.thrash_cat>2)
		ThrashCat = function(numEnemies)
			return Target.Debuff(Aura.Thrash).Refreshable()
			   and numEnemies > 2;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Regrowth, self.Requirements.Regrowth);
		action.EvaluateAction(Talent.BrutalSlash, self.Requirements.BrutalSlash.Use, Enemies.GetEnemies(getRange()));
		action.EvaluatePoolAction(Spell.ThrashCat, self.Requirements.ThrashCat, Enemies.GetEnemies(getRange()));
		action.EvaluatePoolAction(Spell.Rake, self.Requirements.Rake.Down);
		action.EvaluatePoolAction(Spell.Rake, self.Requirements.Rake.Use);
		action.EvaluateAction(Talent.BrutalSlash, self.Requirements.BrutalSlash.Charges);
		action.EvaluateCycleAction(Spell.Moonfire, self.Requirements.Moonfire);
		action.EvaluatePoolAction(Spell.Swipe, self.Requirements.Swipe, Enemies.GetEnemies(getRange()));
		action.EvaluateAction(Spell.Shred, self.Requirements.Shred);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationGenerators = Generators("Generators");

-- SingleTarget Rotation
local function SingleTarget(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.single_target=cat_form,if=!buff.cat_form.up
		CatForm = function()
			return not Player.Buff(Aura.CatForm).Up();
		end,

		FerociousBite = {
			-- actions.single_target+=/ferocious_bite,if=buff.apex_predator.up&((combo_points>4&(buff.incarnation.up|talent.moment_of_clarity.enabled))|(talent.bloodtalons.enabled&buff.bloodtalons.up&combo_points>3))
			ApexPredator = function()
				return Player.Buff(Aura.ApexPredator).Up()
				   and ((Player.ComboPoints() > 4 and (Player.Buff(Aura.IncarnationKingOfTheJungle).Up() or Talent.MomentOfClarity.Enabled())) or (Talent.Bloodtalons.Enabled() and Player.Buff(Aura.Bloodtalons).Up() and Player.ComboPoints() > 3));
			end,

			-- actions.single_target+=/ferocious_bite,target_if=dot.rip.ticking&dot.rip.remains<3&target.time_to_die>10&(target.health.pct<25|talent.sabertooth.enabled)
			Use = function()
				return Target.Debuff(Aura.Rip).Ticking()
				   and Target.Debuff(Aura.Rip).Remains() < 3
				   and Target.TimeToDie() > 10
				   and (Target.Health.Percent() < 25 or Talent.Sabertooth.Enabled());
			end,
		},

		-- actions.single_target+=/rake,if=buff.prowl.up|buff.shadowmeld.up
		Rake = function()
			return Player.Buff(Aura.Prowl).Up()
			    or Player.Buff(Aura.Shadowmeld).Up();
		end,

		Regrowth = {
			-- actions.single_target+=/regrowth,if=combo_points>3&talent.bloodtalons.enabled&buff.predatory_swiftness.up&buff.apex_predator.up&buff.incarnation.down
			ApexPredator = function()
				return Player.ComboPoints() > 3
				   and Talent.Bloodtalons.Enabled()
				   and Player.Buff(Aura.PredatorySwiftness).Up()
				   and Player.Buff(Aura.ApexPredator).Up()
				   and Player.Buff(Aura.IncarnationKingOfTheJungle).Down();
			end,

			-- actions.single_target+=/regrowth,if=combo_points=5&buff.predatory_swiftness.up&talent.bloodtalons.enabled&buff.bloodtalons.down&(!buff.incarnation.up|dot.rip.remains<8)
			Use = function()
				return Player.ComboPoints() == 5
				   and Player.Buff(Aura.PredatorySwiftness).Up()
				   and Talent.Bloodtalons.Enabled()
				   and Player.Buff(Aura.Bloodtalons).Down()
				   and (not Player.Buff(Aura.IncarnationKingOfTheJungle).Up() or Target.Debuff(Aura.Rip).Remains() < 8);
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.CatForm, self.Requirements.CatForm)
		action.EvaluateAction(Spell.Rake, self.Requirements.Rake);

		-- actions.single_target+=/call_action_list,name=cooldowns
		action.CallActionList(rotationCooldowns);

		action.EvaluateAction(Spell.FerociousBite, self.Requirements.FerociousBite.Use);
		action.EvaluateAction(Spell.Regrowth, self.Requirements.Regrowth.Use);
		action.EvaluateAction(Spell.Regrowth, self.Requirements.Regrowth.ApexPredator);
		action.EvaluateAction(Spell.FerociousBite, self.Requirements.FerociousBite.ApexPredator);

		action.RunActionList(rotationFinishers);
		-- actions.single_target+=/run_action_list,name=st_generators
		action.RunActionList(rotationGenerators);
	end

	-- actions=run_action_list,name=single_target,if=dot.rip.ticking|time>15
	function self.Use()
		return Target.Debuff(Aura.Rip).Ticking()
		    or coreGeneral.CombatTime() > 15;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationSingleTarget = SingleTarget("SingleTarget");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Xanzara and SimCraft",
		GuideLink = "http://www.wowhead.com/feral-druid-guide",
		WoWVersion = 80001,
	};

	self.Defensives = {
		Renewal = function()
			return Player.DamagePredicted(3) >= 15
			   and Player.Health.Percent() < 70;
		end,

		SurvivalInstincts = function()
			return Player.DamagePredicted(3) >= 25;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- SimCraft doesn't specify this, but only suggest Cat Form if you are not currently in Cat Form.
		-- actions.precombat+=/cat_form
		CatForm = function()
			return not Player.Buff(Aura.CatForm).Up();
		end,

		-- actions+=/dash,if=!buff.cat_form.up
		Dash = function()
			return not Player.Buff(Aura.CatForm).Up();
		end,

		-- actions+=/moonfire_cat,if=talent.lunar_inspiration.enabled&!ticking
		Moonfire = function()
			return Talent.LunarInspiration.Enabled()
			   and not Target.Debuff(Aura.Moonfire).Ticking();
		end,

		-- SimCraft doesn't specify this, but only suggest if you are in Cat Form and not currently prowling.
		-- actions.precombat+=/prowl
		Prowl = function()
			return Player.Buff(Aura.CatForm).Up()
			   and not Player.Buff(Aura.Prowl).Up();
		end,

		-- actions+=/rake,if=!ticking|buff.prowl.up
		Rake = function()
			return not Target.Debuff(Aura.Rake).Ticking()
			    or Player.Buff(Aura.Prowl).Up();
		end,

		Regrowth = {
			-- SimCraft doesn't specify this, but only suggest if you don't have the buff so it will suggest following spells.
			-- actions.precombat+=/regrowth,if=talent.bloodtalons.enabled
			Precombat = function()
				return Talent.Bloodtalons.Enabled()
				   and not Player.Buff(Aura.Bloodtalons).Up();
			end,

			-- This seems to be old left over legion logic, as shapeshifting is now on the GCD its a dps loss to hardcast, which can happen with this, remove the "or" statement.
			-- actions+=/regrowth,if=(talent.sabertooth.enabled|buff.predatory_swiftness.up)&talent.bloodtalons.enabled&buff.bloodtalons.down&combo_points=5
			Use = function()
				return Player.Buff(Aura.PredatorySwiftness).Up()
				   and Talent.Bloodtalons.Enabled()
				   and Player.Buff(Aura.Bloodtalons).Down()
				   and Player.ComboPoints() == 5;
			end,
		},

		-- actions+=/rip,if=combo_points=5
		Rip = function()
			return Player.ComboPoints() == 5;
		end,

		-- actions+=/savage_roar,if=!buff.savage_roar.up
		SavageRoar = function()
			return not Player.Buff(Aura.SavageRoar).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			ProlongedPower = Objects.newItem(142117),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.Berserking, Racial.Shadowmeld);
		config.AOEOptions(options, Talent.BrutalSlash, Spell.Swipe, Spell.ThrashCat);
		config.BuffOptions(options, Spell.CatForm);
		config.CooldownOptions(options, Spell.Berserk, Talent.FeralFrenzy, Talent.IncarnationKingOfTheJungle, Spell.Moonfire, Spell.Regrowth, Talent.SavageRoar, Spell.TigersFury);
		config.DefensiveOptions(options, Talent.Renewal, Spell.SurvivalInstincts);
		config.UtilityOptions(options, Spell.Dash, Spell.Prowl);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.SurvivalInstincts, self.Defensives.SurvivalInstincts);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Talent.Renewal, self.Defensives.Renewal);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.Regrowth, self.Requirements.Regrowth.Precombat);
		action.EvaluateAction(Spell.CatForm, self.Requirements.CatForm);
		action.EvaluateAction(Spell.Prowl, self.Requirements.Prowl);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.ProlongedPower, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		action.RunActionList(rotationSingleTarget);

		action.EvaluateAction(Spell.Rake, self.Requirements.Rake);
		action.EvaluateAction(Spell.Dash, self.Requirements.Dash);
		action.EvaluateAction(Spell.Moonfire, self.Requirements.Moonfire);
		action.EvaluateAction(Talent.SavageRoar, self.Requirements.SavageRoar);
		-- actions+=/berserk
		action.EvaluateAction(Spell.Berserk, true);
		-- actions+=/incarnation
		action.EvaluateAction(Talent.IncarnationKingOfTheJungle, true);
		-- actions+=/tigers_fury
		action.EvaluateAction(Spell.TigersFury, true);
		action.EvaluateAction(Spell.Regrowth, self.Requirements.Regrowth.Use);
		action.EvaluateAction(Spell.Rip, self.Requirements.Rip);
		-- actions+=/shred
		action.EvaluateAction(Spell.Shred, true);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Feral Druid", addonTable.Enum.SpecID.DRUID_FERAL);