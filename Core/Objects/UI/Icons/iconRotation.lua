-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard, Callbacks, Events = addon.API.Blizzard, addon.System.Callbacks, addon.System.Events;
local format, huge, max, pairs, setmetatable, sort, tinsert, wipe = format, math.huge, max, pairs, setmetatable, sort, tinsert, wipe;

-- Table of CLEU events for when a unit is removed from combat.
local CLEU_UNIT_REMOVED = {
	UNIT_DESTROYED = true,
	UNIT_DIED = true,
	UNIT_DISSIPATES = true,
};

local function evaluateCooldown(evaluateAction)
	local Player = addon.Units.Player;
	local Spell = addon.API.Spell;
	local Tables = addon.Core.Tables;

	if not Tables.IsEmpty(evaluateAction) then
		local castingEnd = Player.Casting.End();
		local currentTime = Blizzard.GetTime();
		local endTime = Tables.GetValue(evaluateAction, "End", 0);
		local GCDInfo = Spell.GetSpellCooldown(addonTable.GLOBAL_COOLDOWN);
		local GCDEnd = Tables.GetValue(GCDInfo, "End", 0);

		if endTime > max(GCDEnd, castingEnd) then
			return Tables.GetValue(evaluateAction, "Start", currentTime), endTime;
		elseif castingEnd > max(endTime, GCDEnd) then
			return Player.Casting.Start("player") or currentTime, castingEnd;
		elseif GCDEnd > max(endTime, castingEnd) then
			return Tables.GetValue(GCDInfo, "Start", currentTime), GCDEnd;
		end
	end

	return 0, 0;
end

local function showOutOfRange(evaluateNoRange, evaluateRange)
	local Tables = addon.Core.Tables;

	local firstNoRange = Tables.GetValue(evaluateNoRange, 1, {});
	local firstRange = Tables.GetValue(evaluateRange, 1, {});

	return Tables.GetValue(firstNoRange, "WaitTime", 0) < Tables.GetValue(firstRange, "WaitTime", huge);
end

local function sortActions(a, b)
	local coreTables = addon.Core.Tables;

	if coreTables.GetValue(a, "WaitTime", 0) == coreTables.GetValue(b, "WaitTime", 0) then
		return coreTables.GetValue(a, "RotationIndex", 0) < coreTables.GetValue(b, "RotationIndex", 0);
	end

	return coreTables.GetValue(a, "WaitTime", 0) < coreTables.GetValue(b, "WaitTime", 0);
end

local Cache = setmetatable({}, { __mode = "kv", __metatable = "This table is protected!" });

addonTable.iconRotation = function(ID)
	local self = Cache[ID] or {};

	local actions = {};

	local evaluateNoRange = {};
	local evaluateRange = {};

	local iconName = ID;
	local rotationIndex = 0;
	local rotationPaused = false;
	local stopProcessing = false;

	local function evaluateIcon(Rotation, rotationIndex, Object, useObject, numEnemies, iconText, evaluateAction)
		local ActionBar = addon.Modules.ActionBar;
		local Item = addon.API.Item;
		local Objects = addon.Core.Objects;
		local Player = addon.Units.Player;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		Rotation = Rotation or addon.currentRotation;

		-- Set the evaluation time to when the action will be ready or whenever the player has finished casting, whichever will happen last.
		local evaluateTime = max(Tables.GetValue(evaluateAction, "End", 0), Player.Casting.End());

		if Rotation.ProcessCondition(useObject, numEnemies, evaluateTime > 0 and evaluateTime) then
			local ObjectID = Objects.IsSpell(Object) and Object.SpellID() or Object.ItemID();
			local objectInfo = Objects.IsSpell(Object) and Spell.GetSpellInfo(ObjectID) or Item.GetItemInfo(ObjectID);
			local ObjectType = Objects.GetType(Object);
			local startTime, endTime = evaluateCooldown(evaluateAction);

			if not Tables.IsEmpty(objectInfo) then
				local iconInfo = {
					ID = ObjectID,
					Name = objectInfo.Name,
					Texture = objectInfo.IconID,
					Text = iconText or "",
					Type = ObjectType,
					Keybind = ActionBar.Keybind(ObjectType, ObjectID),
					Wait = endTime > Blizzard.GetTime(),
					WaitStart = startTime,
					WaitEnd = endTime,
					WaitTime = endTime - startTime;
					RotationIndex = rotationIndex,
				};

				if Object.InRange(addon.Units.Target) then
					tinsert(evaluateRange, iconInfo);
				end

				tinsert(evaluateNoRange, iconInfo);
			end
		end
	end

	self.Actions = function()
		return actions;
	end

	self.EndRotation = function()
		local Settings = addon.Core.Settings;
		local Tables = addon.Core.Tables;

		wipe(actions);

		if Settings.GetCharacterValue("ScriptOptions", "OPT_RANGE_CHECK") == 1 then
			sort(evaluateNoRange, sortActions);
			sort(evaluateRange, sortActions);

			self.OutOfRange();

			actions = Tables.Duplicate(evaluateRange);
		else
			sort(evaluateNoRange, sortActions);

			actions = Tables.Duplicate(evaluateNoRange);
		end

		Callbacks.Callback(format("%sIcon", iconName), actions);

		wipe(evaluateNoRange);
		wipe(evaluateRange);
	end

	self.EvaluateAction = function(Rotation, Object, useObject, numEnemies, iconText, evaluateAction)
		local Tables = addon.Core.Tables;

		rotationIndex = (rotationIndex or 0) + 1;
		iconText = iconText or Tables.GetValue(evaluateAction, "Text", "");

		evaluateIcon(Rotation, rotationIndex, Object, useObject, numEnemies, iconText, evaluateAction);
	end

	self.EvaluateInterruptCondition = function(Rotation, Object, useObject, interruptCheck, numEnemies, iconText, evaluateAction)
		local Tables = addon.Core.Tables;

		rotationIndex = (rotationIndex or 0) + 1;
		iconText = iconText or Tables.GetValue(evaluateAction, "Text", "");

		evaluateIcon(Rotation, rotationIndex, Object, useObject, numEnemies, iconText, evaluateAction);
	end

	self.EvaluatePoolAction = function(Rotation, Object, useObject, numEnemies, iconText, evaluateAction)
		local Tables = addon.Core.Tables;

		rotationIndex = (rotationIndex or 0) + 1;
		iconText = iconText or Tables.GetValue(evaluateAction, "Text", "");

		evaluateIcon(Rotation, rotationIndex, Object, useObject, numEnemies, iconText, evaluateAction);
	end

	self.EvaluateWaitCondition = function(Rotation, Object, waitCheck, numEnemies)
		rotationIndex = (rotationIndex or 0) + 1;

		evaluateIcon(Rotation, rotationIndex, Object, waitCheck, numEnemies, "wait");
	end

	self.OutOfRange = function()
		if (iconName == "SingleTarget" or iconName == "MultiTarget") and showOutOfRange(evaluateNoRange, evaluateRange) then
			rotationIndex = (rotationIndex or 0) + 1;

			local iconInfo = {
				ID = 0,
				Name = "Out of Range",
				Texture = 450907,
				Text = "move",
				Type = "Special",
				Keybind = "",
				Wait = false,
				WaitStart = 0,
				WaitEnd = 0,
				WaitTime = 0;
				RotationIndex = rotationIndex,
			};

			-- We are supposed to show out of range so put it first in the queue, this saves needing to resort again.
			tinsert(evaluateRange, 1, iconInfo);
		end
	end

	self.Paused = function()
		return rotationPaused == true;
	end

	self.StartRotation = function()
		rotationIndex = 0;

		rotationPaused = false;
		stopProcessing = false;

		wipe(evaluateNoRange);
		wipe(evaluateRange);
	end

	self.Stopped = function(value)
		if value then
			stopProcessing = value;
		else
			return stopProcessing == true;
		end
	end

	Cache[ID] = self;

	return self;
end

local function timerEvent()
	local Debug = addonTable.Debug;
	local Rotation = addon.Rotation;
	local Target = addon.Units.Target;

	local currentRotation = addon.currentRotation;
	local iconAOE, iconCD, iconOFFGCD, iconST = addon.Icons.AOE, addon.Icons.CD, addon.Icons.OFFGCD, addon.Icons.ST;

	local rotationIcons = {
		SingleTargetIcon = iconST,
		MultiTargetIcon = iconAOE,
		OffGCDIcon = iconOFFGCD,
		CooldownIcon = iconCD,
	};

	if not Debug then
		if currentRotation and Target.IsValidTarget() then
			Rotation.SetRotationObject(nil);
			addonTable.maxRange = 5; -- Melee Range is 5 yards so we never want lower then this.

			addon.MinDistance = Target.InRange.Min();
			addon.MaxDistance = Target.InRange.Max();

			for _, rotationIcon in pairs(rotationIcons) do
				rotationIcon.StartRotation();
			end

			currentRotation.ProcessRotation();

			for _, rotationIcon in pairs(rotationIcons) do
				rotationIcon.EndRotation();
			end
		else
			for iconName, rotationIcon in pairs(rotationIcons) do
				wipe(rotationIcon.Actions());
				Callbacks.Callback(iconName);
			end
		end
	end
end

local function enterCombat()
	local systemTimer = addon.System.Timers;
	local currentRotation = addon.currentRotation;

	if currentRotation and not systemTimer.TimerExists(timerEvent) then
		timerEvent();
		systemTimer.TimerAdd(timerEvent, addon.refreshInterval);
	end
end

local function leaveCombat()
	local systemTimer = addon.System.Timers;
	local iconAOE, iconCD, iconOFFGCD, iconST = addon.Icons.AOE, addon.Icons.CD, addon.Icons.OFFGCD, addon.Icons.ST;

	local rotationIcons = {
		SingleTargetIcon = iconST,
		MultiTargetIcon = iconAOE,
		OffGCDIcon = iconOFFGCD,
		CooldownIcon = iconCD,
	};

	systemTimer.TimerRemove(timerEvent);

	for iconName, rotationIcon in pairs(rotationIcons) do
		wipe(rotationIcon.Actions());
		Callbacks.Callback(iconName);
	end
end

local function combatLog(...)
	local Unit = addon.API.Unit;
	local _, _, combatEvent, _, _, _, _, _, destGUID = ...;

	if CLEU_UNIT_REMOVED[combatEvent] and destGUID == Blizzard.UnitGUID("target") and not Unit.InCombat() then
		leaveCombat();
	end
end

local function targetChanged()
	local Target = addon.Units.Target;
	local Unit = addon.API.Unit;

	if Target.IsValidTarget(true) then
		enterCombat();
	elseif not Unit.InCombat() then
		leaveCombat();
	end
end

local moduleEvents = {
	Disabled = function()
		leaveCombat();
	end,

	Enabled = function()
		local Unit = addon.API.Unit;

		if Unit.InCombat() then
			enterCombat();
		else
			leaveCombat();
		end
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = combatLog,
	INSTANCE_ENCOUNTER_ENGAGE_UNIT = enterCombat,
	PLAYER_REGEN_DISABLED = enterCombat,
	PLAYER_REGEN_ENABLED = leaveCombat,
	PLAYER_TARGET_CHANGED = targetChanged,
};

Callbacks.RegisterAddonCallbacks(moduleEvents);
Events.RegisterEvents(tableEvents);