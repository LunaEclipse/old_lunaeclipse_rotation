-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard = addon.API.Blizzard;
local ipairs, setmetatable, type = ipairs, setmetatable, type;
local Cache = setmetatable({}, { __mode = "kv" });

-- Get if an unit is not immune to stuns
local IsStunnableClassification = {
	["trivial"] = true,
	["minus"] = true,
	["normal"] = true,
	["rare"] = true,
};

-- Get if the unit is stunned or not
local IsStunned = {
	-- Demon Hunter
	-- Druid
	-- General
	5211, -- Mighty Bash
	-- Feral
	203123, -- Maim
	163505, -- Rake
	-- Paladin
	-- General
	853, -- Hammer of Justice
	-- Retribution
	205290, -- Wake of Ashes
	-- Rogue
	-- General
	199804, -- Between the Eyes
	1833, -- Cheap Shot
	408, -- Kidney Shot
	196958, -- Strike from the Shadows
	-- Warrior
	-- General
	132168, -- Shockwave
	132169, -- Storm Bolt
};

-- Check if the unit is coded as blacklisted or not.
local SpecialBlacklistDataSpells = {
	D_DHT_Submerged = 220519,
};

local SpecialBlacklistData = {
	--- Legion
	----- Dungeons (7.0 Patch) -----
	--- Darkheart Thicket
	-- Strangling roots can't be hit while this buff is present
	[100991] = function(self)
		return self.Buff(SpecialBlacklistDataSpells.D_DHT_Submerged, true).Up();
	end,
	--- Mythic+ Affixes
	-- Fel Explosives (7.2 Patch)
	[120651] = true,
	----- Trial of Valor (T19 - 7.1 Patch) -----
	--- Helya
	-- Striking Tentacle cannot be hit.
	[114881] = true,
};

--- Check if the unit is coded as blacklisted for Marked for Death (Rogue) or not.
-- Most of the time if the unit doesn't really die and isn't the last unit of an instance.
local SpecialMFDBlacklistData = {
	--- Legion
	----- Dungeons (7.0 Patch) -----
	--- Halls of Valor
	-- Hymdall leaves the fight at 10%.
	[94960] = true,
	-- Solsten and Olmyr doesn't "really" die
	[102558] = true,
	[97202] = true,
	-- Fenryr leaves the fight at 60%. We take 50% as check value since it doesn't get immune at 60%.
	[95674] = function(self)
		return self.Health.Percent() > 50 and true or false;
	end,

	----- Trial of Valor (T19 - 7.1 Patch) -----
	--- Odyn
	-- Hyrja & Hymdall leaves the fight at 25% during first stage and 85%/90% during second stage (HM/MM)
	[114360] = true,
	[114361] = true,

	--- Warlord of Draenor (WoD)
	----- HellFire Citadel (T18 - 6.2 Patch) -----
	--- Hellfire Assault
	-- Mar'Tak doesn't die and leave fight at 50% (blocked at 1hp anyway).
	[93023] = true,

	----- Dungeons (6.0 Patch) -----
	--- Shadowmoon Burial Grounds
	-- Carrion Worm : They doesn't die but leave the area at 10%.
	[88769] = true,
	[76057] = true,
};

addonTable.classUnit = function(ID)
	local unitID = ID;

	local self = Cache[ID] or {};

	local function iterateAuraTable(auraTable, AnyCaster, returnDuration)
		-- Loop through the auras in the aura table
		for _, auraID in ipairs(auraTable) do
			if self.Aura(auraID, AnyCaster).Up() then
				return returnDuration and self.Aura(auraID, AnyCaster).Remains()
					or true;
			end
		end

		return returnDuration and 0
			or false;
	end

	self.ClassID = function()
		return unitID;
	end

	self.ClassType = function()
		return "Unit";
	end

	-- Get if the unit Affecting Combat.
	self.AffectingCombat = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "AffectingCombat", false);
	end

	-- Add Debuff Properties to unit information.
	-- These functions are located in propertiesAura.lua
	self.Aura = function(Object, AnyCaster)
		return addonTable.propertiesAura(self, Object, AnyCaster);
	end

	self.Buff = self.Aura;
	self.Debuff = self.Aura;

	-- Get if the unit CanAttack the other one.
	self.CanAttack = function(Other)
		local addonCache = addonTable.Cache;
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		if Other ~= nil then
			local GUID = self.GUID();
			local otherID = Objects.IsUnit(Other) and Other.ClassID() or Other;
			local OtherGUID = Blizzard.UnitGUID(otherID);

			if GUID and OtherGUID then
				local unitInfo = Unit.GetUnitInfo(unitID);

				if not Tables.IsEmpty(unitInfo) then
					unitInfo.CanAttack = unitInfo.CanAttack or {};

					if Tables.GetValue(unitInfo.CanAttack, OtherGUID) == nil then
						unitInfo.CanAttack[OtherGUID] = Blizzard.UnitCanAttack(unitID, otherID);

						addonCache.Temporary.UnitInfo[GUID] = unitInfo;
					end

					return Tables.GetValue(unitInfo.CanAttack, OtherGUID, false);
				end
			end
		end

		return false;
	end

	-- Get if an unit can be stunned or not
	self.CanBeStunned = function(IgnoreClassification)
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "CanBeStunned") == nil then
			unitInfo.CanBeStunned = IgnoreClassification
								 or self.IsStunnable()
								 or false;

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "CanBeStunned", 0);
	end

	self.Casting = addonTable.propertiesCasting(self);

	-- Get unit classification
	self.Classification = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "Classification", false);
	end

	-- Get if the unit Exists and is visible.
	self.Exists = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "Exists", false);
	end

	-- Get the unit GUID.
	self.GUID = function()
		return Blizzard.UnitGUID(unitID);
	end

	self.Health = {
		Current = function()
			local Tables = addon.Core.Tables;
			local Unit = addon.API.Unit;

			local unitInfo = Unit.GetUnitInfo(unitID);

			return Tables.GetValue(unitInfo, "Health", 1);
		end,

		-- Get the unit MaxHealth.
		Max = function()
			local Tables = addon.Core.Tables;
			local Unit = addon.API.Unit;

			local unitInfo = Unit.GetUnitInfo(unitID);

			return Tables.GetValue(unitInfo, "MaxHealth", 1);
		end,

		-- Get the unit Health Percentage
		Percent = function()
			return (self.Health.Current() / self.Health.Max()) * 100;
		end,
	};
	setmetatable(self.Health, { __call = function(table, ...) return self.Health.Current(...); end });

	self.InRange = addonTable.propertiesRange(self);

	-- Get if we are on the other units threat table (in combat with it).
	self.IsAttacking = function(Other)
		local addonCache = addonTable.Cache;
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		if Other ~= nil then
			local GUID = self.GUID();
			local otherID = Objects.IsUnit(Other) and Other.ClassID() or Other;
			local OtherGUID = Blizzard.UnitGUID(otherID);

			if GUID and OtherGUID then
				local unitInfo = Unit.GetUnitInfo(unitID);

				if not Tables.IsEmpty(unitInfo) then
					unitInfo.IsAttacking = unitInfo.IsAttacking or {};

					if Tables.GetValue(unitInfo.IsAttacking, OtherGUID) == nil then
						unitInfo.IsAttacking[OtherGUID] = Blizzard.UnitThreatSituation(otherID, unitID) ~= nil;

						addonCache.Temporary.UnitInfo[GUID] = unitInfo;
					end

					return Tables.GetValue(unitInfo.IsAttacking, OtherGUID, false);
				end
			end
		end

		return false;
	end

	self.IsBlacklisted = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsBlacklisted") == nil then
			local npcID = self.NPCID();
			local blackList = SpecialBlacklistData[npcID];

			if blackList ~= nil then
				unitInfo.IsBlacklisted = type(blackList) == "function" and blackList(self) or blackList;
			else
				unitInfo.IsBlacklisted = false;
			end

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsBlacklisted", false);
	end

	-- Get if an unit with a given NPC ID is in the Boss list and has less HP than the given ones.
	self.IsBoss = function(npcID, HP)
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);
		local unitFound = false;

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsBoss") == nil then
			local bossUnit;
			local BossUnits = {	"boss1", "boss2", "boss3", "boss4" };

			for _, unitID in ipairs(BossUnits) do
				bossUnit = addon.Units[unitID];

				if bossUnit.Exists() and bossUnit.NPCID() == (npcID or self.NPCID()) and bossUnit.Health.Percent() <= (HP or 100) then
					unitFound = true;
					break;
				end
			end

			unitInfo.IsBoss = unitFound;
			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsBoss", false);
	end

	-- Get if the unit Is Dead Or Ghost.
	self.IsDeadOrGhost = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "IsDeadOrGhost", false);
	end

	-- Get whether the unit is a combat dummy.
	self.IsDummy = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Tooltip = addon.Core.Tooltip;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsDummy") == nil then
			unitInfo.IsDummy = Tooltip.IsDummy(unitID);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsDummy", false);
	end

	-- Get if two unit are the same.
	self.IsFriendly = function(Other)
		local addonCache = addonTable.Cache;
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		if Other ~= nil then
			local GUID = self.GUID();
			local otherID = Objects.IsUnit(Other) and Other.ClassID() or Other;
			local OtherGUID = Blizzard.UnitGUID(otherID);

			if GUID and OtherGUID then
				local unitInfo = Unit.GetUnitInfo(unitID);

				if not Tables.IsEmpty(unitInfo) then
					unitInfo.IsFriendly = unitInfo.IsFriendly or {};

					if Tables.GetValue(unitInfo.IsFriendly, OtherGUID) == nil then
						unitInfo.IsFriendly[OtherGUID] = Blizzard.UnitIsFriend(unitID, otherID);

						addonCache.Temporary.UnitInfo[GUID] = unitInfo;
					end

					return Tables.GetValue(unitInfo.IsFriendly, OtherGUID, false);
				end
			end
		end

		return false;
	end

	-- Get if two unit are the same.
	self.IsHostile = function(Other)
		local addonCache = addonTable.Cache;
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		if Other ~= nil then
			local GUID = self.GUID();
			local otherID = Objects.IsUnit(Other) and Other.ClassID() or Other;
			local OtherGUID = Blizzard.UnitGUID(otherID);

			if GUID and OtherGUID then
				local unitInfo = Unit.GetUnitInfo(unitID);

				if not Tables.IsEmpty(unitInfo) then
					unitInfo.IsHostile = unitInfo.IsHostile or {};

					if Tables.GetValue(unitInfo.IsHostile, OtherGUID) == nil then
						unitInfo.IsHostile[OtherGUID] = not Blizzard.UnitIsFriend(unitID, otherID);

						addonCache.Temporary.UnitInfo[GUID] = unitInfo;
					end

					return Tables.GetValue(unitInfo.IsHostile, OtherGUID, true);
				end
			end
		end

		return true;
	end

	self.IsMFDBlacklisted = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsMFDBlacklisted") == nil then
			local npcID = self.NPCID();
			local blackList = SpecialMFDBlacklistData[npcID];

			if blackList ~= nil then
				unitInfo.IsMFDBlacklisted = type(blackList) == "function" and blackList(self) or blackList;
			else
				unitInfo.IsMFDBlacklisted = false;
			end

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsMFDBlacklisted", false);
	end

	-- Get if the unit is moving or not.
	self.IsMoving = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "IsMoving", false);
	end

	-- Get if the unit is a Player or not.
	self.IsPlayer = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "IsPlayer", false);
	end

	self.IsStunnable = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsStunnable") == nil then
			unitInfo.IsStunnable = IsStunnableClassification[self.Classification()] or false;
			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsStunnable", false);
	end

	self.IsStunned = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsStunned") == nil then
			unitInfo.IsStunned = iterateAuraTable(IsStunned, true);
			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsStunned", false);
	end

	-- Get if two unit are the same.
	self.IsUnit = function(Other)
		local addonCache = addonTable.Cache;
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		if Other ~= nil then
			local GUID = self.GUID();
			local otherID = Objects.IsUnit(Other) and Other.ClassID() or Other;
			local OtherGUID = Blizzard.UnitGUID(otherID);

			if GUID and OtherGUID then
				local unitInfo = Unit.GetUnitInfo(unitID);

				if not Tables.IsEmpty(unitInfo) then
					unitInfo.IsUnit = unitInfo.IsUnit or {};

					if Tables.GetValue(unitInfo.IsUnit, OtherGUID) == nil then
						unitInfo.IsUnit[OtherGUID] = Blizzard.UnitIsUnit(otherID, unitID);

						addonCache.Temporary.UnitInfo[GUID] = unitInfo;
					end

					return Tables.GetValue(unitInfo.IsUnit, OtherGUID, false);
				end
			end
		end

		return false;
	end

	-- Check if the unit is coded as blacklisted by the user or not.
	self.IsUserBlacklisted = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsUserBlacklisted") == nil then
			local npcID = self.NPCID();
			local blackList = addonTable.Blacklist.UserDefined[npcID];

			if blackList ~= nil then
				unitInfo.IsUserBlacklisted = type(blackList) == "function" and blackList(self) or blackList;
			else
				unitInfo.IsUserBlacklisted = false;
			end

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsUserBlacklisted", false);
	end

	-- Check if the unit is coded as blacklisted for cycling by the user or not.
	self.IsUserCycleBlacklisted = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsUserCycleBlacklisted") == nil then
			local npcID = self.NPCID();
			local blackList = addonTable.Blacklist.CycleUserDefined[npcID];

			if blackList ~= nil then
				unitInfo.IsUserCycleBlacklisted = type(blackList) == "function" and blackList(self) or blackList;
			else
				unitInfo.IsUserCycleBlacklisted = false;
			end

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsUserCycleBlacklisted", false);
	end

	self.IsValidDOTTarget = function(TTDThreshold, HealthThreshold)
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsValidDOTTarget") == nil then
			unitInfo.IsValidDOTTarget = self.IsDummy()
									 or (TTDThreshold and self.TimeToDie() >= TTDThreshold)
									 or (HealthThreshold and self.Health() >= HealthThreshold);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsValidDOTTarget", false);
	end

	-- Check to see if the unit is a valid target for the player.
	self.IsValidTarget = function(skipAttackable)
		local addonCache = addonTable.Cache;
		local Player = addon.Units.Player;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "IsValidTarget") == nil then
			unitInfo.IsValidTarget = self.Exists()
								 and not self.IsDeadOrGhost()
								 and not Player.InVehicle()
								 and (skipAttackable or Player.CanAttack(self));

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "IsValidTarget", false);
	end

	-- Get the level of the unit
	self.Level = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "UnitLevel", 0);
	end

	-- Get the unit Name.
	self.Name = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "Name", "");
	end

	-- Get the unit NPC ID.
	self.NPCID = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "NPCID");
	end

	self.Power = addonTable.propertiesPower(self, Blizzard.UnitPowerType(unitID));

	-- Get the unit's power type
	self.PowerType = function()
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local unitInfo = Unit.GetUnitInfo(unitID);

		return Tables.GetValue(unitInfo, "PowerType", 0);
	end

	-- time remaining on stun debuff
	self.StunRemains = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "StunRemains") == nil then
			unitInfo.StunRemains = iterateAuraTable(IsStunned, true, true);

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "StunRemains", 0);
	end

	-- "foo.time_to_die"
	self.TimeToDie = function(minSamples)
		return addonTable.TimeToDie.TimeToDie(self, minSamples);
	end

	-- Get the unit ID.
	self.UnitID = function()
		return unitID;
	end

	-- Is the unit valid during cycle?
	self.UnitIsCycleValid = function()
		local addonCache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		local GUID = self.GUID();
		local unitInfo = Unit.GetUnitInfo(unitID);

		if GUID and not Tables.IsEmpty(unitInfo) and Tables.GetValue(unitInfo, "UnitIsCycleValid") == nil then
			unitInfo.UnitIsCycleValid = not self.IsBlacklisted()
									and not self.IsUserBlacklisted()
									and not self.IsUserCycleBlacklisted();

			addonCache.Temporary.UnitInfo[GUID] = unitInfo;
		end

		return Tables.GetValue(unitInfo, "UnitIsCycleValid", false);
	end

	return self;
end