-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Callbacks = addon.System.Callbacks;
local huge, max, pairs, type = math.huge, max, pairs, type;

local eliteMobs = {
	elite = true,
	rareelite = true,
};

local normalMobs = {
	minus = true,
	normal = true,
	trivial = true,
};

local stealthSpells = {
	[1784] = true, -- Stealth
	[5215] = true, -- Prowl
	[1856] = true, -- Vanish
	[58984] = true, -- Shadowmeld
};

local rotationObject;

-- Icons
addon.Icons = {
	AOE = addonTable.iconRotation("MultiTarget"),
	CD = addonTable.iconRotation("Cooldown"),
	OFFGCD = addonTable.iconRotation("OffGCD"),
	ST = addonTable.iconRotation("SingleTarget"),
	DEFENSIVE = addonTable.iconDefensive,
	INTERRUPT = {},
};

local function actionCooldown(Object)
	local Objects = addon.Core.Objects;

	return Objects.IsSpell(Object)
	   and not Object.IsOffGCD();
end

local function actionMultiTarget()
	local multiTarget = addon.Icons.AOE;

	return not multiTarget.Paused()
	   and not multiTarget.Stopped();
end

local function actionOffGCD(Object)
	local Objects = addon.Core.Objects;

	return Objects.IsItem(Object)
		or (Objects.IsSpell(Object) and Object.IsOffGCD());
end

local function actionSingleTarget()
	local singleTarget = addon.Icons.ST;

	return not singleTarget.Paused()
	   and not singleTarget.Stopped();
end

local function evalutateCheckBox(checkbox)
	local Settings = addon.Core.Settings;

	return (checkbox == nil and 2)
		or (type(checkbox) == "boolean" and checkbox == true and 2)
		or (type(checkbox) == "boolean" and checkbox == false and 1)
		or Settings.GetCharacterValue("ScriptOptions", checkbox)
		or Settings.GetRotationValue(checkbox);
end

local function setEnemies(numEnemies)
	local Enemies = addon.Modules.Enemies;
	local multiTarget = addon.Icons.AOE;

	if multiTarget.Paused() or multiTarget.Stopped() then
		return 1;
	end

	return numEnemies or Enemies.GetEnemies();
end

local function stopProcessing()
	local singleTarget = addon.Icons.ST;
	local multiTarget =	addon.Icons.AOE;

	return (singleTarget.Paused() or singleTarget.Stopped())
	   and (multiTarget.Paused() or multiTarget.Stopped());
end

local function checkRotationCondition(Object, callFunction, numEnemies)
	if callFunction and type(callFunction) == "function" then
		return callFunction(numEnemies) == nil or callFunction(numEnemies);
	else
		return Object.Use(numEnemies) == nil or Object.Use(numEnemies);
	end
end

local function useActionList(Object, callFunction, numEnemies)
	local Enemies = addon.Modules.Enemies;
	local useSingleTarget, useMultiTarget;

	if actionSingleTarget() then
		useSingleTarget = checkRotationCondition(Object, callFunction, 1);
	end

	if actionMultiTarget() then
		useMultiTarget = checkRotationCondition(Object, callFunction, numEnemies or Enemies.GetEnemies());
	end

	return useSingleTarget or useMultiTarget or false;
end

local function useAlways(Object)
	local Core = addon.Core.General;
	local Player = addon.Units.Player;
	local Settings = addon.Core.Settings;

	local useSolo = Settings.GetCharacterValue("CooldownOptions", "useSolo") == 1;

	return Object.Cooldown.Base() <= 15
		or (useSolo and Player.IsSolo())
		or Core.IsRaidBossFight();
end

local function useDungeonBoss(Object)
	local Core = addon.Core.General;
	local Settings = addon.Core.Settings;

	local useDungeon = Settings.GetCharacterValue("CooldownOptions", "useDungeonBoss") == 1;

	return Core.IsDungeonBossFight()
	   and (not useDungeon or (Object.Cooldown.Base() <= Settings.GetCharacterValue("CooldownOptions", "useDungeonBossMax")));
end

local function useEliteMob(Object)
	local Settings = addon.Core.Settings;
	local Target = addon.Units.Target;

	local useElite = Settings.GetCharacterValue("CooldownOptions", "useElite") == 1;
	local eliteMob = eliteMobs[Target.Classification()] or false;

	return eliteMob
	   and (not useElite or (Object.Cooldown.Base() <= Settings.GetCharacterValue("CooldownOptions", "useEliteMax")));
end

local function useNormalMob(Object)
	local Settings = addon.Core.Settings;
	local Target = addon.Units.Target;

	local useNormal = Settings.GetCharacterValue("CooldownOptions", "useNormal") == 1;
	local normalMob = normalMobs[Target.Classification()] or false;

	return normalMob
	   and (not useNormal or (Object.Cooldown.Base() <= Settings.GetCharacterValue("CooldownOptions", "useNormalMax")));
end

local function usePVP(Object)
	local Settings = addon.Core.Settings;
	local PVP = Settings.GetCharacterValue("CooldownOptions", "usePVP") == 1;
	local Target = addon.Units.Target;

	return Target.IsPlayer()
	   and (not PVP or (Object.Cooldown.Base() <= Settings.GetCharacterValue("CooldownOptions", "usePVPMax")));
end

local function useStealth(Object)
	local Objects = addon.Core.Objects;
	local Player = addon.Units.Player;
	local Settings = addon.Core.Settings;
	local Unit = addon.API.Unit;

	local ObjectID = Objects.GetID(Object);

	if Objects.IsSpell(Object) and stealthSpells[ObjectID] then
		local useSolo = Settings.GetCharacterValue("ScriptOptions", "OPT_STEALTH_SOLO") == 1;

		return not Unit.InCombat() or useSolo or Player.InGroup();
	end

	return false;
end

local function useCooldown(Object)
	local Objects = addon.Core.Objects;

	if Objects.IsSpell(Object) or Objects.IsItem(Object) then
		return useAlways(Object)
		    or useStealth(Object)
		    or usePVP(Object)
		    or useDungeonBoss(Object)
		    or useEliteMob(Object)
		    or useNormalMob(Object);
	end

	-- It must either not be a valid object, or its a passive ability such as Azerite Trait, Passive Talent of Spell so don't suggest it.
	return false;
end

local function validWaitTime(evaluateAction)
	local Settings = addon.Core.Settings;
	local Tables = addon.Core.Tables;

	if Tables.GetValue(evaluateAction, "Use", false) then
		local useMaxWait = Settings.GetAccountValue("AddonSettings", "useMaxWait") == 1;
		local maxWaitTime = Settings.GetAccountValue("AddonSettings", "maxWaitTime", 10);

		return not useMaxWait or Tables.GetValue(evaluateAction, "Duration", huge) <= maxWaitTime;
	end

	return false;
end

addon.Rotation = {
	CallActionList = function(Object, callFunction, numEnemies)
		if not stopProcessing() and Object and useActionList(Object, callFunction, numEnemies) then
			local previousRotation = rotationObject;

			rotationObject = Object;
			Object.ProcessRotation(previousRotation);
		end
	end,

	EvaluateAction = function(Object, useObject, numEnemies, iconText)
		if not stopProcessing() and Object and useObject and useCooldown(Object) then
			local evaluateAction = Object.Evaluate();

			if validWaitTime(evaluateAction) then
				local checkbox = evalutateCheckBox(Object.CheckBox());

				numEnemies = setEnemies(numEnemies);

				if checkbox == 2 then
					if actionSingleTarget() then
						addon.Icons.ST.EvaluateAction(rotationObject, Object, useObject, 1, iconText, evaluateAction);
					end

					if actionMultiTarget() then
						addon.Icons.AOE.EvaluateAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end
				elseif checkbox == 1 then
					if actionOffGCD(Object) then
						addon.Icons.OFFGCD.EvaluateAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end

					if actionCooldown(Object) then
						addon.Icons.CD.EvaluateAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end
				end
			end
		end
	end,

	EvaluateCycleAction = function(Object, useObject, maxTargets, numEnemies, iconText)
		if not stopProcessing() and Object and useObject and useCooldown(Object) then
			local evaluateAction = Object.Evaluate();

			if validWaitTime(evaluateAction) then
				local checkbox = evalutateCheckBox(Object.CheckBox());

				numEnemies = setEnemies(numEnemies);

				if checkbox == 2 then
					if actionSingleTarget() then
						addon.Icons.ST.EvaluateAction(rotationObject, Object, useObject, 1, iconText, evaluateAction);
					end

					if actionMultiTarget() then
						addon.Icons.AOE.EvaluateAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end
				elseif checkbox == 1 then
					if actionOffGCD(Object) then
						addon.Icons.OFFGCD.EvaluateAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end

					if actionCooldown(Object) then
						addon.Icons.CD.EvaluateAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end
				end

				-- Todo: Add multi-dot part of this action.
				-- Only multi-dot if this action if still processing AOE actions, or there is multiple enemies to dot.
				if numEnemies > 1 then
				end
			end
		end
	end,

	EvaluateInterruptCondition = function(Object, useObject, interruptCheck, numEnemies, iconText)
		-- Todo: Add code to handle simcraft interrupt channel condition correctly.
		if not stopProcessing() and Object and useObject and useCooldown(Object) then
			local evaluateAction = Object.Evaluate();

			if validWaitTime(evaluateAction) then
				local checkbox = evalutateCheckBox(Object.CheckBox());

				numEnemies = setEnemies(numEnemies);

				if checkbox == 2 then
					if actionSingleTarget() then
						addon.Icons.ST.EvaluateInterruptCondition(rotationObject, Object, useObject, interruptCheck, 1, iconText, evaluateAction);
					end

					if actionMultiTarget() then
						addon.Icons.AOE.EvaluateInterruptCondition(rotationObject, Object, useObject, interruptCheck, numEnemies, iconText, evaluateAction);
					end
				elseif checkbox == 1 then
					if actionOffGCD(Object) then
						addon.Icons.OFFGCD.EvaluateInterruptCondition(rotationObject, Object, useObject, interruptCheck, numEnemies, iconText, evaluateAction);
					end

					if actionCooldown(Object) then
						addon.Icons.CD.EvaluateInterruptCondition(rotationObject, Object, useObject, interruptCheck, numEnemies, iconText, evaluateAction);
					end
				end
			end
		end
	end,

	EvaluatePoolAction = function(Object, useObject, numEnemies, iconText)
		-- Todo: Add code to handle simcraft pool resources condition correctly.
		if not stopProcessing() and Object and useObject and useCooldown(Object) then
			local evaluateAction = Object.Evaluate();

			if validWaitTime(evaluateAction) then
				local checkbox = evalutateCheckBox(Object.CheckBox());

				numEnemies = setEnemies(numEnemies);

				if checkbox == 2 then
					if actionSingleTarget() then
						addon.Icons.ST.EvaluatePoolAction(rotationObject, Object, useObject, 1, iconText, evaluateAction);
					end

					if actionMultiTarget() then
						addon.Icons.AOE.EvaluatePoolAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end
				elseif checkbox == 1 then
					if actionOffGCD(Object) then
						addon.Icons.OFFGCD.EvaluatePoolAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end

					if actionCooldown(Object) then
						addon.Icons.CD.EvaluatePoolAction(rotationObject, Object, useObject, numEnemies, iconText, evaluateAction);
					end
				end
			end
		end
	end,

	EvaluateWaitCondition = function(Object, waitCheck, numEnemies)
		-- Todo: Add code to handle simcraft wait condition correctly.
		if not stopProcessing() and Object and waitCheck and useCooldown(Object) then
			local checkbox = evalutateCheckBox(Object.CheckBox());
			numEnemies = setEnemies(numEnemies);

			if checkbox == 2 then
				if actionSingleTarget() then
					addon.Icons.ST.EvaluateWaitCondition(rotationObject, Object, waitCheck, 1);
				end

				if actionMultiTarget() then
					addon.Icons.AOE.EvaluateWaitCondition(rotationObject, Object, waitCheck, numEnemies);
				end
			elseif checkbox == 1 then
				if actionOffGCD(Object) then
					addon.Icons.OFFGCD.EvaluateWaitCondition(rotationObject, Object, waitCheck, numEnemies);
				end

				if actionCooldown(Object) then
					addon.Icons.CD.EvaluateWaitCondition(rotationObject, Object, waitCheck, numEnemies);
				end
			end
		end
	end,

	GetActiveRotation = function()
		local General = addon.API.General;
		local Rotation = addon.Rotation;
		local Tables = addon.Core.Tables;

		local specInfo = General.GetSpecializationInfo();

		if not Tables.IsEmpty(specInfo) then
			local profile = Tables.GetValue(addonTable.characterSettings, "ActiveRotations", {});
			local numRotations, rotationList = Rotation.GetRegisteredRotations(specInfo.ID);

			if numRotations == 1 then
				for key in pairs(rotationList) do
					profile[specInfo.Name] = key;

					-- Put the settings into the database
					addonTable.characterSettings.ActiveRotations = profile;

					return key;
				end
			elseif numRotations >= 2 then
				local returnValue = Tables.GetValue(profile, specInfo.Name);

				if not returnValue then
					returnValue = "__none";
					profile[specInfo.Name] = returnValue;

					-- Put the settings into the database
					addonTable.characterSettings.ActiveRotations = profile;
				end

				return returnValue;
			end
		end

		return nil;
	end,

	GetRegisteredRotations = function(classSpec)
		if classSpec then
			local Tables = addon.Core.Tables;
			local specRotations = addonTable.RegisteredRotations[classSpec];

			if specRotations and Tables.IsTable(specRotations) then
				return Tables.Count(specRotations), specRotations;
			end
		end

		return 0, nil;
	end,

	GetRotation = function(classSpec, rotationName)
		if classSpec then
			local Rotation = addon.Rotation;
			local Tables = addon.Core.Tables;

			local numRotations, rotationList = Rotation.GetRegisteredRotations(classSpec);

			rotationName = rotationName or Rotation.GetActiveRotation();

			if not Tables.IsEmpty(rotationList) and rotationName ~= nil then
				return Tables.GetValue(rotationList[rotationName], "callback");
			end
		end

		return nil;
	end,

	GetRotationObject = function(newRotation)
		return rotationObject;
	end,

	RegisterRotation = function(Rotation)
		local Tables = addon.Core.Tables;

		if Rotation then
			local classSpec = Tables.GetValue(Rotation, "SpecID", 0);
			local rotationName = Tables.GetValue(Rotation, "Name", "");
			local rotationDescription = Tables.GetValue(Rotation, "Description", "");

			local specRotations = addonTable.RegisteredRotations[classSpec] or {};

			specRotations[rotationName] = {
				callback = Rotation,
				description = rotationDescription,
			};

			addonTable.RegisteredRotations[classSpec] = specRotations;
		end
	end,

	RunActionList = function(Object, callFunction, numEnemies)
		if not stopProcessing() and Object then
			local useSingleTarget = actionSingleTarget() and useActionList(Object, callFunction, 1);
			local useMultiTarget = actionMultiTarget() and useActionList(Object, callFunction, numEnemies);

			if useSingleTarget or useMultiTarget then
				local previousRotation = rotationObject;

				rotationObject = Object;
				rotationObject.ProcessRotation(previousRotation);

				if useSingleTarget then
					addon.Icons.ST.Stopped(useSingleTarget);
				end

				if useMultiTarget then
					addon.Icons.AOE.Stopped(useMultiTarget);
				end
			end
		end
	end,

	SetActiveRotation = function(rotationName)
		if rotationName then
			local General = addon.API.General;
			local Tables = addon.Core.Tables;

			local specInfo = General.GetSpecializationInfo();

			if not Tables.IsEmpty(specInfo) then
				local profile = Tables.GetValue(addonTable.characterSettings, "ActiveRotations", {});

				profile[specInfo.Name] = rotationName;

				-- Put the settings into the database
				addonTable.characterSettings.ActiveRotations = profile;

				-- Send message so active rotation is changed to the selected one.
				Callbacks.Callback("rotationChanged");
			end
		end
	end,

	SetRotationObject = function(newRotation)
		rotationObject = newRotation;
	end,

	UnregisterRotation = function(Rotation)
		local Tables = addon.Core.Tables;

		if Rotation then
			local classSpec = Tables.GetValue(Rotation, "SpecID", 0);
			local rotationName = Tables.GetValue(Rotation, "Name", "");
			local specRotations = addonTable.RegisteredRotations[classSpec];

			if Tables.GetValue(specRotations, rotationName) then
				addonTable.RegisteredRotations[classSpec][rotationName] = nil;
			end
		end
	end,
};