-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Local variables needed for operation
local itemRequests = {};

local function getBagPosition(itemID)
	local NUM_BAG_SLOTS = NUM_BAG_SLOTS;

	for bag = 0, NUM_BAG_SLOTS do
		for slot = 1, addon.API.Blizzard.GetContainerNumSlots(bag) do
			if(addon.API.Blizzard.GetContainerItemID(bag, slot) == itemID) then
				return bag, slot;
			end
		end
	end

	return nil;
end

local function getEquippedSlot(itemID)
	local ID;
	-- Refresh Gear
	for counter = 1, 19 do
		ID = addon.API.Blizzard.GetInventoryItemID("player", counter);

		-- If there is an item in that slot
		if ID and ID == itemID then
			return counter;
		end
	end

	return nil;
end

local function getItemLink(itemID)
	local slotID = getEquippedSlot(itemID);
	local bag, slot = getBagPosition(itemID);

	if slotID then
		return addon.API.Blizzard.GetInventoryItemLink("player", slotID);
	elseif bag then
		return addon.API.Blizzard.GetContainerItemLink(bag, slot);
	end

	return nil;
end

-- This is stored on the addon so its publically available to all other addons.
addon.API.Item = {
	GetItemCooldown = function(itemID)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;

		local currentTime = addon.API.Blizzard.GetTime();
		local returnValue;

		if itemID then
			returnValue = Tables.GetValue(Cache.Temporary.Cooldowns.Item, itemID);

			if Tables.IsEmpty(returnValue) or currentTime - Tables.GetValue(returnValue, "CacheTime", 0) >= addon.refreshInterval then
				local startTime, duration, enabled = addon.API.Blizzard.GetItemCooldown(itemID);

				if startTime then
					if duration <= 1.5 then
						startTime = 0;
						duration = 0;
					end

					local endTime = startTime + duration;

					returnValue = {
						Start = startTime,
						End = endTime,
						Duration = duration,
						Locked = enabled == 0,
						CacheTime = currentTime,
					};

					Cache.Temporary.Cooldowns.Item[itemID] = returnValue;
				end
			end
		end

		return returnValue;
	end,

	GetItemInfo = function(itemID)
		local Cache = addonTable.Cache;
		local Tables = addon.Core.Tables;
		local Tooltip = addon.Core.Tooltip;

		local currentTime = addon.API.Blizzard.GetTime();
		local returnValue;

		if itemID then
			returnValue = Tables.GetValue(Cache.Temporary.ItemInfo, itemID);

			if Tables.IsEmpty(returnValue) or currentTime - Tables.GetValue(returnValue, "CacheTime", 0) >= addon.refreshInterval then
				local itemName, itemLink, itemRarity, itemLevel, itemMinLevel, itemType, itemSubType, itemStackCount, itemEquipSlot, itemIcon, itemSellPrice, _, _, bindType = addon.API.Blizzard.GetItemInfo(itemID);

				itemLink = getItemLink(itemID) or itemLink;
				itemLevel = Tooltip.GetItemLevel(itemLink, itemLevel);

				if itemName then
					returnValue = {
						ID = itemID,
						Name = itemName,
						Link = itemLink,
						Rarity = itemRarity,
						Level = itemLevel,
						MinLevel = itemMinLevel,
						Type = itemType,
						SubType = itemSubType,
						StackCount = itemStackCount,
						EquipSlot = itemEquipSlot,
						HasOnUse = addon.API.Blizzard.GetItemSpell(itemID) ~= nil,
						IsAvailable = addon.API.Blizzard.IsUsableItem(itemID) ~= nil,
						UseRange = addon.API.Blizzard.ItemHasRange(itemID) ~= nil,
						IconID = itemIcon,
						SellPrice = itemSellPrice,
						BindType = bindType,
						CacheTime = currentTime,
					};
				else
					returnValue = {};

					-- No information available, this means either the itemID is incorrect, or the item hasn't been seen since login and we need to wait for the data.
					itemRequests[itemID] = true;
				end

				Cache.Temporary.ItemInfo[itemID] = returnValue;
			end
		end

		return returnValue;
	end,
};

local function itemInfoReceived(...)
	local _, itemID = ...;

	if itemRequests[itemID] then
		itemRequests[itemID] = nil;
		addon.API.Item.GetItemInfo(itemID);
	end
end

local tableEvents = {
	GET_ITEM_INFO_RECEIVED = itemInfoReceived,
};

addon.System.Events.RegisterEvents(tableEvents, true);