-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- This is stored on the addon table so only the addon has access to it.
addonTable.Cache = {
	-- Temporary cache, cleared every refresh cycle
	Temporary = {
		Cooldowns = {
			Item = {},
			Spell = {},
		},

		ItemInfo = {},
		PowerInfo = {},
		SpellInfo = {},
		UnitInfo = {},
	},

	-- Persistant cache, changed only on events
	Persistant = {
		ActionInfo = {};
		Artifact = {},
		CastingInfo = {},
		EnemiesCount = {},
		Equipment = {},

		IncomingDamage = {
			All = {},
			Magic = {},
			Physical = {},
		},

		NetStats = {},

		Player = {
			Race = {},
			Class = {},
			Specialization = {},
			InstanceInfo = {},
		},

		ReplacedSpells = {},

		SpellLearned = {
			Pet = {},
			Player = {},
		},

		TimeToDie = {
			Refresh = {},
			Units = {},
		},
	},
};