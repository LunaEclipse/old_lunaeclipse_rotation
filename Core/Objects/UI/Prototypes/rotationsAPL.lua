-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Callbacks = addon.System.Callbacks;
local error, format, pairs, rawset, setmetatable, type, unpack = error, format, pairs, rawset, setmetatable, type, unpack;

local privateVariables = {};

local function createAzerites(sourceTable)
	local coreObjects = addon.Core.Objects;
	local coreTables = addon.Core.Tables;

	local returnValue;

	if not coreTables.IsEmpty(sourceTable) then
		returnValue = {};

		for key, data in pairs(sourceTable) do
			returnValue[key] = coreObjects.newAzerite(data);
		end
	end

	return returnValue;
end

local function createSpells(sourceTable)
	local coreObjects = addon.Core.Objects;
	local coreTables = addon.Core.Tables;

	local returnValue;

	if not coreTables.IsEmpty(sourceTable) then
		returnValue = {};

		for key, data in pairs(sourceTable) do
			if type(data) == "table" then
				returnValue[key] = coreObjects.newSpell(unpack(data));
			else
				returnValue[key] = coreObjects.newSpell(data);
			end
		end
	end

	return returnValue;
end

local function createTalents(sourceTable)
	local coreObjects = addon.Core.Objects;
	local coreTables = addon.Core.Tables;

	local returnValue;

	if not coreTables.IsEmpty(sourceTable) then
		returnValue = {};

		for key, data in pairs(sourceTable) do
			if type(data) == "table" then
				returnValue[key] = coreObjects.newTalent(unpack(data));
			else
				returnValue[key] = coreObjects.newTalent(data);
			end
		end
	end

	return returnValue;
end

local Alias, Overridable; -- These are unused features of OOP by this class.
local Cache = setmetatable({}, { __mode = "kv", __metatable = "This table is protected!", });
local Empty = {
	Combat = true,
	Defensive = true,
	Disable = true,
	Enable = true,
	Interrupt = true,
	Opener = true,
	Precombat = true,
	PrecombatVariables = true,
	SetupConfiguration = true,
};
local Prototype = {
	ClassType = function(self)
		return "BaseAPL";
	end,

	Description = function(self)
		return privateVariables[self.Key].Description;
	end,

	Enabled = function(self)
		return privateVariables[self.Key].Enabled;
	end,

	Name = function(self)
		return privateVariables[self.Key].Name;
	end,

	SpecID = function(self)
		return privateVariables[self.Key].SpecID;
	end,

	-- Function for processing conditional checks, should NEVER be overridden by the inherited rotations.
	ProcessCondition = function(self, useObject, numEnemies, evaluateTime, target)
		local Enemies = addon.Modules.Enemies;
		local Target = addon.Units.Target;

		if privateVariables[self.Key].Enabled then
			numEnemies = numEnemies or Enemies.GetEnemies();
			target = target or Target;

			addonTable.EvaluateTime = evaluateTime;

			local returnValue = useObject == true
					         or (type(useObject) == "function" and useObject(numEnemies, target));

			addonTable.EvaluateTime = nil;

			return returnValue;
		end

		return false;
	end,

	ProcessConfiguration = function(self)
		if privateVariables[self.Key].Enabled then
			local Configuration = addon.Options.Rotation;
			local Options = addon.Options.Core;
			local Tables = addon.Core.Tables;

			local configOptions = Configuration.CreateRotationOptions();

			self.SetupConfiguration(Configuration, configOptions);

			configOptions = Options.CleanupOptions(configOptions);

			addon.Options.rotationOptions.args = Tables.Duplicate(configOptions.args);
			Callbacks.Callback("optionsChanged", "LERH_RotationSettings");
		end
	end,

	-- Function for processing defensive spells, should NEVER be overridden by the inherited rotations.
	ProcessDefensive = function(self, action)
		if privateVariables[self.Key].Enabled then
			self.Defensive(action);
		end
	end,

	-- Function for processing disabling of the rotation, should NEVER be overridden by the inherited rotations.
	ProcessDisable = function(self)
		if privateVariables[self.Key].Enabled then
			privateVariables[self.Key].Enabled = false;
			self.Disable();
		end
	end,

	-- Function for processing enabling of the rotation, should NEVER be overridden by the inherited rotations.
	ProcessEnable = function(self, action)
		local coreTables = addon.Core.Tables;

		if not privateVariables[self.Key].Enabled then
			local objectAzerite = createAzerites(addonTable.AzeriteData);
			local objectRacial = createSpells(addonTable.RacialData);
			local objectSpell = createSpells(addonTable.SpellData);
			local objectTalent = createTalents(addonTable.TalentData);

			-- Enable the rotations
			self.Enable(objectRacial, objectSpell, objectTalent, coreTables.Duplicate(addonTable.AuraData), objectAzerite);
			privateVariables[self.Key].Enabled = true;

			-- Set up the rotations configuration options.
			self.ProcessConfiguration();

			-- Ensure Precombat Variables are run at least once to initialize.
			self.PrecombatVariables(action);
		end
	end,

	-- Function for processing interrupts, should NEVER be overridden by the inherited rotations.
	ProcessInterrupt = function(self, action)
		local Target = addon.Units.Target;

		if privateVariables[self.Key].Enabled and Target.Casting.IsInterruptible() then
			-- If the target is casting an interruptable spell, call the Interrupt rotation.
			self.Interrupt(action);
		end
	end,

	-- Function for setting any pre-combat variables, is always called even if you don't have a target.
	ProcessPrecombatVariables = function(self, action)
		if privateVariables[self.Key].Enabled then
			self.PrecombatVariables(action);
		end
	end,

	-- Function for processing opening rotation, should never be overrideen by the inherited rotations.
	-- Should set up the opening rotation using self.Opener, this is used only for processing icon display.
	ProcessOpener = function(self)
		if privateVariables[self.Key].Enabled then
			local Tables = addon.Core.Tables;

			-- If the addon is set to run an opener but there is no spells left in the current opener switch back to normal mode.
			if Tables.IsEmpty(addonTable.Opener) then
				addonTable.OpenerRunning = false;
			end

			-- Todo: Logic for displaying icons based on settings in self.Opener
		end
	end,

	-- Function for controling which rotation function to call, should NEVER be overridden by the inherited rotations.
	ProcessRotation = function(self)
		local Core = addon.Core.General;
		local Rotation = addon.Rotation;
		local Tables = addon.Core.Tables;
		local Unit = addon.API.Unit;

		if privateVariables[self.Key].Enabled then
			-- Process the opening rotation to make suggestions.
			if addonTable.OpenerRunning and not Tables.IsEmpty(addonTable.Opener) then
				self.ProcessOpener(Rotation);
			end

			-- Check if the player is in combat.
			if Unit.InCombat() then
				-- Run the in combat rotation if not currently processing an opening rotation.
				if not addonTable.OpenerRunning then
					self.Combat(Rotation);
				end
			-- Player is not in combat.
			else
				-- Call the Opening rotation setup if a pull timer has been sent and not currently running an opening rotation.
				if not addonTable.OpenerRunning and Core.PullTimer() then
					self.Opener(Rotation);
				end

				-- Run the pre-combat rotation if not currently processing an opening rotation.
				if not addonTable.OpenerRunning then
					self.Precombat(Rotation);
				end
			end
		end
	end,
};

local InstanceMT = {
	__index = function(table, key)
		local Tables = addon.Core.Tables;

		local classKey = Tables.GetValue(Alias, key, key);
		local prototypeEntity = Prototype[classKey];

		if prototypeEntity then
			if type(prototypeEntity) == "function" then
				return function(...)
					return prototypeEntity(table, ...);
				end
			else
				return prototypeEntity;
			end
		elseif Tables.GetValue(Empty, key, false) then
			return function()
			end
		end

		return nil;
	end,

	__newindex = function(table, key, value)
		local Tables = addon.Core.Tables;

		local classKey = Tables.GetValue(Alias, key, key);
		local prototypeEntity = Prototype[classKey];

		if Tables.GetValue(Overridable, key, false) or not prototypeEntity then
			rawset(table, key, value);
		else
			local nameAPL = Tables.GetValue(table, "APL", "");

			error(format("Attempted to overwrite protected function %s in %s main APL!", key, nameAPL));
		end
	end,

	__metatable = "This table is protected!",
};
local ClassMT = {
	__call = function(table, name, description, specID)
		local Rotation = addon.Rotation;

		if Cache[name] then
			return Cache[name];
		else
			local self = setmetatable({ Key = name }, InstanceMT);

			self.scriptInfo = {};
			self.presetBuilds = {};

			privateVariables[name] = {
				Name = name,
				Description = description,
				SpecID = specID or 0,
				Enabled = false,
			};

			Rotation.RegisterRotation(self);

			Cache[name] = self;
			return self;
		end
	end,

	__metatable = "This table is protected!",
};

addonTable.rotationsAPL = setmetatable({}, ClassMT);