-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local function ParseHyperlink(hyperlink)
	local strmatch = strmatch;
	local color, linkType, linkData, text = strmatch(hyperlink, "|?c?f?f?(%x*)|?H?([^:]*):?(%d*):?%d?|?h?%[?([^%[%]]*)%]?|?h?|?r?");

	return color, linkType, linkData, text;
end

local function scanFlyOutSpells()
	local select = select;

	local returnValue = {};
	local numFlyouts = addon.API.Blizzard.GetNumFlyouts();
	local flyoutID, isKnown, maxRange, numSlots, spellID, spellKnown;

	if numFlyouts then
		for flyoutCounter = 1, numFlyouts do
			flyoutID = addon.API.Blizzard.GetFlyoutID(flyoutCounter);
			numSlots, isKnown = select(3, addon.API.Blizzard.GetFlyoutInfo(flyoutID));

			if isKnown and numSlots >= 1 then
				for spellCounter = 1, numSlots do
					spellID, spellKnown = select(2, addon.API.Blizzard.GetFlyoutSlotInfo(flyoutID, spellCounter));

					if spellKnown then
						maxRange = select(6, addon.API.Blizzard.GetSpellInfo(spellID));

						addonTable.nameplateDistance = (maxRange ~= nil and maxRange > addonTable.nameplateDistance and maxRange < 100 and maxRange) or addonTable.nameplateDistance;

						returnValue[spellID] = true;
					end
				end
			end
		end
	end

	return returnValue;
end

local function scanPetSpells()
	local BOOKTYPE_PET, select, tonumber = BOOKTYPE_PET, select, tonumber;

	local returnValue = {};
	local numPetSpells = addon.API.Blizzard.HasPetSpells();
	local linkData, skillType, spellID, spellLink
	if numPetSpells then
		for counter = 1, numPetSpells do
			skillType = addon.API.Blizzard.GetSpellBookItemInfo(counter, BOOKTYPE_PET);

			if skillType == "PETACTION" then
				spellLink = addon.API.Blizzard.GetSpellLink(counter, BOOKTYPE_PET);

				if spellLink then
					linkData = select( 3, ParseHyperlink(spellLink));
					spellID = tonumber(linkData);

					if spellID then
						returnValue[spellID] = true;
					end
				end
			end
		end
	end

	return returnValue;
end

local function scanPlayerSpells()
	local BOOKTYPE_SPELL, select, tonumber = BOOKTYPE_SPELL, select, tonumber;

	local returnValue = {};
	local numSpellTabs = addon.API.Blizzard.GetNumSpellTabs();
	local linkData, numSpells, offset, offSpec, skillType, spellID, spellLink, _;

	if numSpellTabs then
		for counter = 1, numSpellTabs do
			offset, numSpells, _, offSpec = select(3, addon.API.Blizzard.GetSpellTabInfo(counter));

			-- If the OffSpec ID is set to 0, then it's the Main Spec.
			if offSpec == 0 then
				for spellCounter = (offset + 1), (offset + numSpells) do
					skillType = addon.API.Blizzard.GetSpellBookItemInfo(spellCounter, BOOKTYPE_SPELL);

					if skillType == "SPELL" then
						spellLink = addon.API.Blizzard.GetSpellLink(spellCounter, BOOKTYPE_SPELL);

						if spellLink then
							linkData = select(3, ParseHyperlink(spellLink));
							spellID = tonumber(linkData);

							if spellID then
								returnValue[spellID] = true;
							end
						end
					end
				end
			end
		end
	end

	return returnValue;
end

local function spellsFound(spellsLearned)
	local Tables = addon.Core.Tables;

	return not Tables.IsEmpty(spellsLearned.Player) or not Tables.IsEmpty(spellsLearned.Pet);
end

local function spellsChanged()
	local Cache = addonTable.Cache;
	local Tables = addon.Core.Tables;

	-- Reset max range for spells
	addonTable.nameplateDistance = 0;

	local petSpells = scanPetSpells();
	local playerSpells = scanPlayerSpells();
	local flyoutSpells = scanFlyOutSpells();

	local spellsLearned = {
		Player = Tables.Merge(playerSpells, flyoutSpells),
		Pet = Tables.Duplicate(petSpells),
	};

	if spellsFound(spellsLearned) and not Tables.Equate(Cache.Persistant.SpellLearned, spellsLearned) then
		Cache.Persistant.SpellLearned = Tables.Duplicate(spellsLearned);

		if addon.currentRotation then
			addon.currentRotation.ProcessConfiguration();
		end
	end
end

local moduleEvents = {
	playerLogin = function()
		spellsChanged();
	end,
};

local tableEvents = {
	SPELLS_CHANGED = spellsChanged,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);
addon.System.Events.RegisterEvents(tableEvents, true);