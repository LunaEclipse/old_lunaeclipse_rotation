-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
-- Create Addon and Store it in the Global Namespace
local addon = LibStub("AceAddon-3.0"):NewAddon(addonName, "AceEvent-3.0", "AceHook-3.0", "AceTimer-3.0");
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

_G[addonName] = addon;

-- Store local copies of Blizzard API and other addon global functions and variables
local GetBuildInfo = GetBuildInfo;
local max, select, strjoin = max, select, strjoin;

-- Current Versions
addon.CURRENT_ADDON_VERSION = 8000106;
addon.CURRENT_WOW_VERSION = max(select(4, GetBuildInfo()), 80001);

-- Public addon namespaces
addon.API = {};
addon.Core = {};
addon.Features = {};
addon.Modules = {};
addon.Objects = {};
addon.Options = {};
addon.System = {};

-- Tables to hold unit objects
addon.Units = {};
addon.NameplateUnits = {};

-- Set the current rotation to nil, this is done just in case there is a problem.
addon.currentRotation = nil;

-- Set a default refresh rate and reaction time in case a function is called using them before the variables are loaded.
addon.refreshInterval = 0.1;
addon.reactTime = 1;

--- Retrieve information about the addon.
-- @param stripTags: boolean - Specifies whether the returned information should be free of text formating tags.
-- @return addonTite: string - The title of the addon.
-- @return addonAuthor: string - The author of the addon.
-- @return addonVersion: string = The version of the addon in string format such as X.Y.Z
function addon.GetInfo(stripTags)
	-- Must be stored here because this file is loaded before the Blizzard API wrapper.
	local Blizzard = addon.API.Blizzard;

	local addonAuthor = Blizzard.GetAddOnMetadata(addonName, "Author");
	local addonTitle = Blizzard.GetAddOnMetadata(addonName, "Title");
	local addonVersion = Blizzard.GetAddOnMetadata(addonName, "Version");

	addonTable.Author = addonTable.Author or strjoin("", addonTable.COLORS.TAGS.MESSAGE, addonAuthor, addonTable.TAGS.END);
	addonTable.AuthorStripped = addonTable.AuthorStripped or addonAuthor;

	addonTable.Title = addonTable.Title or strjoin("", addonTable.COLORS.TAGS.HIGHLIGHT, addonTitle, addonTable.TAGS.END);
	addonTable.TitleStripped = addonTable.TitleStripped or addonTitle;

	addonTable.Version = addonTable.Version or strjoin("", addonTable.COLORS.TAGS.HIGHLIGHT, addonVersion, addonTable.TAGS.END);
	addonTable.VersionStripped = addonTable.VersionStripped or addonVersion;

	if stripTags then
		return addonTable.TitleStripped, addonTable.AuthorStripped, addonTable.VersionStripped;
	end

	return addonTable.Title, addonTable.Author, addonTable.Version;
end

-- Internal addonTable namespaces
addonTable.Enum = {};
addonTable.Cache = {};
addonTable.Data = {
	Azerite = {
		Class = {},
	},
	Class = {},
	Racial = {},
};

-- Internal addonTable function for handling for raised events
function addonTable.raisedEvents(event, ...)
	addonTable.callbacksHandler(event, ...);
	addonTable.eventsHandler(event, ...);
end