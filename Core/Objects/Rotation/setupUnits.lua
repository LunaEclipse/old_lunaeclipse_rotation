-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Objects = addon.Core.Objects;
local format, pairs, strlower = format, pairs, strlower;

-- Iterable Units
local UnitIDMap = {
	["Arena"] = 5,
	["Boss"] = 4,
	["Party"] = 4,
	["Raid"] = 40,
	["Nameplate"] = 40,
};

-- For WoW events that give UnitID, but that UnitID is not associated with an object a EmptyUnit object is needed.
--
-- When referencing an object for a unitID not tracked it will try to return a tracked object using a different unitID for the same unit.
-- For example if the players pet is attacking the players target, it will use the addon.Units.Target object, because they are the same unit.
--
-- If it can't find a tracked object for the same unit it will use the EmptyUnit object so that addon will use default unit values.
-- This is so it gives default values for functions such as addon.Units.PetTarget.Exists() == false;
addonTable.EmptyUnit = Objects.newUnit("none");

-- The following sets up the tracked unitID objects.
addon.Units.Player = Objects.newUnit("player");
addon.Units.Pet = Objects.newUnit("pet");
addon.Units.Target = Objects.newUnit("target");
addon.Units.Focus = Objects.newUnit("focus");
addon.Units.MouseOver = Objects.newUnit("mouseover");
addon.Units.Vehicle = Objects.newUnit("vehicle");

for key, value in pairs(UnitIDMap) do
	for counter = 1, value do
		local unitID = format("%s%d", key, counter);
		local currentUnit = Objects.newUnit(strlower(unitID))

		addon.Units[unitID] = currentUnit;

		if key == "Nameplate" then
			addon.NameplateUnits[unitID] = currentUnit;
		end
	end
end

Objects.FinalizeUnits(addon.NameplateUnits, addon.Units);