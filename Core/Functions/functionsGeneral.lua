-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- This is stored on the addon so its publically available to all other addons.
addon.Core.General = {
	-- Get the time since combat has started.
	CombatTime = function()
		return addonTable.Combat.Start ~= 0 and addon.API.General.GetTime() - addonTable.Combat.Start or 0;
	end,

	-- Get the desired targets, this is permanent targets, usually targets with a boss frame.
	DesiredTargets = function()
		local ipairs = ipairs;
		local BossUnits = {	"boss1", "boss2", "boss3", "boss4" };
		local returnValue = 0;

		for _, unitID in ipairs(BossUnits) do
			if addon.Units[unitID].Exists() then
				returnValue = returnValue + 1;
			end
		end

		return returnValue;
	end,

	-- Get the SpellID of the spell, returns a replacement ID if it was replaced.
	-- Parameters:
	--		spellID - The base SpellID to check for replacement.
	GetSpellReplacementID = function(spellID)
		local spellName = addon.API.Blizzard.GetSpellInfo(spellID);
		local newSpellName, _, _, _, _, _, newSpellID = addon.API.Blizzard.GetSpellInfo(spellName);

		return (spellName ~= nil and newSpellID ~= nil and newSpellName ~= nil and newSpellID ~= spellID and newSpellName == spellName and newSpellID)
			or spellID;
	end,

	IsDungeonBossFight = function()
		local ipairs = ipairs;

		if addon.Units.Player.DungeonInstance() then
			local BossUnits = {	"boss1", "boss2", "boss3", "boss4" };

			for _, unitID in ipairs(BossUnits) do
				if addon.Units[unitID].Exists() then
					return true;
				end
			end
		end

		return false;
	end,

	IsRaidBossFight = function()
		if addon.Units.Player.RaidInstance() then
			local ipairs = ipairs;
			local BossUnits = {	"boss1", "boss2", "boss3", "boss4" };

			for _, unitID in ipairs(BossUnits) do
				if addon.Units[unitID].Exists() then
					return true;
				end
			end
		else
			return (addon.Units.Target.Exists() and (addon.Units.Target.Classification() == "worldboss" or addon.Units.Target.Level("target") < 0))
				or false;
		end

		return false;
	end,

	-- Get the world server latency
	Latency = function()
		local Tables = addon.Core.Tables;
		local netStats = addon.API.General.GetNetStats();

		return Tables.GetValue(netStats, "WorldLatency", 0);
	end,

	-- Get the time since combat has ended.
	OutOfCombatTime = function()
		return addonTable.Combat.End ~= 0 and addon.API.General.GetTime() - addonTable.Combat.End
			or 0;
	end,

	-- Get the Boss Mod Pull Timer.
	PullTimer = function()
		local returnValue;

		if addonTable.PullTimer.Duration then
			returnValue = addonTable.PullTimer.End - addon.API.General.GetTime();

			if returnValue < 0 then
				returnValue = nil;
			end
		end

		return returnValue;
	end,

	ToBoolean = function(value)
		local tonumber, type = tonumber, type;

		if type(value) == "nil" then
			return false;
		elseif type(value) == "boolean" then
			return value;
		else
			local number = tonumber(value);

			if number then
				return number ~= 0;
			else
				return true;
			end
		end
	end,

	ToNumber = function(value)
		local tonumber, type = tonumber, type;

		if type(value) == "nil" then
			return 0;
		elseif type(value) == "boolean" then
			return value and 1 or 0;
		else
			return tonumber(value);
		end
	end,
};