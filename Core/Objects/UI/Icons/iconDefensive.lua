-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copy of Blizzard API wrapper and other addon global functions and variables
local Blizzard, Callbacks, Events = addon.API.Blizzard, addon.System.Callbacks, addon.System.Events;
local error, format, setmetatable, sort, tinsert, type, wipe = error, format, setmetatable, sort, tinsert, type, wipe;

local rotationIndex = 0;
local actions = {};
local evaluateActions = {};

-- Table of CLEU events for when a unit is removed from combat.
local CLEU_UNIT_REMOVED = {
	UNIT_DESTROYED = true,
	UNIT_DIED = true,
	UNIT_DISSIPATES = true,
};

local function evalutateCheckBox(checkbox)
	local Settings = addon.Core.Settings;

	return (checkbox == nil and 2)
		or (type(checkbox) == "boolean" and checkbox == true and 2)
		or (type(checkbox) == "boolean" and checkbox == false and 1)
		or Settings.GetCharacterValue("ScriptOptions", checkbox)
		or Settings.GetRotationValue(checkbox);
end

local function sortActions(a, b)
	if a.WaitTime == b.WaitTime then
		return a.RotationIndex < b.RotationIndex;
	end

	return a.WaitTime < b.WaitTime;
end

local Prototype = {
	Actions = function()
		return actions;
	end,

	EvaluateDefensiveAction = function(Object, useObject, checkbox)
		local ActionBar = addon.Modules.ActionBar;
		local Objects = addon.Core.Objects;
		local Spell = addon.API.Spell;
		local Tables = addon.Core.Tables;

		local currentRotation = addon.currentRotation;
		local evaluateAction = Object.Evaluate();

		rotationIndex = rotationIndex + 1;

		if Objects.IsSpell(Object) and Tables.GetValue(evaluateAction, "Use", false)
								   and Object.Charges() >= 1
								   and evalutateCheckBox(checkbox or Object.CheckBox()) >= 1
								   and currentRotation.ProcessCondition(useObject) then
			local spellID = Object.SpellID();
			local objectInfo = Spell.GetSpellInfo(spellID);

			if not Tables.IsEmpty(objectInfo) then
				local iconInfo = {
					ID = spellID,
					Name = objectInfo.Name,
					Texture = objectInfo.IconID,
					Text = "",
					Type = "Spell",
					Keybind = ActionBar.Keybind("Spell", spellID),
					Wait = false,
					WaitStart = 0,
					WaitEnd = 0,
					WaitTime = 0;
					RotationIndex = rotationIndex,
				};

				tinsert(evaluateActions, iconInfo);
			end
		end
	end,
};

local ClassMT = {
	__index = Prototype,

	__newindex = function(table, key, value)
		error(format("Attempted to overwrite protected function %s in the Defensive Icon object!", key));
	end,

	__metatable = "This table is protected!",
};

addonTable.iconDefensive = setmetatable({}, ClassMT);

local function timerEvent()
	local Debug = addonTable.Debug;
	local Tables = addon.Core.Tables;
	local Target = addon.Units.Target;

	local currentRotation = addon.currentRotation;
	local iconDefensive = addonTable.iconDefensive;

	if not Debug then
		if Target.IsValidTarget() and currentRotation then
			rotationIndex = 0;
			evaluateActions = {};

			currentRotation.ProcessDefensive(iconDefensive);
			sort(evaluateActions, sortActions);

			wipe(actions);
			actions = Tables.Duplicate(evaluateActions);

			Callbacks.Callback("DefensiveIcon", actions);

			evaluateActions = nil;
		else
			wipe(actions);

			Callbacks.Callback("DefensiveIcon");
		end
	end
end

local function enterCombat()
	local systemTimers = addon.System.Timers;
	local currentRotation = addon.currentRotation;

	if currentRotation and not systemTimers.TimerExists(timerEvent) then
		timerEvent();
		systemTimers.TimerAdd(timerEvent, 1);
	end
end

local function leaveCombat()
	local systemTimers = addon.System.Timers;

	systemTimers.TimerRemove(timerEvent);
	wipe(actions);

	Callbacks.Callback("DefensiveIcon");
end

local function combatLog(...)
	local Unit = addon.API.Unit;
	local _, _, combatEvent, _, _, _, _, _, destGUID = ...;

	if CLEU_UNIT_REMOVED[combatEvent] and destGUID == Blizzard.UnitGUID("target") and not Unit.InCombat() then
		leaveCombat();
	end
end

local function targetChanged()
	local apiUnit = addon.API.Unit;
	local Target = addon.Units.Target;

	if Target.IsValidTarget(true) then
		enterCombat();
	elseif not apiUnit.InCombat() then
		leaveCombat();
	end
end

local moduleEvents = {
	Disabled = function()
		leaveCombat();
	end,

	Enabled = function()
		local currentRotation = addon.currentRotation;
		local apiUnit = addon.API.Unit;

		if currentRotation and apiUnit.InCombat() then
			enterCombat();
		end
	end,
};

local tableEvents = {
	COMBAT_LOG_EVENT_UNFILTERED = combatLog,
	INSTANCE_ENCOUNTER_ENGAGE_UNIT = enterCombat,
	PLAYER_REGEN_DISABLED = enterCombat,
	PLAYER_REGEN_ENABLED = leaveCombat,
	PLAYER_TARGET_CHANGED = targetChanged,
};

Callbacks.RegisterAddonCallbacks(moduleEvents);
Events.RegisterEvents(tableEvents);