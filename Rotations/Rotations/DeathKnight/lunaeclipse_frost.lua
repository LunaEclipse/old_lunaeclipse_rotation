local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local val = coreGeneral.ToNumber;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "lunaeclipse_deathknight_frost";

-- AOE Rotation
local function AOE(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.aoe+=/frostscythe,if=buff.killing_machine.up
		Frostscythe = function()
			return Player.Buff(Aura.KillingMachine).Up();
		end,

		FrostStrike = {
			-- actions.aoe+=/frost_strike,if=cooldown.remorseless_winter.remains<=2*gcd&talent.gathering_storm.enabled
			GatheringStorm = function()
				return Spell.RemorselessWinter.Cooldown.Remains() <= 2 * Player.GCD()
				   and Talent.GatheringStorm.Enabled();
			end,

			-- actions.aoe+=/frost_strike,if=runic_power.deficit<(15+talent.runic_attenuation.enabled*3)
			Use = function()
				return Player.RunicPower.Deficit() < (15 + val(Talent.RunicAttenuation.Enabled()) * 3);
			end,
		},

		GlacialAdvance = {
			-- actions.aoe+=/glacial_advance,if=talent.frostscythe.enabled
			Frostscythe = function()
				return Talent.Frostscythe.Enabled();
			end,

			-- actions.aoe+=/glacial_advance,if=runic_power.deficit<(15+talent.runic_attenuation.enabled*3)
			Use = function()
				return Player.RunicPower.Deficit() < (15 + val(Talent.RunicAttenuation.Enabled()) * 3);
			end,
		},

		-- actions.aoe+=/howling_blast,if=buff.rime.up
		HowlingBlast = function()
			return Player.Buff(Aura.Rime).Up();
		end,

		-- actions.aoe+=/obliterate,if=runic_power.deficit>(25+talent.runic_attenuation.enabled*3)
		Obliterate = function()
			return Player.RunicPower.Deficit() > (25 + val(Talent.RunicAttenuation.Enabled()) * 3);
		end,

		-- actions.aoe=remorseless_winter,if=talent.gathering_storm.enabled
		RemorselessWinter = function()
			return Talent.GatheringStorm.Enabled();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.RemorselessWinter, self.Requirements.RemorselessWinter);
		action.EvaluateAction(Talent.GlacialAdvance, self.Requirements.GlacialAdvance.Frostscythe);
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike.GatheringStorm);
		action.EvaluateAction(Spell.HowlingBlast, self.Requirements.HowlingBlast);
		action.EvaluateAction(Talent.Frostscythe, self.Requirements.Frostscythe);
		action.EvaluateAction(Talent.GlacialAdvance, self.Requirements.GlacialAdvance.Use);
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike.Use);
		-- actions.aoe+=/remorseless_winter
		action.EvaluateAction(Spell.RemorselessWinter, true);
		-- actions.aoe+=/frostscythe
		action.EvaluateAction(Talent.Frostscythe, true);
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate);
		-- actions.aoe+=/glacial_advance
		action.EvaluateAction(Talent.GlacialAdvance, true);
		-- actions.aoe+=/frost_strike
		action.EvaluateAction(Spell.FrostStrike, true);
		-- actions.aoe+=/horn_of_winter
		action.EvaluateAction(Talent.HornOfWinter, true);
		-- actions.aoe+=/arcane_torrent
		action.EvaluateAction(Racial.ArcaneTorrent, true);
	end

	-- actions+=/run_action_list,name=aoe,if=active_enemies>=2
	function self.Use(numEnemies)
		return numEnemies >= 2;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationAOE = AOE("AOE");

-- BOSPooling Rotation
local function BOSPooling(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.bos_pooling+=/frostscythe,if=buff.killing_machine.up&runic_power.deficit>(15+talent.runic_attenuation.enabled*3)
		Frostscythe = function()
			return Player.Buff(Aura.KillingMachine).Up()
			   and Player.RunicPower.Deficit() > (15 + val(Talent.RunicAttenuation.Enabled()) * 3);
		end,

		FrostStrike = {
			-- actions.bos_pooling+=/frost_strike,if=runic_power.deficit<20&cooldown.pillar_of_frost.remains>rune.time_to_4
			RunicPower = function()
				return Player.RunicPower.Deficit() < 20
				   and Spell.PillarOfFrost.Cooldown.Remains() > Player.Runes.TimeToX(4);
			end,

			-- actions.bos_pooling+=/frost_strike,if=cooldown.pillar_of_frost.remains>rune.time_to_4&runic_power.deficit<40
			Use = function()
				return Spell.PillarOfFrost.Cooldown.Remains() > Player.Runes.TimeToX(4)
				   and Player.RunicPower.Deficit() < 40
			end,
		},

		-- actions.bos_pooling+=/glacial_advance,if=runic_power.deficit<20&cooldown.pillar_of_frost.remains>rune.time_to_4
		GlacialAdvance = {
			-- actions.bos_pooling+=/glacial_advance,if=cooldown.pillar_of_frost.remains>rune.time_to_4&runic_power.deficit<40&spell_targets.glacial_advance>=2
			AOE = function(numEnemies)
				return Spell.PillarOfFrost.Cooldown.Remains() > Player.Runes.TimeToX(4)
				   and Player.RunicPower.Deficit() < 40
			       and numEnemies >= 2;
			end,

			-- actions.bos_pooling+=/glacial_advance,if=runic_power.deficit<20&cooldown.pillar_of_frost.remains>rune.time_to_4
			Use = function()
				return Player.RunicPower.Deficit() < 20
				   and Spell.PillarOfFrost.Cooldown.Remains() > Player.Runes.TimeToX(4);
			end,
		},

		-- actions.bos_pooling=howling_blast,if=buff.rime.up
		HowlingBlast = function()
			return Player.Buff(Aura.Rime).Up();
		end,

		Obliterate = {
			-- actions.bos_pooling+=/obliterate,if=rune.time_to_4<gcd&runic_power.deficit>=25
			Runes = function()
				return Player.Runes.TimeToX(4) < Player.GCD()
				   and Player.RunicPower.Deficit() >= 25;
			end,

			-- actions.bos_pooling+=/obliterate,if=runic_power.deficit>=(25+talent.runic_attenuation.enabled*3)
			Use = function()
				return Player.RunicPower.Deficit() >= (25 + val(Talent.RunicAttenuation.Enabled()) * 3);
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.HowlingBlast, self.Requirements.HowlingBlast);
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.Runes);
		action.EvaluateAction(Talent.GlacialAdvance, self.Requirements.GlacialAdvance.Use);
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike.RunicPower);
		action.EvaluateAction(Talent.Frostscythe, self.Requirements.Frostscythe);
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.Use);
		action.EvaluateAction(Talent.GlacialAdvance, self.Requirements.GlacialAdvance.AOE);
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike.Use);
	end

	-- actions+=/run_action_list,name=bos_pooling,if=talent.breath_of_sindragosa.enabled&cooldown.breath_of_sindragosa.remains<5
	function self.Use()
		return Talent.BreathOfSindragosa.Enabled()
		   and Talent.BreathOfSindragosa.Cooldown.Remains() < 5;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationBOSPooling = BOSPooling("BOSPooling");

-- BOSTicking Rotation
local function BOSTicking(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.bos_ticking+=/arcane_torrent,if=runic_power.deficit>20
		ArcaneTorrent = function()
			return Player.RunicPower.Deficit() > 20;
		end,

		Frostscythe = {
			-- actions.bos_ticking+=/frostscythe,if=spell_targets.frostscythe>=2
			AOE = function(numEnemies)
				return numEnemies >= 2;
			end,

			-- actions.bos_ticking+=/frostscythe,if=buff.killing_machine.up
			Use = function()
				return Player.Buff(Aura.KillingMachine).Up();
			end,
		},

		-- actions.bos_ticking+=/horn_of_winter,if=runic_power.deficit>=30&rune.time_to_3>gcd
		HornOfWinter = function()
			return Player.RunicPower.Deficit() >= 30
			   and Player.Runes.TimeToX(3) > Player.GCD();
		end,

		-- actions.bos_ticking+=/howling_blast,if=buff.rime.up
		HowlingBlast = function()
			return Player.Buff(Aura.Rime).Up();
		end,

		Obliterate = {
			-- actions.bos_ticking+=/obliterate,if=rune.time_to_5<gcd|runic_power<=45
			Runes = function()
				return Player.Runes.TimeToX(5) < Player.GCD()
				    or Player.RunicPower() <= 45;
			end,

			-- actions.bos_ticking=obliterate,if=runic_power<=30
			Starved = function()
				return Player.RunicPower() <= 30;
			end,

			-- actions.bos_ticking+=/obliterate,if=runic_power.deficit>25|rune>3
			Use = function()
				return Player.RunicPower.Deficit() > 25
				    or Player.Runes() > 3;
			end,
		};

		-- actions.bos_ticking+=/remorseless_winter,if=talent.gathering_storm.enabled
		RemorselessWinter = function()
			return Talent.GatheringStorm.Enabled();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.Starved);
		action.EvaluateAction(Spell.RemorselessWinter, self.Requirements.RemorselessWinter);
		action.EvaluateAction(Spell.HowlingBlast, self.Requirements.HowlingBlast);
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.Runes);
		action.EvaluateAction(Talent.Frostscythe, self.Requirements.Frostscythe.Use);
		action.EvaluateAction(Talent.HornOfWinter, self.Requirements.HornOfWinter);
		-- actions.bos_ticking+=/remorseless_winter
		action.EvaluateAction(Spell.RemorselessWinter, true);
		action.EvaluateAction(Talent.Frostscythe, self.Requirements.Frostscythe.AOE, Enemies.GetEnemies(8));
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.Use);
		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
	end

	-- actions+=/run_action_list,name=bos_ticking,if=dot.breath_of_sindragosa.ticking
	function self.Use()
		return Player.Buff(Aura.BreathOfSindragosa).Ticking();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationBOSTicking = BOSTicking("BOSTicking");

-- ColdHeart Rotation
local function ColdHeart(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		ChainsOfIce = {
			-- Skip the legendary check as legendaries stop working after level 115 so this really only applies to the pre-patch.
			-- actions.cold_heart=chains_of_ice,if=(buff.cold_heart_item.stack>5|buff.cold_heart_talent.stack>5)&target.time_to_die<gcd
			Dying = function()
				return Player.Buff(Aura.ColdHeart).Stack() > 5
				   and Target.TimeToDie() < Player.GCD();
			end,

			-- actions.cold_heart+=/chains_of_ice,if=(buff.pillar_of_frost.remains<=gcd*(1+cooldown.frostwyrms_fury.ready)|buff.pillar_of_frost.remains<rune.time_to_3)&buff.pillar_of_frost.up
			Use = function()
				return (Player.Buff(Aura.PillarOfFrost).Remains() <= Player.GCD() * (1 + val(Talent.FrostwyrmsFury.Cooldown.Ready())) or Player.Buff(Aura.PillarOfFrost).Remains() < Player.Runes.TimeToX(3))
				   and Player.Buff(Aura.PillarOfFrost).Up();
			end,
		};
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.ChainsOfIce, self.Requirements.ChainsOfIce.Dying);
		action.EvaluateAction(Spell.ChainsOfIce, self.Requirements.ChainsOfIce.Use);
	end

	-- Skip the legendary check as legendaries stop working after level 115 so this really only applies to the pre-patch.
	-- actions.cooldowns+=/call_action_list,name=cold_heart,if=(equipped.cold_heart|talent.cold_heart.enabled)&(((buff.cold_heart_item.stack>=10|buff.cold_heart_talent.stack>=10)&debuff.razorice.stack=5)|target.time_to_die<=gcd)
	function self.Use()
		return Talent.ColdHeart.Enabled()
		   and ((Player.Buff(Aura.ColdHeart).Stack() >= 10 and Target.Debuff(Aura.Razorice).Stack() == 5) or Target.TimeToDie() <= Player.GCD());
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationColdHeart = ColdHeart("ColdHeart");

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cooldowns+=/berserking,if=buff.pillar_of_frost.up
		Berserking = function()
			return Player.Buff(Aura.PillarOfFrost).Up();
		end,

		-- actions.cooldowns+=/blood_fury,if=buff.pillar_of_frost.up&buff.empower_rune_weapon.up
		BloodFury = function()
			return Player.Buff(Aura.PillarOfFrost).Up()
			   and Player.Buff(Aura.EmpowerRuneWeapon).Up();
		end,

		EmpowerRuneWeapon = {
			-- actions.cooldowns+=/empower_rune_weapon,if=cooldown.pillar_of_frost.ready&talent.breath_of_sindragosa.enabled&rune>=3&runic_power>60
			BreathOfSindragosa = function()
				return Spell.PillarOfFrost.Cooldown.Ready()
				   and Talent.BreathOfSindragosa.Enabled()
				   and Player.Runes() >= 3
				   and Player.RunicPower() > 60;
			end,

			-- actions.cooldowns+=/empower_rune_weapon,if=cooldown.pillar_of_frost.ready&!talent.breath_of_sindragosa.enabled&rune.time_to_5>gcd&runic_power.deficit>=10
			Use = function()
				return Spell.PillarOfFrost.Cooldown.Ready()
				   and not Talent.BreathOfSindragosa.Enabled()
				   and Player.Runes.TimeToX(5) > Player.GCD()
				   and Player.RunicPower.Deficit() >= 10;
			end,
		},

		-- actions.cooldowns+=/frostwyrms_fury,if=(buff.pillar_of_frost.remains<=gcd&buff.pillar_of_frost.up)
		FrostwyrmsFury = function()
			return Player.Buff(Aura.PillarOfFrost).Remains() <= Player.GCD()
			   and Player.Buff(Aura.PillarOfFrost).Up();
		end,

		-- Simcraft will interpret 0 seconds remaining as a boolean false, LUA can't do this so instead check for Down() which returns a boolean with 0 seconds being false
		-- actions.cooldowns+=/pillar_of_frost,if=cooldown.empower_rune_weapon.remains
		PillarOfFrost = function()
			return Spell.EmpowerRuneWeapon.Cooldown.Down();
		end,

		-- actions.cooldowns+=/potion,if=buff.pillar_of_frost.up&buff.empower_rune_weapon.up
		ProlongedPower = function()
			return Player.Buff(Aura.PillarOfFrost).Up()
			   and Player.Buff(Aura.EmpowerRuneWeapon).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Consumable.ProlongedPower, self.Requirements.ProlongedPower);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
		action.EvaluateAction(Spell.PillarOfFrost, self.Requirements.PillarOfFrost);
		action.EvaluateAction(Spell.EmpowerRuneWeapon, self.Requirements.EmpowerRuneWeapon.Use);
		action.EvaluateAction(Spell.EmpowerRuneWeapon, self.Requirements.EmpowerRuneWeapon.BreathOfSindragosa);

		action.CallActionList(rotationColdHeart);

		action.EvaluateAction(Talent.FrostwyrmsFury, self.Requirements.FrostwyrmsFury);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Obliteration Rotation
local function Obliteration(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.obliteration+=/frostscythe,if=(buff.killing_machine.react|(buff.killing_machine.up&(prev_gcd.1.frost_strike|prev_gcd.1.howling_blast|prev_gcd.1.glacial_advance)))&(rune.time_to_4>gcd|spell_targets.frostscythe>=2)
		Frostscythe = function(numEnemies)
			return (Player.Buff(Aura.KillingMachine).React() or (Player.Buff(Aura.KillingMachine).Up() and (Player.PrevGCD(1, Spell.FrostStrike) or Player.PrevGCD(1, Spell.HowlingBlast) or Player.PrevGCD(1, Talent.GlacialAdvance))))
			   and (Player.Runes.TimeToX(4) > Player.GCD() or numEnemies >= 2);
		end,

		-- actions.obliteration+=/frost_strike,if=!buff.rime.up|runic_power.deficit<10|rune.time_to_2>gcd
		FrostStrike = function()
			return not Player.Buff(Aura.Rime).Up()
			    or Player.RunicPower.Deficit() < 10
			    or Player.Runes.TimeToX(2) > Player.GCD();
		end,

		-- actions.obliteration+=/glacial_advance,if=(!buff.rime.up|runic_power.deficit<10|rune.time_to_2>gcd)&spell_targets.glacial_advance>=2
		GlacialAdvance = function(numEnemies)
			return (not Player.Buff(Aura.Rime).Up() or Player.RunicPower.Deficit() < 10 or Player.Runes.TimeToX(2) > Player.GCD())
			   and numEnemies >= 2;
		end,

		HowlingBlast = {
			-- actions.obliteration+=/howling_blast,if=buff.rime.up&spell_targets.howling_blast>=2
			AOE = function(numEnemies)
				return Player.Buff(Aura.Rime).Up()
				   and numEnemies >= 2;
			end,

			-- actions.obliteration+=/howling_blast,if=buff.rime.up
			Use = function()
				return Player.Buff(Aura.Rime).Up();
			end,
		},

		Obliterate = {
			-- actions.obliteration+=/obliterate,if=!talent.frostscythe.enabled&!buff.rime.up&spell_targets.howling_blast>=3
			AOE = function(numEnemies)
				return not Talent.Frostscythe.Enabled()
				   and not Player.Buff(Aura.Rime).Up()
				   and numEnemies >= 3;
			end,

			-- actions.obliteration+=/obliterate,if=buff.killing_machine.react|(buff.killing_machine.up&(prev_gcd.1.frost_strike|prev_gcd.1.howling_blast|prev_gcd.1.glacial_advance))
			Use = function()
				return Player.Buff(Aura.KillingMachine).React()
				    or (Player.Buff(Aura.KillingMachine).Up() and (Player.PrevGCD(1, Spell.FrostStrike) or Player.PrevGCD(1, Spell.HowlingBlast) or Player.PrevGCD(1, Talent.GlacialAdvance)));
			end,
		},

		-- actions.obliteration=remorseless_winter,if=talent.gathering_storm.enabled
		RemorselessWinter = function()
			return Talent.GatheringStorm.Enabled();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.RemorselessWinter, self.Requirements.RemorselessWinter);
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.AOE, Enemies.GetEnemies(Spell.HowlingBlast));
		action.EvaluateAction(Talent.Frostscythe, self.Requirements.Frostscythe, Enemies.GetEnemies(8));
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.Use);
		action.EvaluateAction(Talent.GlacialAdvance, self.Requirements.GlacialAdvance);
		action.EvaluateAction(Spell.HowlingBlast, self.Requirements.HowlingBlast.AOE, Enemies.GetEnemies(Spell.HowlingBlast));
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike);
		action.EvaluateAction(Spell.HowlingBlast, self.Requirements.HowlingBlast.Use);
		-- actions.obliteration+=/obliterate
		action.EvaluateAction(Spell.Obliterate, true);
	end

	-- actions+=/run_action_list,name=obliteration,if=buff.pillar_of_frost.up&talent.obliteration.enabled
	function self.Use()
		return Player.Buff(Aura.PillarOfFrost).Up()
		   and Talent.Obliteration.Enabled();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationObliteration = Obliteration("Obliteration");

-- Standard Rotation
local function Standard(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.standard+=/frostscythe,if=buff.killing_machine.up&rune.time_to_4>=gcd
		Frostscythe = function()
			return Player.Buff(Aura.KillingMachine).Up()
			   and Player.Runes.TimeToX(4) >= Player.GCD();
		end,

		FrostStrike = {
			-- actions.standard+=/frost_strike,if=cooldown.remorseless_winter.remains<=2*gcd&talent.gathering_storm.enabled
			GatheringStorm = function()
				return Spell.RemorselessWinter.Cooldown.Remains() <= 2 * Player.GCD()
				   and Talent.GatheringStorm.Enabled();
			end,

			-- actions.standard+=/frost_strike,if=runic_power.deficit<(15+talent.runic_attenuation.enabled*3)
			Use = function()
				return Player.RunicPower.Deficit() < (15 + val(Talent.RunicAttenuation.Enabled()) * 3);
			end,
		},

		-- actions.standard+=/howling_blast,if=buff.rime.up
		HowlingBlast = function()
			return Player.Buff(Aura.Rime).Up();
		end,

		Obliterate = {
			-- actions.standard+=/obliterate,if=!buff.frozen_pulse.up&talent.frozen_pulse.enabled
			FrozenPulse = function()
				return not Player.Buff(Aura.FrozenPulse).Up()
				   and Talent.FrozenPulse.Enabled();
			end,

			-- actions.standard+=/obliterate,if=runic_power.deficit>(25+talent.runic_attenuation.enabled*3)
			Use = function()
				return Player.RunicPower.Deficit() > (25 + val(Talent.RunicAttenuation.Enabled()) * 3);
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.standard=remorseless_winter
		action.EvaluateAction(Spell.RemorselessWinter, true);
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike.GatheringStorm);
		action.EvaluateAction(Spell.HowlingBlast, self.Requirements.HowlingBlast);
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.FrozenPulse);
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike.Use);
		action.EvaluateAction(Talent.Frostscythe, self.Requirements.Frostscythe);
		action.EvaluateAction(Spell.Obliterate, self.Requirements.Obliterate.Use);
		-- actions.standard+=/frost_strike
		action.EvaluateAction(Spell.FrostStrike, true);
		-- actions.standard+=/horn_of_winter
		action.EvaluateAction(Talent.HornOfWinter, true);
		-- actions.standard+=/arcane_torrent
		action.EvaluateAction(Racial.ArcaneTorrent, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationStandard = Standard("Standard");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Bicepspump and SimCraft",
		GuideLink = "http://www.wowhead.com/frost-death-knight-guide",
		WoWVersion = 80001,
	};

	self.Defensives = {
		AntiMagicShell = function()
			return Player.MagicDamagePredicted(3) >= 30;
		end,

		DeathPact = function()
			return Talent.DeathPact.Enabled()
			   and Player.Health.Percent() < 50;
		end,

		DeathStrike = function()
			return Player.Buff(Aura.DarkSuccor).Up()
			   and Player.Health.Percent() < 90;
		end,

		IceboundFortitude = function()
			return Player.DamagePredicted(4) >= 25;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- Simcraft will interpret 0 seconds remaining as a boolean false, LUA can't do this so instead check for Down() which returns a boolean with 0 seconds being false
		-- actions+=/breath_of_sindragosa,if=cooldown.empower_rune_weapon.remains&cooldown.pillar_of_frost.remains
		BreathOfSindragosa = function()
			return Spell.EmpowerRuneWeapon.Cooldown.Down()
			   and Spell.PillarOfFrost.Cooldown.Down();
		end,

		-- actions+=/frost_strike,if=buff.icy_talons.remains<=gcd&buff.icy_talons.up&(!talent.breath_of_sindragosa.enabled|cooldown.breath_of_sindragosa.remains>15)
		FrostStrike = function()
			return Player.Buff(Aura.IcyTalons).Remains() <= Player.GCD()
			   and Player.Buff(Aura.IcyTalons).Up()
			   and (not Talent.BreathOfSindragosa.Enabled() or Talent.BreathOfSindragosa.Cooldown.Remains() > 15);
		end,

		-- actions+=/glacial_advance,if=buff.icy_talons.remains<=gcd&buff.icy_talons.up&spell_targets.glacial_advance>=2&(!talent.breath_of_sindragosa.enabled|cooldown.breath_of_sindragosa.remains>15)
		GlacialAdvance = function(numEnemies)
			return Player.Buff(Aura.IcyTalons).Remains() <= Player.GCD()
			   and Player.Buff(Aura.IcyTalons).Up()
			   and numEnemies >= 2
			   and (not Talent.BreathOfSindragosa.Enabled() or Talent.BreathOfSindragosa.Cooldown.Remains() > 15);
		end,

		-- actions+=/howling_blast,if=!dot.frost_fever.ticking&(!talent.breath_of_sindragosa.enabled|cooldown.breath_of_sindragosa.remains>15)
		HowlingBlast = function()
			return not Target.Debuff(Aura.FrostFever).Ticking()
			   and (not Talent.BreathOfSindragosa.Enabled() or Talent.BreathOfSindragosa.Cooldown.Remains() > 15);
		end,
  	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			ProlongedPower = Objects.newItem(142117),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.GiftOfTheNaaru, Racial.Shadowmeld);
		config.AOEOptions(options, Talent.Frostscythe, Talent.GlacialAdvance, Spell.RemorselessWinter);
		config.CooldownOptions(options, Talent.BreathOfSindragosa, Spell.EmpowerRuneWeapon, Talent.FrostwyrmsFury, Talent.HornOfWinter, Spell.PillarOfFrost);
		config.DefensiveOptions(options, Spell.AntiMagicShell, Talent.DeathPact, Spell.DeathStrike, Spell.IceboundFortitude);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against magic damage
		action.EvaluateDefensiveAction(Spell.AntiMagicShell, self.Defensives.AntiMagicShell);

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.IceboundFortitude, self.Defensives.IceboundFortitude);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Talent.DeathPact, self.Defensives.DeathPact);
		action.EvaluateDefensiveAction(Spell.DeathStrike, self.Defensives.DeathStrike);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.ProlongedPower, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		Player.Runes();
		action.EvaluateAction(Spell.HowlingBlast, self.Requirements.HowlingBlast);
		action.EvaluateAction(Talent.GlacialAdvance, self.Requirements.GlacialAdvance);
		action.EvaluateAction(Spell.FrostStrike, self.Requirements.FrostStrike);
		action.EvaluateAction(Talent.BreathOfSindragosa, self.Requirements.BreathOfSindragosa);
		-- actions+=/call_action_list,name=cooldowns
		action.CallActionList(rotationCooldowns);
		action.RunActionList(rotationBOSPooling);
		action.RunActionList(rotationBOSTicking);
		action.RunActionList(rotationObliteration);
		action.RunActionList(rotationAOE);
		-- actions+=/call_action_list,name=standard
		action.CallActionList(rotationStandard);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Frost Death Knight", addonTable.Enum.SpecID.DEATHKNIGHT_FROST);