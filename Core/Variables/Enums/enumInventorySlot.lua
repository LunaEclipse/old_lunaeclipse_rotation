local _, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

addonTable.Enum.InventorySlot = {
	HEAD = 1,
	NECK = 2,
	SHOULDER = 3,
	BODY = 4,
	CHEST = 5,
	WAIST = 6,
	LEGS = 7,
	FEET = 8,
	WRIST = 9,
	HAND = 10,
	FINGER1 = 11,
	FINGER2 = 12,
	TRINKET1 = 13,
	TRINKET2 = 14,
	BACK = 15,
	MAINHAND = 16,
	OFFHAND = 17,
	RANGED = 18,
	TABARD = 19,
};