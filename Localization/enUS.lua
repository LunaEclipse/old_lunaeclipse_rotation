local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.

--localization file for english/United States
local L = LibStub("AceLocale-3.0"):NewLocale("LunaEclipse_Rotation", "enUS", true);

if L then
	-- Tooltip Scanning
	L["Dummy"] = "Dummy";
	L["For"] = "For";
	L["Generates"] = "Generates";
	L["Lasts"] = "Lasts";
	L["Next"] = "Next";
	L["Over"] = "Over";
	L["Potion"] = "Potion";
end