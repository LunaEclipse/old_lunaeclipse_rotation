local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local apiGeneral = addon.API.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local Settings = addon.Core.Settings;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {
	average_burn_length = 0,
	startTime = 0,
	total_burns = 0,
};

-- Rotation Variables
local nameAPL = "lunaeclipse_mage_arcane";

local function burn_phase()
	return Variables.startTime > 0;
end

local function burn_phase_duration()
	return apiGeneral.GetTime() - Variables.startTime;
end

local function start_burn_phase()
	-- SimCraft never mentions this directly, but we have to store the the burn started, and set burn_phase to true
	Variables.startTime = apiGeneral.GetTime();
end

local function stop_burn_phase()
	if burn_phase() then
		-- # Now that we're done burning, we can update the average_burn_length with the length of this burn.
		-- This was moved here from later in the rotation because the addon can't do it there because of how it processes rotations
		-- actions.burn+=/variable,name=average_burn_length,op=set,value=(variable.average_burn_length*variable.total_burns-variable.average_burn_length+(burn_phase_duration))%variable.total_burns
		Variables.average_burn_length = (Variables.average_burn_length * Variables.total_burns - Variables.average_burn_length + burn_phase_duration()) / Variables.total_burns;

		-- SimCraft never mentions this directly, but we need to set startTime to 0
		Variables.startTime = 0;
	end
end

-- Burn Rotation
local function Burn(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		ArcaneBarrage = {
			-- actions.burn+=/arcane_barrage,if=variable.pressure_rotation&buff.arcane_charge.stack=buff.arcane_charge.max_stack
			PressureRotation = function()
				return Variables.pressure_rotation
				   and Player.ArcaneCharges() == Player.ArcaneCharges.Max();
			end,

			-- actions.burn+=/arcane_barrage,if=(active_enemies>=3|(active_enemies>=2&talent.resonance.enabled))&(buff.arcane_charge.stack=buff.arcane_charge.max_stack)
			Use = function(numEnemies)
				return (numEnemies >= 3 or (numEnemies >= 2 and Talent.Resonance.Enabled()))
				   and Player.ArcaneCharges() == Player.ArcaneCharges.Max();
			end,
		},

		-- actions.burn+=/arcane_explosion,if=active_enemies>=3|(active_enemies>=2&talent.resonance.enabled)
		ArcaneExplosion = function(numEnemies)
			return numEnemies >= 3
			    or (numEnemies >= 2 and Talent.Resonance.Enabled());
		end,

		-- actions.burn+=/arcane_missiles,if=(buff.clearcasting.react&mana.pct<=95)&variable.pressure_rotation=0,chain=1
		ArcaneMissiles = function()
			return (Player.Buff(Aura.Clearcasting).React() and Player.Mana.Percent() <= 95)
			   and not Variables.pressure_rotation;
		end,

		-- actions.burn+=/arcane_orb,if=buff.arcane_charge.stack=0|(active_enemies<3|(active_enemies<2&talent.resonance.enabled))
		ArcaneOrb = function(numEnemies)
			return Player.ArcaneCharges() == 0
			    or (numEnemies < 3 or (numEnemies < 2 and Talent.Resonance.Enabled()));
		end,

		-- actions.burn+=/charged_up,if=buff.arcane_charge.stack<=1&(!set_bonus.tier20_2pc|cooldown.presence_of_mind.remains>5)
		ChargedUp = function()
			return Player.ArcaneCharges() <= 1
			   and (not addonTable.Tier20_2PC or Spell.PresenceOfMind.Cooldown.Remains() > 5);
		end,

		-- actions.burn+=/evocation,interrupt_if=mana.pct>=97|(buff.clearcasting.react&mana.pct>=92)
		Evocation = function()
			return Player.Mana.Percent() >= 97
			    or (Player.Buff(Aura.Clearcasting).React() and Player.Mana.Percent() >= 92);
		end,

		-- actions.burn+=/lights_judgment,if=buff.arcane_power.down
		LightsJudgment = function()
			return Player.Buff(Aura.ArcanePower).Down();
		end,

		-- Refreshable also returns true if the buff doesn't exist so no need for !ticking
		-- actions.burn+=/nether_tempest,if=(refreshable|!ticking)&buff.arcane_charge.stack=buff.arcane_charge.max_stack&buff.rune_of_power.down&buff.arcane_power.down
		NetherTempest = function()
			return Target.Debuff(Aura.NetherTempest).Refreshable()
			   and Player.ArcaneCharges() == Player.ArcaneCharges.Max()
			   and Player.Buff(Aura.RuneOfPower).Down()
			   and Player.Buff(Aura.ArcanePower).Down();
		end,

		-- We need to add a check to make sure its not on cooldown because while it has charges it also has a seperate cooldown.
		-- actions.burn+=/rune_of_power,if=!buff.arcane_power.up&(mana.pct>=50|cooldown.arcane_power.remains=0)&(buff.arcane_charge.stack=buff.arcane_charge.max_stack)
		RuneOfPower = function()
			return Talent.RuneOfPower.Cooldown.Up()
			   and not Player.Buff(Aura.ArcanePower).Up()
			   and (Player.Mana.Percent() >= 50 or Spell.ArcanePower.Cooldown.Remains() == 0)
			   and (Player.ArcaneCharges() == Player.ArcaneCharges.Max());
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- # Increment our burn phase counter. Whenever we enter the `burn` actions without being in a burn phase, it means that we are about to start one.
		if not burn_phase() then
			-- actions.burn=variable,name=total_burns,op=add,value=1,if=!burn_phase
			Variables.total_burns = Variables.total_burns + 1;
			-- actions.burn+=/start_burn_phase,if=!burn_phase
			start_burn_phase();
		end

		-- # End the burn phase when we just evocated.
		if burn_phase() and Player.PrevGCD(1, Spell.Evocation) and Target.TimeToDie() > Variables.average_burn_length and burn_phase_duration() > 0 then
			-- actions.burn+=/stop_burn_phase,if=burn_phase&prev_gcd.1.evocation&target.time_to_die>variable.average_burn_length&burn_phase_duration>0
			stop_burn_phase();
		end

		-- actions.burn+=/mirror_image
		action.EvaluateAction(Talent.MirrorImage, true);
		action.EvaluateAction(Talent.ChargedUp, self.Requirements.ChargedUp);
		action.EvaluateAction(Talent.NetherTempest, self.Requirements.NetherTempest);
		action.EvaluateAction(Racial.LightsJudgment, self.Requirements.LightsJudgment);
		action.EvaluateAction(Talent.RuneOfPower, self.Requirements.RuneOfPower);
		-- actions.burn+=/arcane_power
		action.EvaluateAction(Spell.ArcanePower, true);
		-- actions.burn+=/blood_fury
		action.EvaluateAction(Racial.BloodFury, true);
		-- actions.burn+=/berserking
		action.EvaluateAction(Racial.Berserking, true);
		-- actions.burn+=/fireblood
		action.EvaluateAction(Racial.Fireblood, true);
		-- actions.burn+=/ancestral_call
		action.EvaluateAction(Racial.AncestralCall, true);
		-- actions.burn+=/presence_of_mind
		action.EvaluateAction(Spell.PresenceOfMind, true);
		action.EvaluateAction(Talent.ArcaneOrb, self.Requirements.ArcaneOrb);
		action.EvaluateAction(Spell.ArcaneBarrage, self.Requirements.ArcaneBarrage.Use);
		action.EvaluateAction(Spell.ArcaneExplosion, self.Requirements.ArcaneExplosion);
		action.EvaluateAction(Spell.ArcaneBarrage, self.Requirements.ArcaneBarrage.PressureRotation);
		action.EvaluateInterruptCondition(Spell.ArcaneMissiles, self.Requirements.ArcaneMissiles, true);
		-- actions.burn+=/arcane_blast
		action.EvaluateAction(Spell.ArcaneBlast, true);

		action.EvaluateInterruptCondition(Spell.Evocation, true, self.Requirements.Evocation);
		-- # For the rare occasion where we go oom before evocation is back up. (Usually because we get very bad rng so the burn is cut very short)
		-- actions.burn+=/arcane_barrage
		action.EvaluateAction(Spell.ArcaneBarrage, true);
	end

	-- # Start a burn phase when important cooldowns are available. Start with 4 arcane charges, unless there's a good reason not to. (charged up)
	-- actions+=/call_action_list,name=burn,if=burn_phase|target.time_to_die<variable.average_burn_length|(cooldown.arcane_power.remains=0&cooldown.evocation.remains<=variable.average_burn_length&(buff.arcane_charge.stack=buff.arcane_charge.max_stack|(talent.charged_up.enabled&cooldown.charged_up.remains=0)))
	function self.Use()
		return burn_phase()
		    or Target.TimeToDie() < Variables.average_burn_length
		    or (Spell.ArcanePower.Cooldown.Remains() == 0 and Spell.Evocation.Cooldown.Remains() <= Variables.average_burn_length and (Player.ArcaneCharges() == Player.ArcaneCharges.Max() or (Talent.ChargedUp.Enabled() and Talent.ChargedUp.Cooldown.Remains() == 0)));
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationBurn = Burn("Burn");

-- Conserve Rotation
local function Conserve(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- # During conserve, we still just want to continue not dropping charges as long as possible.So keep 'burning' as long as possible (aka conserve_mana threshhold) and then swap to a 4x AB->Abarr conserve rotation. This is mana neutral for RoT, mana negative with arcane familiar. If we do not have 4 AC, we can dip slightly lower to get a 4th AC.
		-- actions.conserve+=/arcane_barrage,if=((buff.arcane_charge.stack=buff.arcane_charge.max_stack)&(mana.pct<=variable.conserve_mana|variable.pressure_rotation)|(talent.arcane_orb.enabled&cooldown.arcane_orb.remains<=gcd&cooldown.arcane_power.remains>10))|mana.pct<=(variable.conserve_mana-10)
		ArcaneBarrage = function()
			return ((Player.ArcaneCharges() == Player.ArcaneCharges.Max()) and (Player.Mana.Percent() <= Variables.conserve_mana or Variables.pressure_rotation) or (Talent.ArcaneOrb.Enabled() and Talent.ArcaneOrb.Cooldown.Remains() <= Player.GCD() and Spell.ArcanePower.Cooldown.Remains() > 10))
			    or Player.Mana.Percent() <= Variables.conserve_mana - 10;
		end,

		ArcaneBlast = {
			-- # Arcane Blast shifts up in priority when running rule of threes.
			-- actions.conserve+=/arcane_blast,if=buff.rule_of_threes.up&buff.arcane_charge.stack>=3
			RuleOfThrees = function()
				return Player.Buff(Aura.RuleOfThrees).Up()
				   and Player.ArcaneCharges() >= 3;
			end,

			-- SimCraft doesn't mention this, but don't arcane blast if you are already at max arcane charges
			-- actions.conserve+=/arcane_blast
			Use = function()
				return Player.ArcaneCharges.Deficit() >= 1;
			end,
		},

		-- # Keep 'burning' in aoe situations until conserve_mana pct. After that only cast AE with 3 Arcane charges, since it's almost equal mana cost to a 3 stack AB anyway. At that point AoE rotation will be AB x3 -> AE -> Abarr
		-- actions.conserve+=/arcane_explosion,if=active_enemies>=3&(mana.pct>=variable.conserve_mana|buff.arcane_charge.stack=3)
		ArcaneExplosion = function(numEnemies)
			return numEnemies >= 3
			   and (Player.Mana.Percent() >= Variables.conserve_mana or Player.ArcaneCharges() == 3);
		end,

		-- actions.conserve+=/arcane_missiles,if=mana.pct<=95&buff.clearcasting.react&variable.pressure_rotation=0,chain=1
		ArcaneMissiles = function()
			return Player.Mana.Percent() <= 95
			   and Player.Buff(Aura.Clearcasting).React()
			   and not Variables.pressure_rotation;
		end,

		-- actions.conserve+=/arcane_orb,if=buff.arcane_charge.stack<=2&(cooldown.arcane_power.remains>10|active_enemies<=2)
		ArcaneOrb = function(numEnemies)
			return Player.ArcaneCharges() <= 2
			   and (Spell.ArcanePower.Cooldown.Remains() > 10 or numEnemies <= 2);
		end,

		-- actions.conserve+=/charged_up,if=buff.arcane_charge.stack=0
		ChargedUp = function()
			return Player.ArcaneCharges() == 0;
		end,

		-- Refreshable also returns true if the buff doesn't exist so no need for !ticking
		-- actions.conserve+=/nether_tempest,if=(refreshable|!ticking)&buff.arcane_charge.stack=buff.arcane_charge.max_stack&buff.rune_of_power.down&buff.arcane_power.down
		NetherTempest = function()
			return Target.Debuff(Aura.NetherTempest).Refreshable()
					and Player.ArcaneCharges() == Player.ArcaneCharges.Max()
					and Player.Buff(Aura.RuneOfPower).Down()
					and Player.Buff(Aura.ArcanePower).Down();
		end,

		-- actions.conserve+=/presence_of_mind,if=set_bonus.tier20_2pc&buff.arcane_charge.stack=0
		PresenceOfMind = function()
			return addonTable.Tier20_2PC
			   and Player.ArcaneCharges() == 0;
		end,

		-- actions.conserve+=/rune_of_power,if=buff.arcane_charge.stack=buff.arcane_charge.max_stack&(full_recharge_time<=execute_time|recharge_time<=cooldown.arcane_power.remains|target.time_to_die<=cooldown.arcane_power.remains)
		RuneOfPower = function()
			return Player.ArcaneCharges() == Player.ArcaneCharges.Max()
			   and (Talent.RuneOfPower.Charges.FullRechargeTime() <= Talent.RuneOfPower.ExecuteTime() or Talent.RuneOfPower.Charges.Recharge() <= Spell.ArcanePower.Cooldown.Remains() or Target.TimeToDie() <= Spell.ArcanePower.Cooldown.Remains());
		end,

		-- # Supernova is barely worth casting, which is why it is so far down, only just above AB.
		-- actions.conserve+=/supernova,if=mana.pct<=95
		Supernova = function()
			return Player.Mana.Percent() <= 95;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.conserve=mirror_image
		action.EvaluateAction(Talent.MirrorImage, true);
		action.EvaluateAction(Talent.ChargedUp, self.Requirements.ChargedUp);
		action.EvaluateAction(Spell.PresenceOfMind, self.Requirements.PresenceOfMind);
		action.EvaluateAction(Talent.NetherTempest, self.Requirements.NetherTempest);
		action.EvaluateAction(Talent.ArcaneOrb, self.Requirements.ArcaneOrb);
		action.EvaluateAction(Spell.ArcaneBlast, self.Requirements.ArcaneBlast.RuleOfThrees);
		action.EvaluateAction(Talent.RuneOfPower, self.Requirements.RuneOfPower);
		action.EvaluateInterruptCondition(Spell.ArcaneMissiles, self.Requirements.ArcaneMissiles, true);
		action.EvaluateAction(Spell.ArcaneBarrage, self.Requirements.ArcaneBarrage);
		action.EvaluateAction(Talent.Supernova, self.Requirements.Supernova);
		action.EvaluateAction(Spell.ArcaneExplosion, self.Requirements.ArcaneExplosion);
		action.EvaluateAction(Spell.ArcaneBlast, self.Requirements.ArcaneBlast.Use);
		-- actions.conserve+=/arcane_barrage
		action.EvaluateAction(Spell.ArcaneBarrage, true);
	end

	-- actions+=/call_action_list,name=conserve,if=!burn_phase
	function self.Use()
		return not burn_phase();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationConserve = Conserve("Conserve");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Malon and SimCraft",
		GuideLink = "https://www.wowhead.com/arcane-mage-simple-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		-- Greater invisibility will only be used when solo if you have use stealth while solo checked, because it will drop you out of combat.
		GreaterInvisibility = function()
			return (not Player.IsSolo() or Settings.GetCharacterValue("ScriptOptions", "OPT_STEALTH_SOLO") == 1)
			   and Player.DamagePredicted(5) >= 40;
		end,

		IceBlock = function()
			return Player.DamagePredicted(5) >= 60;
		end,

		PrismaticBarrier = function()
			return not Player.Buff(Aura.PrismaticBarrier).Up()
			   and Player.DamagePredicted(5) >= 15;
		end
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- SimCraft doesn't specify this, but only cast the buff if you don't have it.
		-- actions.precombat+=/summon_arcane_familiar
		ArcaneFamiliar = function()
			return not Player.Buff(Aura.ArcaneFamiliar).Up();
		end,

		-- SimCraft doesn't specify this, but only cast the buff if you don't have it.
		-- actions.precombat+=/arcane_intellect
		ArcaneIntellect = function()
			return not Player.Buff(Aura.ArcaneIntellect).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Aura.DivineRight = Objects.newSpell(278523);

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfIntellect = Objects.newItem(163222),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.AncestralCall, Racial.Berserking, Racial.BloodFury, Racial.Fireblood, Racial.LightsJudgment);
		config.AOEOptions(options, Spell.ArcaneExplosion, Talent.ArcaneOrb, Talent.NetherTempest, Talent.Supernova);
		config.BuffOptions(options, Talent.ArcaneFamiliar, Spell.ArcaneIntellect);
		config.CooldownOptions(options, Spell.ArcanePower, Talent.ChargedUp, Spell.Evocation, Talent.MirrorImage, Spell.PresenceOfMind, Talent.RuneOfPower);
		config.DefensiveOptions(options, Spell.GreaterInvisibility, Spell.IceBlock, Spell.PrismaticBarrier);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Spell.IceBlock, self.Defensives.IceBlock);
		action.EvaluateDefensiveAction(Spell.GreaterInvisibility, self.Defensives.GreaterInvisibility);
		action.EvaluateDefensiveAction(Spell.PrismaticBarrier, self.Defensives.PrismaticBarrier);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	function self.PrecombatVariables()
		-- SimCraft doesn't specify it, but we are out of combat, so lets make sure that the burn phase start time is reset to 0
		Variables.startTime = 0;

		-- # conserve_mana is the mana percentage we want to go down to during conserve. It needs to leave enough room to worst case scenario spam AB only during AP.
		-- actions.precombat+=/variable,name=conserve_mana,op=set,value=35,if=talent.overpowered.enabled
		if Talent.Overpowered.Enabled() then
			Variables.conserve_mana = 35;
		end
		-- actions.precombat+=/variable,name=conserve_mana,op=set,value=45,if=!talent.overpowered.enabled
		if not Talent.Overpowered.Enabled() then
			Variables.conserve_mana = 45;
		end
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.ArcaneIntellect, self.Requirements.ArcaneIntellect);
		action.EvaluateAction(Talent.ArcaneFamiliar, self.Requirements.ArcaneFamiliar);
		-- actions.precombat+=/mirror_image
		action.EvaluateAction(Talent.MirrorImage, true);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfIntellect, true);
		-- actions.precombat+=/arcane_blast
		action.EvaluateAction(Spell.ArcaneBlast, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- # Swap to the 'Arcane Pressure' rotation when we got enough traits for it. This implies stuff like no longer casting Arcane Missiles, and throwing out Arcane Barrage whenever we got 4 AC.
		-- actions+=/variable,name=pressure_rotation,op=set,value=(azerite.arcane_pressure.rank>=2|(talent.resonance.enabled&active_enemies>=2))&target.health.pct<=35
		Variables.pressure_rotation = (Azerite.ArcanePressure.Rank() >= 2 or (Talent.Resonance.Enabled() and Enemies.GetEnemies() >= 2)) and Target.Health.Percent() <= 35;

		action.CallActionList(rotationBurn);
		action.CallActionList(rotationConserve);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Arcane Mage", addonTable.Enum.SpecID.MAGE_ARCANE);
