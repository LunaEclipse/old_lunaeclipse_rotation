-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local ipairs, pairs, tinsert, type = ipairs, pairs, tinsert, type;
local isLoggedIn = false;

local addonCallbacks = {
	ADDON_LOADED = "addonLoaded",
	PLAYER_LOGIN = "playerLogin",
	PLAYER_LOGOUT = "playerLogout",
	PLAYER_REGEN_DISABLED = "enterCombat",
	PLAYER_REGEN_ENABLED = "leaveCombat",
	SAVED_VARIABLES_TOO_LARGE = "errorSavedVariables",
};

local callbacksLibrary = {};
local callbacksModules = {};
local registeredCallbacks = {};

local function registerCallbacks(callbacks)
	if callbacks then
		local modulesTable = callbacksModules or {};

		tinsert(modulesTable, callbacks);

		callbacksModules = modulesTable;
	end
end

local function registerLibraries(callbacks)
	if callbacks then
		local callbackTable = callbacksLibrary or {};
		local library, tableCallbacks;

		for _, callbackData in ipairs(callbacks) do
			library = callbackData.library or {};
			tableCallbacks = callbackData.libraryCallbacks or {};

			registeredCallbacks[library] = registeredCallbacks[library] or {};
			callbackTable[library] = callbackTable[library] or {};

			for key, value in pairs(tableCallbacks) do
				registeredCallbacks[library][key] = true;
				callbackTable[library][key] = callbackTable[library][key] or {};

				tinsert(callbackTable[library][key], value);
			end
		end

		callbacksLibrary = callbackTable;
	end
end

local function rotationChanged()
	local apiGeneral = addon.API.General;
	local coreTables = addon.Core.Tables;

	local specInfo = apiGeneral.GetSpecializationInfo();
	local specID = coreTables.GetValue(specInfo, "ID");

	return addon.currentRotation ~= addon.Rotation.GetRotation(specID);
end

addon.System.Callbacks = {
	--- Perform an event callback for anyone that is registered to receive callbacks.
	-- @param event: string - This is the callback event to be called, for example playerLogin, leaveCombat, etc...
	-- @param ...: any - This is any number of arguments of any type that will be passed to the callback function.
	Callback = function(event, ...)
		if event then
			local coreTables = addon.Core.Tables;
			local modulesTable = callbacksModules or {};
			local callbackFunction;

			if not coreTables.IsEmpty(modulesTable) then
				if event == "changeSpecFinish" then
					local addonCache = addonTable.Cache;
					local specInfo = ...;

					if not coreTables.IsEmpty(specInfo) and not coreTables.Equate(addonCache.Persistant.Player.Specialization, specInfo) then
						-- Update Specialization information.
						addonCache.Persistant.Player.Specialization = coreTables.Duplicate(specInfo);

						for _, module in ipairs(modulesTable) do
							callbackFunction = module[event];

							if type(callbackFunction) == "function" then
								callbackFunction(...);
							end
						end
					end

					if rotationChanged() then
						addon.System.Callbacks.Callback("rotationChanged");
					end
				else
					for _, module in ipairs(modulesTable) do
						callbackFunction = module[event];

						if type(callbackFunction) == "function" then
							callbackFunction(...);
						end
					end
				end
			end
		end
	end,

	--- Register a table of functions to receive the callback events.
	-- @param event: table - This is the table in which the addon will attempt to execute the callback functions if they exist.
	RegisterAddonCallbacks = function(addonCallbacks)
		registerCallbacks(addonCallbacks);
	end,

	--- Register a table of functions to receive the callback events.
	-- @param event: table - This table holds a link to the library as well as callback functions to be handled with a reference to the receiving function.
	RegisterLibraryCallbacks = function(libraryCallbacks)
		registerLibraries(libraryCallbacks);
	end,
};

local callbackEvents = {
	enterCombat = function(event, ...)
		if isLoggedIn then
			addon.System.Callbacks.Callback(addonCallbacks[event], ...);
		end
	end,

	leaveCombat = function(event, ...)
		if isLoggedIn then
			addon.System.Callbacks.Callback(addonCallbacks[event], ...);
		end
	end,

	playerLogin = function(event, ...)
		if not isLoggedIn then
			isLoggedIn = true;

			local addonCache = addonTable.Cache;
			local apiGeneral = addon.API.General;
			local coreTables = addon.Core.Tables;

			local specInfo = apiGeneral.GetSpecializationInfo(true);

			if not coreTables.IsEmpty(specInfo) then
				-- Update Specialization information.
				addonCache.Persistant.Player.Specialization = coreTables.Duplicate(specInfo);
			end

			addon.System.Callbacks.Callback(addonCallbacks[event], specInfo, ...);

			if not coreTables.IsEmpty(specInfo) then
				addon.System.Callbacks.Callback("changeSpecFinish", specInfo, ...);
			end
		end
	end,
};

function addonTable.callbacksHandler(event, ...)
	local addonEvent = addonCallbacks[event];

	if addonEvent then
		local callbackFunction = callbackEvents[addonEvent];

		if callbackFunction then
			if type(callbackFunction) == "function" then
				return callbackFunction(event, ...);
			else
				return callbackFunction;
			end
		else
			addon.System.Callbacks.Callback(addonCallbacks[event], ...);
		end
	end
end;

local moduleEvents = {
	playerLogin = function()
		local Tables = addon.Core.Tables;

		if not Tables.IsEmpty(registeredCallbacks) then
			for library, callbacks in pairs(registeredCallbacks) do
				if not Tables.IsEmpty(callbacks) then
					for event in pairs(callbacks) do
						library.RegisterCallback(addon, event, addonTable.callbacksHandler);
					end
				end
			end
		end
	end,
};

for event in pairs(addonCallbacks) do
	addon:RegisterEvent(event, addonTable.raisedEvents);
end

-- Register this as a module of the addon.
addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);