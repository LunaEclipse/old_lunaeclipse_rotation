local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local Objects = addon.Core.Objects;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;

-- Rotation Variables
local nameAPL = "lunaeclipse_demonhunter_vengeance";

-- Brand Rotation
local function Brand(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.brand+=/fel_devastation,if=dot.fiery_brand.ticking
		FelDevastation = function()
			return Target.Debuff(Aura.FieryBrand).Ticking();
		end,

		-- actions.brand+=/immolation_aura,if=dot.fiery_brand.ticking
		ImmolationAura = function()
			return Target.Debuff(Aura.FieryBrand).Ticking();
		end,
		
		InfernalStrike = {
			-- actions.brand+=/infernal_strike,if=cooldown.fiery_brand.remains=0
			FieryBrand = function()
				return Spell.FieryBrand.Cooldown.Remains() == 0;
			end,

			-- actions.brand+=/infernal_strike,if=dot.fiery_brand.ticking
			Use = function()
				return Target.Debuff(Aura.FieryBrand).Ticking();
			end,
		},

		SigilOfFlame = {
			-- actions.brand=sigil_of_flame,if=cooldown.fiery_brand.remains<2
			FieryBrand = function() 
				return Spell.FieryBrand.Cooldown.Remains() < 2;
			end,

			-- actions.brand+=/sigil_of_flame,if=dot.fiery_brand.ticking
			Use = function() 
				return Target.Debuff(Aura.FieryBrand).Ticking();
			end,
		},
		
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.SigilOfFlame, self.Requirements.SigilOfFlame.FieryBrand);
		action.EvaluateAction(Spell.InfernalStrike, self.Requirements.InfernalStrike.FieryBrand);
		-- actions.brand+=/fiery_brand
		action.EvaluateAction(Spell.FieryBrand, true);
		action.EvaluateAction(Spell.ImmolationAura, self.Requirements.ImmolationAura);
		action.EvaluateAction(Talent.FelDevastation, self.Requirements.FelDevastation);
		action.EvaluateAction(Spell.InfernalStrike, self.Requirements.InfernalStrike.Use);
		action.EvaluateAction(Spell.SigilOfFlame, self.Requirements.SigilOfFlame.Use);
	end

	-- actions+=/call_action_list,name=brand,if=talent.charred_flesh.enabled
	function self.Use()
		return Talent.CharredFlesh.Enabled();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationBrand = Brand("Brand");

-- Defensives Rotation
local function Defensives(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- SimCraft doesn't specify but don't use the spell if the buff is already present.
		-- actions.defensives=demon_spikes
		DemonSpikes = function()
			return not Player.Buff(Aura.DemonSpikes).Up();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.DemonSpikes, self.Requirements.DemonSpikes);
		-- actions.defensives+=/metamorphosis
		action.EvaluateAction(Spell.Metamorphosis, true);
		-- actions.defensives+=/fiery_brand
		action.EvaluateAction(Spell.FieryBrand, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationDefensives = Defensives("Defensives");

-- Normal Rotation
local function Normal(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.normal+=/felblade,if=pain<=70
		Felblade = function()
			return Player.Pain() <= 70;
		end,

		-- actions.normal+=/fracture,if=soul_fragments<=3
		Fracture = function()
			return Player.Buff(Aura.SoulFragments).Stack() <= 3;
		end,

		-- actions.normal+=/immolation_aura,if=pain<=90
		ImmolationAura = function()
			return Player.Pain() <- 90;
		end,

		SoulCleave = {
			-- actions.normal+=/soul_cleave,if=talent.spirit_bomb.enabled&soul_fragments=0
			SpiritBomb = function()
				return Talent.SpiritBomb.Enabled()
				   and Player.Buff(Aura.SoulFragments).Stack() == 0;
			end,

			-- actions.normal+=/soul_cleave,if=!talent.spirit_bomb.enabled
			Use = function()
				return not Talent.SpiritBomb.Enabled();
			end,
		},

		-- actions.normal+=/spirit_bomb,if=soul_fragments>=4
		SpiritBomb = function()
			return Player.Buff(Aura.SoulFragments).Stack() >= 4;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.normal=infernal_strike
		action.EvaluateAction(Spell.InfernalStrike, true);
		action.EvaluateAction(Talent.SpiritBomb, self.Requirements.SpiritBomb);
		action.EvaluateAction(Spell.ImmolationAura, self.Requirements.ImmolationAura);
		action.EvaluateAction(Talent.Felblade, self.Requirements.Felblade);
		action.EvaluateAction(Talent.Fracture, self.Requirements.Fracture);
		-- actions.normal+=/fel_devastation
		action.EvaluateAction(Talent.FelDevastation, true);
		action.EvaluateAction(Spell.SoulCleave, self.Requirements.SoulCleave.Use);
		action.EvaluateAction(Spell.SoulCleave, self.Requirements.SoulCleave.SpiritBomb);
		-- actions.normal+=/sigil_of_flame
		action.EvaluateAction(Spell.SigilOfFlame, true);
		-- actions.normal+=/shear
		action.EvaluateAction(Spell.Shear, true);
		-- actions.normal+=/throw_glaive
		action.EvaluateAction(Spell.ThrowGlaive, true);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationNormal = Normal("Normal");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Munkky and SimCraft",
		GuideLink = "http://www.wowhead.com/vengeance-demon-hunter-guide",
		WoWVersion = 80001,
		ImportantNotes = "This rotation provides suggestions for doing damage, and offering some very basic defensive suggestions.\n\nThis rotation SHOULD NOT be used for group content as it offers no actual tanking suggestions, such as managing threat or dealing with mechanics.\n\nThis rotation is really only suitable for open world content.",
	};

	self.Defensives = {};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfAgility = Objects.newItem(163223),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Shadowmeld);
		config.AOEOptions(options, Spell.ImmolationAura, Spell.SigilOfFlame, Spell.SoulCleave, Talent.SpiritBomb, Spell.ThrowGlaive);
		config.CooldownOptions(options, Spell.DemonSpikes, Talent.Felblade, Talent.FelDevastation, Spell.FieryBrand, Talent.Fracture, Spell.Metamorphosis);
		config.UtilityOptions(options, Spell.InfernalStrike);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- This is a tanking spec rotation so Defensive actions are listed in the main rotations.
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfAgility, true);
		-- Not specified by SimCraft but lets add something to initiate combat
		action.EvaluateAction(Spell.InfernalStrike, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		action.CallActionList(rotationBrand);
		-- actions+=/call_action_list,name=defensives
		action.CallActionList(rotationDefensives);
		-- actions+=/call_action_list,name=normal
		action.CallActionList(rotationNormal);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Vengeance Demon Hunter", addonTable.Enum.SpecID.DEMONHUNTER_VENGEANCE);