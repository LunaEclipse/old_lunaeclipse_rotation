-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local AoEInsensibleUnit = {
	--- Mythic+ Affixes
	-- Fel Explosives (7.2 Patch)
	[120651] = true,
};

local function countEnemy(currentUnit)
	local apiUnit = addon.API.Unit;
	local Objects = addon.Core.Objects;
	local PetThreat = addonTable.PetThreat;
	local Settings = addon.Core.Settings;

	if Objects.IsUnit(currentUnit) then
		local enemyCount = Settings.GetAccountValue("AddonSettings", "enemyCount");

		if currentUnit.IsDummy() then
			return true;
		elseif not apiUnit.InCombat() or enemyCount == 0 then
			-- Count all enemies irrespective of combat status.
			return true;
		elseif enemyCount == 1 then
			-- Count only enemies in combat regardless of who they are fighting.
			return currentUnit.AffectingCombat();
		elseif enemyCount == 2 then
			local format = format;

			-- Count only enemies in combat with the player or raid/party.
			if currentUnit.IsAttacking(addon.Units.Player) or PetThreat.IsAttacking(currentUnit.ClassID()) then
				return true;
			elseif addon.Units.Player.InRaid() then
				for counter = 1, 40 do
					if currentUnit.IsAttacking(addon.Units[format("raid%s", counter)]) then
						return true;
					end
				end
			elseif addon.Units.Player.InParty() then
				for counter = 1, 4 do
					if currentUnit.IsAttacking(addon.Units[format("party%s", counter)]) then
						return true;
					end
				end
			end

			return false;
		end
	else
		-- Not a unit, so don't count it.
		return false;
	end
end

addon.Modules.Enemies = {
	GetEnemies = function(Object)
		local max, pairs = max, pairs;
		local Cache = addonTable.Cache;
		local Core = addon.Core.General;
		local Objects = addon.Core.Objects;
		local Tables = addon.Core.Tables;

		local tableKey = (Objects.IsObject(Object) and Object.Name()) or (Core.ToNumber(Object) > 0 and Core.ToNumber(Object)) or "ALL";
		local returnValue = Tables.GetValue(Cache.Persistant.EnemiesCount, tableKey);

		if returnValue == nil then
			returnValue = 0;

			for _, currentUnit in pairs(addon.NameplateUnits) do
				if currentUnit.IsValidTarget() and not currentUnit.IsBlacklisted()
											   and not currentUnit.IsUserBlacklisted()
											   and not AoEInsensibleUnit[currentUnit.NPCID()]
											   and countEnemy(currentUnit)
											   and (not Object or currentUnit.InRange(Object)) then
					returnValue = returnValue + 1;
				end
			end

			Cache.Persistant.EnemiesCount[tableKey] = returnValue;
		end

		return max(returnValue, 1);
	end,
};

local function timerEvent()
	local wipe = wipe;
	local Cache = addonTable.Cache;

	-- Clear the cache to force getting new values.
	wipe(Cache.Persistant.EnemiesCount);

	-- Set the addon value for enemy count to total number of enemies.
	addon.Enemies = addon.Modules.Enemies.GetEnemies();
end

local moduleEvents = {
	Disabled = function()
		addon.System.Timers.TimerRemove(timerEvent);
	end,

	Enabled = function()
		-- Make sure the timer event is called immediately before starting the timer so it doesn't need to wait 1 second to get enemy data.
		timerEvent();

		addon.System.Timers.TimerAdd(timerEvent, 1);
	end,
};

addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);