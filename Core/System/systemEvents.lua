-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Store local copies of Blizzard API and other addon global functions and variables
local setmetatable = setmetatable;

local isLoggedIn = false;

local addonCallbacks = {
	ADDON_LOADED = true,
	PLAYER_LOGIN = true,
	PLAYER_LOGOUT = true,
	PLAYER_REGEN_DISABLED = true,
	PLAYER_REGEN_ENABLED = true,
	SAVED_VARIABLES_TOO_LARGE = true,
};

local callbackFunctions = {
	AlwaysActive = {},
	Rotation = {},
};

local eventChanges = {
	[80000] = {
		Removed = {
			GLYPH_ADDED = true,
			GLYPH_REMOVED = true,
			GLYPH_UPDATED = true
		},
		Replaced = {
			UNIT_POWER = "UNIT_POWER_UPDATE"
		},
	},
};

local eventQueue = {
	Queue = {},
	SingleTrigger = {},
};

local registeredEvents = {};

local function getEventName(eventName)
	if addon.CURRENT_WOW_VERSION >= 80000 then
		if not eventChanges[80000].Removed[eventName] then
			return eventChanges[80000].Replaced[eventName] or eventName;
		else
			return nil;
		end
	end

	return eventName;
end

local function registerEvents(callbacks, alwaysActive)
	local pairs, tinsert = pairs, tinsert;

	if callbacks then
		local eventTable = callbackFunctions or {};
		local typeKey = (alwaysActive == nil or alwaysActive == false) and "Rotation" or "AlwaysActive";

		for key, value in pairs(callbacks) do
			local eventName = getEventName(key);

			if key then
				local found = false;

				registeredEvents[eventName] = true;
				eventTable[typeKey][eventName] = eventTable[typeKey][eventName] or {};

				for _, callback in pairs(eventTable[typeKey][eventName]) do
					if value == callback then
						found = true;
						break;
					end
				end

				if not found then
					tinsert(eventTable[typeKey][eventName], value);
				end
			end
		end

		callbackFunctions = eventTable;
	end
end

addon.System.Events = {
	CombatEventDelay = function(functionName, ...)
		local tinsert, type = tinsert, type;

		if type(functionName) == "function" then
			local apiUnit = addon.API.Unit;

			if apiUnit.InCombat() then
				local Tables = addon.Core.Tables;
				local Parameters = {...};

				if not Tables.IsEmpty(Parameters) then
					local eventInfo = {
						callBack = functionName,
						parameters = Parameters,
					};

					tinsert(eventQueue.Queue, eventInfo);
				elseif not Tables.GetValue(eventQueue.SingleTrigger, functionName) then
					local eventInfo = { callBack = functionName };

					tinsert(eventQueue.Queue, eventInfo);
					eventQueue.SingleTrigger[functionName] = true;
				end
			else
				functionName(...);
			end
		end
	end,

	EventAdd = function(key, value, alwaysActive)
		local ipairs, tinsert = ipairs, tinsert;
		local Tables = addon.Core.Tables;

		local eventTable = callbackFunctions or {};
		local typeKey = (alwaysActive == nil or alwaysActive == false) and "Rotation" or "AlwaysActive";

		local eventName = getEventName(key);

		if eventName then
			local found = false;

			if not Tables.GetValue(addonCallbacks, eventName) then
				registeredEvents[eventName] = true;
			end

			eventTable[typeKey][eventName] = eventTable[typeKey][eventName] or {};

			if not Tables.IsEmpty(eventTable[typeKey][eventName]) then
				for _, callback in ipairs(eventTable[typeKey][eventName]) do
					if value == callback then
						found = true;
						break;
					end
				end
			end

			if not found then
				tinsert(eventTable[typeKey][eventName], value);
			end
		end

		callbackFunctions = eventTable;
	end,

	EventRemove = function(key, value, alwaysActive)
		local ipairs = ipairs;
		local Tables = addon.Core.Tables;

		local eventTable = callbackFunctions or {};
		local typeKey = (alwaysActive == nil or alwaysActive == false) and "Rotation" or "AlwaysActive";

		local eventName = getEventName(key);

		if eventName then
			eventTable[typeKey][eventName] = eventTable[typeKey][eventName] or {};

			if not Tables.IsEmpty(eventTable[typeKey][eventName]) then
				for index, callback in ipairs(eventTable[typeKey][eventName]) do
					if value == callback then
						eventTable[typeKey][eventName][index] = nil;
						break;
					end
				end
			end

			if not Tables.GetValue(addonCallbacks, eventName) and Tables.IsEmpty(eventTable[typeKey][eventName]) then
				registeredEvents[eventName] = nil;
			end
		end

		callbackFunctions = eventTable;
	end,

	RegisterEvents = function(eventCallbacks, alwaysActive)
		registerEvents(eventCallbacks, alwaysActive);
	end,
};

local function processEvent(event, eventTable, eventActive, ...)
	local ipairs, type = ipairs, type;

	if eventActive then
		local Tables = addon.Core.Tables;
		local currentEvent = Tables.GetValue(eventTable, event, {});

		if not Tables.IsEmpty(currentEvent) then
			for _, eventFunction in ipairs(currentEvent) do
				if type(eventFunction) == "function" then
					eventFunction(event, ...);
				end
			end
		end
	end
end

local Alias = {
	COMBAT_LOG_EVENT_UNFILTERED  = "COMBAT_LOG_EVENT",
	UNIT_SPELLCAST_CHANNEL_START = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_CHANNEL_STOP = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_CHANNEL_UPDATE = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_DELAYED = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_FAILED = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_FAILED_QUIET = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_INTERRUPTED = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_START = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_STOP = "UNIT_SPELLCAST",
	UNIT_SPELLCAST_SUCCEEDED = "UNIT_SPELLCAST",
};

local Prototype = {
	-- API Version History
	-- 8.0 - Removed loadout from event
	COMBAT_LOG_EVENT = function(event, ...)
		return (addon.CURRENT_WOW_VERSION >= 80000 and { addon.API.Blizzard.CombatLogGetCurrentEventInfo() }) or { ... };
	end,

	-- API Version History
	-- 8.0 - Removed spell name and rank from event
	UNIT_SPELLCAST = function(event, ...)
		local unitID, _, _, lineID, spellID = ...;

		return (addon.CURRENT_WOW_VERSION >= 80000 and { ... }) or { unitID, lineID, spellID };
	end,
};

local metaTable = {
	__index = function(table, key)
		local Tables = addon.Core.Tables;

		local classKey = Tables.GetValue(Alias, key, key);
		local classPrototype = Prototype[classKey];

		if classPrototype then
			if type(classPrototype) == "function" then
				return function(...)
					return classPrototype(...);
				end
			else
				return classPrototype;
			end
		end

		return nil;
	end,

	__metatable = "This table is protected!",
};

local gameEvents = setmetatable({}, metaTable);

function addonTable.eventsHandler(event, ...)
	local type, unpack = type, unpack;
	local eventFunction = gameEvents[event];
	local parameters = { ... };

	if eventFunction and type(eventFunction) == "function" then
		parameters = eventFunction(event, ...);
	end

	processEvent(event, callbackFunctions.AlwaysActive, isLoggedIn, unpack(parameters));
	processEvent(event, callbackFunctions.Rotation, addon.currentRotation ~= nil, unpack(parameters));
end

local moduleEvents = {
	playerLogin = function()
		local pairs = pairs;
		local Tables = addon.Core.Tables;

		isLoggedIn = true;

		if not Tables.IsEmpty(registeredEvents) then
			for event in pairs(registeredEvents) do
				addon:RegisterEvent(event, addonTable.raisedEvents);
			end
		end
	end,

	leaveCombat = function()
		local ipairs, unpack, wipe = ipairs, unpack, wipe;
		local Tables = addon.Core.Tables;

		if not Tables.IsEmpty(eventQueue.Queue) then
			for _, eventInfo in ipairs(eventQueue.Queue) do
				if not Tables.IsEmpty(eventInfo.parameters) then
					eventInfo.callBack(unpack(eventInfo.parameters));
				else
					eventInfo.callBack();
				end
			end

			wipe(eventQueue.Queue);
			wipe(eventQueue.SingleTrigger);
		end
	end,
};

-- Register this as a module of the addon.
addon.System.Callbacks.RegisterAddonCallbacks(moduleEvents);