-- Pulls back the Addon-Local Variables and store them locally.
local addonName, addonTable = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local strupper = strupper;

-- This is stored on the addon so its publically available to all other addons.
addon.Core.Settings = {
	GetAccountValue = function(section, key, default)
		local Tables = addon.Core.Tables;
		local returnValue = Tables.GetValue(addonTable.accountSettings, section);

		if key then
			returnValue = Tables.GetValue(returnValue, key);
		end

		return returnValue or default;
	end,

	GetCharacterValue = function(section, key, default)
		local Tables = addon.Core.Tables;
		local returnValue = Tables.GetValue(addonTable.characterSettings, section);

		if key then
			returnValue = Tables.GetValue(returnValue, key);
		end

		return returnValue or default;
	end,

	GetRotationValue = function(key, default)
		local apiGeneral = addon.API.General;
		local Tables = addon.Core.Tables;
		local returnValue;

		if key then
			local specInfo = apiGeneral.GetSpecializationInfo();

			if not Tables.IsEmpty(specInfo) then
				local profile = Tables.GetValue(addonTable.characterSettings.RotationSettings, Tables.GetValue(specInfo, "Name"));

				returnValue = Tables.GetValue(profile, strupper(key));
			end
		end

		return returnValue or default or 2;
	end,

	SetAccountValue = function(section, key, value)
		local coreTables = addon.Core.Tables;
		local profile = coreTables.GetValue(addonTable.accountSettings, section);

		if not coreTables.IsEmpty(profile) then
			if key then
				addonTable.accountSettings[section][key] = value;
			else
				addonTable.accountSettings[section] = value;
			end
		end
	end,

	SetCharacterValue = function(section, key, value)
		local coreTables = addon.Core.Tables;
		local profile = coreTables.GetValue(addonTable.characterSettings, section);

		if not coreTables.IsEmpty(profile) then
			if key then
				addonTable.characterSettings[section][key] = value;
			else
				addonTable.characterSettings[section] = value;
			end
		end
	end,

	SetRotationValue = function(key, value)
		local apiGeneral = addon.API.General;
		local coreTables = addon.Core.Tables;

		if key then
			local specInfo = apiGeneral.GetSpecializationInfo();
			local section = coreTables.GetValue(specInfo, "Name", "");

			if section ~= "" then
				local profile = coreTables.GetValue(addonTable.characterSettings, section);

				if not coreTables.IsEmpty(profile) then
					addonTable.characterSettings[section][key] = value;
				end
			end
		end
	end,
};