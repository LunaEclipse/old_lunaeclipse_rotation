local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local val = coreGeneral.ToNumber;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

-- Rotation Variables
local nameAPL = "lunaeclipse_rogue_subtlety";

-- Builders Rotation
local function Builders(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- Legendaries will become a stat stick after level 115 so skip the legendary buff.
		-- actions.build=shuriken_storm,if=spell_targets.shuriken_storm>=2|buff.the_dreadlords_deceit.stack>=29
		ShurikenStorm = function(numEnemies)
			return numEnemies >= 2;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.ShurikenStorm, self.Requirements.ShurikenStorm, Enemies.GetEnemies(10));
		-- actions.build+=/gloomblade
		action.EvaluateAction(Talent.Gloomblade, true);
		-- actions.build+=/backstab
		action.EvaluateAction(Spell.Backstab, true);
	end

	-- actions+=/call_action_list,name=build,if=energy.deficit<=variable.stealth_threshold-40*!(talent.alacrity.enabled|talent.shadow_focus.enabled|talent.master_of_shadows.enabled)
	function self.Use()
		return Player.Energy.Deficit() <= Variables.stealth_threshold - 40 * val(not(Talent.Alacrity.Enabled() or Talent.ShadowFocus.Enabled() or Talent.MasterOfShadows.Enabled()));
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationBuilders = Builders("Builders");

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cds+=/berserking,if=stealthed.rogue
		Berserking = function()
			return Player.IsStealthed();
		end,

		-- actions.cds+=/blood_fury,if=stealthed.rogue
		BloodFury = function()
			return Player.IsStealthed();
		end,

		MarkedForDeath = {
			-- This choses the target that will die first, this is part of the cycle action and will be done once EvaluateCycleAction is completed.
			-- actions.cds+=/marked_for_death,target_if=min:target.time_to_die,if=target.time_to_die<combo_points.deficit
			Dying = function(numEnemies, Target)
				return Target.TimeToDie() < Player.ComboPoints.Deficit();
			end,

			-- Can't do raid add events so just skip this, have no idea how to do ComboPoints max spend, so as it gives full combo points just check for 0 combo points.
			-- actions.cds+=/marked_for_death,if=raid_event.adds.in>30&!stealthed.all&combo_points.deficit>=cp_max_spend
			Use = function()
				return not Player.IsStealthed()
				   and Player.ComboPoints() == 0;
			end,
		},

		-- actions.cds=potion,if=buff.bloodlust.react|target.time_to_die<=60|(buff.vanish.up&(buff.shadow_blades.up|cooldown.shadow_blades.remains<=30))
		BattlePotionOfAgility = function()
			return Player.HasBloodlust()
			    or Target.TimeToDie() <= 60
			    or (Player.Buff(Aura.Vanish).Up() and (Player.Buff(Aura.ShadowBlades).Up() or Spell.ShadowBlades.Cooldown.Remains() <= 30));
		end,

		-- The addon doesn't seperate out class stealth buffs from other stealth buffs unless done manually, so lets just check if the player is stealthed in the equation.
		-- actions.cds+=/shadow_blades,if=combo_points.deficit>=2+stealthed.all
		ShadowBlades = function()
			return Player.ComboPoints.Deficit() >= 2 + val(Player.IsStealthed());
		end,

		-- actions.cds+=/shadow_dance,if=!buff.shadow_dance.up&target.time_to_die<=5+talent.subterfuge.enabled
		ShadowDance = function()
			return not Player.Buff(Aura.ShadowDance).Up()
			   and Target.TimeToDie() <= 5 + val(Talent.Subterfuge.Enabled());
		end,

		-- actions.cds+=/shuriken_tornado,if=spell_targets>=3&dot.nightblade.ticking&buff.symbols_of_death.up&buff.shadow_dance.up
		ShurikenTornado = function(numEnemies)
			return numEnemies >= 3
			   and Target.Debuf(Aura.Nightblade).Ticking()
			   and Player.Buff(Aura.SymbolsOfDeath).Up()
			   and Player.Buff(Aura.ShadowDance).Up();
		end,

		-- actions.cds+=/symbols_of_death,if=dot.nightblade.ticking
		SymbolsOfDeath = function()
			return Target.Debuff(Aura.Nightblade).Ticking();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Consumable.BattlePotionOfAgility, self.Requirements.BattlePotionOfAgility);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
		action.EvaluateAction(Spell.SymbolsOfDeath, self.Requirements.SymbolsOfDeath);
		action.EvaluateCycleAction(Talent.MarkedForDeath, self.Requirements.MarkedForDeath.Dying);
		action.EvaluateAction(Talent.MarkedForDeath, self.Requirements.MarkedForDeath.Use);
		action.EvaluateAction(Spell.ShadowBlades, self.Requirements.ShadowBlades);
		action.EvaluateAction(Talent.ShurikenTornado, self.Requirements.ShurikenTornado, Enemies.GetEnemies(10));
		action.EvaluateAction(Spell.ShadowDance, self.Requirements.ShadowDance);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Finishers Rotation
local function Finishers(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		Nightblade = {
			-- actions.finish+=/nightblade,cycle_targets=1,if=spell_targets.shuriken_storm>=2&(spell_targets.shuriken_storm<=5|talent.secret_technique.enabled)&!buff.shadow_dance.up&target.time_to_die>=(5+(2*combo_points))&refreshable
			AOE = function(numEnemies, Target)
				return numEnemies >= 2
				   and (numEnemies <= 5 or Talent.SecretTechnique.Enabled())
				   and not Player.Buff(Aura.ShadowDance).Up()
				   and Target.TimeToDie() >= (5 + (2 * Player.ComboPoints()))
				   and Target.Debuff(Aura.Nightblade).Refreshable();
			end,

			-- Debuff ticks every two seconds, for now just hard code this
			-- actions.finish=nightblade,if=(!talent.dark_shadow.enabled|!buff.shadow_dance.up)&target.time_to_die-remains>6&remains<tick_time*2&(spell_targets.shuriken_storm<4|!buff.symbols_of_death.up)
			Refresh = function(numEnemies)
				local tick_time = 2;

				return (not Talent.DarkShadow.Enabled() or not Player.Buff(Aura.ShadowDance).Up())
				   and Target.TimeToDie() - Target.Debuff(Aura.Nightblade).Remains() > 6
				   and Target.Debuff(Aura.Nightblade).Remains() < tick_time * 2
				   and (numEnemies < 4 or not Player.Buff(Aura.SymbolsOfDeath).Up());
			end,

			-- actions.finish+=/nightblade,if=remains<cooldown.symbols_of_death.remains+10&cooldown.symbols_of_death.remains<=5&target.time_to_die-remains>cooldown.symbols_of_death.remains+5
			Use = function()
				return Target.Debuff(Aura.Nightblade).Remains() < Spell.SymbolsOfDeath.Cooldown.Remains() + 10
				   and Spell.SymbolsOfDeath.Cooldown.Remains() <= 5
				   and Target.TimeToDie() - Target.Debuff(Aura.Nightblade).Remains() > Spell.SymbolsOfDeath.Cooldown.Remains() + 5;
			end,
		},

		SecretTechnique = {
			-- actions.finish+=/secret_technique,if=buff.symbols_of_death.up&(!talent.dark_shadow.enabled|spell_targets.shuriken_storm<2|buff.shadow_dance.up)
			SymbolsOfDeath = function(numEnemies)
				return Player.Buff(Aura.SymbolsOfDeath).Up()
				   and (not Talent.DarkShadow.Enabled() or numEnemies < 2 or Player.Buff(Aura.ShadowDance).Up());
			end,

			-- actions.finish+=/secret_technique,if=spell_targets.shuriken_storm>=2+talent.dark_shadow.enabled+talent.nightstalker.enabled
			Use = function(numEnemies)
				return numEnemies >= 2 + val(Talent.DarkShadow.Enabled()) + val(Talent.Nightstalker.Enabled());
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Nightblade, self.Requirements.Nightblade.Refresh, Enemies.GetEnemies(10));
		action.EvaluateCycleAction(Spell.Nightblade, self.Requirements.Nightblade.AOE, Enemies.GetEnemies(10));
		action.EvaluateAction(Spell.Nightblade, self.Requirements.Nightblade.Use, Enemies.GetEnemies(10));
		action.EvaluateAction(Talent.SecretTechnique, self.Requirements.SecretTechnique.SymbolsOfDeath, Enemies.GetEnemies(10));
		action.EvaluateAction(Talent.SecretTechnique, self.Requirements.SecretTechnique.Use, Enemies.GetEnemies(10));
		-- actions.finish+=/eviscerate
		action.EvaluateAction(Spell.Eviscerate, true);
	end

	-- actions+=/call_action_list,name=finish,if=combo_points>=4+talent.deeper_stratagem.enabled|target.time_to_die<=1&combo_points>=3
	function self.Use()
		return Player.ComboPoints() >= 4 + val(Talent.DeeperStratagem.Enabled())
		    or Target.TimeToDie() <= 1
		   and Player.ComboPoints() >= 3;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationFinishers = Finishers("Finishers");

-- StealthCooldowns Rotation
local function StealthCooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		ShadowDance = {
			-- actions.stealth_cds+=/shadow_dance,if=target.time_to_die<cooldown.symbols_of_death.remains
			Dying = function()
				return Target.TimeToDie() < Spell.SymbolsOfDeath.Cooldown.Remains();
			end,

			-- actions.stealth_cds+=/shadow_dance,if=(!talent.dark_shadow.enabled|dot.nightblade.remains>=5+talent.subterfuge.enabled)&(variable.shd_threshold|buff.symbols_of_death.remains>=1.2|spell_targets>=4&cooldown.symbols_of_death.remains>10)
			Use = function(numEnemies)
				return (not Talent.DarkShadow.Enabled() or Target.Debuff(Aura.Nightblade).Remains() >= 5 + val(Talent.Subterfuge.Enabled()))
				   and (Variables.shd_threshold or Player.Buff(Aura.SymbolsOfDeath).Remains() >= 1.2 or numEnemies >= 4 and Spell.SymbolsOfDeath.Cooldown.Remains() > 10);
			end,
		},

		-- actions.stealth_cds+=/shadowmeld,if=energy>=40&energy.deficit>=10&!variable.shd_threshold&debuff.find_weakness.remains<1
		Shadowmeld = function()
			return Player.Energy() >= 40
			   and Player.Energy.Deficit() >= 10
			   and not Variables.shd_threshold
			   and Target.Debuff(Aura.FindWeakness).Remains() < 1;
		end,

		-- actions.stealth_cds+=/vanish,if=!variable.shd_threshold&debuff.find_weakness.remains<1
		Vanish = function()
			return not Variables.shd_threshold
			   and Target.Debuff(Aura.FindWeakness).Remains() < 1;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		-- actions.stealth_cds=variable,name=shd_threshold,value=cooldown.shadow_dance.charges_fractional>=1.75
		Variables.shd_threshold = Spell.ShadowDance.Charges.Fractional() >= 1.75;

		action.EvaluateAction(Spell.Vanish, self.Requirements.Vanish);
		action.EvaluatePoolAction(Racial.Shadowmeld, self.Requirements.Shadowmeld);
		action.EvaluateAction(Spell.ShadowDance, self.Requirements.ShadowDance.Use);
		action.EvaluateAction(Spell.ShadowDance, self.Requirements.ShadowDance.Dying);
	end

	-- actions+=/call_action_list,name=stealth_cds,if=energy.deficit<=variable.stealth_threshold&combo_points.deficit>=4
	function self.Use()
		return Player.Energy.Deficit() <= Variables.stealth_threshold
		   and Player.ComboPoints.Deficit() >= 4;
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationStealthCooldowns = StealthCooldowns("StealthCooldowns");

-- Stealthed Rotation
local function Stealthed(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.stealthed+=/call_action_list,name=finish,if=combo_points.deficit<=1-(talent.deeper_stratagem.enabled&buff.vanish.up)
		Finishers = function()
			return Player.ComboPoints.Deficit() <= 1 - val(Talent.DeeperStratagem.Enabled() and Player.Buff(Aura.Vanish).Up());
		end,

		Shadowstrike = {
			-- actions.stealthed=shadowstrike,if=buff.stealth.up
			Stealth = function()
				return Player.IsStealthed();
			end,

			-- actions.stealthed+=/shadowstrike,cycle_targets=1,if=talent.secret_technique.enabled&talent.find_weakness.enabled&debuff.find_weakness.remains<1&spell_targets.shuriken_storm=2&target.time_to_die-remains>6
			Use = function(numEnemies, Target)
				return Talent.SecretTechnique.Enabled()
				   and Talent.FindWeakness.Enabled()
				   and Target.Debuff(Aura.FindWeakness).Remains() < 1
				   and numEnemies == 2
				   and Target.TimeToDie() - Target.Debuff(Aura.FindWeakness).Remains() > 6;
			end,
		},

		-- actions.stealthed+=/shuriken_storm,if=spell_targets.shuriken_storm>=3
		ShurikenStorm = function(numEnemies)
			return numEnemies >= 3;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Shadowstrike, self.Requirements.Shadowstrike.Stealth);

		action.CallActionList(rotationFinishers, self.Requirements.Finishers);

		action.EvaluateCycleAction(Spell.Shadowstrike, self.Requirements.Shadowstrike.Use, Enemies.GetEnemies(10));
		action.EvaluateAction(Spell.ShurikenStorm, self.Requirements.ShurikenStorm, Enemies.GetEnemies(10));
		-- actions.stealthed+=/shadowstrike
		action.EvaluateAction(Spell.Shadowstrike, true);
	end

	-- actions+=/run_action_list,name=stealthed,if=stealthed.all
	function self.Use()
		return Player.IsStealthed();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationStealthed = Stealthed("Stealthed");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "GrayHound and SimCraft",
		GuideLink = "http://www.wowhead.com/subtlety-rogue-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		CloakOfShadows = function()
			return Player.MagicDamagePredicted(5) >= 60;
		end,

		CrimsonVial = function()
			return Player.Health.Percent() < 70
			   and Player.DamagePredicted(5) >= 25;
		end,

		Evasion = function()
			return Player.PhysicalDamagePredicted(5) >= 60;
		end,

		Feint = function()
			return Player.DamagePredicted(5) >= 40;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/arcane_torrent,if=energy.deficit>=15+energy.regen
		ArcaneTorrent = function()
			return Player.Energy.Deficit() >= 15 + Player.Energy.CastRegen(Racial.ArcaneTorrent);
		end,

		-- actions+=/nightblade,if=target.time_to_die>6&remains<gcd.max&combo_points>=4-(time<10)*2
		Nightblade = function()
			return Target.TimeToDie() > 6
			   and Target.Debuff(Aura.Nightblade).Remains() < Player.GCD()
			   and Player.ComboPoints() >= 4 - val(coreGeneral.CombatTime() < 10) * 2;
		end,

		-- SimCraft doesn't specify it, but make sure we are not already stealthed before suggesting stealth.
		-- actions.precombat+=/stealth
		Stealth = function()
			return not Player.IsStealthed();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfAgility = Objects.newItem(163223),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.AncestralCall, Racial.ArcanePulse, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood, Racial.Shadowmeld);
		config.AOEOptions(options, Talent.SecretTechnique, Spell.ShurikenStorm, Talent.ShurikenTornado);
		config.CooldownOptions(options, Talent.Gloomblade, Talent.MarkedForDeath, Spell.ShadowBlades, Spell.ShadowDance, Spell.SymbolsOfDeath);
		config.DefensiveOptions(options, Spell.CloakOfShadows, Spell.CrimsonVial, Spell.Evasion, Spell.Feint);
		config.UtilityOptions(options, Spell.Stealth, Spell.Vanish);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against Magic damage
		action.EvaluateDefensiveAction(Spell.CloakOfShadows, self.Defensives.CloakOfShadows);
		-- Protects against Physical damage
		action.EvaluateDefensiveAction(Spell.Evasion, self.Defensives.Evasion);
		-- Protects against All damage (AOE)
		action.EvaluateDefensiveAction(Spell.Feint, self.Defensives.Feint);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Spell.CrimsonVial, self.Defensives.CrimsonVial);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	function self.PrecombatVariables(action)
		-- actions.precombat+=/variable,name=stealth_threshold,value=60+talent.vigor.enabled*35+talent.master_of_shadows.enabled*10
		Variables.stealth_threshold = 60 + val(Talent.Vigor.Enabled()) * 35 + val(Talent.MasterOfShadows.Enabled()) * 10;
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		action.EvaluateAction(Spell.Stealth, self.Requirements.Stealth);
		-- Addon doesn't know how long until combat starts unless there is a pull timer, so just recommend this without restriction.
		-- actions.precombat+=/marked_for_death,precombat_seconds=15
		action.EvaluateAction(Talent.MarkedForDeath, true);
		-- Addon doesn't know how long until combat starts unless there is a pull timer, so just recommend this without restriction.
		-- actions.precombat+=/shadow_blades,precombat_seconds=1
		action.EvaluateAction(Spell.ShadowBlades, true);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfAgility, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions=call_action_list,name=cds
		action.CallActionList(rotationCooldowns);
		action.RunActionList(rotationStealthed);

		action.EvaluateAction(Spell.Nightblade, self.Requirements.Nightblade);

		action.CallActionList(rotationStealthCooldowns);
		action.CallActionList(rotationFinishers);
		action.CallActionList(rotationBuilders);

		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
		-- actions+=/arcane_pulse
		action.EvaluateAction(Racial.ArcanePulse, true);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Subtlety Rogue", addonTable.Enum.SpecID.ROGUE_SUBTLETY);
