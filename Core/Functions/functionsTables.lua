-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

local assert, format, ipairs, pairs, print, setmetatable, sort, strlen, strlower, strrep, tostring, type = assert, format, ipairs, pairs, print, setmetatable, sort, strlen, strlower, strrep, tostring, type;

-- This is stored on the addon so its publically available to all other addons.
addon.Core.Tables = {
	ApplyDefaults = function(tableName, defaultValues)
		local Tables = addon.Core.Tables;
		local returnValue = Tables.Duplicate(tableName) or {};

		if not Tables.IsEmpty(defaultValues) then
			for key, value in pairs(defaultValues) do
				if not returnValue[key] then
					returnValue[key] = value;
				elseif not Tables.IsEmpty(value) then
					returnValue[key] = Tables.ApplyDefaults(returnValue[key], value);
				end
			end
		end

		return returnValue;
	end,

	CaseInsensitive = function(...)
		local Tables = addon.Core.Tables;
		local args = {...};

		-- Set the metatable to allow keys to be case insensitive.
		local metatable = {
			__mode = "k",

			__index = function(table, key)
				for k in pairs(table) do
					if strlower(k) == strlower(key) then
						return table[k];
					end
				end
			end,

			__metatable = "This table is protected!",
		};

		if not Tables.IsEmpty(args) then
			for _, table in ipairs(args) do
				-- Attach the metatable
				setmetatable(table, metatable);
			end
		else
			local returnValue = {};

			setmetatable(returnValue, metatable);

			return returnValue;
		end
	end,

	Compare = function(firstTable, secondTable)
		local Tables = addon.Core.Tables;

		if Tables.IsTable(firstTable) and Tables.IsTable(secondTable) then
			for key, value in pairs(firstTable) do
				if Tables.IsTable(value) then
					if not Tables.Compare(value, secondTable[key]) then
						return false;
					end
				else
					if value ~= secondTable[key] then
						return false;
					end
				end
			end

			return true;
		end

		return firstTable == secondTable;
	end,

	Count = function(tableName)
		local returnValue = 0;

		if type(tableName) == "table" then
			for _, _ in pairs(tableName) do
				returnValue = returnValue + 1;
			end
		end

		return returnValue;
	end,

	Differences = function(firstTable, secondTable)
		local Tables = addon.Core.Tables;
		local returnValue = {};

		if Tables.IsTable(firstTable) and Tables.IsTable(secondTable) then
			for key, value in pairs(firstTable) do
				if Tables.IsTable(value) then
					local changes = Tables.Differences(value, secondTable[key]);

					if not Tables.IsEmpty(changes) then
						returnValue[key] = changes;
					end
				else
					if value ~= secondTable[key] then
						returnValue[key] = secondTable[key];
					end
				end
			end
		end

		if Tables.IsEmpty(returnValue) then
			returnValue = nil;
		end

		return returnValue;
	end,

	Duplicate = function(tableName)
		local Tables = addon.Core.Tables;
		local returnValue = {};

		if not Tables.IsEmpty(tableName) then
			for key, value in pairs(tableName) do
				if Tables.IsTable(value) then
					returnValue[key] = Tables.Duplicate(value);
				else
					returnValue[key] = value;
				end
			end
		end

		return returnValue;
	end,

	Equate = function(firstTable, secondTable)
		local Tables = addon.Core.Tables;
		local returnValue = firstTable == secondTable;

		if Tables.IsTable(firstTable) and Tables.IsTable(secondTable) then
			-- Compare the contents of the first table to the second table for missing or unequal values
			returnValue = Tables.Compare(firstTable, secondTable);

			-- Check to see if the contents were the same
			if returnValue then
				-- Compare the contents of the second table to the first table for missing values
				returnValue = Tables.Compare(secondTable, firstTable);
			end
		end

		return returnValue;
	end,

	-- Gets an enum table key from the value.
	GetKey = function(enumTable, checkValue)
		local Tables = addon.Core.Tables;

		if not Tables.IsEmpty(enumTable) then
			for key, value in pairs(enumTable) do
				if value == checkValue then
					return key;
				end
			end
		end

		return nil;
	end,

	GetValue = function(Object, property, default)
		local Tables = addon.Core.Tables;
		local returnValue = default;

		if not Tables.IsEmpty(Object) then
			local propertyValue = Object[property];
			local valueType = type(propertyValue);

			if valueType == "function" then
				returnValue = propertyValue();
			elseif propertyValue ~= nil then
				returnValue = propertyValue;
			end
		end

		return returnValue;
	end,

	IsEmpty = function(tableName)
		local Tables = addon.Core.Tables;

		return Tables.Count(tableName) == 0;
	end,

	IsTable = function(tableName)
		return (tableName and type(tableName) == "table") or false;
	end,

	Merge = function(firstTable, secondTable, noOverwrite)
		local Tables = addon.Core.Tables;
		local returnValue;

		if Tables.IsTable(firstTable) or Tables.IsTable(secondTable) then
			returnValue = Tables.Duplicate(firstTable);

			if not Tables.IsEmpty(secondTable) then
				for key, value in pairs(secondTable) do
					assert(not noOverwrite or (noOverwrite and firstTable[key] == nil), format("Can't merge %s into first table as the key already exists!", key));

					if Tables.IsTable(value) then
						returnValue[key] = Tables.Merge(returnValue[key], value);
					else
						returnValue[key] = value;
					end
				end
			end
		end

		return returnValue;
	end,

	Sort = function(tableName, order)
		-- collect the keys
		local tableKeys = {};

		for key in pairs(tableName) do
			tableKeys[#tableKeys +1] = key;
		end

		-- if order function given, sort by it by passing the table and keys a, b, otherwise just sort the keys
		if order then
			sort(tableKeys, function(a, b) return order(tableName, a, b) end);
		else
			sort(tableKeys);
		end

		-- return the iterator function
		local counter = 0;

		return function()
			counter = counter + 1;

			if tableKeys[counter] then
				return tableKeys[counter], tableName[tableKeys[counter]];
			end
		end
	end,

	Wipe = function(...)
		local Tables = addon.Core.Tables;
		local args = {...};

		if not Tables.IsEmpty(args) then
			for _, table in ipairs(args) do
				if not Tables.IsEmpty(table) then
					for key, value in pairs(table) do
						if type(value) == "table" then
							Tables.Wipe(value);
						else
							table[key] = nil;
						end
					end
				end
			end
		end
	end,
};