local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];

-- Localize Vars
local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local val = coreGeneral.ToNumber;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

local nameAPL = "lunaeclipse_demonhunter_havoc";

-- Cooldown Rotation
local function Cooldown(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		Metamorphosis = {
			-- actions.cooldown+=/metamorphosis,if=talent.demonic.enabled&buff.metamorphosis.up
			Demonic = function()
				return Talent.Demonic.Enabled() and Player.Buff(Aura.Metamorphosis).Up();
			end,

			-- actions.cooldown=metamorphosis,if=!(talent.demonic.enabled|variable.pooling_for_meta|variable.waiting_for_nemesis)|target.time_to_die<25
			Use = function()
				return not (Talent.Demonic.Enabled() or Variables.pooling_for_meta or Variables.waiting_for_nemesis)
				    or Target.TimeToDie() < 25;
			end,
		},

		-- Can't do raid_event.adds so just skip these.  Skip target_if as this is for multi-dot and will be implimented once EvaluateCycleAction is finished.
		-- actions.cooldown+=/nemesis,target_if=min:target.time_to_die,if=raid_event.adds.exists&debuff.nemesis.down&(active_enemies>desired_targets|raid_event.adds.in>60)
		Nemesis = function(numEnemies, Target)
			return Target.Debuff(Aura.Nemesis).Down()
			   and numEnemies > coreGeneral.DesiredTargets();
		end,

		-- actions.cooldown+=/potion,if=buff.metamorphosis.remains>25|target.time_to_die<60
		BattlePotionOfAgility = function()
			return Player.Buff(Aura.Metamorphosis).Remains() > 25
			    or Target.TimeToDie() < 60;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Metamorphosis, self.Requirements.Metamorphosis.Use);
		action.EvaluateAction(Spell.Metamorphosis, self.Requirements.Metamorphosis.Demonic);
		action.EvaluateCycleAction(Talent.Nemesis, self.Requirements.Nemesis);
		-- Can't do raid_event.adds so just skip this.
		-- actions.cooldown+=/nemesis,if=!raid_event.adds.exists
		action.EvaluateAction(Consumable.BattlePotionOfAgility, self.Requirements.BattlePotionOfAgility);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldown = Cooldown("Cooldown");

-- DarkSlash Rotation
local function DarkSlash(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.dark_slash+=/annihilation,if=debuff.dark_slash.up
		Annihilation = function()
			return Target.Debuff(Aura.DarkSlash).Up();
		end,

		-- actions.dark_slash+=/chaos_strike,if=debuff.dark_slash.up
		ChaosStrike = function()
			return Target.Debuff(Aura.DarkSlash).Up();
		end,

		-- actions.dark_slash=dark_slash,if=fury>=80&(!variable.blade_dance|!cooldown.blade_dance.ready)
		DarkSlash = function()
			return Player.Fury() >= 80
			   and (not Variables.blade_dance or not Spell.BladeDance.Cooldown.Ready());
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.DarkSlash, self.Requirements.DarkSlash);
		action.EvaluateAction(Spell.Annihilation, self.Requirements.Annihilation);
		action.EvaluateAction(Spell.ChaosStrike, self.Requirements.ChaosStrike);
	end

	-- actions+=/call_action_list,name=dark_slash,if=talent.dark_slash.enabled&(variable.waiting_for_dark_slash|debuff.dark_slash.up)
	function self.Use()
		return Talent.DarkSlash.Enabled()
		   and (Variables.waiting_for_dark_slash or Target.Debuff(Aura.DarkSlash).Up());
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationDarkSlash = DarkSlash("DarkSlash");

-- Demonic Rotation
local function Demonic(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.demonic+=/annihilation,if=(talent.blind_fury.enabled|fury.deficit<30|buff.metamorphosis.remains<5)&!variable.pooling_for_blade_dance
		Annihilation = function()
			return (Talent.BlindFury.Enabled() or Player.Fury.Deficit() < 50 or Player.Buff(Aura.Metamorphosis).Remains() < 5)
			   and not Variables.pooling_for_blade_dance;
		end,

		-- actions.demonic+=/blade_dance,if=variable.blade_dance&cooldown.eye_beam.remains>5&!cooldown.metamorphosis.ready
		BladeDance = function()
			return Variables.blade_dance
			   and Spell.EyeBeam.Cooldown.Remains() > 5
			   and not Spell.Metamorphosis.Cooldown.Ready();
		end,

		-- actions.demonic+=/chaos_strike,if=(talent.blind_fury.enabled|fury.deficit<30)&!variable.pooling_for_meta&!variable.pooling_for_blade_dance
		ChaosStrike = function()
			return (Talent.BlindFury.Enabled() or Player.Fury.Deficit() < 50)
			   and not Variables.pooling_for_meta
			   and not Variables.pooling_for_blade_dance;
		end,

		-- actions.demonic+=/death_sweep,if=variable.blade_dance
		DeathSweep = function()
			return Variables.blade_dance;
		end,

		-- To determine if metamorphosis was extended we will check for buff duration longer than standard metamorphosis.
		-- actions.demonic+=/eye_beam,if=(!talent.blind_fury.enabled|fury.deficit>=70)&(!buff.metamorphosis.extended_by_demonic|(set_bonus.tier21_4pc&buff.metamorphosis.remains>16))
		EyeBeam = function()
			return (not Talent.BlindFury.Enabled() or Player.Fury.Deficit() >= 70)
			   and (Player.Buff(Aura.Metamorphosis).Duration() <= Spell.Metamorphosis.BaseDuration() or (addonTable.Tier21_4PC and Player.Buff(Aura.Metamorphosis).Remains() > 16));
		end,

		-- Can't do raid_event.adds so just skip these.
		-- actions.demonic=fel_barrage,if=active_enemies>desired_targets|raid_event.adds.in>30
		FelBarrage = function(numEnemies)
			return numEnemies > coreGeneral.DesiredTargets();
		end,

		-- actions.demonic+=/felblade,if=fury<40|(buff.metamorphosis.down&fury.deficit>=40)
		Felblade = function()
			return Player.Fury() < 40
			    or (Player.Buff(Aura.Metamorphosis).Down() and Player.Fury.Deficit() >= 40);
		end,

		FelRush = {
			-- Can't do raid_event.adds or raid_event.movement so just skip these.
			-- actions.demonic+=/fel_rush,if=talent.demon_blades.enabled&!cooldown.eye_beam.ready&(charges=2|(raid_event.movement.in>10&raid_event.adds.in>10))
			DemonBlades = function()
				return Talent.DemonBlades.Enabled()
				   and not Spell.EyeBeam.Cooldown.Ready()
				   and Spell.FelRush.Charges() == 2;
			end,

			-- Treat buff.out_of_range.up as outside 8 yard range
			-- actions.demonic+=/fel_rush,if=movement.distance>15|buff.out_of_range.up
			Use = function()
				return not Target.InRange(8);
			end,
		},

		ThrowGlaive = {
			-- Treat buff.out_of_range.up as outside 8 yard range
			-- actions.demonic+=/throw_glaive,if=buff.out_of_range.up
			OutOfRange = function()
				return not Target.InRange(8);
			end,

			-- actions.demonic+=/throw_glaive,if=talent.demon_blades.enabled
			Use = function()
				return Talent.DemonBlades.Enabled();
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Talent.FelBarrage, self.Requirements.FelBarrage);
		action.EvaluateAction(Spell.DeathSweep, self.Requirements.DeathSweep);
		action.EvaluateAction(Spell.BladeDance, self.Requirements.BladeDance);
		-- actions.demonic+=/immolation_aura
		action.EvaluateAction(Talent.ImmolationAura, true);
		action.EvaluateAction(Talent.Felblade, self.Requirements.Felblade);
		action.EvaluateAction(Spell.EyeBeam, self.Requirements.EyeBeam);
		action.EvaluateAction(Spell.Annihilation, self.Requirements.Annihilation);
		action.EvaluateAction(Spell.ChaosStrike, self.Requirements.ChaosStrike);
		action.EvaluateAction(Spell.FelRush, self.Requirements.FelRush.DemonBlades);
		-- actions.demonic+=/demons_bite
		action.EvaluateAction(Spell.DemonsBite, true);
		action.EvaluateAction(Spell.ThrowGlaive, self.Requirements.ThrowGlaive.OutOfRange);
		action.EvaluateAction(Spell.FelRush, self.Requirements.FelRush.Use);
		-- We can't do movement events so lets just skip this.
		-- actions.demonic+=/vengeful_retreat,if=movement.distance>15
		action.EvaluateAction(Spell.ThrowGlaive, self.Requirements.ThrowGlaive.Use);
	end

	-- actions+=/run_action_list,name=demonic,if=talent.demonic.enabled
	function self.Use()
		return Talent.Demonic.Enabled();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationDemonic = Demonic("Demonic");

-- Normal Rotation
local function Normal(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.normal+=/annihilation,if=(talent.demon_blades.enabled|!variable.waiting_for_momentum|fury.deficit<30|buff.metamorphosis.remains<5)&!variable.pooling_for_blade_dance&!variable.waiting_for_dark_slash
		Annihilation = function()
			return (Talent.DemonBlades.Enabled() or not Variables.waiting_for_momentum or Player.Fury.Deficit() < 50 or Player.Buff(Aura.Metamorphosis).Remains() < 5)
			   and not Variables.pooling_for_blade_dance
			   and not Variables.waiting_for_dark_slash;
		end,

		-- actions.normal+=/blade_dance,if=variable.blade_dance
		BladeDance = function()
			return Variables.blade_dance;
		end,

		-- actions.normal+=/chaos_strike,if=(talent.demon_blades.enabled|!variable.waiting_for_momentum|fury.deficit<30)&!variable.pooling_for_meta&!variable.pooling_for_blade_dance&!variable.waiting_for_dark_slash
		ChaosStrike = function()
			return (Talent.DemonBlades.Enabled() or not Variables.waiting_for_momentum or Player.Fury.Deficit() < 50)
			   and not Variables.pooling_for_meta
			   and not Variables.pooling_for_blade_dance
			   and not Variables.waiting_for_dark_slash;
		end,

		-- actions.normal+=/death_sweep,if=variable.blade_dance
		DeathSweep = function()
			return Variables.blade_dance;
		end,

		EyeBeam = {
			-- Can't do raid_event.adds so just skip these.
			-- actions.normal+=/eye_beam,if=active_enemies>1&(!raid_event.adds.exists|raid_event.adds.up)&!variable.waiting_for_momentum
			AOE = function(numEnemies)
				return numEnemies > 1
				   and not Variables.waiting_for_momentum;
			end,

			-- Can't do raid_event.adds so just skip these.
			-- actions.normal+=/eye_beam,if=talent.blind_fury.enabled&raid_event.adds.in>cooldown
			BlindFury = function()
				return Talent.BlindFury.Enabled();
			end,

			-- Can't do raid_event.adds so just skip these.
			-- actions.normal+=/eye_beam,if=!talent.blind_fury.enabled&!variable.waiting_for_dark_slash&raid_event.adds.in>cooldown
			Use = function()
				return not Talent.BlindFury.Enabled()
				   and not Variables.waiting_for_dark_slash;
			end,
		},

		-- Can't do raid_event.adds so just skip these.
		-- actions.normal+=/fel_barrage,if=!variable.waiting_for_momentum&(active_enemies>desired_targets|raid_event.adds.in>30)
		FelBarrage = function(numEnemies)
			return not Variables.waiting_for_momentum
			   and numEnemies > coreGeneral.DesiredTargets();
		end,

		-- actions.normal+=/felblade,if=fury.deficit>=40
		Felblade = function()
			return Player.Fury.Deficit() >= 40;
		end,

		FelRush = {
			-- Can't do raid_event.movement so just skip these.
			-- actions.normal+=/fel_rush,if=!talent.momentum.enabled&raid_event.movement.in>charges*10&talent.demon_blades.enabled
			DemonBlades = function()
				return not Talent.Momentum.Enabled()
				   and Talent.DemonBlades.Enabled();
			end,

			-- Can't do raid_event.adds or raid_event.movement so just skip these.
			-- actions.normal+=/fel_rush,if=(variable.waiting_for_momentum|talent.fel_mastery.enabled)&(charges=2|(raid_event.movement.in>10&raid_event.adds.in>10))
			FelMastery = function()
				return (Variables.waiting_for_momentum or Talent.FelMastery.Enabled())
				   and Spell.FelRush.Charges() == 2;
			end,

			-- Treat buff.out_of_range.up as outside 8 yard range
			-- Can't do raid_event.movement so just skip these.
			-- actions.normal+=/fel_rush,if=movement.distance>15|(buff.out_of_range.up&!talent.momentum.enabled)
			Use = function()
				return not Target.InRange(8)
				   and not Talent.Momentum.Enabled();
			end,
		},

		-- actions.normal+=/throw_glaive,if=talent.demon_blades.enabled
		ThrowGlaive = function()
			return Talent.DemonBlades.Enabled();
		end,

		-- actions.normal=vengeful_retreat,if=talent.momentum.enabled&buff.prepared.down
		VengefulRetreat = function()
			return Talent.Momentum.Enabled()
			   and Player.Buff(Aura.Prepared).Down();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.VengefulRetreat, self.Requirements.VengefulRetreat);
		action.EvaluateAction(Spell.FelRush, self.Requirements.FelRush.FelMastery);
		action.EvaluateAction(Talent.FelBarrage, self.Requirements.FelBarrage);
		-- actions.normal+=/immolation_aura
		action.EvaluateAction(Talent.ImmolationAura, true);
		action.EvaluateAction(Spell.EyeBeam, self.Requirements.EyeBeam.AOE);
		action.EvaluateAction(Spell.DeathSweep, self.Requirements.DeathSweep)
		action.EvaluateAction(Spell.BladeDance, self.Requirements.BladeDance)
		action.EvaluateAction(Talent.Felblade, self.Requirements.Felblade);
		action.EvaluateAction(Spell.EyeBeam, self.Requirements.EyeBeam.Use);
		action.EvaluateAction(Spell.Annihilation, self.Requirements.Annihilation);
		action.EvaluateAction(Spell.ChaosStrike, self.Requirements.ChaosStrike);
		action.EvaluateAction(Spell.EyeBeam, self.Requirements.EyeBeam.BlindFury);
		-- actions.normal+=/demons_bite
		action.EvaluateAction(Spell.DemonsBite, true);
		action.EvaluateAction(Spell.FelRush, self.Requirements.FelRush.DemonBlades);
		-- actions.normal+=/felblade,if=movement.distance>15|buff.out_of_range.up
		action.EvaluateAction(Spell.FelRush, self.Requirements.FelRush.Use);
		-- We can't do movement events so lets just skip this.
		-- actions.normal+=/vengeful_retreat,if=movement.distance>15
		action.EvaluateAction(Spell.ThrowGlaive, self.Requirements.ThrowGlaive);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationNormal = Normal("Normal");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "Kib and SimCraft",
		GuideLink = "http://www.wowhead.com/havoc-demon-hunter-guide",
		WoWVersion = 80001,
	};

	self.Defensives = {
		Blur = function()
			return Player.DamagePredicted(5) >= 25;
		end,

		Darkness = function()
			return Player.DamagePredicted(4) >= 30;
		end,

		Netherwalk = function()
			return Player.DamagePredicted(3) >= 50;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		DeathSweep = function()
			return Talent.FirstBlood.Enabled();
		end,

		FelRush = function()
			return Talent.Momentum.Enabled();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Item = {};

		Consumable = {
			-- Potions
			BattlePotionOfAgility = Objects.newItem(163223),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.ArcaneTorrent, Racial.Shadowmeld);
		config.AOEOptions(options, Spell.BladeDance, Spell.EyeBeam, Talent.FelBarrage, Talent.ImmolationAura, Spell.ThrowGlaive);
		config.CooldownOptions(options, Talent.DarkSlash, Spell.DemonsBite, Talent.Felblade, Spell.Metamorphosis, Talent.Nemesis);
		config.DefensiveOptions(options, Spell.Blur, Spell.Darkness, Talent.Netherwalk);
		config.UtilityOptions(options, Spell.FelRush, Spell.VengefulRetreat);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against all types of damage
		action.EvaluateDefensiveAction(Talent.Netherwalk, self.Defensives.Netherwalk);
		action.EvaluateDefensiveAction(Spell.Darkness, self.Defensives.Darkness);
		action.EvaluateDefensiveAction(Spell.Blur, self.Defensives.Blur);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfAgility, true);
		-- actions.precombat+=/metamorphosis
		action.EvaluateAction(Spell.Metamorphosis, true);
		-- Not specified by SimCraft but lets add some additional actions to start combat
		action.EvaluateAction(Talent.Nemesis, true);
		action.EvaluateAction(Talent.Felblade, true);
		action.EvaluateAction(Spell.FelRush, self.Requirements.FelRush);
		action.EvaluateAction(Spell.DeathSweep, self.Requirements.DeathSweep);
		action.EvaluateAction(Spell.Annihilation, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions+=/variable,name=blade_dance,value=talent.first_blood.enabled|set_bonus.tier20_4pc|spell_targets.blade_dance>=(3-talent.trail_of_ruin.enabled)
		Variables.blade_dance = Talent.FirstBlood.Enabled() or addonTable.Tier20_4PC or Enemies.GetEnemies(Spell.BladeDance) >= (3 - val(Talent.TrailOfRuin.Enabled()));
		-- actions+=/variable,name=waiting_for_nemesis,value=!(!talent.nemesis.enabled|cooldown.nemesis.ready|cooldown.nemesis.remains>target.time_to_die|cooldown.nemesis.remains>60)
		Variables.waiting_for_nemesis = not (not Talent.Nemesis.Enabled() or Talent.Nemesis.Cooldown.Ready() or Talent.Nemesis.Cooldown.Remains() > Target.TimeToDie() or Talent.Nemesis.Cooldown.Remains() > 60);
		-- actions+=/variable,name=pooling_for_meta,value=!talent.demonic.enabled&cooldown.metamorphosis.remains<6&fury.deficit>30&(!variable.waiting_for_nemesis|cooldown.nemesis.remains<10)
		Variables.pooling_for_meta = not Talent.Demonic.Enabled() and Spell.Metamorphosis.Cooldown.Remains() < 6 and Player.Fury.Deficit() > 30 and (not Variables.waiting_for_nemesis or Talent.Nemesis.Cooldown.Remains() < 10);
		-- actions+=/variable,name=pooling_for_blade_dance,value=variable.blade_dance&(fury<75-talent.first_blood.enabled*20)
		Variables.pooling_for_blade_dance = Variables.blade_dance and (Player.Fury() < 75 - val(Talent.FirstBlood.Enabled()) * 20);
		-- actions+=/variable,name=waiting_for_dark_slash,value=talent.dark_slash.enabled&!variable.pooling_for_blade_dance&!variable.pooling_for_meta&cooldown.dark_slash.up
		Variables.waiting_for_dark_slash = Talent.DarkSlash.Enabled() and not Variables.pooling_for_blade_dance and not Variables.pooling_for_meta and Talent.DarkSlash.Cooldown.Up();
		-- actions+=/variable,name=waiting_for_momentum,value=talent.momentum.enabled&!buff.momentum.up
		Variables.waiting_for_momentum = Talent.Momentum.Enabled() and not Player.Buff(Aura.Momentum).Up();
		-- This is fine for a simulation being run by a computer, but a human being will almost never have no GCD active so we will run this without restriction
		-- actions+=/call_action_list,name=cooldown,if=gcd.remains=0
		action.CallActionList(rotationCooldown);
		-- We can't do this in game because there is no way of knowing how many fragments exist and where they are
		-- actions+=/pick_up_fragment,if=fury.deficit>=35
		action.CallActionList(rotationDarkSlash);
		action.RunActionList(rotationDemonic);
		-- actions+=/run_action_list,name=normal
		action.RunActionList(rotationNormal);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Havoc Demon Hunter", addonTable.Enum.SpecID.DEMONHUNTER_HAVOC);