local addonName, addonTable = ...; -- Pulls back the Addon-Local Variables and store them locally.
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

--- Localize Vars
local coreGeneral = addon.Core.General;
local Enemies = addon.Modules.Enemies;
local Objects = addon.Core.Objects;
local val = coreGeneral.ToNumber;

-- Objects
local Player = addon.Units.Player;
local Target = addon.Units.Target;
local Racial, Spell, Talent, Aura, Azerite, Item, Consumable;
local Variables = {};

-- Rotation Variables
local nameAPL = "lunaeclipse_rogue_assassination";

local function exsanguinated(Spell, Aura, target)
	target = target or Target;

	return target.Debuff(Aura).Duration() < Aura.BaseDuration();
end

local function cp_max_spend()
	return 5;
end

local function poisoned_bleeds()
	return Target.Debuff(Aura.DeadlyPoison).Count() + Target.Debuff(Aura.WoundPoison).Count() + Target.Debuff(Aura.Garrote).Count() + Target.Debuff(Aura.Rupture).Count() + Target.Debuff(Aura.InternalBleeding).Count();
end

local function spell_haste()
	return 1 + (addon.Core.Misc.GetHaste() / 100);
end

-- Cooldowns Rotation
local function Cooldowns(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.cds+=/ancestral_call,if=debuff.vendetta.up
		AncestralCall = function()
			return Target.Debuff(Aura.Vendetta).Up();
		end,

		-- actions.cds=potion,if=buff.bloodlust.react|target.time_to_die<=60|debuff.vendetta.up&cooldown.vanish.remains<5
		BattlePotionOfAgility = function()
			return Player.HasBloodlust()
			    or Target.TimeToDie() <= 60
			    or Target.Debuff(Aura.Vendetta).Up()
			   and Spell.Vanish.Cooldown.Remains() < 5;
		end,

		-- actions.cds+=/berserking,if=debuff.vendetta.up
		Berserking = function()
			return Target.Debuff(Aura.Vendetta).Up();
		end,

		-- actions.cds+=/blood_fury,if=debuff.vendetta.up
		BloodFury = function()
			return Target.Debuff(Aura.Vendetta).Up();
		end,

		-- # Exsanguinate when both Rupture and Garrote are up for long enough
		-- actions.cds+=/exsanguinate,if=dot.rupture.remains>4+4*cp_max_spend&!dot.garrote.refreshable
		Exsanguinate = function()
			return Target.Debuff(Aura.Rupture).Remains() > 4 + 4 * cp_max_spend()
			   and not Target.Debuff(Aura.Garrote).Refreshable();
		end,

		-- actions.cds+=/fireblood,if=debuff.vendetta.up
		Fireblood = function()
			return Target.Debuff(Aura.Vendetta).Up();
		end,

		-- We can't do raid adds events so just skip this.
		-- actions.cds+=/marked_for_death,target_if=min:target.time_to_die,if=target.time_to_die<combo_points.deficit*1.5|(raid_event.adds.in>40&combo_points.deficit>=cp_max_spend)
		MarkedForDeath = function(_, Target)
			return Target.TimeToDie() < Player.ComboPoints.Deficit() * 1.5
			    or Player.ComboPoints.Deficit() >= cp_max_spend();
		end,

		-- actions.cds+=/toxic_blade,if=dot.rupture.ticking
		ToxicBlade = function()
			return Target.Debuff(Aura.Rupture).Ticking();
		end,

		Vanish = {
			-- # Vanish with Exsg + (Nightstalker, or Subterfuge only on 1T): Maximum CP and Exsg ready for next GCD
			-- actions.cds+=/vanish,if=talent.exsanguinate.enabled&(talent.nightstalker.enabled|talent.subterfuge.enabled&spell_targets.fan_of_knives<2)&combo_points>=cp_max_spend&cooldown.exsanguinate.remains<1
			Exsanguinate = function(numEnemies)
				return Talent.Exsanguinate.Enabled()
				   and (Talent.Nightstalker.Enabled() or Talent.Subterfuge.Enabled() and numEnemies < 2)
				   and Player.ComboPoints() >= cp_max_spend()
				   and Talent.Exsanguinate.Cooldown.Remains() < 1;
			end,

			-- # Vanish with Master Assasin: No stealth and no active MA buff, Rupture not in refresh range
			-- actions.cds+=/vanish,if=talent.master_assassin.enabled&!stealthed.all&master_assassin_remains<=0&!dot.rupture.refreshable
			MasterAssassin = function()
				return Talent.MasterAssassin.Enabled()
				   and not Player.IsStealthed()
				   and Player.Buff(Aura.MasterAssassin).Remains() <= 0
				   and not Target.Debuff(Aura.Rupture).Refreshable();
			end,

			-- # Vanish with Nightstalker + No Exsg: Maximum CP and Vendetta up
			-- actions.cds+=/vanish,if=talent.nightstalker.enabled&!talent.exsanguinate.enabled&combo_points>=cp_max_spend&debuff.vendetta.up
			Nightstalker = function()
				return Talent.Nightstalker.Enabled()
				   and not Talent.Exsanguinate.Enabled()
				   and Player.ComboPoints() >= cp_max_spend()
				   and Target.Debuff(Aura.Vendetta).Up();
			end,

			-- # Vanish with Subterfuge + (No Exsg or 2T+): No stealth/subterfuge, Garrote Refreshable, enough space for incoming Garrote CP
			-- actions.cds+=/vanish,if=talent.subterfuge.enabled&(!talent.exsanguinate.enabled|spell_targets.fan_of_knives>=2)&!stealthed.rogue&cooldown.garrote.up&dot.garrote.refreshable&(spell_targets.fan_of_knives<=3&combo_points.deficit>=1+spell_targets.fan_of_knives|spell_targets.fan_of_knives>=4&combo_points.deficit>=4)
			Subterfuge = function(numEnemies)
				return Talent.Subterfuge.Enabled()
				   and (not Talent.Exsanguinate.Enabled() or numEnemies >= 2)
				   and not Player.IsStealthed()
				   and Spell.Garrote.Cooldown.Up()
				   and Target.Debuff(Aura.Garrote).Refreshable()
				   and (numEnemies <= 3 and Player.ComboPoints.Deficit() >= 1 + numEnemies or numEnemies >= 4 and Player.ComboPoints.Deficit() >= 4);
			end,
		},

		-- actions.cds+=/vendetta,if=dot.rupture.ticking
		Vendetta = function()
			return Target.Debuff(Aura.Rupture).Ticking();
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Consumable.BattlePotionOfAgility, self.Requirements.BattlePotionOfAgility);
		-- actions.cds+=/use_item,name=galecallers_boon
		action.EvaluateAction(Item.GalecallersBoon, true);
		action.EvaluateAction(Racial.BloodFury, self.Requirements.BloodFury);
		action.EvaluateAction(Racial.Berserking, self.Requirements.Berserking);
		action.EvaluateAction(Racial.Fireblood, self.Requirements.Fireblood);
		action.EvaluateAction(Racial.AncestralCall, self.Requirements.AncestralCall);
		action.EvaluateCycleAction(Talent.MarkedForDeath, self.Requirements.MarkedForDeath);
		action.EvaluateAction(Spell.Vendetta, self.Requirements.Vendetta);
		action.EvaluateAction(Spell.Vanish, self.Requirements.Vanish.Exsanguinate, Enemies.GetEnemies(10));
		action.EvaluateAction(Spell.Vanish, self.Requirements.Vanish.Nightstalker);
		action.EvaluateAction(Spell.Vanish, self.Requirements.Vanish.Subterfuge, Enemies.GetEnemies(10));
		action.EvaluateAction(Spell.Vanish, self.Requirements.Vanish.MasterAssassin);
		action.EvaluateAction(Talent.Exsanguinate, self.Requirements.Exsanguinate);
		action.EvaluateAction(Talent.ToxicBlade, self.Requirements.ToxicBlade);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationCooldowns = Cooldowns("Cooldowns");

-- Direct Rotation
local function Direct(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.direct+=/blindside,if=variable.use_filler&(buff.blindside.up|!talent.venom_rush.enabled)
		Blindside = function()
			return Variables.use_filler
			   and (Player.Buff(Aura.Blindside).Up() or not Talent.VenomRush.Enabled());
		end,

		-- # Envenom at 4+ (5+ with DS) CP. Immediately on 2+ targets, with Vendetta, or with TB; otherwise wait for some energy. Also wait if Exsg combo is coming up.
		-- actions.direct=envenom,if=combo_points>=4+talent.deeper_stratagem.enabled&(debuff.vendetta.up|debuff.toxic_blade.up|energy.deficit<=25+variable.energy_regen_combined|spell_targets.fan_of_knives>=2)&(!talent.exsanguinate.enabled|cooldown.exsanguinate.remains>2)
		Envenom = function(numEnemies)
			return Player.ComboPoints() >= 4 + val(Talent.DeeperStratagem.Enabled())
			   and (Target.Debuff(Aura.Vendetta).Up() or Target.Debuff(Aura.ToxicBlade).Up() or Player.Energy.Deficit() <= 25 + Variables.energy_regen_combined or numEnemies >= 2)
			   and (not Talent.Exsanguinate.Enabled() or Talent.Exsanguinate.Cooldown.Remains() > 2);
		end,

		-- We are not doing legion legendaries because they become stat sticks after level 115, so just ignore legendary buff.
		-- actions.direct+=/fan_of_knives,if=variable.use_filler&(buff.hidden_blades.stack>=19|spell_targets.fan_of_knives>=2+stealthed.rogue|buff.the_dreadlords_deceit.stack>=29)
		FanOfKnives = function(numEnemies)
			return Variables.use_filler
			   and (Player.Buff(Aura.HiddenBlades).Stack() >= 19 or numEnemies >= 2 + val(Player.IsStealthed()));
		end,

		-- actions.direct+=/mutilate,if=variable.use_filler
		Mutilate = function()
			return Variables.use_filler;
		end,

		-- # Poisoned Knife at 29+ stacks of Sharpened Blades. Up to 4 targets with Rank 1, more otherwise.
		-- actions.direct+=/poisoned_knife,if=variable.use_filler&buff.sharpened_blades.stack>=29&(azerite.sharpened_blades.rank>=2|spell_targets.fan_of_knives<=4)
		PoisonedKnife = function(numEnemies)
			return Variables.use_filler
			   and Player.Buff(Aura.SharpenedBlades).Stack() >= 29
			   and (Azerite.SharpenedBlades.Rank() >= 2 or numEnemies <= 4);
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Envenom, self.Requirements.Envenom, Enemies.GetEnemies(10));

		-- actions.direct+=/variable,name=use_filler,value=combo_points.deficit>1|energy.deficit<=25+variable.energy_regen_combined|spell_targets.fan_of_knives>=2
		Variables.use_filler = Player.ComboPoints.Deficit() > 1 or Player.Energy.Deficit() <= 25 + Variables.energy_regen_combined or Enemies.GetEnemies(10) >= 2;

		action.EvaluateAction(Spell.PoisonedKnife, self.Requirements.PoisonedKnife, Enemies.GetEnemies(10));
		action.EvaluateAction(Spell.FanOfKnives, self.Requirements.FanOfKnives, Enemies.GetEnemies(10));
		action.EvaluateAction(Talent.Blindside, self.Requirements.Blindside);
		action.EvaluateAction(Spell.Mutilate, self.Requirements.Mutilate);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationDirect = Direct("Direct");

-- DOT Rotation
local function DOT(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- # Crimson Tempest only on multiple targets at 4+ CP when running out in 2s (up to 4 targets) or 3s (5+ targets)
		-- actions.dot+=/crimson_tempest,if=spell_targets>=2&remains<2+(spell_targets>=5)&combo_points>=4
		CrimsonTempest = function(numEnemies)
			return numEnemies >= 2
			   and Target.Debuff(Aura.CrimsonTempest).Remains() < 2 + val(numEnemies >= 5)
			   and Player.ComboPoints() >= 4;
		end,

		-- # Garrote upkeep, also tries to use it as a special generator for the last CP before a finisher
		-- Tick time has not been done for auras, so just use refreshable
		-- actions.dot+=/garrote,cycle_targets=1,if=(!talent.subterfuge.enabled|!(cooldown.vanish.up&cooldown.vendetta.remains<=4))&combo_points.deficit>=1&refreshable&(pmultiplier<=1|remains<=tick_time)&(!exsanguinated|remains<=tick_time*2)&(target.time_to_die-remains>4&spell_targets.fan_of_knives<=1|target.time_to_die-remains>12)
		Garrote = function(numEnemies, Target)
			return (not Talent.Subterfuge.Enabled() or not(Spell.Vanish.Cooldown.Up() and Spell.Vendetta.Cooldown.Remains() <= 4))
			   and Player.ComboPoints.Deficit() >= 1
			   and Target.Debuff(Aura.Garrote).Refreshable()
			   and (Target.Debuff(Aura.Garrote).PersistentMultiplier() <= 1 or Target.Debuff(Aura.Garrote).Refreshable())
			   and (not exsanguinated(Spell.Garrote, Aura.Garrote, Target) or Target.Debuff(Aura.Garrote).Refreshable())
			   and (Target.TimeToDie() - Target.Debuff(Aura.Garrote).Remains() > 4 and numEnemies <= 1 or Target.TimeToDie() - Target.Debuff(Aura.Garrote).Remains() > 12);
		end,

		Rupture = {
			-- # Keep up Rupture at 4+ on all targets (when living long enough and not snapshot)
			-- Tick time has not been done for auras, so just use refreshable
			-- actions.dot+=/rupture,cycle_targets=1,if=combo_points>=4&refreshable&(pmultiplier<=1|remains<=tick_time)&(!exsanguinated|remains<=tick_time*2)&target.time_to_die-remains>4
			Cycle = function(numEnemies, Target)
				return Player.ComboPoints() >= 4
				   and Target.Debuff(Aura.Rupture).Refreshable()
				   and (Target.Debuff(Aura.Rupture).PersistentMultiplier() <= 1 or Target.Debuff(Aura.Rupture).Refreshable())
				   and (not exsanguinated(Spell.Rupture, Aura.Rupture, Target) or Target.Debuff(Aura.Rupture).Refreshable())
				   and Target.TimeToDie() - Target.Debuf(Aura.Rupture).Remains() > 4;
			end,

			-- # Special Rupture setup for Exsg
			-- actions.dot=rupture,if=talent.exsanguinate.enabled&((combo_points>=cp_max_spend&cooldown.exsanguinate.remains<1)|(!ticking&(time>10|combo_points>=2)))
			Use = function()
				return Talent.Exsanguinate.Enabled()
				   and ((Player.ComboPoints() >= cp_max_spend() and Talent.Exsanguinate.Cooldown.Remains() < 1) or (not Target.Debuff(Aura.Rupture).Ticking() and (coreGeneral.CombatTime() > 10 or Player.ComboPoints() >= 2)));
			end,
		},
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Rupture, self.Requirements.Rupture.Use);
		action.EvaluateCycleAction(Spell.Garrote, self.Requirements.Garrote, Enemies.GetEnemies(10));
		action.EvaluateAction(Talent.CrimsonTempest, self.Requirements.CrimsonTempest, Enemies.GetEnemies(10));
		action.EvaluateCycleAction(Spell.Rupture, self.Requirements.Rupture.Cycle);
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationDOT = DOT("DOT");

-- Stealth Rotation
local function Stealth(rotationName)
	-- Inherits Rotation Class so get the base class.
	local self = addonTable.rotationsClass(nameAPL, rotationName);

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions.stealthed+=/envenom,if=combo_points>=cp_max_spend
		Envenom = function()
			return Player.ComboPoints() >= cp_max_spend();
		end,

		Garrote = {
			-- # Subterfuge: Apply or Refresh with buffed Garrotes
			-- Tick time has not been done for auras, so just use refreshable
			-- actions.stealthed+=/garrote,cycle_targets=1,if=talent.subterfuge.enabled&refreshable&(!exsanguinated|remains<=tick_time*2)&target.time_to_die-remains>2
			Buffed = function(numEnemies, Target)
				return Talent.Subterfuge.Enabled()
				   and Target.Debuff(Aura.Garrote).Refreshable()
				   and (not exsanguinated(Spell.Garrote, Aura.Garrote, Target) or Target.Debuff(Aura.Garrote).Refreshable())
				   and Target.TimeToDie() - Target.Debuff(Aura.Garrote).Remains() > 2;
			end,

			-- # Subterfuge: Override normal Garrotes with snapshot versions
			-- actions.stealthed+=/garrote,cycle_targets=1,if=talent.subterfuge.enabled&remains<=10&pmultiplier<=1&!exsanguinated&target.time_to_die-remains>2
			PersistentMultiplier = function(numEnemies, Target)
				return Talent.Subterfuge.Enabled()
				   and Target.Debuff(Aura.Garrote).Remains() < 10
				   and Target.Debuff(Aura.Rupture).PersistentMultiplier() <= 1
				   and not exsanguinated(Spell.Garrote, Aura.Garrote, Target)
				   and Target.TimeToDie() - Target.Debuff(Aura.Garrote).Remains() > 2;
			end,

			-- # Subterfuge + Exsg: Even override a snapshot Garrote right after Rupture before Exsanguination
			-- actions.stealthed+=/pool_resource,for_next=1
			-- actions.stealthed+=/garrote,if=talent.subterfuge.enabled&talent.exsanguinate.enabled&cooldown.exsanguinate.remains<1&prev_gcd.1.rupture&dot.rupture.remains>5+4*cp_max_spend
			Use = function()
				return Talent.Subterfuge.Enabled()
				   and Talent.Exsanguinate.Enabled()
				   and Talent.Exsanguinate.Cooldown.Remains() < 1
				   and Player.PrevGCD(1, Spell.Rupture)
				   and Target.Debuff(Aura.Rupture).Remains() > 5 + 4 * cp_max_spend();
			end,
		},

		-- # Nightstalker, or Subt+Exsg on 1T: Snapshot Rupture; Also use Rupture over Envenom if it's not applied (Opener)
		-- actions.stealthed=rupture,if=combo_points>=4&(talent.nightstalker.enabled|talent.subterfuge.enabled&talent.exsanguinate.enabled&spell_targets.fan_of_knives<2|!ticking)&target.time_to_die-remains>6
		Rupture = function(numEnemies)
			return Player.ComboPoints() >= 4
			   and (Talent.Nightstalker.Enabled() or Talent.Subterfuge.Enabled() and Talent.Exsanguinate.Enabled() and numEnemies < 2 or not Target.Debuff(Aura.Rupture).Ticking())
			   and Target.TimeToDie() - Target.Debuff(Aura.Rupture).Remains() > 6;
		end,
	};

	Objects.FinalizeRequirements(self.Requirements);

	function self.Rotation(action)
		action.EvaluateAction(Spell.Rupture, self.Requirements.Rupture, Enemies.GetEnemies(10));
		action.EvaluateAction(Spell.Envenom, self.Requirements.Envenom);
		action.EvaluateCycleAction(Spell.Garrote, self.Requirements.Garrote.Buffed);
		action.EvaluateCycleAction(Spell.Garrote, self.Requirements.Garrote.PersistentMultiplier);
		action.EvaluatePoolAction(Spell.Garrote, self.Requirements.Garrote.Use);
	end

	-- actions+=/call_action_list,name=stealthed,if=stealthed.rogue
	function self.Use()
		return Player.IsStealthed();
	end

	return self;
end

-- Create a variable so we can call the rotations functions.
local rotationStealth = Stealth("Stealth");

-- Base APL Class
local function APL(rotationName, rotationDescription, specID)
	-- Inherits APL Class so get the base class.
	local self = addonTable.rotationsAPL(rotationName, rotationDescription, specID);

	-- Store the information for the script.
	self.scriptInfo = {
		SpecializationID = self.SpecID,
		ScriptAuthor = "LunaEclipse",
		GuideAuthor = "GrayHound and SimCraft",
		GuideLink = "https://www.wowhead.com/assassination-rogue-guide",
		WoWVersion = 80001,
	};

	-- Table to hold requirements to use spells for defensive reasons.
	self.Defensives = {
		CloakOfShadows = function()
			return Player.MagicDamagePredicted(5) >= 60;
		end,

		CrimsonVial = function()
			return Player.Health.Percent() < 70
			   and Player.DamagePredicted(5) >= 25;
		end,

		Evasion = function()
			return Player.PhysicalDamagePredicted(5) >= 60;
		end,

		Feint = function()
			return Player.DamagePredicted(5) >= 40;
		end,
	};

	-- Table to hold requirements to use spells and items listed in the rotation.
	self.Requirements = {
		-- actions+=/arcane_torrent,if=energy.deficit>=15+variable.energy_regen_combined
		ArcaneTorrent = function()
			return Player.Energy.Deficit() >= 15 + Variables.energy_regen_combined;
		end,

		CripplingPoison = function()
			return Player.Buff(Aura.CripplingPoison).Refreshable();
		end,

		DeadlyPoison = function()
			return Player.Buff(Aura.DeadlyPoison).Refreshable();
		end,

		-- SimCraft doesn't specify it, but make sure we are not already stealthed before suggesting stealth.
		-- actions.precombat+=/stealth
		Stealth = function()
			return not Player.IsStealthed();
		end,
	};

	Objects.FinalizeRequirements(self.Defensives, self.Requirements);

	-- Function for setting up action objects such as spells, buffs, debuffs and items, called when the rotation becomes the active rotation.
	function self.Enable(...)
		Racial, Spell, Talent, Aura, Azerite = ...;

		Aura.SharpenedBlades = Objects.newSpell(272916);

		Item = {
			GalecallersBoon = Objects.newItem(159614);
		};

		Consumable = {
			-- Potions
			BattlePotionOfAgility = Objects.newItem(163223),
		};

		Objects.FinalizeActions(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for setting up the configuration screen, called when rotation becomes the active rotation.
	function self.SetupConfiguration(config, options)
		config.RacialOptions(options, Racial.AncestralCall, Racial.ArcanePulse, Racial.ArcaneTorrent, Racial.Berserking, Racial.BloodFury, Racial.Fireblood, Racial.Shadowmeld);
		config.AOEOptions(options, Talent.CrimsonTempest, Spell.FanOfKnives);
		config.BuffOptions(options, Spell.CripplingPoison, Spell.DeadlyPoison);
		config.CooldownOptions(options, Talent.Blindside, Talent.Exsanguinate, Item.GalecallersBoon, Talent.MarkedForDeath, Talent.ToxicBlade, Spell.Vendetta);
		config.DefensiveOptions(options, Spell.CloakOfShadows, Spell.CrimsonVial, Spell.Evasion, Spell.Feint);
		config.UtilityOptions(options, Spell.Stealth, Spell.Vanish);
	end

	-- Function for destroying action objects such as spells, buffs, debuffs and items, called when the rotation is no longer the active rotation.
	function self.Disable()
		local coreTables = addon.Core.Tables;

		coreTables.Wipe(Racial, Spell, Talent, Aura, Azerite, Item, Consumable);
	end

	-- Function for checking the rotation that displays on the Defensives icon.
	function self.Defensive(action)
		-- The abilities here should be listed from highest damage required to suggest to lowest,
		-- Specific damage types before all damage types.

		-- Protects against Magic damage
		action.EvaluateDefensiveAction(Spell.CloakOfShadows, self.Defensives.CloakOfShadows);
		-- Protects against Physical damage
		action.EvaluateDefensiveAction(Spell.Evasion, self.Defensives.Evasion);
		-- Protects against All damage (AOE)
		action.EvaluateDefensiveAction(Spell.Feint, self.Defensives.Feint);

		-- Self Healing goes at the end and is only suggested if a major cooldown is not needed.
		action.EvaluateDefensiveAction(Spell.CrimsonVial, self.Defensives.CrimsonVial);
	end

	-- Function for displaying opening rotation.
	function self.Opener(action)
	end

	-- Function for displaying any actions before combat starts.
	function self.Precombat(action)
		-- SimCraft doesn't specifically specify this, but refresh poisones as needed only.
		-- actions.precombat+=/apply_poison
		action.EvaluateAction(Spell.DeadlyPoison, self.Requirements.DeadlyPoison);
		action.EvaluateAction(Spell.CripplingPoison, self.Requirements.CripplingPoison);
		action.EvaluateAction(Spell.Stealth, self.Requirements.Stealth);
		-- actions.precombat+=/potion
		action.EvaluateAction(Consumable.BattlePotionOfAgility, true);
		-- Addon doesn't know how long until combat starts unless there is a pull timer, so just recommend this without restriction.
		-- actions.precombat+=/marked_for_death,precombat_seconds=5,if=raid_event.adds.in>40
		action.EvaluateAction(Talent.MarkedForDeath, true);
	end

	-- Function for checking the rotation that displays on the Single Target, AOE, Off GCD and CD icons.
	function self.Combat(action)
		-- actions=variable,name=energy_regen_combined,value=energy.regen+poisoned_bleeds*7%(2*spell_haste)
		Variables.energy_regen_combined = Player.Energy.Regen() + poisoned_bleeds() * 7 / ( 2 * spell_haste());

		action.CallActionList(rotationStealth);
		-- actions+=/call_action_list,name=cds
		action.CallActionList(rotationCooldowns);
		-- actions+=/call_action_list,name=dot
		action.CallActionList(rotationDOT);
		-- actions+=/call_action_list,name=direct
		action.CallActionList(rotationDirect);

		action.EvaluateAction(Racial.ArcaneTorrent, self.Requirements.ArcaneTorrent);
		-- actions+=/arcane_pulse
		action.EvaluateAction(Racial.ArcanePulse, true);
	end

	return self;
end

local rotationAPL = APL(nameAPL, "LunaEclipse: Assassination Rogue", addonTable.Enum.SpecID.ROGUE_ASSASSINATION);
