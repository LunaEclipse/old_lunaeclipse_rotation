-- Pulls back the Addon-Local Variables and store them locally.
local addonName = ...;
local addon = _G[addonName];
local L = LibStub("AceLocale-3.0"):GetLocale(addonName);

-- Custom tooltip for tooltip parsing
local WorldFrame = WorldFrame;
local Tooltip = addon.API.Blizzard.CreateFrame("GameTooltip", "LERH_Tooltip", nil, "GameTooltipTemplate");
	  Tooltip:SetOwner(WorldFrame, "ANCHOR_NONE");

local function textSearch(text, compareText)
	local gmatch, ipairs, strupper, tinsert = gmatch, ipairs, strupper, tinsert;
	local coreTables = addon.Core.Tables;

	local returnValue = {};

	if text ~= nil and compareText ~= nil then
		if not coreTables.IsEmpty(compareText) then
			for _, patternText in ipairs(compareText) do
				for value in gmatch(strupper(text), patternText) do
					returnValue = returnValue or {};
					tinsert(returnValue, value);
				end
			end
		else
			for value in gmatch(strupper(text), compareText) do
				returnValue = returnValue or {};
				tinsert(returnValue, value);
			end
		end
	end

	return returnValue;
end

local function getText(line)
	local format = format;

	line = line or 1;

	local textLeft = format("%s%s%i", "LERH_Tooltip", "TextLeft", line);
	local textRight = format("%s%s%i", "LERH_Tooltip", "TextRight", line);

	return _G[textLeft] ~= nil and _G[textLeft]:GetText(), _G[textRight] ~= nil and _G[textRight]:GetText();
end

local function textExists(compareText)
	local returnValue, textLeft, textRight;

	if compareText then
		local strmatch, strupper = strmatch, strupper;

		for counter = 1, Tooltip:NumLines() do
			textLeft, textRight = getText(counter);

			if textLeft ~= nil and strmatch(strupper(textLeft), compareText) then
				return true;
			end

			if textRight ~= nil and strmatch(strupper(textRight), compareText) then
				return true;
			end
		end
	end

	return false;
end

local function getValue(compareText)
	local coreTables = addon.Core.Tables;

	local returnValue = {};
	local textLeft, textRight;

	if compareText then
		for counter = 1, Tooltip:NumLines() do
			textLeft, textRight = getText(counter);

			returnValue = coreTables.Merge(returnValue, textSearch(textLeft, compareText));
			returnValue = coreTables.Merge(returnValue, textSearch(textRight, compareText));
		end
	end

	return returnValue;
end

local function iLevel(compareText)
	local returnValue, textLeft, textRight;

	if compareText then
		local strmatch, tonumber = strmatch, tonumber;

		for counter = 1, Tooltip:NumLines() do
			textLeft, textRight = getText(counter);

			returnValue = textLeft ~= nil and tonumber(strmatch(textLeft, compareText));

			if returnValue then
				return returnValue;
			end

			returnValue = textRight ~= nil and tonumber(strmatch(textRight, compareText));

			if returnValue then
				return returnValue;
			end
		end
	end

	return nil;
end

-- This is stored on the addon so its publically available to all other addons.
addon.Core.Tooltip = {
	GetCreatureName = function(creatureID)
		local format, select = format, select;

		Tooltip:ClearLines();
		Tooltip:SetHyperlink(format("unit:Creature-----%s", creatureID));

		return select(1, getText());
	end,

	GetItemArtifactPower = function(itemID)
		local ARTIFACT_POWER, ITEM_SPELL_TRIGGER_ONUSE, gsub, select, strfind = ARTIFACT_POWER, ITEM_SPELL_TRIGGER_ONUSE, gsub, select, strfind;
		local amount, textLeft, textRight;

		Tooltip:ClearLines();
		Tooltip:SetItemByID(itemID);

		for counter = 1, Tooltip:NumLines() do
			textLeft, textRight = getText(counter);

			if strfind(textLeft, ITEM_SPELL_TRIGGER_ONUSE) and strfind(textLeft, ARTIFACT_POWER) then
				amount = select(3, strfind(textLeft, "(%d[%d,.%s]+)"));

				if amount then
					return gsub(amount, "[^%d]", "") + 0;
				end
			end

			if strfind(textRight, ITEM_SPELL_TRIGGER_ONUSE) and strfind(textRight, ARTIFACT_POWER) then
				amount = select(3, strfind(textRight, "(%d[%d,.%s]+)"));

				if amount then
					return gsub(amount, "[^%d]", "") + 0;
				end
			end
		end

		return 0;
	end,

	GetItemLevel = function(itemLink, ilevel)
		local ITEM_LEVEL, gsub = ITEM_LEVEL, gsub;
		local searchText = gsub(ITEM_LEVEL, "%%d", "(%%d+)");
		local returnValue;

		if itemLink then
			Tooltip:ClearLines();
			Tooltip:SetHyperlink(itemLink);

			returnValue = iLevel(searchText);
		end

		return returnValue or ilevel or 0;
	end,

	GetQuestTitle = function(questID)
		local format, select = format, select;

		Tooltip:ClearLines();
		Tooltip:SetHyperlink(format("quest:%s", questID));

		return select(1, getText());
	end,

	GetSpellBaseDuration = function(spellID)
		local sort, strupper = sort, strupper;
		local minutes = false;

		Tooltip:ClearLines();
		Tooltip:SetSpellByID(spellID);

		local returnValue = getValue({ strupper(L["Over"]) .." (%d*%.?%d+) SEC", strupper(L["Next"]) .." (%d*%.?%d+) SEC", strupper(L["Lasts"]) .." (%d*%.?%d+) SEC", strupper(L["For"]) .." (%d*%.?%d+) SEC" });

		if not returnValue then
			returnValue = getValue({ strupper(L["Over"]) .." (%d*%.?%d+) MIN", strupper(L["Next"]) .." (%d*%.?%d+) MIN", strupper(L["Lasts"]) .." (%d*%.?%d+) MIN", strupper(L["For"]) .." (%d*%.?%d+) MIN" });
			minutes = returnValue ~= nil;
		end

		if returnValue then
			sort(returnValue, function(a, b) return b < a; end);

			return (returnValue[1] or 0) * (minutes == true and 60 or 1);
		end

		return 0;
	end,

	GetSpellValue = function(spellID, compareText)
		local sort = sort;
		local returnValue;

		Tooltip:ClearLines();
		Tooltip:SetSpellByID(spellID);

		returnValue = getValue(compareText);

		if returnValue then
			sort(returnValue, function(a, b) return b < a; end);

			return returnValue[1];
		end

		return 0;
	end,

	IsContainerItemSoulbound = function(bag, slot)
		local ITEM_SOULBOUND = ITEM_SOULBOUND;

		Tooltip:ClearLines();
		Tooltip:SetBagItem(bag, slot);

		textExists(ITEM_SOULBOUND);
	end,

	IsDummy = function(unitID)
		local strupper = strupper;

		Tooltip:ClearLines();
		Tooltip:SetUnit(unitID);

		return textExists(strupper(L["Dummy"]));
	end,

	IsItemArtifactPower = function(itemID)
		local ARTIFACT_POWER, format, strrep = ARTIFACT_POWER, format, strrep;
		local searchText = format("^|c%s%s|r$", strrep("%x", 8), ARTIFACT_POWER);

		Tooltip:ClearLines();
		Tooltip:SetItemByID(itemID);

		return textExists(searchText);
	end,

	IsMerchantItemAlreadyKnown = function(index)
		local ITEM_SPELL_KNOWN = ITEM_SPELL_KNOWN;

		Tooltip:ClearLines();
		Tooltip:SetMerchantItem(index);

		return textExists(ITEM_SPELL_KNOWN);
	end,

	IsSpellType = function(spellID, compareText)
		local ipairs, type = ipairs, type;
		local Tables = addon.Core.Tables;

		Tooltip:ClearLines();
		Tooltip:SetSpellByID(spellID);

		if not Tables.IsEmpty(compareText) then
			for _, value in ipairs(compareText) do
				if textExists(value) then
					return true;
				end
			end
		elseif type(compareText) == "string" then
			return textExists(compareText);
		end

		return false;
	end,
};